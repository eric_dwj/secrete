#! /bin/bash
if [ -z "$1" ] ;then
echo "help command:"
echo "./docker.sh init network"
echo "./docker.sh init mongo-user"
echo "./docker.sh init mongo-replica"
echo "./docker.sh status replica"
echo "mongo password:  echo -n seed | md5sum"
echo "./docker.sh build cryptolib"
exit
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

initMongoUser(){
    mongo mongodb://mongodb1:25015 $DIR/mongo/mongo-init.js
    mongo mongodb://mongodb2:25016 $DIR/mongo/mongo-init.js
    echo "init mongo db, please check status"
}

statusReplica(){
    mongo mongodb://mongodb1:25015 $DIR/mongo/replica-status.js
    mongo mongodb://mongodb2:25016 $DIR/mongo/replica-status.js
}

initReplica(){
    mongo mongodb://mongodb1:25015  $DIR/mongo/replica-init.js
    echo "init replica, please check status '$DIR'/mongo/replica-init.js"
}

initNetWork(){
    # remove existed app_net
    docker network remove app_net
    # add docker network
    docker network create app_net --subnet 172.26.0.0/24

    # add hostname
    echo "127.0.0.1  mongodb1" >> /etc/hosts
    echo "127.0.0.1  mongodb2" >> /etc/hosts
    echo "127.0.0.1  mongodb3" >> /etc/hosts
    echo "127.0.0.1  zoo1" >> /etc/hosts
    echo "127.0.0.1  zoo2" >> /etc/hosts
    echo "127.0.0.1  zoo3" >> /etc/hosts
    echo "127.0.0.1  party2" >> /etc/hosts

    # remove duplicates in hosts file
    sort -u /etc/hosts > /tmp/hosts.new && mv /tmp/hosts.new /etc/hosts

    # check docker network status
    docker network inspect app_net
    echo "init net work status, please check status"
}

if [  "$1" = 'status' ] && [  "$2" = 'replica' ]  ; then
    statusReplica
    exit
fi

if [  "$1" = 'init' ] && [  "$2" = 'mongo-replica' ]  ; then
    initReplica
    exit
fi

if [  "$1" = 'init' ] && [  "$2" = 'mongo-user' ]  ; then
    initMongoUser
    exit
fi

if [  "$1" = 'init' ] && [  "$2" = 'network' ]  ; then
    initNetWork
    exit
fi

