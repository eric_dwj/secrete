#! /bin/bash
if [ -z "$1" ] ;then
echo "help command:"
echo "./docker.sh start mongo"
echo "./docker.sh start zk"
echo "./docker.sh start party1"
echo "./docker.sh start party2"
exit
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
startParty1(){
    docker-compose -f $DIR/cryptolib/docker-compose.yml up -d party1
    echo "start party one"
}

startParty2(){
    docker-compose -f $DIR/cryptolib/docker-compose.yml up -d party2
    echo "start party one"
}

startZK(){
    docker-compose -f $DIR/zookeeper/docker-compose.yml up -d 
    echo "start zookeeper"
}

startMongo(){
    docker-compose -f $DIR/mongo/docker-compose.yml up -d 
    echo "start mongo"
}


if [  "$1" = 'start' ] && [  "$2" = 'party1' ]  ; then
    startParty1
    exit
fi

if [  "$1" = 'start' ] && [  "$2" = 'party2' ]  ; then
    startParty2
    exit
fi

if [  "$1" = 'start' ] && [  "$2" = 'zk' ]  ; then
    startZK
    exit
fi

if [  "$1" = 'start' ] && [  "$2" = 'mongo' ]  ; then
    startMongo
    exit
fi