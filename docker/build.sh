#! /bin/bash

if [ -z "$1" ] ;then
echo "help command:"
echo "./build.sh build cryptolib"
exit
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

buildCrypto(){
    docker build -t cryptolib $(dirname "$DIR")/futurelib/
}

if [  "$1" = 'build' ] && [  "$2" = 'cryptolib' ]  ; then
    buildCrypto
    exit
fi