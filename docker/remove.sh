#! /bin/bash
if [ -z "$1" ] ;then
echo "help command:"
echo "./docker.sh rm crypto"
exit
fi

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

removeCrypto() {
    docker rm -f party1
    docker rm -f party2
    echo "removed crypto library"
}


if [  "$1" = 'rm' ] && [  "$2" = 'crypto' ]  ; then
    removeCrypto
    exit
fi

