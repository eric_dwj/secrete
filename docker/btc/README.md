# btc-node

### bitcoin

version: 0.20.0

official website: https://bitcoin.org/en/bitcoin-core/

repo: https://github.com/bitcoin/bitcoin

### mongo

version: 4.2.5

docker hub: https://hub.docker.com/_/mongo

repo: https://github.com/docker-library/mongo/tree/master/4.2

### bitcore

version: 8.16.2

official website: https://bitcore.io/

repo: https://github.com/bitpay/bitcore

