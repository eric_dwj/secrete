#include "backupRecovery.h"	
#include "headers.h"
#include <string>
using namespace CryptoPP;
using json = nlohmann::json;

#ifdef WIN32
#include <windows.h>
#else
#include <termios.h>
#include <unistd.h>
#endif

void SetStdinEcho(bool enable = true)
{
#ifdef WIN32
    HANDLE hStdin = GetStdHandle(STD_INPUT_HANDLE); 
    DWORD mode;
    GetConsoleMode(hStdin, &mode);

    if( !enable )
        mode &= ~ENABLE_ECHO_INPUT;
    else
        mode |= ENABLE_ECHO_INPUT;

    SetConsoleMode(hStdin, mode );

#else
    struct termios tty;
    tcgetattr(STDIN_FILENO, &tty);
    if( !enable )
        tty.c_lflag &= ~ECHO;
    else
        tty.c_lflag |= ECHO;

    (void) tcsetattr(STDIN_FILENO, TCSANOW, &tty);
#endif
}



int main(int argc, char* argv[])
{
 RSA::PublicKey k1;
    /*RSA::PrivateKey k2, k3;
  
    FileSink fs1("rsa-pub.cryptopp.pem");
    std::string jsonpub= "{ \"n\": \"9cb31084deb42be9befa3e99cd0f089c81ce5c9dda56493712cdeb3730d1412c07af5f91a7fe3f86abe1bbd373cd09bdc71519d0baf53e8dbfabe9d7efbf38f83d9c6be39bf830513d05f54a33e2deeb2fe2dc7297908f4c8fc5bf972bd567a24dcd828247512de3a55bbaf4c47882aedbc6cd58936f7d8d01d56f881c07ad328b502ee61ad5cd68da19bb1bc77be19a07c0bb7e22bd653ff3966576d7714562260471581816fe55764095eeb1e7d45baeb2474cd26475ba8d871e1582ed267f733300fae9d5aec2cadc2d9c689d0fd26673c6f36ebbb135c285ec1d56d7e4efeed9fdbcc2794128ee07cbd48e393d01197f15a7b12ec5b280eb86ea206209eb0caf1f06e39b6930a45dc0718fe9546421e66ebd9e7a68cb1fa76c395c3bcefc4c4e10d23627f75d7625b8d0fbc1a1c60db4a5a894aa85ff3941b4785ad67f7c6cf79f3502c6c70d221215df74a8129953502c39e68f647db06ff2e2662804aeb1be347a88b203ce7cf6e041ffa2d79eac628b62911ba1ecdf0975c0138a75f9h\", \"e\": \"11h\"  }";
    json pk=json::parse(jsonpub);
    std::cout << "N: " << encodeInteger(Integer( ((std::string )pk["n"]).data() )) <<std::endl;
    std::cout << "e: " << encodeInteger(Integer( ((std::string )pk["e"]).data() )) <<std::endl;
    
     k1.Initialize(Integer( ((std::string )pk["n"]).data() ),Integer(  ( ((std::string )pk["e"]).data() )));
     PEM_Save(fs1, k1);
      k2.Initialize(Integer("9cb31084deb42be9befa3e99cd0f089c81ce5c9dda56493712cdeb3730d1412c07af5f91a7fe3f86abe1bbd373cd09bdc71519d0baf53e8dbfabe9d7efbf38f83d9c6be39bf830513d05f54a33e2deeb2fe2dc7297908f4c8fc5bf972bd567a24dcd828247512de3a55bbaf4c47882aedbc6cd58936f7d8d01d56f881c07ad328b502ee61ad5cd68da19bb1bc77be19a07c0bb7e22bd653ff3966576d7714562260471581816fe55764095eeb1e7d45baeb2474cd26475ba8d871e1582ed267f733300fae9d5aec2cadc2d9c689d0fd26673c6f36ebbb135c285ec1d56d7e4efeed9fdbcc2794128ee07cbd48e393d01197f15a7b12ec5b280eb86ea206209eb0caf1f06e39b6930a45dc0718fe9546421e66ebd9e7a68cb1fa76c395c3bcefc4c4e10d23627f75d7625b8d0fbc1a1c60db4a5a894aa85ff3941b4785ad67f7c6cf79f3502c6c70d221215df74a8129953502c39e68f647db06ff2e2662804aeb1be347a88b203ce7cf6e041ffa2d79eac628b62911ba1ecdf0975c0138a75f9h"),
                    Integer("11h"),Integer("14bd58c64aa6e7b1c2b02deeb57e399c3e5b5005d55e3e656822c88b11c157af3d409f78ecd2977778a1a42747cc141ce1e0e91054facfceff03ee023a158b4e082786c001d1ca28deaa3349d2295235b74770001b96e5ca2216686a98a0026c11d3aba04d3427f4b7c49c82472e114bd952c85ac8382af84f4d30a4d6888ba10ae8bae9bfc9794de0a185f0da66fbfa088e36eecfe45c75a40114f56047765c0c8fa596f4212930f54c50144c4090cfe62e2f157632f176cef787bf1517dbaec767f05fb985e96ade3346e6e6f68712623547036fd9460817ca0477739bec2efad7337e453ff7fbbb2647a8b1c9133cece121303470bfe5e89c8c76e4cc54acea7d58f5c6a6c94d000ef02cfeb7647339d57891aac47397decbe8cc5df710aac392c696ca6baef9f10c6bf80050cc0b2cd22a580851324d31cd01f204da32557ea275e5182563c99267d72ec157712e5db6aa653f4be6a4b6fa9c5f811bcb8eaf607bb46cfd409447c2b6426f74544b1990bec0987fbfe2a06b142d12a48e75h"));
    FileSink fs2s("rsa-priv.cryptopp.pem");
       PEM_Save(fs2s, k2);         
       FileSink fs3s("rsa-enc-priv.cryptopp.pem");
        PEM_Save(fs3s, k2, prng, "AES-128-CBC", "abcde", 5);
    CryptoPP::FileSource fs3l("rsa-enc-priv.cryptopp.pem", true);
    try
    {
         PEM_Load(fs3l, k2);
    }
    catch(const CryptoPP::InvalidArgument& e)
    {
        std::cerr << e.what() << '\n';
    }
    */
   

    if (argc<1)
    {
        //should never be the case;
        exit(1);
    }
    if (argc<3)
    {
        std::cout << "Usage: " << std::string(argv[0]) << " <RSA private key pem> <json file or string>" <<std::endl;
        exit(1);
    }
  std::string pemfile=argv[1];
  std::string jsonfile=argv[2];
  RSA::PrivateKey recoveryKey;
     AutoSeededRandomPool prng;
  std::cout << "Loading RSA private key" << std::endl;
  
  try
    {
    CryptoPP::FileSource fs2(pemfile.c_str(), true);
    PEM_Type pt=  PEM_GetType(fs2);
    switch(pt)
    {
        case PEM_RSA_PRIVATE_KEY:
        { PEM_Load(fs2, recoveryKey); } break;
        case PEM_RSA_ENC_PRIVATE_KEY:
        {
            std::cout << "Passphrase: ";
            SetStdinEcho(false);
            std::string password;
            std::getline(std::cin, password);
             SetStdinEcho(true);
             PEM_Load(fs2, recoveryKey,password.data(),password.size());
             std::cout << std::endl;
        } break;
        default:
        {
           std::cerr << "Pem file should be RSA private key file"<<std::endl;
           exit(1);
        }
    }
     
    }
     catch(const CryptoPP::FileStore::OpenErr& e)
     {
        std::cerr << "Pem file could not be opened";
        std::cerr << e.what() << '\n';
        exit(1);
     }
 catch(const CryptoPP::InvalidArgument& e)
    {
        std::cerr<< "Invalid argument: " << e.what() << '\n';
        exit(1);
    }
    catch(const CryptoPP::Exception& e)
    {
         std::cerr<< "Pem parsing error, probably invalid passphrase" <<std::endl;
        exit(1); 
    }
    json recoveryData;
    try
    {
        CryptoPP::FileSource jsf(jsonfile.c_str(), true);
        lword size = (std::min)(jsf.MaxRetrievable(), lword(128000));
        std::vector<byte> str(size);
        jsf.Peek(&str[0], str.size());
        recoveryData=json::parse(str);
    }
    catch(const CryptoPP::FileStore::OpenErr& e)
    {
        try
        {
           recoveryData=json::parse(jsonfile);
        }
        catch(const std::exception& e)
        {
            std::cerr << "Second parameter should be json file or valid formatted json" << '\n';
            exit(1);
        }
        
    }
      catch(const std::exception& e)
      {
          std::cerr << "Second parameter should be a valid json file" << '\n';
           std::cerr << e.what() << '\n';
      }
      if(recoveryData.size()!=2)
      {
        std::cerr << "Json does not have a correct number of entries" <<std::endl;
        exit(1);
      }
    Integer ours, theirs,chaincode;
    unsigned int set=0;
    for (auto kv:recoveryData.items())
    {
        Integer i=decodeInteger(kv.value());//  Integer(((std::string)kv.value()).data());
        Integer akey = recoveryKey.CalculateInverse (prng,i);
        unsigned int bits=akey.BitCount() ;
        if(bits>512)
        {
            std::cerr << "Could not decrypt json entries, key or json invalid" <<std::endl;
            exit(1);
        }
        if(bits>256)
        {
            //our share. only case where this will not be the case is zero chaincode? maybe?  which should be degenerate.
            Integer::Divide(chaincode,ours, akey, Integer::Power2(256));
           set|=1;
        }
        else
        {
        theirs=akey;
        set|=2;
        }
    }
    if(set!=3)
    {
        std::cerr << "Could not load all keys from json" <<std::endl;
            exit(1);
    }
    GroupParameters group;
	group.Initialize(ASN1::secp256k1());
	Integer q=group.GetGroupOrder();;
	Integer ckey=(ours+theirs)%q;
    bprv bp=bgetRootFromPrivate(ckey,chaincode);
    cout << bprvEncode(bp) <<endl;
    bpub kpub=bgetPublic(bp);
    cout << bpubEncode(kpub) <<endl;
    return 0;
}