## Build:
 `make pp`
 `make`

## Use:

 `./backupRecovery rsa-priv.cryptopp.pem tst.json`

## Output check

Can enter xprv on https://iancoleman.io/bip39/ and check if xpub on derivation path m/ matches

## Key format

Described in some detail at https://learnmeabitcoin.com/guide/extended-keys
