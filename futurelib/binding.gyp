{
    "targets": [{
        "target_name": "Cryptolib",
        "cflags!": [ "-fno-exceptions"],
        "cflags_cc!": [ "-fno-exceptions", "-fno-rtti" ],
        "cflags ": ["-Wimplicit-fallthrough=0"] ,
        "cflags_cc": ["-std=c++11","-Wno-fallthrough"],
        "sources": [
          "cryptolib/main.cpp",
          "cryptopp-pem/pem_common.cpp",
          "cryptopp-pem/pem_read.cpp",
          "cryptopp-pem/pem_write.cpp",
          "cryptolib/cryptoPrimitive/clientProtocols.cpp",
          "cryptolib/cryptoPrimitive/serverProtocols.cpp",
          "cryptolib/cryptoPrimitive/cryptoUtilities.cpp",
          "cryptolib/cryptoPrimitive/utils.cpp",
          "cryptolib/main.cpp",
        ],
        'include_dirs': [
            "<!@(node -p \"require('node-addon-api').include\")",
            "out/cryptopp/include",
            "cryptolib/cryptoPrimitive",
            "out/cryptopp/include/cryptopp",
            "cryptopp-pem"
        ],
        'libraries': [
            "../out/cryptopp/lib/libcryptopp.a"
        ],
        'dependencies': [
            "<!(node -p \"require('node-addon-api').gyp\")"
        ],
        'defines': [
            'NAPI_DISABLE_CPP_EXCEPTIONS',
            'NODE_ADDON_API_DISABLE_DEPRECATED'
        ]
    }]
}
