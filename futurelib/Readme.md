# Cryptolib Use
#### 1. Native package
- build
```sh
$ npm run build-pp-native
$ npm i
$ npm run build-lib-native
```
- test local
```sh
$ npm run p2 
$ npm run test-local
$ npm run test-deep
```
#### 2.Docker file 
- build
```sh
$ docker build -t cryptolib ./
```
- test
```sh
$ docker run --name p2 -d -p your_port:3002 cryptolib
```
- access test page:
>  http://your_host:your_port/cryptolib_test.html
#### 3.Build Native and wasm package 
```sh
$ ./emsdk/emsdk install 1.40.1 
$ ./emsdk/emsdk activate 1.40.1
$ source ./emsdk/emsdk_env.sh
$ em++ -v
$ npm run build-pp-wasm
$ npm run build-pp-native
$ npm run build-lib-wasm
$ npm i
$ npm run build-lib-native
$ npm run bundle
```