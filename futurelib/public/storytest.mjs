/* eslint-disable prefer-arrow-callback */
/* eslint-disable mocha/no-mocha-arrows */
/* eslint-disable mocha/no-setup-in-describe */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/no-unresolved */
/* eslint-disable guard-for-in */
/* global CryptolibClient, expect, request */

// skip testing
const publicKeyToAddress = () => { };

function formRpcParam (method, ...params) {
  return {
    jsonrpc: '2.0',
    id: 1,
    method,
    params
  };
}
const clientObj = new CryptolibClient();
const serverObj = {
  config: {
    server: window.location.origin
  },
  async post (cmd, ...params) {
    const { server } = this.config;
    return request.post(`${server}/server`).send(formRpcParam(cmd, ...params));
  },
  async get (path = '/server') {
    const { server } = this.config;
    return request.get(`${server}${path}`);
  }
};

async function rpc_call (cmd, params) {
  const resp = await serverObj.post(cmd, params);
  const { result, error } = resp.body;
  if (error) throw Error(`cryptolib error: ${JSON.stringify(error)}`);
  return { result };
}

function deleteKey (party, key_id, ...params) {
  if (party === 1) {
    it(`deleteKey(${key_id}, ${JSON.stringify(params)}) at party ${party}`,
      async () => {
        console.log(key_id,...params);
        clientObj.deleteKey(key_id, ...params);
      });
  } else {
    it(`deleteKey(${key_id}, ${JSON.stringify(params)}) at party ${party}`, async function () {
      console.log(key_id,...params);
      const { status } = await serverObj.post('deleteKey', key_id, ...params);
      expect(status).to.equal(200);
    });
  }
}

function generateKeyClient (tx, key_id, recovery_service_provider, passphrase) {
  it(`generateKeyClient(${key_id})`, async function () {
    this.timeout(100000);
    const { result, error } = await clientObj.generateKeyClient(key_id, passphrase, recovery_service_provider, rpc_call);
    if (error) throw new Error(error);
    Object.assign(tx, result);
  });
}

function getRecoveryKey (key_id) {
  it('getRecoveryKey', async () => {
    const { result, error } = await clientObj.getRecoveryKey(key_id);
    if (error) throw Error(error);
    // console.log(result);
    expect(result).to.be.an('array');
    expect(result[0]).to.be.a('string');
    expect(result[1]).to.be.a('string');
  });
}

function preSignClient (tx, key_id, sid, passphrase, derivePath) {
  it(`preSignClient(${key_id})`, async function () {
    this.timeout(1000000);
    const { result, error } = await clientObj.preSignClient(key_id, sid, passphrase, derivePath, rpc_call);
    if (error) throw new Error(error);
    Object.assign(tx, result);
  });
}

function signClient (tx, key_id, sid, passphrase, derivePath, message) {
  it(`signClient(${key_id})`, async function () {
    this.timeout(1000000);
    const ret = await clientObj.signClient(key_id, sid, passphrase, derivePath, message, rpc_call);
    if (ret.error) throw new Error(ret.error);
    Object.assign(tx, ret);
  });
}

function fullSignClient (tx, key_id, sid, passphrase, derivePath, message) {
  it(`fullSignClient(${key_id})`, async function () {
    this.timeout(1000000);
    const { result, error } = await clientObj.fullSignClient(key_id, sid, passphrase, derivePath, message, rpc_call);
    if (error) throw new Error(error);
    expect(result).to.be.an('object');
    const { r, s, recoveryParam } = result;
    expect(r).to.be.a('string');
    expect(s).to.be.a('string');
    expect(recoveryParam).to.be.a('number');
    Object.assign(tx, result);
  });
}


function getXpub (out, party, key_id) {
  if (party === 1) {
    it(`getXpub(${key_id}) at party ${party}`, (done) => {
      clientObj.getXpub(key_id)
        .then((ret) => { const { result, error } = ret; if (error) throw new Error(error); out.xpub = result; done(); });
    });
  } else {
    it(`getXpub(${key_id}) at party ${party}`, async () => {
      const { status, body: { result, error } } = await serverObj
        .post('getXpub', key_id);
      expect(status).to.equal(200);
      if (error) throw new Error(error);
      expect(result).to.be.a('string');
      out.xpub = result;
    });
  }
}

function run () {
  describe('CryptoLib2 Story Test', () => {
    const wallets = [
      { _id: '5df1111111', recovery_service_provider: 'sbi_japannext' },
      { _id: '5df2222222', recovery_service_provider: 'sbi_vc', passphrase: '5df2222222' },
      { _id: '5dSECOM', recovery_service_provider: 'secom', passphrase: '5dSECOM' }
    ];
    const derivationPath = 'm/44/60/0/0/';
    const message = '7B49146DA0D9A233B383FBDD28131D9683DB075304E832292355EA208BA2F904h';
    it('ping p2', async () => {
      const { status } = await serverObj.get('/');
      expect(status).to.equal(200);
    });
    wallets.forEach((w) => {
      deleteKey(1, w._id);
      deleteKey(2, w._id);
    });
    describe('Test key generation', () => {
      wallets.forEach((w) => {
        const out = {};
        const xpub1 = {};
        const xpub2 = {};
        generateKeyClient(out, w._id, w.recovery_service_provider, w.passphrase);
        getRecoveryKey(w._id);
        getXpub(xpub1, 1, w._id);
        getXpub(xpub2, 2, w._id);
        it('compare xpub of p1, p2', async () => {
          w.xpub = xpub1.xpub;
          w.out = out;
          expect(xpub1.xpub).to.be.a('string');
          expect(xpub2.xpub).to.be.a('string');
          expect(xpub1.xpub).to.equal(xpub2.xpub);
        });

        if (w.passphrase) {
          it(`get seed for wallet_id: ${w._id}`, async () => {
            const seed = await clientObj.getEncryptedSeed(w._id);
            expect(seed.result).to.be.a('string');
          });
        }
      });

      describe('verify addresses', () => {
        for (let i = 0; i < 10; ++i) {
          // eslint-disable-next-line prefer-arrow-callback
          it(`ETH test address ${i}`, async function () {
            const { status, body: { result, error } } = await serverObj
              .post('getDerivedPublic', wallets[1].xpub, `${derivationPath}${i}`);
            expect(status).to.equal(200);
            if (error) throw new Error(error);
            expect(result).to.be.a('string');
            try {
              publicKeyToAddress(result);
            } catch (err) {
              console.log(`public key: ${result}`);
              throw err;
            }
          });
        }
      });
    });
    describe('test sign', () => {
      wallets.forEach((w) => {
        deleteKey(1, w._id);
      });
      wallets.forEach((w) => {
        if (!w.passphrase) return;
        const xpub_obj = {};
        it('merge key back to party 1', async () => {
          clientObj.assignToKey(w._id, 'keygen', 'default', { encrypt: w.out.encrypt });
          clientObj.mergeToKey(w._id, 'keygen', 'default', { xpub: w.xpub });
        });
        it('p1 getXpub', async () => {
          const { result: xpub } = await clientObj.getXpub(w._id);
          expect(xpub).to.be.a('string');
          xpub_obj.xpub = xpub;
        });
        it(`p1 getDerivedPublic for ${derivationPath}0`, async () => {
          const { result: publicKey } = await clientObj.getDerivedPublic(xpub_obj.xpub, `${derivationPath}0`);
          expect(publicKey).to.be.a('string');
        });
        const _tmp = {};
        const signature1 = {};
        const signature2 = {};
        const sid = `${w._id}_${new Date().getTime()}_sep`;
        const sid2 = `${w._id}_${new Date().getTime()}_full`;
        preSignClient(_tmp, w._id, `${sid}`, w.passphrase, `${derivationPath}0`);
        for (let i = 0; i < 20; ++i) {
        fullSignClient(signature1, w._id, `${i}`, w.passphrase, `${derivationPath}0`, message);
        }
        signClient(signature2, w._id, `${sid}`, w.passphrase, `${derivationPath}0`, message);
      });
    });
  });
}

export default class StoryTest {
  constructor (params) {
    Object.assign(this, params);
  }

  run () {
    run();
  }
}
