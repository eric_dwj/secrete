const cryptoJS = require('crypto-js');
const { wallet_call } = require('./utils/rpc');
const { getCryptoData, hashPassword } = require('./utils/sign');
const CryptolibClient = require('./CryptolibClient');
const { signFrontend: ethSignFront } = require('./coins/eth/pay');
const { signFrontend: btcSignFront } = require('./coins/btc/pay');

const signHandler = { ETH: ethSignFront, BTC: btcSignFront };
const cryptolibClient = new CryptolibClient();

const preSign = async ({ token = '', wallet_id, walletSeed }) => {
  const xpub = await getCryptoData({ wallet_id, type: 'Xpub', token });
  await cryptolibClient.assignToKey(wallet_id, 'keygen', 'default', { encrypt: walletSeed });
  await cryptolibClient.mergeToKey(wallet_id, 'keygen', 'default', { xpub });
};

const signTransaction = async ({ coin, wallet_id, password, rawTransaction, from, token = '' }) => {
  const walletSeed = await getCryptoData({ wallet_id, type: 'WalletSeed', token });
  await preSign({ wallet_id, walletSeed, token });
  const { txBlob } = await signHandler[coin](token, { rawTransaction, from, wallet_id, password });
  await cryptolibClient.deleteKey(wallet_id);
  return { result: { txBlob, wallet_id } };
};

const createWallet = async ({ passphrase, coin, label = '', token = '', recovery_service_provider }) => {
  if (!passphrase) throw Error('passphrase cannot be empty');
  if (!recovery_service_provider) throw Error('recovery service provider cannot be empty');
  const { wallet_id, paperKeyId, passphrase_key } = await wallet_call({ token }, 'initWalletFirstStage', [coin, label, recovery_service_provider]);
  await cryptolibClient.generateKeyClient(wallet_id, passphrase, recovery_service_provider,
    async (cmd, { key_id, _message, recovery_service_provider: provider }) => {
      const result = await wallet_call({ token }, 'generateKey', [key_id, _message, provider]);
      return { result };
    });

  const paperKey = await cryptolibClient.getRecoveryKey(wallet_id);
  const { result: walletSeedEncrypted } = await cryptolibClient.getEncryptedSeed(wallet_id) || {};
  if (!walletSeedEncrypted) throw Error('create wallet failed');
  const passphraseEncrypted = cryptoJS.AES.encrypt(passphrase, passphrase_key).toString();
  await wallet_call({ token }, 'initWalletSecondStage', [wallet_id, walletSeedEncrypted, hashPassword(passphrase)]);
  await cryptolibClient.deleteKey(wallet_id);
  return { result: { wallet_id, paperKeyId, paperKey, passphraseEncrypted } };
};

module.exports = { createWallet, signTransaction };
