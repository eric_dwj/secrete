const bitcoinjs = require('bitcoinjs-lib');
const { Wallet } = require('./signAdapter');

const getSecretsCrypto = (params) => {
  const { from } = params;
  return from.reduce((a,b)=>{
    const { address, derivePath } = b;
    return { ...a, [address]: derivePath };
  }, {});
};

const doSignCrypto = async (params) => {
  const { psbt, inputs, from, wallet_id, token, password } = params;
  const derivePaths = await getSecretsCrypto({ from });
  const wallet = new Wallet({ psbt, networks: bitcoinjs.networks.testnet, token });
  for (let i = 0; i < inputs.length; i++) {
    await wallet.sign({ index: i, derivePath: derivePaths[inputs[i].address], wallet_id, inputs, password });
  }
};

const signFrontend = async (token, params) => {
  const { rawTransaction, from, wallet_id, password } = params;
  const secrets = getSecretsCrypto({ from });
  const psbt = new bitcoinjs.Psbt({ network: bitcoinjs.networks.testnet });
  const signSecrets = [];
  for (const input of rawTransaction.inputs) {
    if (!input.isSegwit) {
      psbt.addInput({
        hash: input.txid,
        index: parseInt(input.vout, 10),
        nonWitnessUtxo: Buffer.from(input.rawTx, 'hex')
      });
    } else {
      psbt.addInput({
        hash: input.txid,
        sequence: 0xffffffff,
        index: parseInt(input.vout, 10),
        witnessUtxo: {
          script: Buffer.from(
            input.script,
            'hex',
          ),
          value: input.value,
        }
      });
    }

    signSecrets.push(secrets[input.address]);
  }
  for (const output of rawTransaction.outputs) {
    psbt.addOutput({
      address: output.address,
      value: output.value,
    });
  }
  await doSignCrypto({ psbt, from, wallet_id, token, inputs: rawTransaction.inputs, password });

  for (let i = 0; i < rawTransaction.inputs.length; i++) {
    psbt.validateSignaturesOfInput(i);
  }
  psbt.finalizeAllInputs();
  const txBlob = psbt.extractTransaction().toHex();
  if (!txBlob) throw Error('Sign transaction error');
  return { txBlob };
};

module.exports = { signFrontend };
