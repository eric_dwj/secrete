const bip66 = require('bip66');
const bitcoinjs = require('bitcoinjs-lib');
const { mpcSign, getCryptoData, getDerivedPublic } = require('../../utils/sign');

const ZERO = Buffer.alloc(1, 0);

class Wallet {
  constructor ({ psbt, networks, token }) {
    this.psbt = psbt;
    this.networks = networks;
    this.hashType = 1;
    this.token = token;
  }

  async sign (params) {
    const { index, inputs, derivePath, wallet_id, password } = params;
    const xpub = await getCryptoData({ wallet_id, type: 'Xpub', token: this.token });
    const publicKey = await getDerivedPublic(xpub, derivePath);
    const pubkey = Buffer.from(publicKey.substring(2), 'hex');
    const { redeem } = bitcoinjs.payments.p2sh({
      redeem: bitcoinjs.payments.p2wpkh({
        pubkey,
        network: this.networks
      })
    });
    const signingScript = bitcoinjs.payments.p2pkh({ hash: redeem.output.slice(2) }).output;
    const inputValue = inputs[index].value;
    const signingData = this.psbt.__CACHE.__TX.hashForWitnessV0(parseInt(index, 10), signingScript, parseInt(inputValue, 10), this.hashType).toString('hex');
    const signed = await mpcSign({ token: this.token, key_id: wallet_id, passphrase: password, derivationPath: derivePath, message: `0x${signingData}` });
    delete signed.v;
    const partialSig = [
      {
        pubkey,
        signature: Wallet.encode(this.hashType, signed.r, signed.s)
      }
    ];
    this.psbt.updateInput(index, { partialSig });
  }

  static toDER (x) {
    let i = 0;
    while (x[i] === 0) ++i;
    if (i === x.length) return ZERO;
    const _x = x.slice(i);
    // eslint-disable-next-line no-bitwise
    if (_x[0] & 0x80) return Buffer.concat([ZERO, _x], 1 + _x.length);
    return _x;
  }

  static encode (hashType, r, s) {
    const hashTypeBuffer = Buffer.allocUnsafe(1);
    hashTypeBuffer.writeUInt8(hashType, 0);
    const RBuffer = Wallet.toDER(Buffer.from(Wallet.padding64(r), 'hex'));
    const SBuffer = Wallet.toDER(Buffer.from(Wallet.padding64(s), 'hex'));
    return Buffer.concat([bip66.encode(RBuffer, SBuffer), hashTypeBuffer]);
  }

  static padding64 (_data) {
    let data = JSON.parse(JSON.stringify(_data));
    if (data.length < 64) {
      for (let i = 0; i < 64 - data.length; i++) {
        data = `0${data}`;
      }
    }
    return data;
  }
}

module.exports = { Wallet };
