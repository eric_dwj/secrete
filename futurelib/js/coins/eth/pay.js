const { Wallet } = require('./signAdapter');

const doSignCrypto = async (params) => {
  const { rawTransaction, derivePath, wallet_id, token, password } = params;
  if (typeof rawTransaction !== 'object') throw Error('invalid rawTransaction or privatekey');
  const wallet = new Wallet({ token });
  return wallet.sign(rawTransaction, wallet_id, derivePath, password);
};

const signFrontend = async (token, params) => {
  const { rawTransaction, from: [derivePath = 'm/44/60/0/0/0'] = [], wallet_id, password } = params;
  const txBlob = await doSignCrypto({ rawTransaction, derivePath, wallet_id, token, password });
  if (!txBlob) throw Error('Sign transaction error');
  return { txBlob };
};
module.exports = { signFrontend };
