const ethers = require('ethers');
const { mpcSign } = require('../../utils/sign');

class Wallet {
  constructor ({ token } = {}) {
    this.token = token;
  }

  async sign (transaction, key, derivePath, password) {
    const params = {
      password, derivePath, key_id: key
    };

    return ethers.utils.resolveProperties(transaction).then(async (tx) => {
      const rawTx = ethers.utils.serializeTransaction(tx);
      const signature = await this.signDigest(ethers.utils.keccak256(rawTx), params);
      return ethers.utils.serializeTransaction(tx, signature);
    }).then((tx) => tx);
  }

  async signDigest (digest, params) {
    const { key_id, derivePath, password } = params;
    let msg = digest;
    if (digest.startsWith('0x')) msg = `${digest.substring(2)}h`;
    const signed = await mpcSign({ token: this.token, key_id, passphrase: password, derivationPath: derivePath, message: msg });
    signed.r = `0x${signed.r}`;
    signed.s = `0x${signed.s}`;
    return signed;
  }
}

module.exports = { Wallet };
