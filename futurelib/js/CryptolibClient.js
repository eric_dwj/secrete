const cryptoJS = require('crypto-js');
const CryptolibBase = require('./CryptolibBase');
let CryptolibNative = require('./bindings');

if (CryptolibNative.default) CryptolibNative = CryptolibNative.default;

const config = require('./config');

function decrypt (key_json, passphrase) {
  if (passphrase && key_json.encrypt) {
    const clear_text = cryptoJS.AES.decrypt(key_json.encrypt, passphrase).toString(cryptoJS.enc.Utf8);
    Object.assign(key_json, JSON.parse(clear_text));
  }
}

class CryptolibClient extends CryptolibBase {
  async _process (id, category, sid, subcategory, passphrase, callSelf, callCounterparty) {
    // check if this ID is already done
    const key_json = await this.getSubcategory(id, category, sid, subcategory);
    if (key_json._complete) {
      return { error: 'This ID already completed protocol, delete to reuse', state: 'failed' };
    }
    const clstate = {};
    let countdown = 120; // avoid eternal loops
    let osave = {};
    while (countdown > 0) {
      const { error, result, save, output } = JSON.parse(await callSelf(clstate));
      if (error) return { error };
      if (output) { osave = output; break; }
      const { error: errorServer, result: resultServer } = await callCounterparty(result);
      if (errorServer) return { error: errorServer };
      clstate.result = resultServer;
      clstate.save = save;
      countdown -= 1;
    }

    // encrypt all
    if (passphrase) {
      const nsave = {};
      const exported = osave.export || [];
      const enc = {};
      const exp = {};
      Object.entries(osave).filter(([k]) => exported.includes(k)).forEach(([key, value]) => {
        exp[key] = value;
      });
      Object.entries(osave).filter(([k]) => !exported.includes(k)).forEach(([key, value]) => {
        enc[key] = value;
      });
      Object.assign(nsave, exp);
      if (Object.keys(enc).length > 0) nsave.encrypt = cryptoJS.AES.encrypt(JSON.stringify(enc), passphrase).toString();
      osave = nsave;
    }
    osave._complete = true;
    delete osave.export;
    await this.replaceSubcategory(id, category, sid, subcategory, osave);
    return { result: osave };
  }

  async fullSignClient (key_id, sid, passphrase, derivationPath = 'm', message, callCounterparty) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'default'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    decrypt(key_json, passphrase);
    const params = { derivationPath, message, key: key_json };

    return this._process(key_id, 'sign', sid, 'sign', passphrase,
      async (state) => CryptolibNative.fullSignClient(JSON.stringify(params), JSON.stringify(state)),
      async (msg) => callCounterparty('fullSignServer', { key_id, sid, derivationPath, message, _message: msg }));
  }

  async generateKeyClient (key_id, passphrase, recovery_service_provider, callCounterparty) {
    const backupPublicKey = config.backupPublicKey[recovery_service_provider];
    return this._process(key_id, 'keygen', 'default', null, passphrase,
      async (state) => CryptolibNative.generateKeyClient(JSON.stringify({ backupPublicKey }), JSON.stringify(state)),
      async (msg) => callCounterparty('generateKeyServer', { key_id, _message: msg, recovery_service_provider }));
  }

  async preSignClient (key_id, sid, passphrase, derivationPath = 'm', callCounterparty) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'default'); // get key
    if (!key_json) {
      return { error: 'This key does not exist' };
    }
    decrypt(key_json, passphrase);
    const params = { derivationPath, key: key_json };
    return this._process(key_id, 'sign', sid, 'presign', passphrase,
      async (state) => CryptolibNative.preSignClient(JSON.stringify(params), JSON.stringify(state)),
      async (msg) => callCounterparty('preSignServer', { key_id, sid, derivationPath, _message: msg }));
  }

  async signClient (key_id, sid, passphrase, derivationPath = 'm', message, callCounterparty) {
    const key_json = await this.getSubcategory(key_id, 'sign', sid, 'presign'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    decrypt(key_json, passphrase);
    const params = { derivationPath, message, preSignData: key_json };
    return this._process(key_id, 'sign', sid, 'sign', passphrase,
      async (state) => CryptolibNative.signClient(JSON.stringify(params), JSON.stringify(state)),
      async (msg) => callCounterparty('signServer', { key_id, sid, derivationPath, message, _message: msg }));
  }
}

module.exports = CryptolibClient;
