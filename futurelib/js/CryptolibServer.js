const CryptolibBase = require('./CryptolibBase');
const CryptolibNative = require('./bindings');
const config = require('./config');

class CryptolibServer extends CryptolibBase {
  async _process (id, category, sid, subcategory, callSelf, params, message) {
    const key_json = await this.getSubcategory(id, category, sid, subcategory);
    if (key_json._complete) {
      return { error: 'This ID already completed protocol, delete to reuse' };
    }
    // console.log("MESSAGE:", _message);
    const state = {
      result: message,
      save: key_json
    };
    const { error, result, complete, output, save } = JSON.parse(await callSelf(JSON.stringify(params), JSON.stringify(state)));
    if (error) return { error };
    let toSave = save;
    if (complete) {
      toSave = output;
      toSave._complete = true;
    }
    if (toSave) await this.replaceSubcategory(id, category, sid, subcategory, toSave);
    return { result };
  }

  async generateKeyServer ({ key_id, _message, recovery_service_provider }) {
    const backupPublicKey = config.backupPublicKey[recovery_service_provider];
    return this._process(key_id, 'keygen', 'default', null, CryptolibNative.generateKeyServer, { backupPublicKey }, _message);
  }

  async preSignServer ({ key_id, sid, _message, derivationPath }) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'default'); // get key
    if (!key_json) {
      return { error: 'This key does not exist' };
    }
    const params = {
      derivationPath, key: key_json
    };
    return this._process(key_id, 'sign', sid, 'presign', CryptolibNative.preSignServer, params, _message);
  }

  async signServer ({ key_id, sid, _message, derivationPath, message }) {
    const key_json = await this.getSubcategory(key_id, 'sign', sid, 'presign'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    const params = {
      derivationPath, message, preSignData: key_json
    };
    return this._process(key_id, 'sign', sid, 'sign', CryptolibNative.signServer, params, _message);
  }

  async fullSignServer ({ key_id, sid, _message, derivationPath, message }) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'default'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    const params = {
      derivationPath, message, key: key_json
    };
    return this._process(key_id, 'sign', sid, 'sign', CryptolibNative.fullSignServer, params, _message);
  }
}

module.exports = CryptolibServer;
