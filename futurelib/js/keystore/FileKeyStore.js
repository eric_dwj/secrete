// const _ = require('lodash');
const merge = require('lodash/merge');
const json_store = require('../json_store');

module.exports = class FileKeyStore {
  constructor ({ store_path }) {
    this.store = null;
    this.store_file = store_path;
  }

  async load () {
    this.store = await json_store.loadJSON(this.store_file);
  }

  async save () {
    return json_store.updateJSON(this.store, this.store_file);
  }

  async ensure (id, category, sid) {
    if (!this.store) await this.load();
    if (!(id in this.store)) this.store[id] = {};
    if (!(category in this.store[id])) this.store[id][category] = {};
    if (!(sid in this.store[id][category])) this.store[id][category][sid] = {};
  }

  async getKey (id, category, sid) {
    await this.load();
    await this.ensure(id, category, sid);
    return this.store[id][category][sid];
  }

  async mergeToKey (id, category, sid, value) {
    await this.ensure(id, category, sid);
    merge(this.store[id][category][sid], value);
    return this.save();
  }

  async assignToKey (id, category, sid, value) {
    await this.ensure(id, category, sid);
    Object.assign(this.store[id][category][sid], value);
    return this.save();
  }

  async setKeyValue (id, category, sid, value) {
    await this.ensure(id, category, sid);
    this.store[id][category][sid] = value;
    return this.save();
  }

  async deleteKey (id, category, sid) {
    if (!this.store) await this.load();
    if (id in this.store) {
      if (category && sid) {
        delete this.store[id][category][sid];
      } else {
        delete this.store[id];
      }
      await this.save();
    }
  }
};
