/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */

module.exports = {
  create: (config) => {
    const { scheme } = config;
    let Store;
    if (scheme === 'memory') Store = require('./MemoryKeyStore');
    else if (scheme === 'file') Store = require('./FileKeyStore');
    else if (scheme === 'redis') Store = require('./RedisStore');
    else Store = require('./MongoKeyStore');
    return new Store(config);
  }
};
