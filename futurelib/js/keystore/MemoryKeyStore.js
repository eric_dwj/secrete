const FileKeyStore = require('./FileKeyStore');

module.exports = class MemoryKeyStore extends FileKeyStore {
  constructor (config) {
    super(config);
    this.store = {};
    this.load = async () => {};
    this.save = async () => {};
  }
};
