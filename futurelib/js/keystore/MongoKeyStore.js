const _ = require('lodash');
// const cache = require('node-cache');
const mongoose = require('mongoose');
const CryptoJS = require('crypto-js');
const moment = require('moment');
const log = require('../utils/log4js').getLogger('[dbcon]');

const key_store_schema = mongoose.Schema({
  key_id: String,
  category: String,
  sid: String,
  value: String,
  ts: {
    type: Date,
    default: Date.now,
  }
});

const collection = mongoose.model('key_store', key_store_schema);

module.exports = class MongoKeyStore {
  constructor ({ dbcon, remove_interval = 86400000, salt }) {
    this.dbcon = dbcon;
    this.salt = salt;
    this.connected = false;
    this.connect();
    this.removeExpired(remove_interval);
  }

  async removeExpired (remove_interval) {
    setTimeout(async () => {
      await this.connect();
      await collection.deleteMany({ category: 'sign', ts: { $lt: moment().subtract(3, 'days').toDate() } });
    }, remove_interval);
  }

  async connect () {
    try {
      if (!this.connected) {
        await mongoose.connect(this.dbcon, {
          // native_parser: true,
          useCreateIndex: true,
          useNewUrlParser: true,
          useUnifiedTopology: true
        });
        this.connected = true;
        log.info('connected to db successfully');
      }
    } catch (err) {
      if (!this.connected) log.error('connected to db failed');
      else log.info('db already connected');
    }
  }

  disconnect () {
    this.connected = false;
    mongoose.connection.close();
    log.info('disconnected to db successfully');
  }

  encrypt (data) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), this.salt).toString();
  }

  decrypt (data) {
    return JSON.parse(CryptoJS.AES.decrypt(data, this.salt).toString(CryptoJS.enc.Utf8));
  }

  async getKey (key_id, category, sid) {
    await this.connect();
    const doc = await collection.findOne({ key_id, category, sid }).lean().exec();
    if (doc) return this.decrypt(doc.value);
    return {};
  }

  async mergeToKey (key_id, category, sid, value) {
    await this.connect();
    const old = await this.getKey(key_id, category, sid);
    _.merge(old, value);
    return this.setKeyValue(key_id, category, sid, old);
  }

  async assignToKey (key_id, category, sid, value) {
    await this.connect();
    const old = await this.getKey(key_id, category, sid);
    Object.assign(old, value);
    return this.setKeyValue(key_id, category, sid, old);
  }

  async setKeyValue (key_id, category, sid, value) {
    await this.connect();
    return collection.updateOne({ key_id, category, sid }, { value: this.encrypt(value) },
      { upsert: true, setDefaultsOnInsert: true }).exec();
  }

  async deleteKey (key_id, category, sid) {
    await this.connect();
    const condition = { key_id };
    if (category && sid) Object.assign(condition, { category, sid });
    return collection.deleteMany(condition).exec();
  }
};
