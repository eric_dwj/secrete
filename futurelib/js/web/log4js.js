module.exports = {
  getLogger: (label) => {
    const obj = { label };
    obj.debug = (...args) => { console.debug(label, ...args); };
    obj.info = (...args) => { console.info(label, ...args); };
    obj.error = (...args) => { console.error(label, ...args); };
    return obj;
  }
};
