const config = require('../config/default');

module.exports = {
  ...config,
  keyStore: {
    scheme: 'memory'
  }
};
