const MemoryKeyStore = require('../keystore/MemoryKeyStore');

module.exports = {
  create: (config) => new MemoryKeyStore(config)
};
