const fs = require('fs');

function loadJSON (storeFile = 'key_store.json') {
  if (!fs.existsSync(storeFile)) {
    return {};
  }
  const content = fs.readFileSync(storeFile);
  return JSON.parse(content);
}

function updateJSON (obj, storeFile = 'key_store.json') {
  fs.writeFileSync(storeFile, JSON.stringify(obj));
}

module.exports = {
  loadJSON,
  updateJSON,
};
