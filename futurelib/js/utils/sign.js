const cryptoJS = require('crypto-js');
const CryptolibClient = require('../CryptolibClient');
const { wallet_call } = require('./rpc');

const cryptolibClient = new CryptolibClient();

const getDerivedPublic = async (xpub, derivationPath) => {
  const { result } = await cryptolibClient.getDerivedPublic(xpub, derivationPath);
  return result;
};
const mpcSign = async ({ token, key_id, passphrase, derivationPath, message }) => {
  const sid = `${new Date().getTime() + Math.floor(1000000 * Math.random())}`;
  const { result: ret, error } = await cryptolibClient.fullSignClient(key_id, sid, passphrase, derivationPath, message, async (cmd, params) => {
    const result = await wallet_call({ token }, 'cryptoSign', [params.key_id, params.sid, params.derivationPath, params.message, params._message]);
    return { result };
  });
  const { recoveryParam, r, s } = ret;
  const signed = { recoveryParam, r, s };
  signed.v = 27 + signed.recoveryParam;
  return signed;
};
const hashPassword = (password, salt = '') => cryptoJS.SHA512(password + salt).toString();
const getCryptoData = async ({ wallet_id, type, token }) => {
  const { walletSeedEncrypted, xpub } = await wallet_call({ token }, 'getCryptoData', [wallet_id, type]);
  if (walletSeedEncrypted) return walletSeedEncrypted;
  if (xpub) return xpub;
  return null;
};
module.exports = { mpcSign, hashPassword, getCryptoData, getDerivedPublic };
