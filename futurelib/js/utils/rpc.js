const axios = require('axios');

const wallet = process.env.WALLET_URL || 'http://localhost:1380/secure';

const wallet_call = async (headers, method, params) => {
  const { data: { result, error } } = await axios.post(wallet, { jsonrpc: '2.0', id: 1, method, params }, { headers });
  if (error) throw Error(JSON.stringify(error));
  return result;
};

module.exports = { wallet_call };
