## Story Test

Story test checks basic functionality of Cryptolib (generate key, extract public key, sign)

### Preparation

- at one console, start p2 (server) by
```
npm run start-test-p2
```

- at the other console, run story test by
```
npm run test
```

### "Deep" story test


To run it, one needs "backupRecovery" executable (compiled from backupRecovery repository for a system) and `rsa-priv.cryptopp.pem` private key file (for testing *only*!), both placed in the root directory. 


Start p2 server the same way as above.


On the other console, run

```
npm run deepTest
```

It runs key generation and random path and signature verification, comparing Cryptolib results to bip32 library.
