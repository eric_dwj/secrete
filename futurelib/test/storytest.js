const request = require('supertest');
const expect = require('expect.js');
const publicKeyToAddress = require('ethereum-public-key-to-address');
const bitcoinjs = require('bitcoinjs-lib');

const CryptolibClient = require('../js/CryptolibClient');

const clientObj = new CryptolibClient();
const p2 = request(process.env.SERVER_URL || 'http://localhost:3002/server');

function formRpcParam (method, ...params) {
  return {
    jsonrpc: '2.0',
    id: 1,
    method,
    params
  };
}

async function rpc_call (cmd, params) {
  const resp = await p2.post('/').send(formRpcParam(cmd, params));
  const { result, error } = resp.body;
  if (error) throw Error(`cryptolib error: ${JSON.stringify(error)}`);
  return { result };
}

function deleteKey (party, key_id, ...params) {
  if (party === 1) {
    it(`deleteKey(${key_id}, ${JSON.stringify(params)}) at party ${party}`, async () => clientObj.deleteKey(key_id, ...params));
  } else {
    it(`deleteKey(${key_id}, ${JSON.stringify(params)}) at party ${party}`, async function () {
      return p2.post('/')
        .send(formRpcParam('deleteKey', key_id, ...params))
        .expect(200);
    });
  }
}

/* function mergeToKey (party, key_id, category, sid, value) {
  if (party === 1) {
    it(`mergeToKey(${key_id}, ${JSON.stringify(category)}) at party ${party}`, async () => Cryptolib.mergeToKey(key_id, category, sid, value));
  } else {
    it(`mergeToKey(${key_id}, ${category}, ${sid}) at party ${party}`, function (done) {
      p2.post('/')
        .send(formRpcParam('mergeToKey', key_id, category, sid, value))
        .expect(200, done);
    });
  }
} */

function generateKeyClient (tx, key_id, recovery_service_provider, passphrase) {
  it(`generateKeyClient(${key_id})`, async function () {
    this.timeout(100000);
    const { result, error } = await clientObj.generateKeyClient(key_id, passphrase, recovery_service_provider, rpc_call);
    if (error) throw new Error(error);
    Object.assign(tx, result);
  });
}
function getRecoveryKey (key_id) {
  it('getRecoveryKey', async function () {
    const { result, error } = await clientObj.getRecoveryKey(key_id);
    if (error) throw Error(error);
    // console.log(result);
    expect(result).to.be.an('array');
    expect(result[0]).to.be.a('string');
    expect(result[1]).to.be.a('string');
  });
}

function preSignClient (tx, key_id, sid, passphrase, derivePath) {
  it(`preSignClient(${key_id})`, async function () {
    this.timeout(10000);
    const { result, error } = await clientObj.preSignClient(key_id, sid, passphrase, derivePath, rpc_call);
    if (error) throw new Error(error);
    Object.assign(tx, result);
  });
}

function signClient (tx, key_id, sid, passphrase, derivePath, message) {
  it(`signClient(${key_id})`, async function () {
    this.timeout(10000);
    const ret = await clientObj.signClient(key_id, sid, passphrase, derivePath, message, rpc_call);
    if (ret.error) throw new Error(ret.error);
    Object.assign(tx, ret);
  });
}

function fullSignClient (tx, key_id, sid, passphrase, derivePath, message) {
  it(`fullSignClient(${key_id})`, async function () {
    this.timeout(10000);
    const { result, error } = await clientObj.fullSignClient(key_id, sid, passphrase, derivePath, message, rpc_call);
    if (error) throw new Error(error);
    expect(result).to.be.an('object');
    const { r, s, recoveryParam } = result;
    expect(r).to.be.a('string');
    expect(s).to.be.a('string');
    expect(recoveryParam).to.be.a('number');
    Object.assign(tx, result);
  });
}

function getXpub (out, party, key_id, testnet = false) {
  if (party === 1) {
    it(`getXpub(${key_id}) at party ${party}`, function (done) {
      clientObj.getXpub(key_id, testnet)
        .then((ret) => { const { result, error } = ret; if (error) throw new Error(error); out.xpub = result; done(); });
    });
  } else {
    const p = p2;
    it(`getXpub(${key_id}) at party ${party}`, async function () {
      const { body: { result, error } } = await p.post('/')
        .send(formRpcParam('getXpub', key_id, testnet))
        .expect(200);
      if (error) throw new Error(error);
      // console.log(`getXpub result = ${result}`);
      expect(result).to.be.a('string');
      out.xpub = result;
    });
  }
}

class BtcTester {
  constructor (testnet = false) {
    this.testnet = testnet;
  }

  btcNetworks () {
    if (this.testnet) {
      return bitcoinjs.networks.testnet;
    }
    return bitcoinjs.networks.bitcoin;
  }

  parseDerivationPath (derivePathIndex, isChangeAddress = false) {
    if (this.testnet) {
      if (isChangeAddress) return `m/44/1/0/1/${derivePathIndex}`;
      return `m/44/1/0/0/${derivePathIndex}`;
    }
    if (isChangeAddress) return `m/44/0/0/1/${derivePathIndex}`;
    return `m/44/0/0/0/${derivePathIndex}`;
  }

  generateSingleAddress ({ xpub, derivePathIndex, isChangeAddress = false }) {
    const dpath = this.parseDerivationPath(derivePathIndex, isChangeAddress);
    // console.log('derive path', dpath, 'xpub', xpub);
    const { publicKey } = bitcoinjs.bip32.fromBase58(xpub, this.btcNetworks()).derivePath(dpath);
    const { address } = bitcoinjs.payments.p2wpkh({ pubkey: Buffer.from(publicKey, 'hex'), network: this.btcNetworks() });
    return { address };
  }
}

describe('CryptoLib2 Story Test', function () {
  const wallets = [
    { _id: '5df1111111', recovery_service_provider: 'sbi_japannext' },
    { _id: '5df2222222', recovery_service_provider: 'sbi_vc', passphrase: '5df2222222' },
    { _id: '5dSECOM', recovery_service_provider: 'secom', passphrase: '5dSECOM' }
  ];
  const derivationPath = 'm/44/1/0/0/';
  const message = '7B49146DA0D9A233B383FBDD28131D9683DB075304E832292355EA208BA2F904h';
  it('ping p2', function (done) {
    p2.get('/').expect(200, done);
  });
  wallets.forEach((w) => {
    deleteKey(1, w._id);
    deleteKey(2, w._id);
  });
  describe('Test key generation', function () {
    wallets.forEach((w) => {
      const out = {};
      const xpub1 = {};
      const xpub2 = {};
      const tpub1 = {};
      const tpub2 = {};
      generateKeyClient(out, w._id, w.recovery_service_provider, w.passphrase);
      getRecoveryKey(w._id);
      getXpub(xpub1, 1, w._id);
      getXpub(tpub1, 1, w._id, true);
      getXpub(xpub2, 2, w._id);
      getXpub(tpub2, 2, w._id, true);
      it('compare xpub of p1, p2', async function () {
        w.xpub = xpub1.xpub;
        w.out = out;
        expect(xpub1.xpub).to.be.a('string');
        expect(xpub2.xpub).to.be.a('string');
        expect(xpub1.xpub).to.equal(xpub2.xpub);
      });
      it('compare tpub of p1, p2', async function () {
        w.tpub = tpub1.xpub;
        expect(tpub1.xpub).to.be.a('string');
        expect(tpub2.xpub).to.be.a('string');
        expect(tpub1.xpub).to.equal(tpub2.xpub);
      });
      if (w.passphrase) {
        it(`get seed for wallet_id: ${w._id}`, async function () {
          const seed = await clientObj.getEncryptedSeed(w._id);
          expect(seed.result).to.be.a('string');
        });
      }
    });

    describe('verify addresses', function () {
      for (let i = 0; i < 10; ++i) {
        it(`ETH test address ${i}`, async function () {
          const { body: { result, error } } = await p2.post('/')
            .send(formRpcParam('getDerivedPublic', wallets[1].xpub, `${derivationPath}${i}`))
            .expect(200);
          if (error) throw new Error(error);
          expect(result).to.be.a('string');
          try {
            publicKeyToAddress(result);
          } catch (err) {
            console.log(`public key: ${result}`);
            throw err;
          }
        });
      }
      for (let i = 0; i < 10; ++i) {
        it(`BTC mainnet address ${i}`, async function () {
          const btc = new BtcTester(false);
          const { address } = btc.generateSingleAddress({ xpub: wallets[1].xpub, derivePathIndex: i });
          expect(address).to.be.a('string');
          // console.log(address);
        });
      }
      for (let i = 0; i < 10; ++i) {
        it(`BTC testnet address ${i}`, async function () {
          const btc = new BtcTester(true);
          const { address } = btc.generateSingleAddress({ xpub: wallets[1].tpub, derivePathIndex: i });
          expect(address).to.be.a('string');
          // console.log(address);
        });
      }
    });
  });
  describe('test sign', function () {
    wallets.forEach((w) => {
      deleteKey(1, w._id);
    });
    wallets.forEach((w) => {
      if (!w.passphrase) return;
      const xpub_obj = {};
      it('merge key back to party 1', async function () {
        clientObj.assignToKey(w._id, 'keygen', 'default', { encrypt: w.out.encrypt });
        clientObj.mergeToKey(w._id, 'keygen', 'default', { xpub: w.xpub });
      });
      it('p1 getXpub', async function () {
        const { result: xpub } = await clientObj.getXpub(w._id);
        expect(xpub).to.be.a('string');
        xpub_obj.xpub = xpub;
      });
      it(`p1 getDerivedPublic for ${derivationPath}0`, async function () {
        const { result: publicKey } = await clientObj.getDerivedPublic(xpub_obj.xpub, `${derivationPath}0`);
        expect(publicKey).to.be.a('string');
      });
      const _tmp = {};
      const signature1 = {};
      const signature2 = {};
      const sid = `${w._id}_${new Date().getTime()}_sep`;
      const sid2 = `${w._id}_${new Date().getTime()}_full`;
      preSignClient(_tmp, w._id, `${sid}`, w.passphrase, `${derivationPath}0`);
      fullSignClient(signature1, w._id, `${sid2}`, w.passphrase, `${derivationPath}0`, message);
      signClient(signature2, w._id, `${sid}`, w.passphrase, `${derivationPath}0`, message);
    });
  });
});
