/* eslint-disable func-names */
/* eslint-disable prefer-rest-params */
/* eslint-disable prefer-spread */
/* eslint-disable no-unused-vars */
/* eslint-disable no-bitwise */
// const Module = require('./cryptolib');

import cryptolib from './cryptolib.js';
import cryptolibWasm from './cryptolib.wasm';

const Module = cryptolib({
  locateFile (path) {
    if (path.endsWith('.wasm')) {
      console.debug('loading', cryptolibWasm);
      return cryptolibWasm;
    }
    return path;
  }
});

const {
  _freeStr, stringToUTF8, writeArrayToMemory, UTF8ToString, stackSave, stackRestore, stackAlloc
} = Module;

function getCFunc (ident) {
  return Module[`_${ident}`]; // closure exported function
}

function scall (ident, returnType, argTypes, args, opts) {
  const toC = {
    string (str) {
      let ret = 0;
      if (str !== null && str !== undefined && str !== 0) {
        const len = (str.length << 2) + 1;
        ret = stackAlloc(len);
        stringToUTF8(str, ret, len);
      }
      return ret;
    },
    array (arr) {
      const ret = stackAlloc(arr.length);
      writeArrayToMemory(arr, ret);
      return ret;
    }
  };
  function convertReturnValue (ret) {
    if (returnType === 'string') return UTF8ToString(ret);
    if (returnType === 'boolean') return Boolean(ret);
    return ret;
  }
  const func = getCFunc(ident);
  const cArgs = [];
  let stack = 0;
  if (args) {
    for (let i = 0; i < args.length; i++) {
      const converter = toC[argTypes[i]];
      if (converter) {
        if (stack === 0) stack = stackSave();
        cArgs[i] = converter(args[i]);
      } else {
        cArgs[i] = args[i];
      }
    }
  }
  const _ret = func.apply(null, cArgs);
  const ret = convertReturnValue(_ret);
  _freeStr(_ret);
  if (stack !== 0) stackRestore(stack);
  return ret;
}

function swrap (ident, returnType, argTypes, opts) {
  argTypes = argTypes || [];
  const numericArgs = argTypes.every((type) => type === 'number');
  const numericRet = returnType !== 'string';
  if (numericRet && numericArgs && !opts) {
    return getCFunc(ident);
  }
  return function () {
    return scall(ident, returnType, argTypes, arguments, opts);
  };
}

const CryptolibNative = {};

CryptolibNative.generateKeyClient = swrap('generateKeyClient', 'string', ['string', 'string']);
CryptolibNative.generateKeyServer = swrap('generateKeyServer', 'string', ['string', 'string']);
CryptolibNative.preSignClient = swrap('preSignClient', 'string', ['string', 'string']);
CryptolibNative.preSignServer = swrap('preSignServer', 'string', ['string', 'string']);
CryptolibNative.signClient = swrap('signClient', 'string', ['string', 'string']);
CryptolibNative.signServer = swrap('signServer', 'string', ['string', 'string']);
CryptolibNative.fullSignClient = swrap('fullSignClient', 'string', ['string', 'string']);
CryptolibNative.fullSignServer = swrap('fullSignServer', 'string', ['string', 'string']);


CryptolibNative.getDerivedPublic = swrap('getDerivedPublic', 'string', ['string', 'string']);

export default CryptolibNative;
