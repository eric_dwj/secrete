#pragma once
#include <napi.h>
#include "cryptoPrimitive/headers.h"
#include "cryptoPrimitive/clientProtocols.h"
#include "cryptoPrimitive/serverProtocols.h"

// internal
int getVal(char x);
string getAddrFromPk(Integer& pk);

namespace CryptoLib {
  std::string generateKeyClient(std::string parameters,std::string store);
  std::string generateKeyServer(std::string parameters,std::string store);
  std::string getDerivedPublic(std::string xpub, std::string derivationPath);
  std::string preSignClient(std::string parameters,std::string store);
  std::string preSignServer(std::string parameters,std::string store);
  std::string signClient(std::string parameters,std::string store);
  std::string signServer(std::string parameters,std::string store);
  std::string fullSignClient(std::string parameters,std::string store);
  std::string fullSignServer(std::string parameters,std::string store);
 
  // wrapper
  Napi::String generateKeyClientJS(const Napi::CallbackInfo& info);
  Napi::String generateKeyServerJS(const Napi::CallbackInfo& info);
  Napi::String preSignClientJS(const Napi::CallbackInfo& info);
  Napi::String preSignServerJS(const Napi::CallbackInfo& info);
  Napi::String signClientJS(const Napi::CallbackInfo& info);
  Napi::String signServerJS(const Napi::CallbackInfo& info);
  Napi::String fullSignClientJS(const Napi::CallbackInfo& info);
  Napi::String fullSignServerJS(const Napi::CallbackInfo& info);
  
  Napi::String getDerivedPublicJS(const Napi::CallbackInfo& info);
 // Napi::String getRecoveryKeyJS(const Napi::CallbackInfo& info);
}
