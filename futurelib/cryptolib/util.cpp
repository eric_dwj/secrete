#include "main.h"
using namespace CryptoPP;

int getVal(char x)
{
  if (x >= '0' && x <= '9') return x - '0';
  if (x >= 'a' && x <= 'f') return x - 'a' + 10;
  if (x >= 'A' && x <= 'F') return x - 'A' + 10;
  return -1;
}

string getAddrFromPk(Integer &pk)
{
  SHA256 hash;
  std::string msg = IntToString(pk, 16);
  //std::string msg = "50863ad64a87ae8a2fe83c1af1a8403cb53f53e486d8511dad8a04887e5b2352";
  // cout << "msg length is " << msg.size() << endl;
  msg = "02" + msg;

  // cout <<"msg is " << msg << endl;
  // cout << "msg length is " << msg.size() << endl;

  int len = 33;
  //int len = sizeof(pk);
  uint8_t b[len];
  //unsignedchar b[len];
  //memcpy(b, msg.data(), msg.length());
  for (int i = 0; i < len; ++i)
  {
    int idx = i * 2;
    int xval = getVal(msg[idx]);
    int yval = getVal(msg[idx + 1]);
    // TODO: check xval > 0
    b[i] = 16 * xval + yval;
  }
  //cout << endl;
  //for(int i = 0; i < len; ++i){
  //		cout << b[i] <<" ";
  //	}
  // cout << endl;
  std::string digest;
  hash.Update(b, len);
  //hash.Update((const byte*)b, userRandomness.size());
  digest.resize(hash.DigestSize());
  hash.Final((byte *)&digest[0]);
  //	cout << digest << endl;
  // cout <<"SHA256 HASH is ";
  // HexEncoder encoder(new FileSink(std::cout));
  // StringSource(digest, true, new Redirector(encoder));
  // std::cout << std::endl;
  //HexEncoder hex(new FileSink(std::cout));
  RIPEMD160 h;
  h.Update((const byte *)digest.data(), digest.size());
  digest.resize(h.DigestSize());
  h.Final((byte *)&digest[0]);
  // cout <<"RIPE MD160 HASH is ";
  // StringSource(digest, true, new Redirector(encoder));
  // std::cout << std::endl;
  //cout << endl;
  /*Add prefix version*/

  /* BTC
	char tempC = '\0';
	digest = tempC + digest;
	*/

  /*BTC test net*/
  char tempC = '\x6f';
  digest = tempC + digest;
  std::string res = digest;

  hash.Update((const byte *)digest.data(), digest.size());
  digest.resize(hash.DigestSize());
  hash.Final((byte *)&digest[0]);
  // StringSource(digest, true, new Redirector(encoder));
  // std::cout << std::endl;

  hash.Update((const byte *)digest.data(), digest.size());
  digest.resize(hash.DigestSize());
  hash.Final((byte *)&digest[0]);
  // StringSource(digest, true, new Redirector(encoder));
  // std::cout << std::endl;

  res = res + digest.substr(0, 4);
  // StringSource(res, true, new Redirector(encoder));
  // std::cout << std::endl;

  string code_string = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
  string result;
  Integer xx((const byte *)(res.data()), res.size());
  // cout <<"xx is "<<xx <<endl;
  while (xx > Integer::Zero())
  {
    int r = xx % 58;
    xx = xx / 58;
    result.push_back(code_string[r]);
  }
  int leadingZero = 0;
  unsigned int i = 0;
  while (i < res.length() && res[i] == '\0')
  {
    i++;
    leadingZero++;
  }
  //result.reverse();
  for (int j = 0; j < leadingZero; ++j)
  {
    result.push_back(code_string[0]);
  }
  string tempString = result;
  for (unsigned int j = 0; j < result.length(); ++j)
  {
    result[j] = tempString[result.length() - 1 - j];
  }
  // cout<< "result is " << result << endl;

  return result;
}
