#include "mtaProtocol.h"
#include "serverProtocols.h"
#include "packets.h"
#include <string>
#include <algorithm>
#include <iterator>


#ifdef MPRONLY
int main()
{
    cout<<"Generating.." <<endl;
    RSA::PrivateKey privateKey;
    privateKey.GenerateRandomWithKeySize(_globalContext.prng, 2048);
   fujiCore core1=generateInitialKeys(1024);
  fujiCore core2=generateInitialKeys(1024);
    cout<<"Cores Generated" <<endl;
}
#endif
#ifdef MTAMAIN
// g++ -DNDEBUG -DMTAMAIN -g2 -O2 -I ../.. paillier.cpp prime.cpp mtaProtocol.cpp   -o mtat ../../cryptopp/libcryptopp.a
void interactDriver(AbstractHierarchyProtocolSide* client, AbstractHierarchyProtocolSide * server,const std::string& iname)
{
    json clstate=json();
    json svstate=server->initialState();
    int messagen=0;
    size_t total_server=0;
    size_t total_client=0;
    
    while(!(server->isTerminated()&&client->isTerminated()))
    {
        clstate=client->process_state(clstate);
        #ifdef DPRINT
         cout << "Client" <<endl;
         cout << clstate.dump()<<endl;
         cout << "------" <<endl;
        #endif 

        json clmsg=clstate["result"];
        total_client+=clmsg.dump().length();
        svstate["result"]=clmsg;
        if(!clmsg.is_null()) messagen++;
        svstate=server->process_state(svstate);
        #ifdef DPRINT
         cout << "Server" <<endl;
         cout << svstate.dump()<<endl;
         cout << "------" <<endl;
        #endif 
        //send message back
        clstate["result"]=svstate["result"];
        total_server+=svstate["result"].dump().length();
    }
      #ifdef DPRINT
         cout << "Total requests to server for "<<iname<<" : " <<messagen << endl;
         cout << "Total data sent by client for "<<iname<<" : " <<total_client << endl;
          cout << "Total data sent by server for "<<iname<<" : " <<total_server << endl;
         cout << "------" <<endl;
        #endif 
}
int main()
{
    cout<<"Generating.." <<endl;
    fujiCore core1=generateInitialKeys(2048);
    fujiCore core2=generateInitialKeys(2048);
    cout<<"Cores Generated" <<endl;
    baseContext con;
    Integer a1=genRandZ(con.q,con.prng,false);
    Integer b1=genRandZ(con.q,con.prng,false);
    Integer a2=genRandZ(con.q,con.prng,false);
    Integer b2=genRandZ(con.q,con.prng,false);
    Element loglog=con.group.GetCurve().ScalarMultiply(con.base,a1);
    verifiedSideData vsd1;
    vsd1.h1=core2.b1;
    vsd1.h2=core2.b2;
    vsd1.E=core2.E.pk;
    verifiedSideData vsd2;
    vsd2.h1=core1.b1;
    vsd2.h2=core1.b2;
    vsd2.E=core1.E.pk;
    schnorrBaseProof prfff=proveSchnorr(a1,con);
    cout << "Initial schnorr  "<< verifySchnorr(loglog,prfff,con) << endl;
    MtaProtocolClient client=MtaProtocolClient(a1,b1,false,true,&core1,&vsd1,Element());
    MtaProtocolServer server=MtaProtocolServer(a2,b2,false,true,&core2,&vsd2,Element());
    interactDriver(&client,&server,"MTA");
    Integer abc=(a1*b2)%con.q;
    Integer alb=(client.outpAlBr+server.outpArBl) %con.q;
    cout << IntToString(abc) <<endl;
    cout << IntToString(alb) <<endl;

    Integer cba=(a2*b1)%con.q;
    Integer a2b=(client.outpArBl+server.outpAlBr) %con.q;
     cout << "Modulo: " << IntToString(con.q)<<endl;
    cout << IntToString(cba) <<endl;
    cout << IntToString(a2b) <<endl;
    FeldmanVssClient fclient=FeldmanVssClient(a1,con.q);
    FeldmanVssServer fserver=FeldmanVssServer(a2,con.q);
    interactDriver(&fclient,&fserver,"Feldman");
    cout << fclient.final_output().dump() <<endl;
    cout << fserver.final_output().dump() <<endl;
    cout << "KEYGEN" <<endl;
    RSA::PrivateKey privKey;
privKey.GenerateRandomWithKeySize(_globalContext.prng, 2048);
RSA::PublicKey pubKey(privKey);

    KeyGenClient kclient=KeyGenClient(pubKey);
    KeyGenServer kserver=KeyGenServer(pubKey);
    interactDriver(&kclient,&kserver,"Keygen");
    cout << "Client " << kclient.final_output().dump() <<endl;
    cout << "Server " << kserver.final_output().dump() <<endl;

    string bc="xprv9s21ZrQH143K32WX7F4tVXU5dRHv2N1ipA9K4dQnKidC4M9kZYBhUqWHYBrqCPkk8jPzDkYQ7PxxcVzVAZHY5TDb1mUursWxydrnK76WMSK";
    Integer bkey=base58decode(bc);
    vector<byte> bkeyv=concatEncode(bkey);

    cout << "Root " <<IntToString(bkey) <<endl;
    cout << "Hex " <<IntToString(bkey,16) <<endl;
    cout << "Rootbits " <<bkey.BitCount() <<endl;
    cout << "Rootbytes " <<bkey.ByteCount() <<endl;
    cout << "Root0 " << (int)bkeyv[0] <<endl;
    for (int i=0;i<bkeyv.size();i++)
     {
      cout <<   (int)bkeyv[i] <<" ";

     }
     cout <<endl;
     cout << bitcoin_checksum(bkeyv,78) <<endl;
     cout << (uint32_t)*((uint32_t *)&bkeyv[78] ) <<endl;
    bprv decoded=bprvDecode(bc);
    cout << decoded.version <<endl;
    cout << bprvEncode(decoded) <<endl;
    bpub bpubkey=bgetPublic(decoded);
     bpub tpubkey=bgetPublic(decoded,true);
    cout <<"pk:" << IntToString(bpubkey.pub_key.x)<<endl;
    
    string pubenc=bpubEncode(bpubkey) ;
     string tpubenc=bpubEncode(tpubkey) ;
    cout << pubenc<<endl;
     cout << tpubenc<<endl;
    bpubkey=bpubDecode(pubenc);
    cout << "pk: "<<IntToString(bpubkey.pub_key.x)<<endl;
    cout << getDerivedPublicInternal(pubenc,"m").dump() <<endl;
    cout << getDerivedPublicInternal(pubenc,"m/0").dump() <<endl;
    cout << getDerivedPublicInternal(tpubenc,"m/0").dump() <<endl;
    cout << getDerivedPublicInternal(pubenc,"m/1").dump() <<endl;
    cout << getDerivedPublicInternal(tpubenc,"m/1").dump() <<endl;
    cout << pubenc<<endl;
    string tprv_check="tprv8ZgxMBicQKsPcsbCVeqqF1KVdH7gwDJbxbzpCxDUsoXHdb6SnTPYxdwSAKDC6KKJzv7khnNWRAJQsRA8BBQyiSfYnRt6zuu4vZQGKjeW4YF";
    bprv tdecoded=bprvDecode(tprv_check);
    cout << "Tprv version:" <<tdecoded.version <<endl;
    bprv next=deriveIndex(decoded,0);
    string nxv=bprvEncode(next);
    cout << "m/0" << nxv <<endl;
    deriveAccumulator acc;
    acc.pubkey=bpubkey.pub_key;
    acc.chaincode=decoded.chaincode;
    acc.accumulated=0;
    deriveAccumulate(acc,0);
    bpub bpubtest=bgetPublic(next);
    cout << "pkd: "<<IntToString(acc.pubkey.x)<<endl;
    cout << "pkp: "<<IntToString(bpubtest.pub_key.x)<<endl;
    string xpb="xpub661MyMwAqRbcFxvYjXUNLTMLg4wtHKkN4sn6cG5ATKFWiqc9Sn5UqmpeqKc4K3TCnaQ8R9VitUswvupHNohSTtWPKQraQQVy9E5ovviBsr7";
    bpub datacheck=bpubDecode(xpb);
    cout << "Proper: m  "<< getDerivedPublicInternal(xpb,"m").dump() <<endl;
    cout << "Proper: m/44 "<< getDerivedPublicInternal(xpb,"m/44").dump() <<endl;
    cout << "Proper: m/44/1 "<< getDerivedPublicInternal(xpb,"m/44/1").dump() <<endl;
    cout << "Proper: m/44/1/0 "<< getDerivedPublicInternal(xpb,"m/44/1/0").dump() <<endl;
    cout << "Proper: m/44/1/0/0 "<< getDerivedPublicInternal(xpb,"m/44/1/0/0").dump() <<endl;
    cout << "Proper: m/44/1/0/0/2 "<< getDerivedPublicInternal(xpb,"m/44/1/0/0/2").dump() <<endl;
    cout <<" Check1 " << IntToString(datacheck.pub_key.x,16) << "  " << IntToString(datacheck.pub_key.y,16)<< endl;
    bc="xprv9v1uivb3LLkLw7YZsMm82QenZPKWLtPWZsTYi5W7nZdmaQPPmCYr2fLp4QXg2cG3hmE3tmjX4xMzwGY7C9FJM3Qaf1BS5Bwan2zdUgRWK61";
    Integer bkey2=base58decode(bc);
    vector<byte> bkeyv2=concatEncode(bkey2);
    for (int i=0;i<bkeyv2.size();i++)
     {
      cout <<   (int)bkeyv2[i] <<" ";

     }
     cout <<endl;
     bkey2=base58decode(nxv);
    bkeyv2=concatEncode(bkey2);
      for (int i=0;i<bkeyv2.size();i++)
     {
      cout <<   (int)bkeyv2[i] <<" ";

     }
     cout <<endl;
    bprv  next2=deriveIndex(next,1);
     nxv=bprvEncode(next2);

    cout << "m/0/1 " << nxv <<endl;
    deriveAccumulate(acc,1);
    bpubtest=bgetPublic(next2);
    cout << "pkd: "<<IntToString(acc.pubkey.x)<<endl;
    cout << "pkp: "<<IntToString(bpubtest.pub_key.x)<<endl;

    bc="xprv9xCqEnh67D6P4onA7Z5DU1bYXfP3pXucqfQwuCZENUSRVZGAiqNXTGc7LHy4VQ3BnsLfRX3iMgUYkMRHQPxQ9mTS9qVhnuDbpLFPPyCyaoF";
    bkey2=base58decode(bc);
    bkeyv2=concatEncode(bkey2);
    for (int i=0;i<bkeyv2.size();i++)
     {
      cout <<   (int)bkeyv2[i] <<" ";

     }
     cout <<endl;
     bkey2=base58decode(nxv);
    bkeyv2=concatEncode(bkey2);
      for (int i=0;i<bkeyv2.size();i++)
     {
      cout <<   (int)bkeyv2[i] <<" ";

     }
     cout <<endl;
    vector<uint32_t>outspt;
     cout <<splitDerivationPath("m/",outspt) <<endl;
     cout <<splitDerivationPath("m/0",outspt) <<endl;
cout <<splitDerivationPath("m/0/1",outspt) <<endl;
cout << outspt[0] << "  " << outspt[1] <<endl;
cout <<splitDerivationPath("m/0'/1",outspt) <<endl;
cout <<splitDerivationPath("s/0'/1",outspt) <<endl;
cout <<splitDerivationPath("s/0//1",outspt) <<endl;

Integer sigms=genRandZ(con.q,con.prng,false);
Integer el=genRandZ(con.q,con.prng,false);
Integer priv=genRandZ(con.q,con.prng,false);
Element g=con.base;
Element h=con.group.GetCurve().ScalarMultiply(g,priv);
Element a1ss=con.group.GetCurve().ScalarMultiply(g,sigms);
Element a2ss=con.group.GetCurve().ScalarMultiply(h,el);
 Element T=con.group.GetCurve().Add(a1ss,a2ss);

Element R=con.group.GetCurve().ScalarMultiply(g,el+2*sigms);
Element S=con.group.GetCurve().ScalarMultiply(R,sigms);
sigmaLProof sgm=createSigmalProof(g,  h, T, sigms, el);
cout <<"Sigmal ver: " << verifySigmalProof(sgm,g,  h,T) <<endl;

SRLProof srl=createSRLProof(g,  h, R,S,T, sigms, el);
cout <<"SRL ver: " << verifySRLProof(srl,g,  h, R,S,T) <<endl;

auto fclj= kclient.final_output();
auto fcrj= kserver.final_output();
KeyGenClient::output fcl;
KeyGenClient::output fcr;
fcl.loadJson(fclj);
fcr.loadJson(fcrj);
//PreSignClient clientps=PreSignClient("m/0",&fcl);
//PreSignServer serverps=PreSignServer("m/0",&fcr);
//interactDriver(&clientps,&serverps);
//fujiCore paillier;
		//verifiedSideData other;
Integer nu=genRandZ(con.q,con.prng,false);
Integer rr1=genRandZ(fcl.paillier.E.pk.n,con.prng,true);
Integer ww1=paillierEncWithRand(nu,fcl.paillier.E.pk,rr1);
Element yy9=con.group.GetCurve().ScalarMultiply(R,nu);
 expPaillierProof epr=createExpPaillierProof(nu,rr1,R,yy9, ww1,fcl.paillier,fcl.other);
cout <<"EP ver: " <<verifyExpPaillierProof(epr,R, yy9,ww1,fcr.paillier,fcr.other)<<endl;

PreSignClient clientps=PreSignClient("m/0",&fcl);
PreSignServer serverps=PreSignServer("m/0",&fcr);
interactDriver(&clientps,&serverps,"Pre-sign");
    cout << "Client " << clientps.final_output().dump() <<endl;
    cout << "Server " << serverps.final_output().dump() <<endl;

auto psclj= clientps.final_output();
auto pscrj= serverps.final_output();
PreSignClient::output pscl;
PreSignClient::output pscr;
pscl.loadJson(psclj);
pscr.loadJson(pscrj);
Integer msgtos=Integer("7B49146DA0D9A233B383FBDD28131D9683DB075304E832292355EA208BA2F904h");
SignClient clientss=SignClient("m/0",&pscl,msgtos);
SignServer serverss=SignServer("m/0",&pscr,msgtos);
interactDriver(&clientss,&serverss,"Sign");
    cout << "Client sig" << clientss.final_output().dump() <<endl;
    cout << "Server sig" << serverss.final_output().dump() <<endl;
FullSignClient fclientps=FullSignClient("m/0",&fcl,msgtos);
FullSignServer fserverps=FullSignServer("m/0",&fcr,msgtos);
interactDriver(&fclientps,&fserverps,"FullSign");
    cout << "Client fsig" << fclientps.final_output().dump() <<endl;
    cout << "Server fsig" << fserverps.final_output().dump() <<endl;

rsaEllipticProof rsae=createRsaEllipticProof(pubKey,fcr.pub_mine,fcr.privKeyShare);
cout << "RSA  backup verify: " << verifyRsaEllipticProof(rsae, pubKey,fcl.pub_other,fcl.encryptedTheirBackup) <<endl;
cout << "RSA  backup verify fail: " << verifyRsaEllipticProof(rsae, pubKey,fcl.pubkey,fcl.encryptedTheirBackup) <<endl;
string tst="xprv9s21ZrQH143K3i2JoSuV2kjssUugLRfBJA7PZ7dpitiBLHDD2madGNBb7Jp5ohM1M8Q2wSSQryeLNDeFY7j15CQiDCEnKp8gp4UHLrMAKUV";
bprv testme=bprvDecode(tst);
cout << "Code:"<<endl;
cout << "depth " << testme.depth<<endl;
cout << "fp0 " << (int) testme.parent_fingerprint[0]<<endl;
cout << "chn " << testme.childNumber<<endl;
cout << "chcode " << IntToString(testme.chaincode,16) <<endl;
cout << "Priv " << IntToString(testme.priv_key,16) <<endl;
cout << "mark " << testme.privmark<<endl;
tst=bprvEncode(testme);//"xprv9s21ZrQH143K3i2JoSuV2kjssUugLRfBJA7PZ7dpitiBLHDD2madGNBb9TDj6Y53tMgXZwdYt5NHQk1p9R8xgGLka6mfwQA4Rzu6vh7aqPk";
cout<<tst<<endl;
testme=bprvDecode(tst);
cout << "Code2:"<<endl;
cout << "depth " << testme.depth<<endl;
cout << "fp0 " << (int)testme.parent_fingerprint[0]<<endl;
cout << "chn " << testme.childNumber<<endl;
cout << "chcode " << IntToString(testme.chaincode,16) <<endl;
cout << "Priv " << IntToString(testme.priv_key,16) <<endl;
cout << "mark " << testme.privmark<<endl;
}

#endif
