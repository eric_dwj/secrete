#include "clientProtocols.h"

FeldmanVssClient::FeldmanVssClient(Integer& secr, Integer& modul)
{
    this->secret=secr;
    this->modulo=modul;
    //vi_oth.identity=true;

}
 

json KeyGenClient::process_state(json& _input)
{
    baseContext con;
    Integer chlimit=Integer::Power2(256);
    SV_PROTO_START
    privatePaillier=generateInitialKeys(2048);
    ui=genRandZ(con.q,con.prng,false);
    kgenS1 outp;
    outp.paillierProofs=privatePaillier.extractPacket();
    Element y=con.group.GetCurve().ScalarMultiply(con.base,ui);
    std::pair<hashCommitment,hashDecommitment> coms=getCommitment<SHA3_256>(y,256);
    outp.com=coms.first;
    decom=coms.second;
    chaincode=rotateEntropy(chaincode,chlimit,outp.com.digest,outp.paillierProofs.En,outp.paillierProofs.b1,outp.paillierProofs.b2);

    SV_PROTO_YIELD_OUTPUT(outp)
    SV_PROTO_LOAD_MESSAGE(kgenS1,inp2);
    if(!fujiVerify(inp2.paillierProofs))
    {
        SV_EOUT("Incorrect paillier key from server")
    }
    chaincode=rotateEntropy(chaincode,chlimit,inp2.com.digest,inp2.paillierProofs.nonSquare.rseed,inp2.paillierProofs.primePower.rseed);
    theirs=inp2.com;
    #ifdef DPRINT
    cout <<"THEIRZ: " <<theirs.toJson().dump() <<endl;
    cout <<"ICOMZ: " <<inp2.com.toJson().dump() <<endl;
    #endif
    other.E=paillierPubKey(inp2.paillierProofs.En);
    other.h1=inp2.paillierProofs.b1;
    other.h2=inp2.paillierProofs.b2;
    SV_PROTO_EXECUTE(FeldmanVssClient,share,ui,con.q);
    xi=share.x;
    kgenS2 nxt;
    nxt.xProof=proveSchnorr(xi, con);
    nxt.decom=decom;
    chaincode=rotateEntropy(chaincode,chlimit,nxt.xProof.s);
    SV_PROTO_YIELD_OUTPUT(nxt)
    SV_PROTO_LOAD_MESSAGE(kgenS2Server,inp3);
    if(!verifyHashCommitment<SHA3_256>(theirs,inp3.decom))
    {
        SV_EOUT("Incorrect public key decom from server")
    }
    if(!(inp3.decom.y==share.v0_oth))
    {
        SV_EOUT("Shared server public key does not match commitment")
    }
     if(!verifySchnorr(share.X_oth,inp3.xProof,con))    
     {
          SV_EOUT("Server could not prove knowledge of xi") 
     }
      if(!verifyRsaEllipticProof(inp3.backupProof,backupPubKey,share.X_oth,inp3.encBackup))    
     { 
          SV_EOUT("Server could not prove backup encryption correctness") 
     }
    encryptedTheirBackup=inp3.encBackup;
    chaincode=rotateEntropy(chaincode,chlimit,inp3.xProof.rseed,inp3.xProof.s);
    SV_PROTO_END
}


json FeldmanVssClient::process_state(json& _input)
{
    baseContext con;
    Integer my_x=FRONTEND_PARTY;
    Integer other_x=BACKEND_PARTY;
    
    SV_PROTO_START
    my_a=genRandZ(modulo,con.prng,false);
    fVssS1 outp;
    my_sigma=(ModularMultiplication(my_a,my_x,modulo)+secret)%modulo;
    outp.sigma=(ModularMultiplication(my_a,other_x,modulo)+secret)%modulo;
    vi_oth=con.group.GetCurve().ScalarMultiply(con.base,outp.sigma);
    outp.vi=con.group.GetCurve().ScalarMultiply(con.base,my_a);
    outp.v0=con.group.GetCurve().ScalarMultiply(con.base,secret);
    SV_PROTO_YIELD_OUTPUT(outp);
    SV_PROTO_LOAD_MESSAGE(fVssS1,inp2);
    Element v1=con.group.GetCurve().ScalarMultiply(con.base,inp2.sigma);
    Element v2=con.group.GetCurve().Add(inp2.v0,con.group.GetCurve().ScalarMultiply(inp2.vi,my_x));
    if(!(v1==v2))
    {
        SV_EOUT("Feldman verification failed for client");
    }
    Element v3=con.group.GetCurve().Add(inp2.v0,con.group.GetCurve().ScalarMultiply(inp2.vi,other_x));
    vi_oth=con.group.GetCurve().Add(vi_oth,v3);
    v0_oth=inp2.v0;
    share=inp2.sigma;
    SV_PROTO_END
}

MtaProtocolClient::MtaProtocolClient(Integer& a,Integer& b,bool useRange, bool useCheck,fujiCore* core,verifiedSideData * counterparty,Element  ce)
{
    sA=a;
    sB=b;
    this->useRange=useRange;
    this->useCheck=useCheck;
    fuji=core;
    other=counterparty;
    pubkey=ce;
}

json MtaProtocolClient::process_state(json& _input)
{
   baseContext con; ///might want top make a parameter... 
   SV_PROTO_START;
   Integer r=genRandZ(fuji->E.pk.n,con.prng,true);
   cA=paillierEncWithRand(sA,fuji->E.pk,r);
   rA=r;
   mtaS1Out outp;
   outp.use_range=useRange;
   outp.cA=cA;
   #if USE_RANGE_PROOFS
   if (useRange)
      outp.mr=rangeProofMtaNiProver(*fuji,*other,cA,sA,r,con.q,con);
   #endif   
   SV_PROTO_YIELD_OUTPUT(outp);
   SV_PROTO_LOAD_MESSAGE(mtaS2Out,inp2);
   if(useCheck!=inp2.use_check||useRange!=inp2.use_range)
   {
      SV_EOUT("At stage 2, invalid version of server protocol");
   }
   #if USE_RANGE_PROOFS
   if(useRange)
   {
    if(!rangeRespProofMtaVerify(inp2.mrp,*fuji, cA ,inp2.cB, con.q,con))
    {
      SV_EOUT("Range proof from server incorrect");  
    }
   }
   #endif  
   if(useCheck)
   {
       if(!verifySchnorr(inp2.chk.B,inp2.chk.Bproof,con))
        SV_EOUT("Schnorr proof for B incorrect");
       if(!verifySchnorr(inp2.chk.Bd,inp2.chk.Bdproof,con))
        SV_EOUT("Schnorr proof for Bd incorrect");    
   }
    
   Integer alphad=paillierDec(inp2.cB,fuji->E);
   outpAlBr=alphad%(con.q);
   if(useCheck)
   {
   Element vc=con.group.GetCurve().ScalarMultiply(con.base,outpAlBr);
   Element v2=con.group.GetCurve().Add(con.group.GetCurve().ScalarMultiply(inp2.chk.B,sA),inp2.chk.Bd);
   if(!(vc==v2))
    {
     SV_EOUT("result does not match public value");    
    }
   //TODO: check for B=pubkey?
   }
   cOther=inp2.servIn.cA;
///// output other part of multiplication
   Integer qc=con.q*con.q*con.q; //K
   Integer beta=genRandZ(other->E.n-qc,con.prng,false);
   outpArBl=(-beta)%con.q;
   Integer r1=genRandZ(other->E.n,con.prng,true);
   cB= ModularMultiplication( a_exp_b_mod_c(inp2.servIn.cA,sB,other->E.nSq),paillierEncWithRand(beta,other->E,r1),other->E.nSq);
   mtaS3Out outp;
   outp.cB=cB;
   outp.use_check=useCheck;
   outp.use_range=useRange;
   #if USE_RANGE_PROOFS
   if(useRange)
   {
       outp.mrp=rangeRespProofMtaNiProver(*this->other,  inp2.servIn.cA ,cB, sB, beta,r1,con.q,con);
   }
   #endif 
   if(useCheck)
   {
       //schnorrBaseProof proveSchnorr(Integer x, baseContext& _con);
       mtaSchnorrBaseProof bp;
       bp.B=con.group.GetCurve().ScalarMultiply(con.base, sB);
       bp.Bd=con.group.GetCurve().ScalarMultiply(con.base, beta);
       bp.Bproof=proveSchnorr(sB, con);
       bp.Bdproof=proveSchnorr(beta, con);
       outp.chk=bp;
   }
   SV_PROTO_YIELD_OUTPUT(outp);
   SV_PROTO_LOAD_MESSAGE(statusMessage,stm);
   if(stm.status!="OK")
   {
       SV_EOUT("Confirmation invalid");
   }
  //bool verifySchnorr(Element& pk,schnorrBaseProof& proof,baseContext& _con);

   SV_PROTO_END;
}



json PreSignClient::process_state(json& _input)
{
    Element elid;
    if(keygenData==nullptr)
    {
        SV_EOUT("Keygen data not set!");
    }
   baseContext con; ///might want top make a parameter or global... 
   SV_PROTO_START;
   if(!splitDerivationPath(derivPath,parsedPath))
   {
       SV_EOUT("Incorrect derivation path");
   } 
    deriveAccumulator acc;
    acc.pubkey=keygenData->pubkey;
    acc.chaincode=keygenData->chaincode;
    acc.accumulated=0;
    for(auto chld :parsedPath)
    {
        if(!deriveAccumulate(acc,chld))
        {
         SV_EOUT("Incorrect derivation path indices");
        }
    }
    offset=acc.accumulated; // should be the difference...
    ki=genRandZ(con.q,con.prng,false);
    gammai=genRandZ(con.q,con.prng,false);
    Element y=con.group.GetCurve().ScalarMultiply(con.base,gammai);
    std::pair<hashCommitment,hashDecommitment> coms=getCommitment<SHA3_256>(y,256);
    //outp.com=coms.first;
    gammacom=coms.second;
    SV_PROTO_YIELD_OUTPUT(coms.first);
    SV_PROTO_LOAD_MESSAGE(hashCommitment,comex);
    theirs=comex;
    //phase 1 END
    SV_PROTO_EXECUTE(MtaProtocolClient,kgm,ki,gammai,false,false,&keygenData->paillier,&keygenData->other,elid);
    SV_PROTO_EXECUTE(MtaProtocolClient,kw,ki,keygenData->privKeyShare,false,true,&keygenData->paillier,&keygenData->other,keygenData->pub_other);
    deltai=(ModularMultiplication(ki,gammai,con.q)+kgm.AlBr+kgm.ArBl)%con.q;
    sigmai=(ModularMultiplication(ki,keygenData->privKeyShare,con.q)+kw.AlBr+kw.ArBl+ModularMultiplication(ki,offset,con.q))%con.q;
    //phase 2 end
    presignS34 step34;
    step34.di=gammacom;
    step34.deltai=deltai;
    SV_PROTO_YIELD_OUTPUT(step34);
    SV_PROTO_LOAD_MESSAGE(presignS34,st34srv);
    if(!verifyHashCommitment<SHA3_256>(theirs,st34srv.di))
    {
     SV_EOUT("Invalid gamma commitment");
    }
    Integer deltotal=(deltai+st34srv.deltai)%con.q;
    deltai=deltotal;  //overwrite? 
    revdelta=deltotal.InverseMod(con.q);
    GM=con.group.GetCurve().Add(st34srv.di.y,gammacom.y);
    Element R=con.group.GetCurve().ScalarMultiply(GM,revdelta);
    r=R.x%con.q;
    int isOdd = (R.y % Integer::Two()) == Integer::One();
	int isEqual = (R.x != r) ? 2 : 0;
	recoveryParam = isOdd | isEqual;
    if(r==0)
    {
        SV_EOUT("Seems like a bad RNG");
    }
    presignS5 sgn5;
    sgn5.Lambda=con.group.GetCurve().ScalarMultiply(GM,ki);
    lambdai=sgn5.Lambda;
    sgn5.lproof=createExpPaillierProof(ki,kw.rA,GM,sgn5.Lambda, kw.cA,keygenData->paillier,keygenData->other);
    #ifdef DPRINT
    cout << "Revdelta: " << IntToString(revdelta) <<endl;
    cout << kgm.toJson().dump() <<endl;
     cout << kw.toJson().dump() <<endl;
    #endif
    SV_PROTO_YIELD_OUTPUT(sgn5);
    SV_PROTO_LOAD_MESSAGE(presignS5,slast);
    if(!verifyExpPaillierProof(slast.lproof,GM,slast.Lambda,kw.cOther,keygenData->paillier,keygenData->other))
    {
    #ifdef DPRINT
    cout << "Could not verify Lambda"  <<endl;
    #endif 
    SV_EOUT("Lambda proof verification failed"); 
    }
   Element tlam=con.group.GetCurve().Add(slast.Lambda, lambdai);
   Element tst=con.group.GetCurve().ScalarMultiply(con.base,deltai); //deltai is total now
    
   if(!(tlam==tst))
    {
    #ifdef DPRINT
    cout << "Could not verify Lambda"  <<endl;
    #endif 
    SV_EOUT("Lambda verification failed"); 
    }
    //done
   SV_PROTO_END;
}

json SignClient::process_state(json& _input)
{
    if(presData==nullptr)
    {
        SV_EOUT("PreSign data not set!");
    }
    if(presData->derPath!=derivePath)
    {
         SV_EOUT("PreSign data path invalid for this signature");
    }
   baseContext con; ///might want top make a parameter or global... 
   SV_PROTO_START;
   recoveryParam=presData->recoveryParam;
   s=(ModularMultiplication(msg,presData->ki,con.q)+ModularMultiplication(presData->r,presData->sigmai,con.q))%con.q;
   signS1 rt;
   rt.si=s;
   SV_PROTO_YIELD_OUTPUT(rt);
   SV_PROTO_LOAD_MESSAGE(signS1,resp);
   s=s+resp.si;
   s=s%con.q;
  if(s>(con.q/2))
  {
      s=con.q-s;
      recoveryParam^=1;
  }
  Integer is=s.InverseMod(con.q);
  Integer u1=ModularMultiplication(msg,is,con.q);
  Integer u2=ModularMultiplication(presData->r,is,con.q);
  Element rq=con.group.GetCurve().Add(con.group.GetCurve().ScalarMultiply(con.base,u1),con.group.GetCurve().ScalarMultiply(presData->pubkey,u2));
  if(!(presData->r==(rq.x%con.q) ))
  {
      SV_EOUT("Resulting signature does not match");
  }

  SV_PROTO_END;
}



json FullSignClient::process_state(json& _input)
{
    Integer chlimit=Integer::Power2(256);
    SV_PROTO_START
    SV_PROTO_EXECUTE(PreSignClient,prevout,derivePath,keygenData);
    SV_PROTO_EXECUTE(SignClient,complout,derivePath,&prevout,msg);
    SV_PROTO_END
}
