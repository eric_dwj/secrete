//g++ -DNDEBUG -DIMAIN -g2 -O2 -I ../.. paillier.cpp prime.cpp  -o paillier ../../cryptopp/libcryptopp.a
#include "headers.h"



//CryptoPP::RIPEMD160 hash;


#ifdef IMAIN
//#include "correctKeyProof.h"
int main()
{
std::cout<<"Hi"<<endl;
AutoSeededRandomPool prng;
Integer a1=genRandZ(65538,prng,false);
Integer a2=genRandZ(65538,prng,false);
Integer a3=genRandZ(Integer::Two()<<20,prng,false);
vector<byte> vv=concatEncode(a1,a2,a3);
cout <<vv.size()<<endl;
std::string nan="NotNumber";
cout <<IntToString(Integer(nan.data()))<<endl; //not throwing, just setting to 0. That is fine. 
paillierKeyPair key=genPaillierKey(2048);
std::cout<<"Public key "<<IntToString(key.pk.n)<<endl;

Integer prime1= largePrimeGen(256,prng);
Integer prime2= largePrimeGen(256,prng);
std::cout<<"Prime1 "<<IntToString(prime1)<<endl;
std::cout<<"Prime2 "<<IntToString(prime2)<<endl;
std::cout<<"Sum "<<IntToString(prime1+prime2)<<endl;

Integer cipher1=paillierEnc(prime1,key.pk);
Integer cipher2=paillierEnc(prime2,key.pk);
std::cout<<"Cipher1 "<<IntToString(cipher1)<<endl;
Integer decr=paillierDec(cipher1,key);
std::cout<<"Decr prime1 "<<IntToString(decr)<<endl;
Integer homo=cipher1*cipher2;
Integer homodecr=paillierDec(homo,key);
std::cout<<"Homomorphy "<<IntToString(homodecr)<<endl;
paillierKeyPair key1=paillierKeyPair(key.sk.lambda,key.pk.n);
std::cout<<"Rec key check1 "<<IntToString(key.sk.u-key1.sk.u)<<endl;
std::cout<<"Rec key check2 "<<IntToString(key.sk.lambda-key1.sk.lambda)<<endl;
nonSquareProof bp=proveNonSquare(key);
cout<<"Verify: " << verifyNonSquare(key.pk,bp)<<endl;
cout<<"Generating initial" <<endl;
fujiCore cc=generateInitialKeys(2048);
cout<<"Generated" <<endl;
fujiCore::fmPacket packet=cc.extractPacket();
json js=packet.toJson();
cout<<"Uploaded: " << IntToString(packet.En)<<endl;
//cout <<js.dump()<<endl;
cout<<"Extracted" <<endl;
fujiCore::fmPacket recv(js);
cout<<"Downloaded: " << IntToString(recv.En)<<endl;
cout<<"Verify fujisaki: "<<fujiVerify(recv)<<endl;

cout<<"Generating secondary" <<endl;
fujiCore c2=generateInitialKeys(2048);
cout<<"Generated" <<endl;
paillierSecKey sk=c2.E.sk;
sk.p=sk.p;
Integer n=sk.p*sk.q;
sk.lambda = (sk.p-1)*(sk.q-1);//simpler formulation //LCM((p - 1) , (q - 1));
sk.u = (sk.lambda).InverseMod(n); 
n=n;
Integer sid=1;
//vector<Integer> vct= correctKeyProve(sk, n, sid);

Integer rndi=genRandZ(n,prng, true);
Integer sqr1=squareRootSafe(rndi,n,sk.p,sk.q);
Integer sqr2=squareRootSafe(n-rndi,n,sk.p,sk.q);
Integer sqr3=squareRootSafe(2*rndi % n,n,sk.p,sk.q);
Integer sqr4=squareRootSafe( (-2*rndi)%n,n,sk.p,sk.q);
cout<<"PQ8: " << IntToString( (sk.p-sk.q)%8)<<endl;
if(sqr1>=0)
{
cout<<"X: " << IntToString(sqr1)<<endl;
cout<<"Diff: " << IntToString( (sqr1*sqr1-rndi)%n)<<endl;

}
if(sqr2>=0)
{
	cout<<"-X: " << IntToString(sqr2)<<endl;
   cout<<"Diff: " << IntToString( (sqr2*sqr2+rndi)%n)<<endl;
}
if(sqr3>=0)
{
cout<<"2*X: " << IntToString(sqr3)<<endl;
cout<<"Diff: " << IntToString( (sqr3*sqr3-2*rndi)%n)<<endl;
}
if(sqr4>=0)
{
cout<<"-2*X: " << IntToString(sqr4)<<endl;
cout<<"Diff: " << IntToString( (sqr4*sqr4+2*rndi)%n)<<endl;

}
primePowerProof ppp=provePrimePowerProduct(c2.E,64);
cout << "Proof verified: " << verifyPrimePowerProduct(c2.E.pk,ppp) <<endl;
baseContext cntx;
Integer qq=Integer(2)<<255;
Integer dm=genRandZ(qq,prng,false);
Integer dr=genRandZ(c2.E.pk.n,prng,true);
Integer dc= paillierEncWithRand( dm, c2.E.pk,  dr);

mtaRangeProof rpr= rangeProofMtaNiProver(c2,  dc , dm, dr, qq,cntx);
cout << "Range proof verified: " << rangeProofMtaVerify(rpr,c2.E.pk,c2.b1,c2.b2,dc,qq)<<endl;

Integer aleph=genRandZ(cntx.q,prng,false);
Integer c1vv=paillierEnc(aleph,c2.E.pk);
Integer beta=genRandZ(c2.E.pk.n,prng,false);
Integer dr2=genRandZ(c2.E.pk.n,prng,true);
Integer cbeta=paillierEncWithRand(beta,c2.E.pk,dr2);

Integer mm=genRandZ(cntx.q,prng,false);
//mm=mm+cntx.q*cntx.q*cntx.q;
Integer c2vv=ModularMultiplication(a_exp_b_mod_c(c1vv,mm,c2.E.pk.nSq),cbeta,c2.E.pk.nSq);
verifiedSideData vsd;
vsd.E=c2.E.pk;
vsd.h1=c2.b1;
vsd.h2=c2.b2;

mtaRespRangeProof prf=rangeRespProofMtaNiProver(vsd,  c1vv ,c2vv, mm, beta,dr2, cntx.q, cntx);
mtaRespRangeProof prfrel;
json dmp=prf.toJson();
prfrel.loadJson(dmp);
cout << "Response Range proof verified: " << rangeRespProofMtaVerify(prfrel,c2,c1vv,c2vv,cntx.q,cntx)<<endl;

Integer amm=mm+cntx.q*cntx.q*cntx.q;
Integer c3vv=ModularMultiplication(a_exp_b_mod_c(c1vv,amm,c2.E.pk.nSq),cbeta,c2.E.pk.nSq);
prf=rangeRespProofMtaNiProver(vsd,  c1vv ,c3vv,amm, beta,dr2, cntx.q, cntx);
cout << "Response Range proof verified (should fail): " << rangeRespProofMtaVerify(prf,c2,c1vv,c3vv,cntx.q,cntx)<<endl;
}
#endif











