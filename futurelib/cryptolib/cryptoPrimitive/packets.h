#pragma once
#include "headers.h"
#include "cryptoUtilities.h"

struct mtaS1Out :public jsonInterface
{
Integer cA;
#ifdef USE_RANGE_PROOFS
mtaRangeProof mr;
#endif
bool use_range;
	virtual void loadJson(json & js)
    {  
        use_range=false;
        for(auto &kv :js.items())
		{
            CNV(cA); 
        #ifdef USE_RANGE_PROOFS
        if(kv.key()=="mr" )
        {
            if (kv.value().is_null())
            {
                use_range=false;
            }
            else
            {
                mr.loadJson(kv.value());
                use_range=true;
            }
        }
        #endif
        }
    }
    virtual json toJson()
    {
        json ret;
        ret["cA"]=encodeInteger(cA);
        #ifdef USE_RANGE_PROOFS
        if(use_range)
        {
            ret["mr"]=mr.toJson();
        }
        #endif  
        return ret;
    }
	virtual bool validate()
    {
        //just a basic check
               return cA!=0;
    }
};

struct mtaS2Out :public jsonInterface
{
    Integer cB;
    #ifdef USE_RANGE_PROOFS
    mtaRespRangeProof mrp;
    #endif
    mtaSchnorrBaseProof chk;
    mtaS1Out servIn; //server initialization for duality

    bool use_check;
    bool use_range;
    virtual void loadJson(json & js)
    {  
        use_check=false;
        use_range=false;
        for(auto &kv :js.items())
		{
        if(kv.key()=="use_check")
           use_check=kv.value();
           CNV(cB)
            if(kv.key()=="servIn")
            {
                servIn.loadJson(kv.value());
            }
        if(kv.key()=="chk")
        {
            if (kv.value().is_null())
            {
                use_check=false;
            }
            else
            {
                chk.loadJson(kv.value());
                use_check=true;
            }
        }
            #ifdef USE_RANGE_PROOFS
         if(kv.key()=="mrp")
         {
         if (kv.value().is_null())
            {
                use_range=false;
            }
            else
            {
                mrp.loadJson(kv.value());
                use_range=true;
            }    
         }
         #endif
        }
    }
    virtual json toJson()
    {
        json ret;
        ITST(cB);
        if(use_check)
        {
            ret["use_check"]=true;
            ret["chk"]=chk.toJson();
        }  
            #ifdef USE_RANGE_PROOFS
         if(use_range)
        {
               ret["mrp"]=mrp.toJson();
        }
        #endif
        ret["servIn"]=servIn.toJson();
        return ret;
    }
	virtual bool validate()
    {
        //just a basic check
        if(cB==0) return false;
        if(!servIn.validate()) return false;
        return true;
    }
};



struct mtaS3Out :public jsonInterface
{
    Integer cB;
        #ifdef USE_RANGE_PROOFS
    mtaRespRangeProof mrp;
    #endif
    mtaSchnorrBaseProof chk;
  
    bool use_check;
    bool use_range;
    virtual void loadJson(json & js)
    {  
        use_check=false;
        use_range=false;
        for(auto &kv :js.items())
		{
        if(kv.key()=="use_check")
           use_check=kv.value();
         CNV(cB);

        if(kv.key()=="chk")
        {
            if (kv.value().is_null())
            {
                use_check=false;
            }
            else
            {
                chk.loadJson(kv.value());
                use_check=true;
            }
        }
            #ifdef USE_RANGE_PROOFS
         if(kv.key()=="mrp")
         {
         if (kv.value().is_null())
            {
                use_range=false;
            }
            else
            {
                mrp.loadJson(kv.value());
                use_range=true;
            }    
         }
         #endif
        }
    }
    virtual json toJson()
    {
        json ret;
        ITST(cB)

        if(use_check)
        {
            ret["use_check"]=true;
            ret["chk"]=chk.toJson();
        }  
         #ifdef USE_RANGE_PROOFS
         if(use_range)
        {
               ret["mrp"]=mrp.toJson();
        }
        #endif
        return ret;
    }
	virtual bool validate()
    {
        //just a basic check
        if(cB==0) return false;
        return true;
    }
};



struct fVssS1 :public jsonInterface
{
  Integer sigma;
  Element vi;
  Element v0;
  virtual void loadJson(json & js)
    {
        for(auto &kv :js.items())
		{
            CNV(sigma)
            CNE(vi);
            CNE(v0);
        }
    }
     virtual json toJson()
     {
         json ret;
         ITSE(vi);
         ITSE(v0);
         ret["sigma"]=encodeInteger(sigma);
         return ret;
     }
     virtual bool validate()
    {
        if(sigma==0) return false;
        if (vi.identity) return false;
        if (v0.identity) return false;
        return true;
    }
};

struct kgenS1:public jsonInterface
{
    fujiCore::fmPacket paillierProofs;
    hashCommitment com;
     virtual void loadJson(json & js)
     {
        for(auto &kv :js.items())
		{
            CNO(paillierProofs);
            CNO(com);
        }  
     }
     virtual json toJson()
     {
    json ret;
    ret["paillierProofs"]=paillierProofs.toJson();
    ret["com"]=com.toJson();
    return ret;
     }
     virtual bool validate()
     {
         if(!com.validate()) return false;
         if(!paillierProofs.validate()) return false;
         return true;
     }

};


struct kgenS2:public jsonInterface
{
 schnorrBaseProof xProof;
 hashDecommitment decom;
 virtual void loadJson(json & js)
     {
        for(auto &kv :js.items())
		{
            CNO(xProof);
            CNO(decom);
        }  
     }
     virtual json toJson()
     {
    json ret;
    ret["xProof"]=xProof.toJson();
    ret["decom"]=decom.toJson();
    return ret;
     }
     virtual bool validate()
     {
         if(!decom.validate()) return false;
         if(!xProof.validate()) return false;
         return true;
     }
};

struct kgenS2Server:public jsonInterface
{
 schnorrBaseProof xProof;
 hashDecommitment decom;
 rsaEllipticProof backupProof;
 Integer encBackup;
 virtual void loadJson(json & js)
     {
        for(auto &kv :js.items())
		{
            CNO(xProof)
            CNO(decom)
            CNV(encBackup);
            CNO(backupProof);
        }  
     }
     virtual json toJson()
     {
    json ret;
    ret["xProof"]=xProof.toJson();
    ret["decom"]=decom.toJson();
      ret["backupProof"]=backupProof.toJson();
    ITST(encBackup);

    return ret;
     }
     virtual bool validate()
     {
         if(!decom.validate()) return false;
         if(!xProof.validate()) return false;
         return true;
     }
};




struct presignS34:public jsonInterface
{
    Integer deltai;
    hashDecommitment di;
    virtual void loadJson(json & js)
     {
        for(auto &kv :js.items())
		{
            CNO(di)
            CNV(deltai)
        }  
     }
     virtual json toJson()
     {
    json ret;
    ret["di"]=di.toJson();
    ITST(deltai);
    return ret;
     }
     virtual bool validate()
     {
         if(!di.validate()) return false;
         if(deltai==0) return false;//unlikely
         return true;
     }
};
struct presignS5:public jsonInterface
{
Element Lambda;
expPaillierProof lproof;
    virtual void loadJson(json & js)
     {
        for(auto &kv :js.items())
		{
           CNE(Lambda);
           CNO(lproof)
        }  
     }
     virtual json toJson()
     {
    json ret;
    ret["lproof"]=lproof.toJson();
    ITSE(Lambda)
    return ret;
     }
     virtual bool validate()
     {
         if(!lproof.validate()) return false;
         if(Lambda.identity) return false;//unlikely
         return true;
     }

};


struct signS1:jsonInterface
{
    Integer si;
	json toJson()
		{
			json ret;
			ret["si"]=encodeInteger(si);  
			return ret;
		}
		void loadJson(json& js)
		{
			 for(auto &kv :js.items())
	     {
             CNV(si)
	     }
		}
		bool validate()
		{
			if(si==0) return false;
			return true;
		}
};


struct statusMessage:jsonInterface
{
std::string status;
statusMessage(){}
statusMessage(const string& st){status=st;}
json toJson()
	{
		json ret;
		ret["status"]=status;
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
			if(kv.key()=="status") status=kv.value();
		}
	}
	bool validate()
	{
		return status.length()>0;
	}
};


struct keygenParams:jsonInterface
{
RSA::PublicKey backupPublicKey;
json toJson()
	{
		json ret;
		ITSRPK(backupPublicKey)
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
			CNRPK(backupPublicKey)
		}
	}
	bool validate()
	{
		return backupPublicKey.Validate(_globalContext.prng,3);
	}
};

