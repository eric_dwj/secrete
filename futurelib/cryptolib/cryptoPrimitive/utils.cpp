#include "headers.h"

baseContext _globalContext;


json jsonError(std::string msg)
{
	json ret;
	ret["state"]="failure";
	ret["error"]=msg;
	return ret;
}


std::string hexStr(vector<byte>&buf)
{
     std::stringstream ss;
     ss << std::hex <<std::uppercase;

     for( size_t i(0) ; i < buf.size(); ++i )
         ss << std::setw(2) << std::setfill('0') << (int)buf[i];

     return ss.str();
}



bool splitDerivationPath(const string & path,vector<uint32_t>& output)
{
  vector<string> links;
  split_string(path,links,'/');
  if(links.size()==0) return false;
  if(links[0]!="m")
  {
      #ifdef DPRINT
      cout << "Invalid first character in path: " <<links[0]<<endl;
      #endif
      return false;
  }
  output.clear();
  bool emptyfnd=false;
  for(auto it=std::next(links.begin()); it!=links.end();++it)
  {
      if(emptyfnd)
      {
    #ifdef DPRINT
      cout << "Invalid subpath: zero length"<<endl;
      #endif
       return false;

      }
   if( (*it).length()==0) 
   {
       emptyfnd=true;
       continue;
   }
   if((*it)[(*it).length()-1]=='\'')
   {
        #ifdef DPRINT
      cout << "Hardened paths not supported"<<endl;
      #endif
       return false;
   }
    try
	{
        std::size_t index = 0;
		uint32_t i = (uint32_t) std::stoul(*it,&index);
		if(index!=(*it).length())
        {
             #ifdef DPRINT
		std::cout << "Bad input: not all string parsed: " << (*it) << endl;
         #endif
         return false;
        }
        output.push_back(i);
	}
	catch (std::invalid_argument const &e)
	{
        #ifdef DPRINT
		std::cout << "Bad input: std::invalid_argument thrown" << endl;
         #endif
         return false;
	}
	catch (std::out_of_range const &e)
	{
        #ifdef DPRINT
		std::cout << "Integer overflow: std::out_of_range thrown" <<  endl;
        #endif
        return false;
	}

  }
  return true;
}


string b58 = "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";
byte odd[] = {0x03};
byte eve[] = {0x02};
Integer base58decode(string input)
{
    int parse;
    Integer key;
    for(size_t i = 0; i < input.length(); i++)
    {
        parse = 0;
        while(true)
        {
            if(input[i] == b58[parse])
            {
                key = (key + parse)*58;
                break;
            }
            else
                parse++;
        }
    }
    key = key/58;
    return key;
}
string base58encode(Integer plain, int zeros)
{
    string adr;
    Integer remainder, temp;
    while(plain != 0)
    {
        plain.Divide(remainder, temp, plain, 58);
        plain = temp;
        adr = b58[remainder.ConvertToLong()] + adr;
    }
    while(zeros)
    {
        adr = "1" + adr;
        zeros--;
    }
    return adr;
}



uint32_t bitcoin_checksum(vector<byte>& buf,int len=-1)
{
	vector<byte> outp;
    SHA256 hash1;
	if (len<=0) len=buf.size();
	if(len>(int)buf.size()) len=(int)buf.size();
	outp.resize(hash1.DigestSize());
	hash1.Update((byte*) buf.data(),len);
    hash1.Final(&outp[0]);
    SHA256 hash2;
	hash2.Update((byte*) outp.data(),outp.size());
	hash2.Final(&outp[0]);
    return *((uint32_t*)&outp[0]);
}
uint32_t readBEUint32(byte *buf)
{
  uint32_t ret=0;
  ret=ret+buf[0];
  ret<<=8;
  ret=ret+buf[1];
  ret<<=8;
  ret=ret+buf[2];
  ret<<=8;
  ret=ret+buf[3];
  return ret;
  
}
inline void serUI32Into(uint32_t num,byte * buf)
{
	buf[0]=(num>>24);
	num-=(buf[0]<<24);
    buf[1]=(num>>16);
	num-=(buf[1]<<16);
	buf[2]=(num>>8);
	num-=(buf[2]<<8);
	buf[3]=num;  
}

bprv bprvDecode(string input)
{
   Integer key=base58decode(input);
   vector<byte> buffer=forcelenEncode(key,82);
   uint32_t checksum= (uint32_t)*((uint32_t *)&buffer[78] );
   uint32_t curcheck=bitcoin_checksum(buffer,78);
   if(curcheck!=checksum)
   {
	   cout <<"Curcheck is" << curcheck <<" yet " << checksum <<endl;
   return bprv();

   }
   bprv ret;
   ret.version=(uint32_t)*((uint32_t *)&buffer[0]);
   ret.depth=(int)buffer[4];
   memcpy(ret.parent_fingerprint,&buffer[5],4);
   ret.childNumber=readBEUint32(&buffer[9]);
   ret.chaincode=Integer(&buffer[13],32);
   ret.privmark=buffer[45];
   ret.priv_key=Integer(&buffer[46],32);
   if(ret.version==0xe4ad8804)
    {
        ret.is_testnet=false;
    }
      if(ret.version==0x94833504)
    {
        ret.is_testnet=true;
    }
   //ret.version=0x1eb28804;
   return ret;
}
string bprvEncode(bprv & inp)
{
	vector<byte> buffer=vector<byte>(82);
	memset(&buffer[0],0,buffer.size());
	*((uint32_t *)&buffer[0])=inp.version;
    buffer[4]=(byte)inp.depth;
	memcpy(&buffer[5],inp.parent_fingerprint,4);
	serUI32Into(inp.childNumber,&buffer[9]);
	inp.chaincode.Encode(&buffer[13],32);
	buffer[45]=(byte)inp.privmark;
	inp.priv_key.Encode(&buffer[46],32);
	uint32_t check=bitcoin_checksum(buffer,78);
	*((uint32_t *)&buffer[78])=check;
    return base58encode(Integer(buffer.data(),buffer.size()),0);
}

vector <byte> encodePoint(Element& point)
{
	vector<byte> ret;
	ret.resize(33);
    point.x.Encode(&ret[1],32);
	if( point.y%2==0 )
	 ret[0]=2;
	 else
	 ret[0]=3;
  return ret;
}
Element decodePoint(byte * buf)
{
   // ECDSA<ECP, SHA256>::PublicKey pubKey;
   baseContext con;
	Element point;
	bool res= con.group.GetCurve().DecodePoint (point, buf, 33);
    if(!res)
	{
		return Element();
	}
	return point;
}

bpub bpubDecode(string input)
{
   Integer key=base58decode(input);
   vector<byte> buffer=forcelenEncode(key,82);
  // while(buffer.size()<82){buffer.push_back(0);}
   uint32_t checksum= (uint32_t)*((uint32_t *)&buffer[78] );
   uint32_t curcheck=bitcoin_checksum(buffer,78);
   if(curcheck!=checksum)
   {
	   cout <<"Curcheck is" << curcheck <<" yet " << checksum <<endl;
   return bpub();

   }
   bpub ret;
   ret.version=(uint32_t)*((uint32_t *)&buffer[0]);
   ret.depth=(int)buffer[4];
   memcpy(ret.parent_fingerprint,&buffer[5],4);
   ret.childNumber=readBEUint32(&buffer[9]);
   ret.chaincode=Integer(&buffer[13],32);
   
   ret.pub_key=decodePoint(&buffer[45]);
      if(ret.version==0x1eb28804)
    {
        ret.is_testnet=false;
    }
      if(ret.version==0xcf873504)
    {
        ret.is_testnet=true;
    }
   return ret;
}


string bpubEncode(bpub & inp)
{
	vector<byte> buffer=vector<byte>(82);
	memset(&buffer[0],0,buffer.size());
	*((uint32_t *)&buffer[0])=inp.version;
    buffer[4]=(byte)inp.depth;
	memcpy(&buffer[5],inp.parent_fingerprint,4);
	serUI32Into(inp.childNumber,&buffer[9]);
	inp.chaincode.Encode(&buffer[13],32);
	vector<byte> point=encodePoint(inp.pub_key);
	memcpy(&buffer[45],&point[0],33);
	uint32_t check=bitcoin_checksum(buffer,78);
	*((uint32_t *)&buffer[78])=check;
    return base58encode(Integer(buffer.data(),buffer.size()),0);
}

inline vector<byte> serUI32(uint32_t num)
{
	vector<byte> ret=vector<byte>(4);
	ret[0]=(num>>24);
	num-=(ret[0]<<24);
    ret[1]=(num>>16);
	num-=(ret[1]<<16);
	ret[2]=(num>>8);
	num-=(ret[2]<<8);
	ret[3]=num;  
	return ret;
}
vector<byte> bipFingerprint(Element& e)
{
  vector<byte> encoding=encodePoint(e);
  SHA256 prefinger;
  byte outpre[32];
  prefinger.Update(encoding.data(),encoding.size());
  prefinger.Final(outpre);
  RIPEMD160 finger;
  byte outfinger[20];
  finger.Update(outpre,32);
  finger.Final(outfinger);
  vector<byte> ret=vector<byte>(4);
  memcpy(&ret[0],outfinger,4);
  return ret;
} 

bool deriveAccumulate(deriveAccumulator & acc,uint32_t index)
{
	baseContext con;
	if(index>=(1U<<31)) return false;
	vector<byte>keybt=forcelenEncode(acc.chaincode,32);
    HMAC<SHA512> hmac(&keybt[0],32);
	vector<byte> concat=encodePoint(acc.pubkey);
    vector<byte> sr=serUI32(index);
    concat.insert( concat.end(), sr.begin(), sr.end() );
    hmac.Update(concat.data(),concat.size());
    byte outbuf[64];
    hmac.Final(outbuf);
	Integer newChain=Integer(&outbuf[32],32);
    Integer il=Integer(&outbuf[0],32);
	if (il>=con.q)
	 {
		 #ifdef DPRINT
		cout << "This should be very rare... Adding 1 to index" <<endl;
		#endif
		 return deriveAccumulate(acc,index+1); 
	 }
	Element epar=con.group.GetCurve().ScalarMultiply(con.base,il);
    if(epar.identity)
	{
		#ifdef DPRINT
		cout << "This should be very rare... Adding 1 to index" <<endl;
		#endif
	 return deriveAccumulate(acc,index+1); 	
	}
	acc.accumulated=(acc.accumulated+il)%con.q;
	acc.chaincode=newChain;
	acc.pubkey= con.group.GetCurve().Add(acc.pubkey,epar);   
	return true;
}
bprv deriveIndex(bprv& parent,uint32_t index)
{
baseContext con;
vector<byte>keybt=forcelenEncode(parent.chaincode,32);


HMAC<SHA512> hmac(&keybt[0],32);
Element epar=con.group.GetCurve().ScalarMultiply(con.base,parent.priv_key);
vector<byte> concat=encodePoint(epar);
vector<byte> sr=serUI32(index);
concat.insert( concat.end(), sr.begin(), sr.end() );
hmac.Update(concat.data(),concat.size());
byte outbuf[64];
hmac.Final(outbuf);
Integer newChain=Integer(&outbuf[32],32);
Integer il=Integer(&outbuf[0],32);
Integer newkey=(parent.priv_key+il)%con.q;
if (il>=con.q||newkey==0)
{
	return deriveIndex(parent,index+1);
}
bprv child;
child.version=parent.version;
child.depth=parent.depth+1;
child.priv_key=newkey;
child.privmark=0;
child.childNumber=index;
child.chaincode=newChain;

vector<byte> fp=bipFingerprint(epar);
memcpy(child.parent_fingerprint,&fp[0],4);
return child;
}

bpub deriveIndexPublic(bpub& parent,uint32_t index)
{
baseContext con;
vector<byte>keybt=forcelenEncode(parent.chaincode,32);

HMAC<SHA512> hmac(&keybt[0],32);
Element& epar=parent.pub_key;
vector<byte> concat=encodePoint(epar);
vector<byte> sr=serUI32(index);
concat.insert( concat.end(), sr.begin(), sr.end() );
hmac.Update(concat.data(),concat.size());
byte outbuf[64];
hmac.Final(outbuf);
Integer newChain=Integer(&outbuf[32],32);
Integer il=Integer(&outbuf[0],32);
//cout << "IL" << IntToString(il,16) << "  " <<il.BitCount() <<endl;
Element newkey=_globalContext.group.GetCurve().Add(parent.pub_key,_globalContext.group.GetCurve().ScalarMultiply(_globalContext.base,il));
if (il>=con.q||newkey.identity)
{
    cout << "This is very rare" <<endl;
	return deriveIndexPublic(parent,index+1);
}
bpub child;
child.version=parent.version;
child.depth=parent.depth+1;
child.pub_key=newkey;
child.childNumber=index;
child.chaincode=newChain;

vector<byte> fp=bipFingerprint(epar);
memcpy(child.parent_fingerprint,&fp[0],4);
return child;
}



bpub bgetPublic(bprv & priv,bool testnet)
{
	bpub ret;
    if(testnet)
    {
        ret.version=0xcf873504;
    }
    else
    {
	ret.version=0x1eb28804;
    }
	ret.depth=priv.depth;
	memcpy(ret.parent_fingerprint,priv.parent_fingerprint,4);
	ret.childNumber=priv.childNumber;
    ret.chaincode=priv.chaincode; //32 bytes
    ret.pub_key=_globalContext.group.GetCurve().ScalarMultiply(_globalContext.base,priv.priv_key);
	return ret;
}

bpub bgetRootFromPublic(Element& pubkey,Integer& chaincode,bool testnet)
{
    bpub ret;
    if(testnet)
    {
  ret.version=0xcf873504;
    }
    else {
	ret.version=0x1eb28804;}
	ret.depth=0;
	memset(ret.parent_fingerprint,0,4);
	ret.childNumber=0;
    ret.chaincode=chaincode; //32 bytes
    ret.pub_key=pubkey;
	return ret;
}

bprv bgetRootFromPrivate(Integer& privkey,Integer& chaincode,bool testnet)
{
    
    bprv ret;
    if(testnet)
    {
     	ret.version=0x94833504;   
    }
    else
    {
	ret.version=0xe4ad8804;
    }
	ret.depth=0;
	memset(ret.parent_fingerprint,0,4);
	ret.childNumber=0;
    ret.chaincode=chaincode; //32 bytes
    ret.priv_key=privkey;
    ret.privmark=0;
	return ret;
}



string TextToBinaryString(string& words) 
{
    string binaryString = "";
    for (char& _char : words) {
        binaryString += std::bitset<8>(_char).to_string();
    }
    return binaryString;
}
string IntegerToBinaryString(Integer& ist,int len)
{
    string binaryString = "";
    vector<byte> bstr;
    bstr.resize(len);
    ist.Encode(&bstr[0],len);
    for (byte& _char : bstr) {
        binaryString += std::bitset<8>(_char).to_string();
    }
    return binaryString;  
} 

json getDerivedPublicInternal(string& xpub, const string& path)
{
    bpub bp=bpubDecode(xpub);
    if(bp.version!=0x1eb28804&&bp.version!=0xcf873504)
    {
        return jsonError("invalid xpub string");
    }
    vector<uint32_t> dstr;
    if(!splitDerivationPath(path,dstr))
    {
        return jsonError("invalid derivation path");
    }
    for(auto p : dstr)
    {
        bp=deriveIndexPublic(bp,(uint32_t)p);
    }
    vector<byte> encpoint=encodePoint(bp.pub_key);
    std::string  out=hexStr(encpoint);

	//cout <<"pub here is " << xpub << endl;
	string output = "0x" + out;
	//cout <<"pub here is " << xpub << endl;
	json res = {
		{ "result", output }
	};
	return res;	
}

vector<Integer> secbitlenSplit(Integer& in,unsigned int len) 
{
 vector<Integer> ret(len);
 if(len==0) return ret;
 if(len>minRSA/securityBits) len=minRSA/securityBits;
 Integer shift=1;
 shift<<=securityBits;
 for(unsigned int i=0;i<len;i++)
 {
	 ret[len-i-1]=(in%(shift));
	 in>>=securityBits;
 }
 return ret;
}
