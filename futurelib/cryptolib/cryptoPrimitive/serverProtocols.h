#include "headers.h"
#include "packets.h"
#include "cryptoUtilities.h" 
#include "clientProtocols.h"

class MtaProtocolServer:public MtaProtocolClient
{
protected:
 public:
 MtaProtocolServer(Integer& a,Integer& b,bool useRange,bool useCheck,fujiCore * core,verifiedSideData * counterparty,Element  ce):MtaProtocolClient( a, b,useRange, useCheck, core, counterparty,ce){};
 virtual json process_state(json& _input);
};




class FeldmanVssServer:public FeldmanVssClient
{
	public:
	FeldmanVssServer(Integer& secret, Integer & modulo):FeldmanVssClient(secret,modulo){};
	virtual json process_state(json& _input);
};

class KeyGenServer:public KeyGenClient
{
	protected:
	public:
	KeyGenServer(RSA::PublicKey& pubKey):KeyGenClient(pubKey){};
	virtual json process_state(json& _input);
	//no backup for server side.

	virtual json final_output()
	{
		output ret;
		ret.privKeyShare=xi;
		ret.paillier=privatePaillier;
		ret.other=other;
		ret.pub_other=share.X_oth;
		ret.chaincode=chaincode;
		ret.encryptedOurBackup=0;
		ret.encryptedTheirBackup=0;
        ret.pub_mine=_globalContext.group.GetCurve().ScalarMultiply(_globalContext.base,xi);
		ret.pubkey=_globalContext.group.GetCurve().Add(ret.pub_other,ret.pub_mine);
		return ret.toJson();
	}

}; 

class PreSignServer:public PreSignClient
{
	protected:
	public:
	PreSignServer(const string & path,KeyGenServer::output * keygen):PreSignClient(path,keygen){}
	virtual json process_state(json& _input);
};




class SignServer:public SignClient
{
	protected:
	public:
	SignServer(const string & path,PreSignClient::output * presign,Integer & message):SignClient(path,presign,message){}
	virtual json process_state(json& _input);
};


class FullSignServer:public FullSignClient
{
	protected:
	public:
	FullSignServer(const string & path,KeyGenServer::output * keygen,Integer & message):FullSignClient(path,keygen,message){}
	virtual json process_state(json& _input);
};





