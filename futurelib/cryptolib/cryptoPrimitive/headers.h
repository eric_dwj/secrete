#pragma once
#include <iostream>
#include <iomanip>
#include <bitset>
#include <time.h>
#include <type_traits>
#include <sstream>
#include "cryptopp/nbtheory.h"
#include "cryptopp/eccrypto.h"
#include "cryptopp/osrng.h"
#include "cryptopp/oids.h"
#include "cryptopp/integer.h"
#include "cryptopp/pubkey.h"
#include "cryptopp/sha3.h"
#include "cryptopp/cryptlib.h"
#include "cryptopp/hex.h"
#include "cryptopp/base64.h"
#include "cryptopp/files.h"
#include "cryptopp/hmac.h"
#include "cryptopp/drbg.h"
#include "cryptopp/ripemd.h"
#include "cryptopp/keccak.h"
#include "cryptopp/rsa.h"
#include "json.hpp"
#include "pem.h"

using json = nlohmann::json;
//using namespace std;
using std::string;
using std::cout;
using std::endl;
using std::map;
using std::vector;
using std::pair;

#define FRONTEND_PARTY 1
#define BACKEND_PARTY 2
//#define USE_RANGE_PROOFS 1
using namespace CryptoPP;
//#pragma comment(lib, "cryptopp") 


typedef DL_GroupParameters_EC<ECP> GroupParameters;
typedef DL_GroupParameters_EC<ECP>::Element Element;
//Security bits can be changed here
#define securityBits 256
#define minRSA 2040

extern Integer base58decode(string input);
extern string base58encode(Integer plain, int zeros); 


inline string encodeInteger64(const Integer& i)
{
	std::string pre;
	Base64Encoder b(new StringSink(pre));
	i.Encode(b,i.MinEncodedSize());
	b.MessageEnd();
	//if(pre.length()<len)
	//    pre.insert(0, len - pre.length(), '0');
	return pre;
}

inline Integer decodeInteger64(const string &i)
{
	StringSource source(i, true, new Base64Decoder);


	return Integer(source, source.MaxRetrievable());
}



inline string encodeInteger(const Integer& i)
{
	//return encodeInteger64(i);
	return base58encode(i,0);
}

inline Integer decodeInteger(const string &i)
{
	// return decodeInteger64(i);
	return base58decode(i);
}

inline string encodeInteger0x(const Integer& i,unsigned int len)
{
	std::string pre=IntToString(i,16);
	if(pre.length()<len)
	    pre.insert(0, len - pre.length(), '0');
	return pre;
}

inline Integer decodeInteger0x(const string &i)
{
	//if(i.length()<2) return 0;
	std::string ref=i+"h";
	return Integer(ref.data());
}



//IntToString(nm,16)+"h"

#define CNMARK(nm) if(kv.key()==#nm ) mark++;

#define CNP(nm) if(kv.key()==#nm ) nm=kv.value();
#define CNV(nm) if(kv.key()==#nm ) \
            { std::string vl=kv.value(); nm=decodeInteger(vl);}

#define CNVx(nm) if(kv.key()==#nm ) \
            { std::string vl=kv.value(); nm=decodeInteger0x(vl);}
#define CNV64(nm) if(kv.key()==#nm ) \
            { std::string vl=kv.value(); nm=decodeInteger64(vl);}

#define CNVHex(nm) if(kv.key()==#nm ) \
            { std::string vl=kv.value(); nm=Integer(vl.data());}

#define CNE(nm) if(kv.key()==#nm ) \
            { std::string vlx=kv.value()[0];std::string vly=kv.value()[1]; nm=Element(decodeInteger(vlx),decodeInteger(vly));}

#define CNO(nm) if(kv.key()==#nm ) \
            { nm.loadJson(kv.value());}
//array
#define CNA(nm) if(kv.key()==#nm ) \
                { nm.clear(); \
                 for (auto& vl : kv.value()) {std::string vls=vl; nm.push_back(decodeInteger64(vl));   }  } ; 

#define CNRPK(nm) if(kv.key()==#nm ) \
                    { auto _fn=kv.value().find("pem"); if (_fn!=kv.value().end()){ \
                      std::string _str=_fn.value(); StringSource src(_str.data(),true); PEM_Load(src, nm); \
					} else { \
						 Integer _n=decodeInteger(kv.value()["n"]); \
					  Integer _e=decodeInteger(kv.value()["e"]);nm.Initialize(_n, _e); } } 


#define ITSE(nm) ret[#nm]=json::array(); ret[#nm].push_back(encodeInteger(nm.x)); ret[#nm].push_back(encodeInteger(nm.y));
#define ITST(nm) ret[#nm]=encodeInteger(nm);  
#define ITSTHex(nm) ret[#nm]=IntToString(nm,16)+"h";


#define ITSTx(nm,ln) ret[#nm]=encodeInteger0x(nm,ln);  
#define ITST64(nm) ret[#nm]=encodeInteger64(nm);  
#define ITSV(nm) ret[#nm]=json::array(); \
                 for(auto& v : nm) { ret[#nm].push_back(encodeInteger64(v)); }; 
                   
#define ITSP(nm) ret[#nm]=nm;

#define ITSRPK(nm) ret[#nm]=json(); \
                { std::string __otp; StringSink _sink(__otp);  PEM_Save(_sink,nm); ret[#nm]["pem"]=__otp; } 
//ret[#nm]["n"]=encodeInteger(nm.GetModulus()); ret[#nm]["e"]=encodeInteger(nm.GetPublicExponent());


class jsonInterface
{
	public:
	virtual void loadJson(json & js)=0;
    virtual json toJson()=0;
	virtual bool validate()=0;
};



struct baseContext
{
	AutoSeededRandomPool prng;
	GroupParameters group;
	Integer maxExponent;
	Integer q;
	Integer hq;
    Element base;
	baseContext()
	{
	group.Initialize(ASN1::secp256k1());
	q = group.GetGroupOrder(); 
	hq=q/2;
	maxExponent=group.GetMaxExponent();
	base = group.ExponentiateBase(1);
	}
};


#define SV_EOUT(msg) {_terminated=true; _error=true; return jsonError(msg);} 

// need to define protocol...
// will have to align it manually, I guess...
class AbstractHierarchyProtocolSide
{
	protected:
	bool _terminated=false;
	bool _error=false;
	int _step_offset=-1;
	public:
	virtual bool loadState(json& js)=0;
  	virtual bool isTerminated(){return _terminated;}
	virtual bool isError(){return _error;}
	virtual json dumpState()=0;
	virtual json process_state(json& _input)=0;
	virtual json final_output(){return json();};
	json initialState()
	{
		json ret;
		ret["result"]=json();
		ret["save"]=dumpState();
		return ret;
	}

	json vd(json& state,json& message);



};
extern json jsonError(std::string msg);




#define SV_ENDSTART_NC(_cnt_) _step_offset=_cnt_+1; \
                              };break; \
							  case _cnt_+1: {
#define SV_ENDSTART SV_ENDSTART_NC(__COUNTER__)


#define SV_ENDSTART_NC_NOBREAK(_cnt_) _step_offset=_cnt_+1; \
                              }; \
							  case _cnt_+1: {
#define SV_ENDSTART_NOBREAK SV_ENDSTART_NC_NOBREAK(__COUNTER__)



#define SV_YIELD SV_ENDSTART

#define SV_PROTO_LOAD_MESSAGE(tp, nm) tp nm;  nm.loadJson(message); \
                               if(!nm.validate())  SV_EOUT("Invalid message of type " #tp) ;
 
#define SV_PROTO_START 	\
    if(_terminated&&!_error) \
	{ json res=dumpState(); json ret; ret["complete"]=true; ret["save"]=res; ret["state"]="success";ret["result"]=json(); return ret; } \
	if(_terminated&&_error) \
	{ json res=dumpState(); json ret; ret["complete"]=false; ret["save"]=res; ret["state"]="error";ret["error"]="Already terminated with error"; return ret; } \
    json state=json({{"__step",0}}); \
	auto ss=_input.find("save"); \
	if (ss!=_input.end()) \
	{ \
		state=ss.value(); \
		if (state.size() >0) { \
		try { \
		if (!loadState(state)) \
	    { SV_EOUT("State corrupted");} \
		} \
		catch(...) \
		{ SV_EOUT("State corrupted with invalid values");}  }\
	} \
	json message; \
    auto sm=_input.find("result"); \
	if (sm!=_input.end()) \
	{ \
		message=sm.value(); \
		if(message.find("error")!=message.end() ) SV_EOUT(message.find("error").value()); \
	} \
	auto hval=state.find("__step"); \
	_step_offset=-1; \
	json _out_message; \
	if (hval==state.end()) \
	{ _step_offset=0; state=json(); state["__step"]=_step_offset; } else {\
	try \
	{ _step_offset=hval.value().get<int>(); } \
	catch(...) \
	{ SV_EOUT("Step not integer, JSON invalid");}  } \
	if(_step_offset<0) \
	{ SV_EOUT("Step not correct, JSON invalid");} \
	switch(_step_offset) \
	{ \
	case 0: \
	{

#define SV_PROTO_EXECUTE(ptype,output,...) SV_ENDSTART_NOBREAK \
                                           ptype __data=ptype(__VA_ARGS__); \
                                           json _final_message=json(); \
                                           auto _child=state.find("__cchild"); \
	                                       json _istate; \
	                                       if (_child!=state.end())	 { _istate=_child.value(); \
										   } \
										   auto _ter=_istate.find("complete"); if (_ter!=_istate.end()&&_ter.value()==true) \
										   { if(_istate.find("save")==_istate.end()||!__data.loadState(_istate["save"])) SV_EOUT("Terminal state was corrupted"); } else {\
	                                       _istate["result"]=message; \
                                           json _cres=__data.process_state(_istate); \
	                                       if (_cres.find("error")!=_cres.end()) \
	                                        { _terminated=true; _error=true; return _cres; }  \
										   if(!__data.isTerminated()||!_cres["result"].is_null()) \
										   { \
										    state=dumpState(); \
											_out_message=_cres["result"]; \
											_cres["result"]=json(); \
                                            state["__cchild"]=_cres; \
											state["__step"]=_step_offset; \
											json ret; \
											ret["complete"]=false; \
	                                        ret["state"]="success"; \
	                                        ret["save"]=state; \
											ret["result"]=_out_message; \
											return ret; \
										   } }\
										   /*cout << "CHILD "  << #ptype <<" DONE " << __data.isTerminated() << endl;*/ \
									       state.erase("__cchild"); \
										   json _out=__data.final_output(); \
	                                        if (_out.is_null()) \
	                                        { SV_EOUT("Child protocol terminated without result"); } \
	                                        output.loadJson(_out); \
											if (!output.validate()) \
											{ SV_EOUT("Child protocol returned incorrect result"); } 
										    

#define SV_PROTO_OUTPUT(blah) _out_message=blah.toJson();
#define SV_PROTO_YIELD_OUTPUT(blah) SV_PROTO_OUTPUT(blah) \
                                    SV_YIELD
#define SV_PROTO_END 	_terminated=true; \
	                    };break; \
	                    default: \
	                    SV_EOUT("Step not defined, JSON invalid? Probably"); \
	                    } \
	                    json res=dumpState(); \
	                    res["__step"]=_step_offset; \
	                    json ret; \
	                    if(_terminated) \
	                     ret["complete"]=true; \
	                    else  \
	                     ret["complete"]=false; \
	                    ret["state"]="success"; \
	                    ret["save"]=res; \
                        ret["result"]=_out_message; \
	                    return ret;

//needs hierarchy... 
/*
do_work()
{
 SV_RECEIVE(struct_with_loadJson data1)
 newstruct=process(data1)
 SV_EXECUTE(mta_protocol,newstruct,data3) <-nested protocol!
 SV_SEND(data3.pub)


}
*/




template <typename Arg, typename... Ts>
vector<byte> concatEncode(Arg& arg, Ts&... all) {
  Arg values[] = {  arg, all... };
  size_t fullsize=0;
  size_t offset=0;
  for (auto&& v : values) {
    fullsize+=v.ByteCount();
  }
  vector<byte> ret;
  ret.resize(fullsize);
  for (auto& v : values) {
    v.Encode(&ret[offset],v.ByteCount());
	offset+=v.ByteCount();
  }
  return ret;
}

template<typename T,typename IT>
IT forcelenGetlen(T& arg, IT ln) {
	return ln;
}
template<typename T,typename IT,typename... Args>
IT forcelenGetlen(T& arg, IT ln,Args... args) {
	return forcelenGetlen(arg,ln)+forcelenGetlen(args...);
}

template<typename T,typename IT>
void forceEncBuf(byte*buf,size_t offs, T& arg, IT ln) {
	arg.Encode(&buf[offs],ln);
}

template<typename T,typename IT,typename... Args>
void forceEncBuf(byte*buf,size_t offs, T& arg, IT ln,Args... args) {
	arg.Encode(&buf[offs],ln);
	offs+=ln;
	forceEncBuf(buf,offs,args...);
}


template<typename... Ts>
vector<byte> forcelenEncode(Ts... all) {
	size_t flen=(size_t)forcelenGetlen(all...);
	vector<byte> ret;
    ret.resize(flen);
    forceEncBuf(&ret[0],0,all...);
	return ret;
}





template <class Container>
void split_string(const std::string& str, Container& cont,
              char delim = ' ')
{
    std::size_t current, previous = 0;
    current = str.find(delim);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find(delim, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}




struct bprv
{
 uint32_t version;
 int depth;
 byte parent_fingerprint[4]; //hash160 of public parent
 uint32_t childNumber;
 Integer chaincode; //32 bytes
 Integer priv_key;
 int privmark;
 bool is_testnet;
 bprv()
 {
	 version=0;
	 depth=-1;
	 privmark=0;
	 is_testnet=false;
 }
};

struct bpub
{
 uint32_t version;
 int depth;
 byte parent_fingerprint[4]; //hash160 of public parent
 uint32_t childNumber;
 Integer chaincode; //32 bytes
 Element pub_key;
 bool is_testnet;
 bpub()
 {
	 version=0;
	 depth=-1;
	 is_testnet=false;
 }
};


struct deriveAccumulator
{
	Element pubkey;
	Integer chaincode;
	Integer accumulated;
};

typedef Hash_DRBG<SHA3_512,512/8,1024/8> SeededRng512;


uint32_t bitcoin_checksum(vector<byte>& buf,int len);
extern string bprvEncode(bprv & inp);
extern bprv bprvDecode(string input);
extern string bpubEncode(bpub & inp);
extern bpub bpubDecode(string input);
extern bprv deriveIndex(bprv& parent,uint32_t index);
extern bpub bgetPublic(bprv & priv,bool testnet=false);

extern bprv bgetRootFromPrivate(Integer& privkey,Integer& chaincode,bool testnet=false);
extern bpub bgetRootFromPublic(Element& pubkey,Integer& chaincode,bool testnet=false);

bpub deriveIndexPublic(bpub& parent,uint32_t index);
extern bool deriveAccumulate(deriveAccumulator & acc,uint32_t index);

extern bool splitDerivationPath(const string & path,vector<uint32_t>& output);
extern std::string hexStr(vector<byte>&buf);
extern string TextToBinaryString(string& words);
extern string IntegerToBinaryString(Integer& ist,int len);
json getDerivedPublicInternal(string& xpub, const string& path);
extern baseContext _globalContext;

template <typename Arg, typename... Ts>
Integer secbitlenCombine(Arg& arg, Ts&... all) 
{
  Arg values[] = {  all... };
  Integer ret=arg;
  for (auto&& v : values) {
   ret=(ret<<securityBits)+v;
  }
  return ret;
}
extern vector<Integer> secbitlenSplit(Integer& in,unsigned int len) ;
