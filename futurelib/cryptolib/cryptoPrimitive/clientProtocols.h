#pragma once
#include "headers.h"
#include "cryptoUtilities.h"
#include "packets.h"



class MtaProtocolClient:public AbstractHierarchyProtocolSide
{
 protected:
 Integer sA,sB;
 Integer rA;
 Integer cA,cB;
 Integer cOther;
 bool useRange;
 bool useCheck;
 fujiCore * fuji;
 verifiedSideData *other;
 Element pubkey;
 public:
  Integer outpAlBr;//A local b Remote
 Integer outpArBl;//A remote b local
 struct output : jsonInterface
 {
 Integer AlBr;//A local b Remote
 Integer ArBl;//A remote b local
 Integer rA,cA,cOther;
 void loadJson(json&js) {
		for(auto &kv :js.items())
	  {
		  CNV64(AlBr);
		  CNV64(ArBl);
		  CNV64(rA);
		  CNV64(cA);
		  CNV64(cOther);
	  }
 }
 	json toJson()
	{
		json ret;
		ITST64(ArBl)
		ITST64(AlBr)
		ITST64(rA)
		ITST64(cA)
		ITST64(cOther)
		return ret;
	}
   bool validate()
	{
		if(ArBl==0)return false;
		if(AlBr==0) return false;
		return true; 
	}
 };
 
 MtaProtocolClient(Integer& a,Integer& b,bool useRange,bool useCheck,fujiCore * core,verifiedSideData * counterparty,Element  ce);
 virtual bool loadState(json& js)
 {	
	 int mark=0;
     try
	 {
	 for(auto &kv :js.items())
	  {
		  CNV64(sA);
		  CNMARK(sA);
		  CNV64(sB);
		  CNMARK(sB);
		  CNV64(cA);
		  CNMARK(cA);
		  CNV64(rA);
		  CNMARK(rA);
		  CNV64(cB);
		  CNMARK(cB);
		  CNV64(cOther);
		  CNMARK(cOther);
		  CNV64(outpAlBr);
		  CNMARK(outpAlBr);
		  CNV64(outpArBl);
		  CNMARK(outpArBl);
          if (kv.key()=="_terminated") _terminated=kv.value();
		  if (kv.key()=="_error") _error=kv.value();
		  CNMARK(_terminated);
		  CNMARK(_error);
	  }
	 }
	 catch(...)
	 {
		 return false;
	 }
	  if(mark!=10) return false;
	 return true;
 }
 virtual json dumpState()
 {
	 	json ret;
        ITST64(sA);
		ITST64(sB);
		ITST64(cA);
		ITST64(cB);
		ITST64(cOther);
		ITST64(rA);
		ITST64(outpAlBr);
		ITST64(outpArBl);
		ret["_terminated"]=_terminated; 
		ret["_error"]=_error; 
		return ret;
 }
 virtual json process_state(json& _input);
 virtual json final_output()
 {
	  if(!_terminated) return json();
	  if(outpAlBr==0||outpArBl==0)
	  {
		  return json();//did not end correctly
	  }
	  output out;
	  out.AlBr=outpAlBr;
	  out.ArBl=outpArBl;  
	  out.rA=rA;
	  out.cA=cA;
	  out.cOther=cOther;
	  return out.toJson();
 };
};

//two-party feldman VSS, summed
class FeldmanVssClient:public AbstractHierarchyProtocolSide
{
 protected:
 Integer secret;
 Integer modulo;
 Integer my_a;
 public:
  Integer my_sigma;
  Element vi_oth;
  Element v0_oth;
  Integer share;
 struct output : jsonInterface
 {
 Element X_oth; 
 Element v0_oth; 
 Integer x;
 void loadJson(json&js) {
		for(auto &kv :js.items())
	  {
		  CNE(v0_oth);
		  CNE(X_oth);
		  CNV(x);
		  
	  }
 }
 	json toJson()
	{
		json ret;
		ITSE(X_oth)
		ITSE(v0_oth)
		ITST(x)
		return ret;
	}
   bool validate()
	{
		if(v0_oth.identity)return false;
		if(x==0) return false;
		return true; 
	}
 };
 
 FeldmanVssClient(Integer& secret, Integer& modulo);
 virtual bool loadState(json& js)
 {	
	 int mark=0;
     try
	 {
	 for(auto &kv :js.items())
	  {
		  CNV(secret);
		  CNMARK(secret);
		  CNV(modulo);
		  CNMARK(modulo);
		  CNV(my_a);
		  CNMARK(my_a);
		  CNE(vi_oth);
		  CNMARK(vi_oth);
		  CNE(v0_oth);
		  CNMARK(v0_oth);
		  CNV(share);
		  CNMARK(share);
		  CNV(my_sigma);
		  CNMARK(my_sigma);
          if (kv.key()=="_terminated") _terminated=kv.value();
		  if (kv.key()=="_error") _error=kv.value();
		  CNMARK(_terminated);
		  CNMARK(_error);
	  }
	 }
	 catch(...)
	 {
		 return false;
	 }
	  if(mark!=9) return false;
	 return true;
 }
 virtual json dumpState()
 {
	 	json ret;
        ITST(secret);
		ITST(modulo);
		ITST(my_a);
		ITSE(vi_oth);
		ITSE(v0_oth);
		ITST(share);
		ITST(my_sigma);
		ret["_terminated"]=_terminated; 
		ret["_error"]=_error; 
		return ret;
 }
 virtual json process_state(json& _input);
 virtual json final_output()
 {
	  if(!_terminated) return json();
	  if(vi_oth.identity||share==0)
	  {
		  return json();//did not end correctly
	  }
	  output out;
	  out.X_oth=vi_oth;
	  out.v0_oth=v0_oth;
	  out.x=(share+my_sigma)%modulo;  
	  return out.toJson();
 };
};


class KeyGenClient:public AbstractHierarchyProtocolSide
{
	protected:
	Integer ui;
	Integer xi;// priv key share
	hashDecommitment decom;
	hashCommitment theirs;
    FeldmanVssClient::output  share;
	fujiCore privatePaillier;
	verifiedSideData other;
    Integer chaincode; //to minimize additional communication, generate chaincode from proofs we already send
    RSA::PublicKey backupPubKey;
    Integer encryptedTheirBackup;
	public:
	KeyGenClient(RSA::PublicKey& pubKey)
	{
chaincode=0;
backupPubKey=pubKey;
	}
	virtual bool loadState(json& js)
	{
	  int mark=0;
	  for(auto &kv :js.items())
	  {
		CNV(ui);
		CNMARK(ui);
		CNV(xi);
		CNMARK(xi);
        CNO(decom);
		CNMARK(decom);
		CNO(theirs);
		CNMARK(theirs);
        CNO(share);
		CNMARK(share);
        CNO(privatePaillier);
		CNMARK(privatePaillier);
		CNO(other);
		CNMARK(other);
		CNV(chaincode);
		CNMARK(chaincode);
        CNV(encryptedTheirBackup);
		CNMARK(encryptedTheirBackup);

		if (kv.key()=="_terminated") _terminated=kv.value();
		if (kv.key()=="_error") _error=kv.value();
		CNMARK(_terminated);
		CNMARK(_error);
	  }
	  if(mark!=11) return false;
     return true;
	}
	 virtual json dumpState()
 {
	 	json ret;
        ITST(ui);
		ITST(xi);
        ITST(encryptedTheirBackup);
		ITST(chaincode);
		ret["decom"]=decom.toJson();
		ret["theirs"]=theirs.toJson();

		ret["share"]=share.toJson();
		ret["privatePaillier"]=privatePaillier.toJson();
		ret["other"]=other.toJson();
		ret["_terminated"]=_terminated; 
		ret["_error"]=_error; 
		return ret;
 }
	virtual json process_state(json& _input);
	struct output :jsonInterface
	{
		Integer privKeyShare;
		fujiCore paillier;
		verifiedSideData other;
		Element pub_mine;
		Element pub_other;
        Element pubkey; 
		Integer chaincode;
		Integer encryptedTheirBackup;
        Integer encryptedOurBackup;
		json toJson()
		{
			json ret;
			ITST(privKeyShare);
			ITSE(pub_mine);
			ITSE(pub_other);
			ITSE(pubkey);
			ITSE(pubkey);
			ITST(chaincode);
            ITST(encryptedTheirBackup);
            ITST(encryptedOurBackup);

			ret["paillier"]=paillier.toJson();
			ret["other"]=other.toJson();
			return ret;
		}
		void loadJson(json& js)
		{
			 for(auto &kv :js.items())
	         {
				CNE(pub_mine);
				CNE(pub_other);
				CNE(pubkey);
				CNV(chaincode);
				CNV(privKeyShare);
				CNO(paillier)
				CNO(other)
                CNV(encryptedTheirBackup);
                CNV(encryptedOurBackup);
			 }
		}
		bool validate()
		{
            if(privKeyShare==0) return false;
			if(chaincode==0) return false;
			if(!paillier.validate()) return false;
			if(!other.validate()) return false;
			if(pubkey.identity) return false;
			return true;
		}
	};
	virtual json final_output()
	{
		output ret;
		ret.privKeyShare=xi;
		ret.paillier=privatePaillier;
		ret.other=other;
		ret.pub_other=share.X_oth;
		ret.chaincode=chaincode;
        ret.encryptedOurBackup=backupPubKey.ApplyFunction(secbitlenCombine(ret.privKeyShare,ret.chaincode) );
        ret.encryptedTheirBackup=encryptedTheirBackup;
        ret.pub_mine=_globalContext.group.GetCurve().ScalarMultiply(_globalContext.base,xi);
		ret.pubkey=_globalContext.group.GetCurve().Add(ret.pub_other,ret.pub_mine);
		return ret.toJson();
	}
};


class PreSignClient:public AbstractHierarchyProtocolSide
{
   protected:
   string derivPath; //may be parsed every time XD
   KeyGenClient::output *keygenData;
   vector<uint32_t> parsedPath;
   Integer offset; //offset from original private key; calculated from chaincode;
   Integer ki,gammai;
   hashDecommitment gammacom;
   hashCommitment theirs;
   MtaProtocolClient::output kgm;
   MtaProtocolClient::output kw;
   Integer deltai;
   Integer sigmai;
   Integer revdelta;
   Integer r;
   int recoveryParam=-1;
   Element GM;
   Element lambdai;
   public:
   PreSignClient(const string & path,KeyGenClient::output * keygen)
   {
 derivPath=path;
 keygenData=keygen;
 recoveryParam=-1;
   }
   virtual json process_state(json& _input);
   virtual json dumpState()
   {
	    json ret;
        ITST(offset);
		ITST(ki);
		ITST(gammai);
		ITST(deltai);
		ITST(sigmai);
		ITST(revdelta);
		ITST(r);
		ITSE(lambdai);
		ITSE(GM);
		ret["recoveryParam"]=recoveryParam;
		ret["kgm"]=kgm.toJson(); 
		ret["gammacom"]=gammacom.toJson(); 
	    ret["theirs"]=theirs.toJson();
		ret["kw"]=kw.toJson(); 
		ret["_terminated"]=_terminated; 
		ret["_error"]=_error; 
		return ret;
   };
  virtual bool loadState(json& js)
	{
	  int mark=0;
	  for(auto &kv :js.items())
	  {
		CNV(deltai);
		CNMARK(deltai);
		CNV(sigmai);
		CNMARK(sigmai);
		CNV(offset);
		CNMARK(offset);
		CNE(lambdai);
		CNMARK(lambdai);
		CNE(GM);
		CNMARK(GM);
		CNV(revdelta);
		CNMARK(revdelta);
		CNV(ki);
		CNMARK(ki);
		CNV(gammai);
		CNMARK(gammai);
		CNV(r);
		CNMARK(r);
		CNO(kgm);
		CNMARK(kgm);
	    CNO(kw);
		CNMARK(kw);
        CNO(theirs);
		CNMARK(theirs);
		if (kv.key()=="recoveryParam") recoveryParam=kv.value();
        CNMARK(recoveryParam)
		CNO(gammacom);
		CNMARK(gammacom);

		if (kv.key()=="_terminated") _terminated=kv.value();
		if (kv.key()=="_error") _error=kv.value();
		CNMARK(_terminated);
		CNMARK(_error);
	  }
	  if(mark!=16) return false;
     return true;
	}
	struct output:jsonInterface
	{
     string derPath;
     Element pubkey;// after offset
	 Integer sigmai;
	 Integer r;
	 Integer ki;
	 int recoveryParam=-1;
	 	json toJson()
		{
			json ret;
			ITST(sigmai);
			ITST(r);
			ITST(ki);
			ITSE(pubkey);
			ret["derPath"]=derPath;
			ret["recoveryParam"]=recoveryParam;
			return ret;
		};
		void loadJson(json& js)
		{
			for(auto &kv :js.items())
	         {
				CNE(pubkey);
				CNV(sigmai);
				CNV(r);
				CNV(ki)
				if(kv.key()=="derPath") derPath=kv.value();
				if(kv.key()=="recoveryParam") recoveryParam=kv.value();
			 }
		}
		bool validate()
		{
            if(r==0) return false;
			if(derPath.empty()) return false;
			if (pubkey.identity) return false;
			return true;
		}
	};
	virtual json final_output()
	{
		if(keygenData==nullptr) return json();
		if(!_terminated) return json();
		if(_error) return json();
		output ret;
		baseContext con;
		ret.pubkey=con.group.GetCurve().Add(keygenData->pubkey,con.group.GetCurve().ScalarMultiply(con.base,offset));
		ret.derPath=derivPath;
		ret.sigmai=sigmai;
		ret.recoveryParam=recoveryParam;
		ret.r=r;
		ret.ki=ki;
		return ret.toJson();
	}
};


class SignClient:public AbstractHierarchyProtocolSide
{
	protected:
	string derivePath;
    PreSignClient::output* presData;
	Integer msg;
	Integer s;
    int recoveryParam=0;
	public:
    SignClient(const string & path,PreSignClient::output * presign,Integer & message)
	{
     derivePath=path;
     msg=message; 
	 presData=presign;
	 recoveryParam=-1;
	}
	virtual json process_state(json& _input);
   virtual json dumpState()
   {
	    json ret;
		ret["derivePath"]=derivePath;
		ret["recoveryParam"]=recoveryParam;
        ITST(msg);
		ITST(s);
		ret["_terminated"]=_terminated; 
		ret["_error"]=_error; 
		return ret;
   };
  virtual bool loadState(json& js)
	{
	  int mark=0;
	  for(auto &kv :js.items())
	  {
		if (kv.key()=="derivePath") derivePath=kv.value();
		if (kv.key()=="recoveryParam") recoveryParam=kv.value();
		CNMARK(derivePath);
		CNMARK(recoveryParam);
		CNV(msg);
		CNMARK(msg);
		CNV(s);
		CNMARK(s);
		if (kv.key()=="_terminated") _terminated=kv.value();
		if (kv.key()=="_error") _error=kv.value();
		CNMARK(_terminated);
		CNMARK(_error);
	  }
	  if(mark!=6) return false;
     return true;
	}
	struct output:jsonInterface
	{
		int recoveryParam=0;
		Integer r;
		Integer s;
		json toJson()
		{
			json ret;
			ITSTx(r,64);
			ITSTx(s,64);
			ret["recoveryParam"]=recoveryParam;
			return ret;
		}
		void loadJson(json& js)
		{
			 for(auto &kv :js.items())
	     {
			 CNVx(s);
			 CNVx(r);
			 if (kv.key()=="recoveryParam") recoveryParam=kv.value();
	     }
		}
		bool validate()
		{
			if(r==0) return false;
			if(s==0) return false;
			return true;
		}
	};
	json final_output()
	{
		if(!_terminated) return json();
		if(_error) return json();
		if(presData==nullptr) return json();
		output ret;
		ret.recoveryParam=recoveryParam;
		ret.r=presData->r;
		ret.s=s;
		return ret.toJson();
	}
  
};


struct preSignParams:jsonInterface
{
 string derivationPath;
 KeyGenClient::output key;   
json toJson()
	{
		json ret;
		ITSP(derivationPath)
        ret["key"]=key.toJson();
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
			CNP(derivationPath)
            CNO(key)
		}
	}
	bool validate()
	{
		if(derivationPath.empty()) return false;
        if(!key.validate()) return false;
        return true;
	}
};

struct signParams:jsonInterface
{
	string derivationPath;
	PreSignClient::output preSignData;
	Integer message;

 json toJson()
	{
		json ret;
		ITSP(derivationPath)
        ret["preSignData"]=preSignData.toJson();
		ITSTHex(message);
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
			CNP(derivationPath)
            CNO(preSignData)
			CNVHex(message)
		}
	}
	bool validate()
	{
		if(derivationPath.empty()) return false;
        if(!preSignData.validate()) return false;
        return true;
	}
};

struct fullSignParams:jsonInterface
{
	string derivationPath;
	KeyGenClient::output key; 
	Integer message;
json toJson()
	{
		json ret;
		ITSP(derivationPath)
        ret["key"]=key.toJson();
		ITSTHex(message);
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
			CNP(derivationPath)
            CNO(key)
			CNVHex(message)
		}
	}
	bool validate()
	{
		if(derivationPath.empty()) return false;
        if(!key.validate()) return false;
        return true;
	}

};






class FullSignClient:public AbstractHierarchyProtocolSide
{
	protected:
	string derivePath;
    PreSignClient::output prevout;
	SignClient::output complout;
	Integer msg;
	Integer s;
    int recoveryParam=-1;
	KeyGenClient::output * keygenData;
	public:
    FullSignClient(const string & path,KeyGenClient::output * keygen,Integer & message)
	{
     derivePath=path;
     msg=message; 
	 recoveryParam=-1;
	 keygenData=keygen;
	}
	virtual json process_state(json& _input);
   virtual json dumpState()
   {
	    json ret;
		ret["derivePath"]=derivePath;
		ret["recoveryParam"]=recoveryParam;
		ret["prevout"]=prevout.toJson();
		ret["complout"]=complout.toJson();
        ITST(msg);
		ITST(s);
		ret["_terminated"]=_terminated; 
		ret["_error"]=_error; 
		return ret;
   };
  virtual bool loadState(json& js)
	{
	  int mark=0;
	  for(auto &kv :js.items())
	  {
		if (kv.key()=="derivePath") derivePath=kv.value();
		if (kv.key()=="recoveryParam") recoveryParam=kv.value();
		CNMARK(derivePath);
		CNMARK(recoveryParam);
		CNV(msg);
		CNMARK(msg);
		CNV(s);
		CNMARK(s);
		CNO(prevout);
		CNMARK(prevout);
		CNO(complout);
		CNMARK(complout);
		if (kv.key()=="_terminated") _terminated=kv.value();
		if (kv.key()=="_error") _error=kv.value();
		CNMARK(_terminated);
		CNMARK(_error);
	  }
	  if(mark!=8) return false;
     return true;
	}
	
	json final_output()
	{
		if(!_terminated) return json();
		if(_error) return json();
		return complout.toJson();
	}
  
};

