#include "cryptoUtilities.h"

/*Generate a large, safe  prime. secBits is the number of bits*/
Integer largePrimeGen(int secBits,RandomNumberGenerator& prng){
	PrimeAndGenerator gen(1,prng,secBits);
	gen.SubPrime();
	return gen.Prime();
}

FujisakiPrep fujiGen(int secBits,RandomNumberGenerator & prng,int rescard=3)
{
	//PrimeAndGenerator gen(1,prng,secBits);
	 //   AutoSeededRandomPool prng;
	   
	    AlgorithmParameters params = MakeParameters("BitLength", secBits)
	                                               ("RandomNumberType", Integer::PRIME)
												   ("EquivalentTo", rescard)
												   ("Mod",8);
		     Integer x=Integer::Power2(secBits-1);
			  try { x.GenerateRandom(prng, params); }
		     catch (...) { x = 1; }

		//	const Integer max=Integer::Power2(secBits);
			/* Integer e;
			 while( e=x%8,(e==1||e==discard))
			 {
		    
			 }*/
			 //x=
			// FirstPrime(x, max, rescard, 8, nullptr);
		
	FujisakiPrep ret;
	ret.p=x;//gen.Prime();
	ret.subP=(x-1)/2;//gen.SubPrime();
	Integer h(prng,Integer::Two(), ret.p - Integer::One());
	Integer g=a_exp_b_mod_c(h,2,ret.p);
	while(g==1)
	{
		h.Randomize(prng,Integer::Two(), ret.p - Integer::One());
	 g=a_exp_b_mod_c(h,2,ret.p);
		
	}
	ret.g=g;
	return ret;
}



/* Generates nonzero Zq up to limit, optionally checks for GCD*/
Integer genRandZ(const Integer & limit,RandomNumberGenerator & prng,bool ast)
{
    Integer rand(prng, Integer::One(), limit - Integer::One());
	int cnt=0;
	while (ast&&GCD(rand, limit) != Integer::One()&&cnt<100)
	{
     rand.Randomize(prng,Integer::One(), limit - Integer::One());
	 cnt++;
	
	}
	 
	if (cnt==100)
   {
    throw Integer::RandomNumberNotFound(); // must be *very* unlucky
   }
   return rand;
}


paillierKeyPair::paillierKeyPair(Integer n, Integer p, Integer q):pk(n)
{

    this->sk.lambda = (p-1)*(q-1);//simpler formulation //LCM((p - 1) , (q - 1));
	this->sk.u = (this->sk.lambda).InverseMod(n); 
	this->sk.p = p;
	this->sk.q = q;

}



paillierKeyPair genPaillierKey(int secBits){
	AutoSeededRandomPool prng;
	Integer p;
	Integer q;

	Integer pk;
	paillierSecKey sk;
	Integer n=Integer::Zero();
	while(n.BitCount()<(unsigned int)secBits)
	{
	p= largePrimeGen(secBits / 2,prng);
	q= largePrimeGen(secBits / 2,prng);
	if((p-q)%8==0){n=0;continue;} //important for zero knowledge
	n = p * q;
//cout<<"Gen"<<endl;
	}
	return paillierKeyPair(n,p,q) ;
	/*Since p q have the same length, we can use a simpler variant of the key
	where g = n + 1, lambda = (p - 1 )(q - 1), u = inverse of lambda mod n 	*/

}

fujiCore::fujiCore(FujisakiPrep& pp, FujisakiPrep& qq,Integer n):E(n,pp.p,qq.p)
{
 gsp=pp.g;
 gsq=qq.g;
 b0=CRT(pp.g,pp.p,qq.g,qq.p,pp.p.InverseMod(qq.p));
 Integer subN=pp.subP*qq.subP;
//cout << "inv0: "<< IntToString(a_exp_b_mod_c(b0,subn,n))<<endl;

  subn=subN;
 AutoSeededRandomPool prng;
 alpha=genRandZ(subn,prng,true);
 beta=genRandZ(subn,prng,true); 
 b1=a_exp_b_mod_c(b0,alpha,n);
 b2=a_exp_b_mod_c(b0,beta,n);
 //Integer ab=(beta*alpha.InverseMod(subn))%subn;
 //cout << "inv: "<<IntToString(ab)<<endl;
 //cout << "wah: "<<IntToString(a_exp_b_mod_c(b1,beta,n)) << endl;
 //cout << "haw: "<<IntToString(a_exp_b_mod_c(b2,alpha,n)) << endl;
 //cout << "wahs: "<<IntToString(a_exp_b_mod_c(b1,ab,n)) << endl;
 // cout << "wahs: "<<IntToString(b2) << endl;
}

fujiCore generateInitialKeys(int secBits)
{
	
    AutoSeededRandomPool prng;
FujisakiPrep pp,qq;
	Integer pk;
	paillierSecKey sk;
	Integer n=Integer::Zero();
	while(n.BitCount()<(unsigned int)secBits)
	{
//	pp= fujiGen(secBits / 2,prng);
	//qq= fujiGen(secBits / 2,prng,(pp.p%8));
	pp= fujiGen(secBits / 2,prng,3);
	qq= fujiGen(secBits / 2,prng,5);
	if((pp.p-qq.p)%8==0){n=0;continue;}
	
	n = pp.p * qq.p;
	//break;
//cout<<"Gen"<<endl;
	}
   	FujisakiPrep tmp;
	if (pp.p>qq.p) //canonicalize
	   {
		 tmp=pp;
		 pp=qq;
		 qq=tmp;  
	   }
	return fujiCore(pp,qq,n);
}



/*Paillier encryption with chosen randomness*/
Integer paillierEncWithRand(Integer& msg, paillierPubKey& pk, Integer& r)
{
	Integer c1 = a_exp_b_mod_c(pk.g, msg, pk.nSq);
	Integer c2 = a_exp_b_mod_c(r, pk.n , pk.nSq);
	Integer c = (c1 * c2) % pk.nSq;
	return c;
}

/*Pallier encryption*/
Integer paillierEnc(Integer& msg, paillierPubKey& pk)
{
	AutoSeededRandomPool prng;
	Integer r=genRandZ(pk.n,prng,true);
	return paillierEncWithRand( msg,  pk,  r);
}




/*Paillier Decryption*/
Integer paillierDec(Integer& c, paillierKeyPair key)
{
	Integer n = key.pk.n;
	Integer m1 = a_exp_b_mod_c(c, key.sk.lambda, n * n);
	Integer m2 = (m1 - 1) / n;
	Integer m = (m2 * (key.sk.u)) % n;
//	cout << "plaintext is :" << m << endl;
	return m;
}

paillierKeyPair::paillierKeyPair(paillierSecKey&& sk):pk(sk.p*sk.q)
{
this->sk=sk;
}

//might swap p and q
paillierKeyPair::paillierKeyPair(Integer totient,Integer n):pk(n)
{
  Integer p_plus_q = n - totient + 1;
  Integer det=p_plus_q * p_plus_q - n * 4;
  Integer p_minus_q=det.SquareRoot();
  Integer q = (p_plus_q - p_minus_q)/2;
  Integer p = p_plus_q - q;

  this->sk.lambda = (p-1)*(q-1);//simpler formulation //LCM((p - 1) , (q - 1));
  this->sk.u = (this->sk.lambda).InverseMod(n); 
  this->sk.p = p;
  this->sk.q = q;
  
}

fujiCore::fmPacket fujiCore::extractPacket()
{
fujiCore::fmPacket ret;
AutoSeededRandomPool prng;
ret.En=E.pk.n;
ret.b1=b1;
ret.b2=b2;
Integer rnd=genRandZ(subn,prng,false);
ret.t=a_exp_b_mod_c(b1,rnd,ret.En);
vector<byte> data=concatEncode(ret.En,ret.b1,ret.b2);
while(data.size()<64) data.push_back(125);
vector<byte> nonce=concatEncode(ret.t);
SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
Integer challenge=genRandZ(ret.En,cgen,false);
//cout <<"Chall:" <<IntToString(challenge)<<endl;
Integer ab=(beta*alpha.InverseMod(subn))%subn;
ret.s=(rnd+challenge*(ab));
ret.nonSquare=proveNonSquare(E);
ret.primePower=provePrimePowerProduct(E,64);
return ret;
}

bool fujiVerify(fujiCore::fmPacket & fuji)
{
paillierPubKey	key(fuji.En);
bool nsp=verifyNonSquare(key,fuji.nonSquare);
bool ppp=verifyPrimePowerProduct(key,fuji.primePower);
//cout <<"Vf:" <<nsp;
vector<byte> data=concatEncode(fuji.En,fuji.b1,fuji.b2);
while(data.size()<64) data.push_back(125);
vector<byte> nonce=concatEncode(fuji.t);
SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
Integer challenge=genRandZ(fuji.En,cgen,false);
//cout <<"Chall:" <<IntToString(challenge)<<endl;

Integer v1=a_exp_b_mod_c(fuji.b1,fuji.s,fuji.En);
Integer v2=(fuji.t*a_exp_b_mod_c(fuji.b2,challenge,fuji.En))%fuji.En;
return (v1==v2)&&nsp&&ppp;

}



//https://cseweb.ucsd.edu/~daniele/papers/GMR.pdf 3.1 
nonSquareProof proveNonSquare(paillierKeyPair & key)
{
	AutoSeededRandomPool prng;
    Integer rand(prng,  256);
	byte buf[256];
	rand.Encode(buf,256);
	vector<byte> dec=concatEncode(key.pk.n);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),buf,256);
    Integer x=genRandZ(key.pk.n,gen,true);
    Integer m=key.pk.n.InverseMod(key.sk.lambda);

   nonSquareProof ret;
   ret.rseed=rand;
   ret.y=a_exp_b_mod_c(x,m,key.pk.n);
   return ret;   
}

bool verifyNonSquare(paillierPubKey& key, nonSquareProof& bp)
{
	byte buf[256];
	bp.rseed.Encode(buf,256);
	vector<byte> dec=concatEncode(key.n);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),buf,256);
	Integer x=genRandZ(key.n,gen,true);
	Integer ver=a_exp_b_mod_c(bp.y,key.n,key.n);
    return (ver==x);
}

sigmaLProof createSigmalProof(Element& g, Element& h,Element& T,Integer& sgm, Integer & el)
{
	AutoSeededRandomPool prng;
	sigmaLProof ret;
	Integer a=genRandZ(_globalContext.q,prng,false);
	Integer b=genRandZ(_globalContext.q,prng,false);
	Element a1=_globalContext.group.GetCurve().ScalarMultiply(g,a);
	Element a2=_globalContext.group.GetCurve().ScalarMultiply(h,b);
    ret.alpha=_globalContext.group.GetCurve().Add(a1,a2);

	Integer rand(prng,  256);
	vector<byte> nonce=concatEncode(rand);
	vector<byte> dec=concatEncode(T.x,T.y,ret.alpha.y,ret.alpha.x);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),nonce.data(),nonce.size());
    Integer c=genRandZ(_globalContext.q,gen,true);
	ret.rseed=rand;
	ret.t=(a+ModularMultiplication(c,sgm,_globalContext.q))%_globalContext.q;
	ret.u=(b+ModularMultiplication(c,el,_globalContext.q))%_globalContext.q;
	return ret;
}
bool verifySigmalProof(sigmaLProof& proof,Element& g, Element& h,Element& T)
{
	vector<byte> nonce=concatEncode(proof.rseed);
	vector<byte> dec=concatEncode(T.x,T.y,proof.alpha.y,proof.alpha.x);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),nonce.data(),nonce.size());
    Integer c=genRandZ(_globalContext.q,gen,true);
	Element a1=_globalContext.group.GetCurve().ScalarMultiply(g,proof.t);
	Element a2=_globalContext.group.GetCurve().ScalarMultiply(h,proof.u);
	Element ver1=_globalContext.group.GetCurve().Add(a1,a2);
	Element ver2=_globalContext.group.GetCurve().Add(proof.alpha,_globalContext.group.GetCurve().ScalarMultiply(T,c));
	return (ver1==ver2);
}


SRLProof createSRLProof(Element& g, Element& h,Element& R,Element& S,Element& T,Integer& sgm, Integer & el)
{
	AutoSeededRandomPool prng;
	SRLProof ret;
	Integer a=genRandZ(_globalContext.q,prng,false);
	Integer b=genRandZ(_globalContext.q,prng,false);
	Element a1=_globalContext.group.GetCurve().ScalarMultiply(g,a);
	Element a2=_globalContext.group.GetCurve().ScalarMultiply(h,b);
    ret.beta=_globalContext.group.GetCurve().Add(a1,a2);
    ret.alpha=_globalContext.group.GetCurve().ScalarMultiply(R,a);
	
	Integer rand(prng,  256);
	vector<byte> nonce=concatEncode(rand);
	vector<byte> dec=concatEncode(T.x,T.y,ret.alpha.y,ret.alpha.x,ret.beta.x,ret.beta.y);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),nonce.data(),nonce.size());
    Integer c=genRandZ(_globalContext.q,gen,true);
	ret.rseed=rand;
	ret.t=(a+ModularMultiplication(c,sgm,_globalContext.q))%_globalContext.q;
	ret.u=(b+ModularMultiplication(c,el,_globalContext.q))%_globalContext.q;
	return ret;
}

bool verifySRLProof(SRLProof& proof,Element& g, Element& h,Element& R,Element& S,Element& T)
{
	vector<byte> nonce=concatEncode(proof.rseed);
	vector<byte> dec=concatEncode(T.x,T.y,proof.alpha.y,proof.alpha.x,proof.beta.x,proof.beta.y);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),nonce.data(),nonce.size());
    Integer c=genRandZ(_globalContext.q,gen,true);
	Element a1=_globalContext.group.GetCurve().ScalarMultiply(g,proof.t);
	Element a2=_globalContext.group.GetCurve().ScalarMultiply(h,proof.u);
	Element ver1=_globalContext.group.GetCurve().Add(a1,a2);
	Element ver2=_globalContext.group.GetCurve().Add(proof.beta,_globalContext.group.GetCurve().ScalarMultiply(T,c));
	Element v3=_globalContext.group.GetCurve().ScalarMultiply(R,proof.t);
	Element v4=_globalContext.group.GetCurve().Add(proof.alpha,_globalContext.group.GetCurve().ScalarMultiply(S,c));
	return (ver1==ver2)&&(v3==v4);
}
//need to add prime power product from GMR, I think... 


struct TonneliPrep
{
	ModularArithmetic ma;
	Integer p;
	Integer z;
    int s;
	Integer q;
	Integer dq;
	Integer c0;
	Integer dcheck;
	Integer mcheck;
	Integer mdcheck;
	vector<Integer> prec;
	vector<Integer> preb;
	TonneliPrep(Integer & p):ma(p) 
	{
	this->p=p;
    s=0;
    q=p-1;
 while(q%2==0)
 {
	 s+=1;
	 q/=2;
 }
  z=129;
 
 while(Jacobi(z,p)!=-1)
 {
	 z++;
	 z=z%p;
 }

Integer c=ma.Exponentiate(z,q);
c0=c;
dq=(q+1)>>1;
int i;
preb.clear();
prec.clear();
for(i=1;i<s;i++)
{
 Integer smpl=Integer::Power2(s-i-1);
 Integer b=ma.Exponentiate(c,smpl);//a_exp_b_mod_c(c,smpl,p);
  preb.push_back(b);
 prec.push_back(ma.Square(b));
 }
	dcheck=ma.Exponentiate(2,((p-1)>>1));
    mcheck=ma.Exponentiate((p-1),((p-1)>>1));
    mdcheck=ma.Exponentiate((p-2),((p-1)>>1));
}
	
};

int tonelliShanksCheck(Integer& x,TonneliPrep& pp)
{
Integer check=pp.ma.Exponentiate(x,((pp.p-1)>>1)); //a_exp_b_mod_c(x,(p-1)/2,p);
int ret=0;
if(check==1) ret|=1;
if(check==pp.dcheck) ret|=2;
if(check==pp.mcheck) ret|=4;
if(check==pp.mdcheck) ret|=8;

return ret;
}
Integer tonelliShanksPrepped(Integer& x,TonneliPrep& pp)
{

  int M=pp.s;
  Integer c=pp.c0;//a_exp_b_mod_c(z,q,p);
  Integer t=pp.ma.Exponentiate(x,pp.q);//a_exp_b_mod_c(x,q,p);
  Integer R=pp.ma.Exponentiate(x,pp.dq);//a_exp_b_mod_c(x,(q+1)/2,p);
  bool first=true;
  while(true)
  {
	  if (t==0)
	  {
		  return 0;
	  }
	  if(t==1)
	  {
		  return R;
	  }
	  Integer mt=t;
	  int i=1;
	  for(i=1;i<M;i++)
	  {
       mt=pp.ma.Square(mt);//a_exp_b_mod_c(mt,2,p);
	   		#ifdef DPRINT
		cout << "mt: " << IntToString(mt,16) <<endl;
	
		#endif
	   if (mt==1) break;
	  }
	  if (i==M)
	  {
		  return -1;
	  }
     if(mt!=1)
	 {
		 return -1;
	 }
    if(first)
	{
    c=pp.prec[i-1];
	R=(R*pp.preb[i-1])%pp.p;
	}
	else
	{
    Integer smpl=Integer::Power2(M-i-1);
	Integer b=pp.ma.Exponentiate(c,smpl);//a_exp_b_mod_c(c,smpl,p);
	c=(b*b)%pp.p;
	R=(R*b)%pp.p;
	}
	M=i;
	t=(t*c)%pp.p;
	first=false;
			#ifdef DPRINT
		cout << "Loop "  <<endl;
	cout << "M: " << IntToString(M,16) <<endl;
	cout << "c: " << IntToString(c,16) <<endl;
		#endif
  }
}
/*p is prime, n is element of prime. prime must be odd*/
Integer tonelliShanks(Integer& x,Integer& p)
{
 ModularArithmetic ma(p);
 Integer check=ma.Exponentiate(x,((p-1)>>1)); //a_exp_b_mod_c(x,(p-1)/2,p);
 if (check!=1)
 {
  return -1;
 }
 int s=0;
 Integer q=p-1;
 while(q%2==0)
 {
	 s+=1;
	 q/=2;
 }
 Integer z=x+1;
 
 while(Jacobi(z,p)!=-1)
 {
	 z++;
 }
  z=z%p;
  		#ifdef DPRINT
	//	cout << "Z found:" << IntToString(z,16) <<endl;
		cout << "S found:" << s <<endl;
		#endif
  int M=s;
  Integer c=ma.Exponentiate(z,q);//a_exp_b_mod_c(z,q,p);
  Integer t=ma.Exponentiate(x,q);//a_exp_b_mod_c(x,q,p);
  Integer R=ma.Exponentiate(x,(q+1)>>1);//a_exp_b_mod_c(x,(q+1)/2,p);
  while(true)
  {
	  if (t==0)
	  {
		  return 0;
	  }
	  if(t==1)
	  {
		  return R;
	  }
	  Integer mt=t;
	  int i=1;
	  for(i=1;i<M;i++)
	  {
       mt=ma.Square(mt);//a_exp_b_mod_c(mt,2,p);
	   		#ifdef DPRINT
		cout << "mt: " << IntToString(mt,16) <<endl;
	
		#endif
	   if (mt==1) break;
	  }
	  if (i==M)
	  {
		  return -1;
	  }
     if(mt!=1)
	 {
		 return -1;
	 }

    Integer smpl=Integer::Power2(M-i-1);
	Integer b=ma.Exponentiate(c,smpl);//a_exp_b_mod_c(c,smpl,p);

	M=i;
	c=(b*b)%p;
	t=(t*c)%p;
	R=(R*b)%p;
			#ifdef DPRINT
		cout << "Loop "  <<endl;
	cout << "M: " << IntToString(M,16) <<endl;
	cout << "c: " << IntToString(c,16) <<endl;
		#endif
  }
  
}
//for safe primes
Integer tonelliShanksSafe(Integer &x,Integer& p)
{
Integer q=(p-1)/2;
Integer t=a_exp_b_mod_c(x,q,p);
Integer R=a_exp_b_mod_c(x,(q+1)/2,p);
if(t==0) return 0;
if(t==1) return R;
return  -1;
}

Integer squareRootSafe(Integer& x,Integer& n, TonneliPrep& p, TonneliPrep& q)
{
	int pcheck=tonelliShanksCheck(x,p);
	int qcheck=tonelliShanksCheck(x,q);
	if((pcheck&qcheck)==0)
	{
		#ifdef DPRINT
		cout << "No square root!" <<endl;
		#endif
		return -1;
	}
	int ccheck=(pcheck&qcheck);
    if(ccheck&1) {} else
	{ if(ccheck&2) {x=(2*x)%n;} else
	{ if(ccheck&4) {x=(n-(x%n));} else
	if(ccheck&8) {x=(n-((2*x)%n));} } } 
	#ifdef DPRINT
		cout << "p-sqrt "  <<endl;
		#endif
	Integer srtP=tonelliShanksPrepped(x,p);//tonelliShanksSafe(x,p);
	#ifdef DPRINT
		cout << "q-sqrt "  <<endl;
		#endif
	Integer srtQ=tonelliShanksPrepped(x,q);//tonelliShanksSafe(x,q);
	if (srtP==-1||srtQ==-1)
	{
		#ifdef DPRINT
		cout << "No square root!" <<endl;
		#endif
		return -1;
	}
	Integer g[3]={p.p, q.p};
    Integer s[3]={1, 0};
    Integer t[3]={0, 1};
   
	unsigned int i0=0, i1=1, i2=2;
	while (g[i1]!=0)
	{
		Integer qi=g[i0]/g[i1];
		g[i2] = g[i0]-qi*g[i1];
		s[i2]=  s[i0]-qi*s[i1];
		t[i2]=  t[i0]-qi*t[i1];		
		unsigned int tm = i0; i0 = i1; i1 = i2; i2 = tm;
		//g[i2] = this->Mod(g[i0], g[i1]);
	}
    if (g[i0]!=1)
	{
		cout <<"odd primes"<<endl;
		return -1;
	}
	return (srtQ*p.p*s[i0]+srtP*q.p*t[i0])%n;
}

//only works for p!=q mod 8
primePowerProof provePrimePowerProduct(paillierKeyPair & key,unsigned int bitlen)
{
	AutoSeededRandomPool prng;
    Integer rand(prng,   256);
	byte buf[256];
	rand.Encode(buf,256);
	vector<byte> dec=concatEncode(key.pk.n);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),buf,256);
	primePowerProof ret;
	ret.bitlen=bitlen;
	ret.rseed=rand;
	//tonelliShanksPrepped
	TonneliPrep p_prep(key.sk.p);
	TonneliPrep q_prep(key.sk.q);
	
	for(unsigned int i=0;i<bitlen;i++) 
	{
	Integer rndi=genRandZ(key.pk.n,gen, false);
		#ifdef DPRINT
		cout << "RELOOP "  <<endl;
		#endif
	Integer sqr=squareRootSafe(rndi,key.pk.n,p_prep,q_prep);
	if (sqr>=0)
	{
    ret.congrs.push_back(sqr);
	continue;
	}
	/*rndi=key.pk.n-rndi;
	sqr=squareRootSafe(rndi,key.pk.n,p_prep,q_prep);
	if (sqr>=0)
	{
    ret.congrs.push_back(sqr);
	continue;
	}
	rndi=(rndi*2)%key.pk.n;
	sqr=squareRootSafe(rndi,key.pk.n,p_prep,q_prep);
	if (sqr>=0)
	{
    ret.congrs.push_back(sqr);
	continue;
	}
	rndi=key.pk.n-rndi;
	sqr=squareRootSafe(rndi,key.pk.n,p_prep,q_prep);
	if (sqr>=0)
	{
    ret.congrs.push_back(sqr);
	continue;
	}*/
	cout <<"Should not reach here, invalid prime setup" <<endl;
	}
    return ret;
   //unsigned int bitlen;
}

bool verifyPrimePowerProduct(paillierPubKey& key, primePowerProof& proof)
{
	if (proof.bitlen<20)
  	return false; //too few bits
	if (proof.congrs.size()!=proof.bitlen)
	{
		return false; //invalid proof
	}
	byte buf[256];
    proof.rseed.Encode(buf,256);
	vector<byte> dec=concatEncode(key.n);
	while(dec.size()<64) dec.push_back(125);
	SeededRng512 gen((byte *) dec.data(),dec.size(),buf,256);
	for(unsigned int i=0;i<proof.bitlen;i++)
	{
	Integer rndi=genRandZ(key.n,gen, false);
	Integer p2=key.n-rndi;
	Integer p3=(2*rndi)%key.n;
	Integer p4=(2*p2)%key.n;
	Integer sqr=(proof.congrs[i]*proof.congrs[i])%key.n;
	if (sqr!=rndi&&sqr!=p2&&sqr!=p3&&sqr!=p4)
	{
		return false;
    }
	}
	return true;
}


expPaillierProof createExpPaillierProof(Integer& nu,Integer& r,Element & g, Element& y,Integer & w,fujiCore& fuji,verifiedSideData& othr)
{
	Integer qcube=_globalContext.q*_globalContext.q*_globalContext.q;
    Integer rand(_globalContext.prng,   256);
	vector<byte> nonce=concatEncode(rand);
    expPaillierProof ret;
	ret.rseed=rand;
    Integer alpha=genRandZ(qcube-_globalContext.q*_globalContext.q,_globalContext.prng,false);
    Integer beta=genRandZ(fuji.E.pk.n,_globalContext.prng,true);
    Integer rho=genRandZ(othr.E.n,_globalContext.prng,false);
	Integer gamma=genRandZ(qcube*othr.E.n,_globalContext.prng,false);
	ret.z=ModularMultiplication(a_exp_b_mod_c(othr.h1,nu,othr.E.n),a_exp_b_mod_c(othr.h2,rho,othr.E.n), othr.E.n);
	ret.u1=_globalContext.group.GetCurve().ScalarMultiply(g,alpha);
	ret.u2=paillierEncWithRand(alpha,fuji.E.pk,beta);
	ret.u3=ModularMultiplication(a_exp_b_mod_c(othr.h1,alpha,othr.E.n),a_exp_b_mod_c(othr.h2,gamma,othr.E.n), othr.E.n);
  vector<byte> data=concatEncode(g.x,y.x,w,ret.z,ret.u1.x,ret.u1.y,ret.u2,ret.u3);
  while(data.size()<64) data.push_back(125);
  SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
  Integer challenge=genRandZ(_globalContext.q,cgen,false);
   ret.s1=challenge*nu+alpha;
   ret.s2=ModularMultiplication(a_exp_b_mod_c(r,challenge,fuji.E.pk.n),beta,fuji.E.pk.n);
   ret.s3=challenge*rho+gamma;
   return ret;
}

bool verifyExpPaillierProof(expPaillierProof& proof,Element & g, Element& y,Integer & w,fujiCore& sec,verifiedSideData& othr)
{
	vector<byte> nonce=concatEncode(proof.rseed);
	vector<byte> data=concatEncode(g.x,y.x,w,proof.z,proof.u1.x,proof.u1.y,proof.u2,proof.u3);
  while(data.size()<64) data.push_back(125);
  SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
  Integer challenge=genRandZ(_globalContext.q,cgen,false);
  if(proof.z>=sec.E.pk.n) return false;
  if(proof.u2>=othr.E.nSq) return false;
  if(proof.u3>=sec.E.pk.n) return false;
  Element v1=_globalContext.group.GetCurve().Add(_globalContext.group.GetCurve().ScalarMultiply(g,proof.s1),_globalContext.group.GetCurve().ScalarMultiply(y,_globalContext.q-challenge));
  if(!(v1==proof.u1))
  {
	  #ifdef DPRINT
	  cout << "U1 failed" <<endl;
	  #endif 
	  return false;
  }
  Integer iw=w.InverseMod(othr.E.nSq);
  Integer _t1= ModularMultiplication(a_exp_b_mod_c(othr.E.g,proof.s1,othr.E.nSq),a_exp_b_mod_c(proof.s2,othr.E.n,othr.E.nSq),othr.E.nSq);
  Integer v2= ModularMultiplication(a_exp_b_mod_c(iw,challenge,othr.E.nSq),_t1,othr.E.nSq);
   if(!(v2==proof.u2))
  {
	  #ifdef DPRINT
	  cout << "U2 failed" <<endl;
	  #endif 
	  return false;
  }
  Integer iz=proof.z.InverseMod(sec.E.pk.n);
  Integer _t2=ModularMultiplication(a_exp_b_mod_c(sec.b1,proof.s1,sec.E.pk.n),a_exp_b_mod_c(sec.b2,proof.s3,sec.E.pk.n), sec.E.pk.n);
  Integer v3=ModularMultiplication(a_exp_b_mod_c(iz,challenge,sec.E.pk.n),_t2,sec.E.pk.n);
  if(!(v3==proof.u3))
  {
	  #ifdef DPRINT
	  cout << "U3 failed" <<endl;
	  #endif 
	  return false;
  }
  return true;
}

#ifdef USE_RANGE_PROOFS
//TODO: fix proof to use other side for h1, h2
mtaRangeProof rangeProofMtaNiProver(fujiCore& fuji,verifiedSideData& other,  Integer&c , Integer& m, Integer& r, Integer& q,baseContext& _con)
{
 if (m>=q||m<0)
 {
     return mtaRangeProof();//jsonError("Invalid M for rangeProofMtaNiProver");
 }
 Integer qcube=q*q*q;
 Integer nasp=other.E.n*qcube;
 Integer alpha= genRandZ(qcube,_con.prng,false);
 Integer beta=genRandZ(fuji.E.pk.n,_con.prng,true);
 Integer gamma= genRandZ(nasp,_con.prng,false);
 Integer rho=genRandZ(other.E.n*q,_con.prng,false);
 Integer t1=a_exp_b_mod_c(other.h1,m , other.E.n);
 Integer t2=a_exp_b_mod_c(other.h2,rho , other.E.n);
 Integer z=(t1*t2)%other.E.n;
 Integer u=ModularMultiplication(a_exp_b_mod_c(fuji.E.pk.g,alpha,fuji.E.pk.nSq),a_exp_b_mod_c(beta,fuji.E.pk.n,fuji.E.pk.nSq),fuji.E.pk.nSq);
 t1=a_exp_b_mod_c(other.h1,alpha , other.E.n);
 t2=a_exp_b_mod_c(other.h2,gamma , other.E.n);
 Integer w=ModularMultiplication(t1,t2,other.E.n);
 Integer rand(_con.prng,   256);
 vector<byte> data=concatEncode(z,u,w);
 while(data.size()<64) data.push_back(125);
 vector<byte> nonce=concatEncode(rand);
 SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
 Integer challenge=genRandZ(q,cgen,false);
 #ifdef DPRINT
 cout <<"Chalgen: " <<IntToString(challenge)<<endl;
 #endif
 Integer s=ModularMultiplication(a_exp_b_mod_c(r,challenge,fuji.E.pk.n),beta,fuji.E.pk.n);
 Integer s1=challenge*m+alpha;
 Integer s2=challenge*rho+gamma;
 mtaRangeProof ret;
 ret.u=u;ret.w=w;ret.z=z;ret.rseed=rand;
 ret.s=s;
 ret.s1=s1;
 ret.s2=s2;
 return ret;
}



bool rangeProofMtaVerify(mtaRangeProof& proof,fujiCore& fuji,verifiedSideData& other,  Integer& c,Integer& q)
{
 if (proof.s1>q*q*q) return false;
 vector<byte> data=concatEncode(proof.z,proof.u,proof.w);
 vector<byte> nonce=concatEncode(proof.rseed);
 while(data.size()<64) data.push_back(125);
 SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
 Integer challenge=genRandZ(q,cgen,false);
 #ifdef DPRINT
 cout <<"Chalver: " <<IntToString(challenge)<<endl;
 #endif 
 Integer t1=a_exp_b_mod_c(other.E.g,proof.s1,other.E.nSq);
 Integer t2=a_exp_b_mod_c(proof.s,other.E.n,other.E.nSq);
 Integer ic=c.InverseMod(other.E.nSq);
 Integer t3=a_exp_b_mod_c(ic,challenge,other.E.nSq);
 Integer uver=ModularMultiplication(t1,t2,other.E.nSq);
 uver=ModularMultiplication(uver,t3,other.E.nSq);
 if(proof.u!=uver)
 {
	 #ifdef IMAIN
	 cout <<"U is not uver" <<endl;
	 #endif
	 return false;
 }
t1=a_exp_b_mod_c(fuji.b1,proof.s1,fuji.E.pk.n);
t2=a_exp_b_mod_c(fuji.b2,proof.s2,fuji.E.pk.n);
Integer iz=proof.z.InverseMod(fuji.E.pk.n);
t3=a_exp_b_mod_c(iz,challenge,fuji.E.pk.n);
Integer wver=ModularMultiplication(t1,t2,fuji.E.pk.n);
wver=ModularMultiplication(wver,t3,fuji.E.pk.n);
if(proof.w!=wver) 
{
	#ifdef DPRINT
	 cout <<"W is not Wver" <<endl;
	 #endif
	 return false;
}
return true;
}



mtaRespRangeProof rangeRespProofMtaNiProver(verifiedSideData& key,  Integer&c1 ,Integer&c2, Integer& m, Integer& y,Integer& r, Integer& q,baseContext& _con)
{
 if (m>=q||m<0)
 {
     return mtaRespRangeProof();//jsonError("Invalid M for rangeProofMtaNiProver");
 }
 Integer qcube=q*q*q;
 Integer nasp=key.E.n*qcube;
 Integer alpha= genRandZ(qcube,_con.prng,false);
 Integer sigma= genRandZ(nasp,_con.prng,false);
 Integer rho=genRandZ(key.E.n*q,_con.prng,false);
 Integer rhod=genRandZ(key.E.n*q,_con.prng,false);
 Integer beta=genRandZ(key.E.n,_con.prng,true);
 Integer gamma=genRandZ(key.E.n,_con.prng,true);
 Integer tau= genRandZ(nasp,_con.prng,false);

 Integer tm1=a_exp_b_mod_c(key.h1,m , key.E.n);
 Integer tm2=a_exp_b_mod_c(key.h2,rho , key.E.n);
 Integer z=ModularMultiplication(tm1,tm2,key.E.n);
 Integer zd=ModularMultiplication(a_exp_b_mod_c(key.h1,alpha , key.E.n),a_exp_b_mod_c(key.h2,rhod , key.E.n),key.E.n);
 Integer t=ModularMultiplication(a_exp_b_mod_c(key.h1,y , key.E.n),a_exp_b_mod_c(key.h2,sigma , key.E.n),key.E.n);
 Integer v=a_exp_b_mod_c(c1,alpha , key.E.nSq);
 v=ModularMultiplication(v,a_exp_b_mod_c(key.E.g,gamma, key.E.nSq),key.E.nSq );
 v=ModularMultiplication(v,a_exp_b_mod_c(beta,key.E.n,key.E.nSq),key.E.nSq );
 Integer w=ModularMultiplication(a_exp_b_mod_c(key.h1,gamma , key.E.n),a_exp_b_mod_c(key.h2,tau, key.E.n),key.E.n);
 
 Integer rand(_con.prng,   256);
 vector<byte> data=concatEncode(z,zd,t,v,w);
 vector<byte> nonce=concatEncode(rand);
 while(data.size()<64) data.push_back(125);
 SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
 Integer challenge=genRandZ(q,cgen,false);
 
 Integer s=ModularMultiplication(a_exp_b_mod_c(r,challenge,key.E.n),beta,key.E.n);

 Integer s1=challenge*m+alpha;
 Integer s2=challenge*rho+rhod;

 Integer t1=challenge*y+gamma;
 Integer t2=challenge*sigma+tau;
 mtaRespRangeProof ret;
 ret.z=z;ret.w=w;ret.v=v;ret.zd=zd;ret.t=t;
 ret.rseed=rand;

 ret.s=s;
 ret.s1=s1;
 ret.s2=s2;
 ret.t1=t1;
 ret.t2=t2;

 return ret;
}



bool rangeRespProofMtaVerify(mtaRespRangeProof& proof,fujiCore& core,Integer& c1,Integer& c2,Integer& q,baseContext& _con)
{
 vector<byte> data=concatEncode(proof.z,proof.zd,proof.t,proof.v,proof.w);
 vector<byte> nonce=concatEncode(proof.rseed);
 while(data.size()<64) data.push_back(125);
 SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
 Integer challenge=genRandZ(q,cgen,false);
 Integer qc=q*q*q;
 if(proof.s1>qc) return false;
 Integer ver1=ModularMultiplication(a_exp_b_mod_c(core.b1,proof.s1 , core.E.pk.n),a_exp_b_mod_c(core.b2,proof.s2 , core.E.pk.n),core.E.pk.n);
 Integer ver1z=ModularMultiplication(a_exp_b_mod_c(proof.z,challenge, core.E.pk.n),proof.zd,core.E.pk.n);
 if(ver1z!=ver1)
 {
	 #ifdef IMAIN
	 cout <<"ZV is not ZVver" <<endl;
	 #endif
	 return false;
 }
 Integer ver2=ModularMultiplication(a_exp_b_mod_c(core.b1,proof.t1 , core.E.pk.n),a_exp_b_mod_c(core.b2,proof.t2 , core.E.pk.n),core.E.pk.n);
 Integer ver2z=ModularMultiplication(a_exp_b_mod_c(proof.t,challenge, core.E.pk.n),proof.w,core.E.pk.n);
  if(ver2z!=ver2)
 {
	 #ifdef IMAIN
	 cout <<"TV is not WVver" <<endl;
	 #endif
	 return false;
 }
 Integer v3=a_exp_b_mod_c(c1,proof.s1, core.E.pk.nSq);
 v3=ModularMultiplication(v3,a_exp_b_mod_c(proof.s,core.E.pk.n,core.E.pk.nSq),core.E.pk.nSq);
 v3=ModularMultiplication(v3,a_exp_b_mod_c(core.E.pk.g,proof.t1,core.E.pk.nSq),core.E.pk.nSq);
 Integer v3z=ModularMultiplication(a_exp_b_mod_c(c2,challenge , core.E.pk.nSq),proof.v, core.E.pk.nSq);
  if(v3!=v3z)
 {
	 #ifdef IMAIN
	 cout <<"Pailier z-check fail" <<endl;
	 #endif
	 return false;
 }
 return true; 
}

#endif



schnorrBaseProof proveSchnorr(Integer x, baseContext& _con)
{
	Integer r(_con.prng, Integer::One(), _con.maxExponent);
	Integer rand(_con.prng,  256);
	Element Qr = _con.group.GetCurve().ScalarMultiply(_con.base, r);
	vector<byte> data=concatEncode(Qr.x,Qr.y);
    vector<byte> nonce=concatEncode(rand);
	while(data.size()<64) data.push_back(125);
    SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
    Integer challenge=genRandZ(_con.q,cgen,false);
	schnorrBaseProof ret;
	ret.Qr=Qr;
	ret.rseed=rand;
	ret.s=r+challenge*x;
    Element Qs= _con.group.GetCurve().ScalarMultiply(_con.base, ret.s);
	return ret;
}

bool verifySchnorr(Element& pk,schnorrBaseProof& proof,baseContext& _con)
{
    vector<byte> data=concatEncode(proof.Qr.x,proof.Qr.y);
    vector<byte> nonce=concatEncode(proof.rseed);
	while(data.size()<64) data.push_back(125);
    SeededRng512 cgen(data.data(),data.size(),nonce.data(),nonce.size());
    Integer challenge=genRandZ(_con.q,cgen,false);
	Element Qs= _con.group.GetCurve().ScalarMultiply(_con.base, proof.s);
    Element Qy= _con.group.GetCurve().Add(_con.group.GetCurve().ScalarMultiply(pk,challenge),proof.Qr);
	if(Qs==Qy) return true;
	return false;
}

rsaEllipticProof createRsaEllipticProof(RSA::PublicKey& pk,Element& pubshare,Integer privshare)
{
    Integer& q=_globalContext.q;
    rsaEllipticProof ret;
    SHA3_256 hash;
	vector<byte> tempStr = concatEncode(pubshare.x);//IntToString(pubshare.x, 16);
	vector<byte> digest;
	Integer edat=pk.ApplyFunction(privshare);
	vector<byte> e=concatEncode(edat);
	hash.Update((const byte*)tempStr.data(), tempStr.size());
    hash.Update((const byte*)e.data(), e.size());
    
	vector<Integer> cc0, cc1, rr, yy;
	cc0.resize(256);
	cc1.resize(256);
	rr.resize(256);
	yy.resize(256);

	
	for(int j = 0; j < 256; ++j)
    {
		Integer r(_globalContext.prng, Integer::One(), q);
		Integer y = (privshare + r) % q;
		Integer c0 = pk.ApplyFunction(r);
		Integer c1 = pk.ApplyFunction(y);

		Element Q_tag = _globalContext.group.GetCurve().ScalarMultiply(_globalContext.base, r);
		tempStr=concatEncode(c0,c1,Q_tag.x);
        hash.Update((const byte*)tempStr.data(), tempStr.size());
		
		rr[j] = r;
		yy[j] = y;
		cc0[j] = c0;
		cc1[j] = c1;
	}
	

	digest.resize(hash.DigestSize());
	hash.Final((byte*)&digest[0]);
    Integer idi=Integer(digest.data(),digest.size());
	ret.binaryDigest = IntegerToBinaryString(idi,256/8);
	for(int j = 0; j < 256; ++j){
		char ej = ret.binaryDigest[j];

		if(ej == '0'){
            ret.c.push_back(cc1[j]);
            ret.ry.push_back(rr[j]);
		}
		else{
            ret.c.push_back(cc0[j]);
            ret.ry.push_back(yy[j]);
		}	
	}
    return ret;
}


bool  verifyRsaEllipticProof(rsaEllipticProof& proof, RSA::PublicKey& pk,Element& pubshare,Integer& backup)
{
    
	SHA3_256 hash;
	vector<byte> tempStr = concatEncode(pubshare.x);
	vector<byte> digest;
	vector<byte> e=concatEncode(backup);
	hash.Update((const byte*)tempStr.data(), tempStr.size());
	  hash.Update((const byte*)e.data(), e.size());
    Element Q_share=pubshare;

	for(int j = 0; j < 256; ++j){
		//string proofStr = t["proof"];
		char ej = proof.binaryDigest[j];
		Element Q_tag;
		
		Integer c0, c1;
		Integer ry=proof.ry[j];
		Integer c=proof.c[j];

		if(ej == '0'){
			c0 = pk.ApplyFunction(ry);
			Q_tag = _globalContext.group.GetCurve().ScalarMultiply(_globalContext.base, ry);
			c1 = c;
		}
		else{
			c1 = pk.ApplyFunction(ry);
			Element tempQ = _globalContext.group.GetCurve().ScalarMultiply(_globalContext.base, ry);
			Q_tag = _globalContext.group.GetCurve().Subtract(tempQ, Q_share);
			c0 = c;
		}
        tempStr=concatEncode(c0,c1,Q_tag.x);
		hash.Update((const byte*)tempStr.data(), tempStr.size());
	}
	digest.resize(hash.DigestSize());
	hash.Final((byte*)&digest[0]);
    Integer idi=Integer(digest.data(),digest.size());
    string outproof =  IntegerToBinaryString(idi,256/8);
	if(outproof == proof.binaryDigest)
    {
		return true;
	}
	else{
		return false;
	}
}





