There are some macros used in the code to achive coroutine-like functionality for protocols. Since they break control flow, some caution is needed when using them.


### Macro descriptions


## Json conversion macros

# Decoding

- CNMARK(nm) : just exists to mark field as read for verification
- CNP(nm) : assigns plain (json) value to variable of the same name 
- CNV(nm) : decodes json string as a large integer and assigns it (main encoding)
- CNVx(nm) : decodes json string as a large integer and assigns it (0x... hex encoding)
- CNV64(nm) : decodes json string as a large integer and assigns it (base64 encoding)
- CNVHex(nm) : decodes json string as a large integer and assigns it (plain hex encoding)
- CNE(nm) : decodes json array of strings as an elliptic point and assigns it 
- CNO(nm) : decodes json object as jsonInterface object and loads it into variable
- CNA(nm) : decodes json array of (currently) base64 strings into array of large integers
- CNRPK(nm) : decodes json object with either (n,e) pair or "pem" entry into RSA public key 


#define ITSE(nm) ret[#nm]=json::array(); ret[#nm].push_back(encodeInteger(nm.x)); ret[#nm].push_back(encodeInteger(nm.y));
#define ITST(nm) ret[#nm]=encodeInteger(nm);  
#define ITSTHex(nm) ret[#nm]=IntToString(nm,16)+"h";


#define ITSTx(nm,ln) ret[#nm]=encodeInteger0x(nm,ln);  
#define ITST64(nm) ret[#nm]=encodeInteger64(nm);  
#define ITSV(nm) ret[#nm]=json::array(); \
                 for(auto& v : nm) { ret[#nm].push_back(encodeInteger64(v)); }; 
                   
#define ITSP(nm) ret[#nm]=nm;

#define ITSRPK(nm) ret[#nm]=json(); \
                { std::string __otp; StringSink _sink(__otp);  PEM_Save(_sink,nm); ret[#nm]["pem"]=__otp; } 
