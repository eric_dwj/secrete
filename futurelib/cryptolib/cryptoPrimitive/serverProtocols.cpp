#include "serverProtocols.h"


json FeldmanVssServer::process_state(json& _input)
{
 baseContext con;
    Integer my_x=BACKEND_PARTY;
    Integer other_x=FRONTEND_PARTY;
    SV_PROTO_START
    SV_PROTO_LOAD_MESSAGE(fVssS1,inp2);
    Element v1=con.group.GetCurve().ScalarMultiply(con.base,inp2.sigma);
    Element v2=con.group.GetCurve().Add(inp2.v0,con.group.GetCurve().ScalarMultiply(inp2.vi,my_x));
    Element v3=con.group.GetCurve().Add(inp2.v0,con.group.GetCurve().ScalarMultiply(inp2.vi,other_x));
    vi_oth=v3;
    if(!(v1==v2))
    {
        SV_EOUT("Feldman verification failed for server");
    }

    v0_oth=inp2.v0; 
    share=inp2.sigma;
    my_a=genRandZ(modulo,con.prng,false);
    fVssS1 outp;
    my_sigma=(ModularMultiplication(my_a,my_x,modulo)+secret)%modulo;
    outp.sigma=(ModularMultiplication(my_a,other_x,modulo)+secret)%modulo;
    vi_oth=con.group.GetCurve().Add(vi_oth,con.group.GetCurve().ScalarMultiply(con.base,outp.sigma)); //needs testing!
    outp.vi=con.group.GetCurve().ScalarMultiply(con.base,my_a);
    outp.v0=con.group.GetCurve().ScalarMultiply(con.base,secret);
    Element vv1=con.group.GetCurve().ScalarMultiply(con.base,outp.sigma);
    Element vv2=con.group.GetCurve().Add(outp.v0,con.group.GetCurve().ScalarMultiply(outp.vi,other_x));
    SV_PROTO_OUTPUT(outp);
    SV_PROTO_END
}


json MtaProtocolServer::process_state(json& _input)
{
   baseContext con; ///might want top make a parameter... 
   SV_PROTO_START;
   SV_PROTO_LOAD_MESSAGE(mtaS1Out,inp1);
   if(useRange!=inp1.use_range) SV_EOUT("Incompatible MTA protocol versions");
   #if USE_RANGE_PROOFS
   if(useRange)
   {
    if(!rangeProofMtaVerify(inp1.mr,*fuji, *other,inp1.cA,con.q))
    {
      SV_EOUT("Range proof from client incorrect");  
    }
   }
   #endif   
   cOther=inp1.cA;
   Integer qc=con.q*con.q*con.q; //K
   Integer beta=genRandZ(other->E.n-qc,con.prng,false);
   outpArBl=(-beta)%con.q;
   Integer r1=genRandZ(other->E.n,con.prng,true);
   cB= ModularMultiplication( a_exp_b_mod_c(inp1.cA,sB,other->E.nSq),paillierEncWithRand(beta,other->E,r1),other->E.nSq);
   mtaS2Out outp;
   outp.cB=cB;
   outp.use_check=useCheck;
   outp.use_range=useRange;
   #if USE_RANGE_PROOFS
   if(useRange)
   {
       outp.mrp=rangeRespProofMtaNiProver(*this->other,  inp1.cA ,cB, sB, beta,r1,con.q,con);
   }
   #endif 
   if(useCheck)
   {
       //schnorrBaseProof proveSchnorr(Integer x, baseContext& _con);
       mtaSchnorrBaseProof bp;
       bp.B=con.group.GetCurve().ScalarMultiply(con.base, sB);
       bp.Bd=con.group.GetCurve().ScalarMultiply(con.base, beta);
       bp.Bproof=proveSchnorr(sB, con);
       bp.Bdproof=proveSchnorr(beta, con);
       outp.chk=bp;
   }

   Integer r=genRandZ(fuji->E.pk.n,con.prng,true);
   cA=paillierEncWithRand(sA,fuji->E.pk,r);
   rA=r;
   mtaS1Out srv;
   srv.use_range=useRange;
   srv.cA=cA;
    #if USE_RANGE_PROOFS
   if (useRange)
      srv.mr=rangeProofMtaNiProver(*fuji,*other,cA,sA,r,con.q,con);
    #endif  
   outp.servIn=srv;
   SV_PROTO_YIELD_OUTPUT(outp)
   SV_PROTO_LOAD_MESSAGE(mtaS3Out,inp3);
    if(useCheck!=inp3.use_check||useRange!=inp3.use_range)
   {
      SV_EOUT("At stage 3, invalid version of server protocol");
   }
   #if USE_RANGE_PROOFS
   if(useRange)
   {
    if(!rangeRespProofMtaVerify(inp3.mrp,*fuji, cA ,inp3.cB, con.q,con))
    {
      SV_EOUT("Range proof 2 from client  incorrect");  
    }
   }
   #endif  
   if(useCheck)
   {
       if(!verifySchnorr(inp3.chk.B,inp3.chk.Bproof,con))
        SV_EOUT("Schnorr proof for B incorrect");
       if(!verifySchnorr(inp3.chk.Bd,inp3.chk.Bdproof,con))
       SV_EOUT ("Schnorr proof for Bd incorrect");    
   }
   Integer alphad=paillierDec(inp3.cB,fuji->E);
   outpAlBr=alphad%(con.q);
   if(useCheck)
   {
   Element vc=con.group.GetCurve().ScalarMultiply(con.base,outpAlBr);
   Element v2=con.group.GetCurve().Add(con.group.GetCurve().ScalarMultiply(inp3.chk.B,sA),inp3.chk.Bd);
   if(!(vc==v2))
    {
     SV_EOUT("result does not match public value");    
    }

   }
   statusMessage msg("OK");
   SV_PROTO_OUTPUT(msg);
   //end of protocol for server?
   SV_PROTO_END;
}


json PreSignServer::process_state(json& _input)
{
    Element elid;
    if(keygenData==nullptr)
    {
        SV_EOUT("Keygen data not set!");
    }
   baseContext con; ///might want top make a parameter or global... 
   SV_PROTO_START;
   if(!splitDerivationPath(derivPath,parsedPath))
   {
       SV_EOUT("Incorrect derivation path");
   } 
    SV_PROTO_LOAD_MESSAGE(hashCommitment,comex);
    theirs=comex;
    deriveAccumulator acc;
    acc.pubkey=keygenData->pubkey;
    acc.chaincode=keygenData->chaincode;
    acc.accumulated=0;
    for(auto chld :parsedPath)
    {
        if(!deriveAccumulate(acc,chld))
        {
         SV_EOUT("Incorrect derivation path indices");
        }
    }
    offset=acc.accumulated; // should be the difference...
    ki=genRandZ(con.q,con.prng,false);
    gammai=genRandZ(con.q,con.prng,false);
    Element y=con.group.GetCurve().ScalarMultiply(con.base,gammai);
    std::pair<hashCommitment,hashDecommitment> coms=getCommitment<SHA3_256>(y,256);
    //outp.com=coms.first;
    gammacom=coms.second;
    SV_PROTO_YIELD_OUTPUT(coms.first);
   
    SV_PROTO_EXECUTE(MtaProtocolServer,kgm,ki,gammai,false,false,&keygenData->paillier,&keygenData->other,elid);
    SV_PROTO_EXECUTE(MtaProtocolServer,kw,ki,keygenData->privKeyShare,false,true,&keygenData->paillier,&keygenData->other,keygenData->pub_other);
    deltai=(ModularMultiplication(ki,gammai,con.q)+kgm.AlBr+kgm.ArBl)%con.q;
    sigmai=(ModularMultiplication(ki,keygenData->privKeyShare,con.q)+kw.AlBr+kw.ArBl+ModularMultiplication(ki,offset,con.q))%con.q;
    SV_PROTO_LOAD_MESSAGE(presignS34,st34cln);
    if(!verifyHashCommitment<SHA3_256>(theirs,st34cln.di))
    {
     SV_EOUT("Invalid gamma commitment");
    }
    Integer deltotal=(deltai+st34cln.deltai)%con.q;
 
    revdelta=deltotal.InverseMod(con.q);
    GM=con.group.GetCurve().Add(st34cln.di.y,gammacom.y);
    Element R=con.group.GetCurve().ScalarMultiply(GM,revdelta);
    r=R.x%con.q;
    //this be weird
    int isOdd = (R.y % Integer::Two()) == Integer::One();
	int isEqual = (R.x != r) ? 2 : 0;
	recoveryParam = isOdd | isEqual;
 
    if(r==0)
    {
        SV_EOUT("Seems like a bad RNG");
    }
    presignS34 out34;
    out34.di=gammacom;
    out34.deltai=deltai;
    deltai=deltotal;  //overwrite? 
    SV_PROTO_YIELD_OUTPUT(out34);
    SV_PROTO_LOAD_MESSAGE(presignS5,slast);
    if(!verifyExpPaillierProof(slast.lproof,GM,slast.Lambda,kw.cOther,keygenData->paillier,keygenData->other))
    {
    #ifdef DPRINT
    cout << "Could not verify Lambda"  <<endl;
    #endif 
    SV_EOUT("Lambda proof verification failed"); 
    }
    presignS5 sgn5;
    sgn5.Lambda=con.group.GetCurve().ScalarMultiply(GM,ki); //have to calc our Lambda first
    lambdai=sgn5.Lambda;
   Element tlam=con.group.GetCurve().Add(slast.Lambda, lambdai);
   Element tst=con.group.GetCurve().ScalarMultiply(con.base,deltai); //deltai is total now
   if(!(tlam==tst))
    {
    #ifdef DPRINT
    cout << "Could not verify Lambda"  <<endl;
    #endif 
    SV_EOUT("Lambda verification failed"); 
    }
 
    sgn5.lproof=createExpPaillierProof(ki,kw.rA,GM,sgn5.Lambda, kw.cA,keygenData->paillier,keygenData->other);
    SV_PROTO_OUTPUT(sgn5);
    #ifdef DPRINT
    cout << kgm.toJson().dump() <<endl;
    cout << kw.toJson().dump() <<endl;
    #endif
      SV_PROTO_END;
}







json KeyGenServer::process_state(json& _input)
{
    baseContext con;
    Integer chlimit=Integer::Power2(256);

    SV_PROTO_START
    SV_PROTO_LOAD_MESSAGE(kgenS1,inp1);
    if(!fujiVerify(inp1.paillierProofs))
    {
        SV_EOUT("Incorrect paillier key from client")
    }
    chaincode=rotateEntropy(chaincode,chlimit,inp1.com.digest,inp1.paillierProofs.En,inp1.paillierProofs.b1,inp1.paillierProofs.b2);
    theirs=inp1.com;
    other.E=paillierPubKey(inp1.paillierProofs.En);
    other.h1=inp1.paillierProofs.b1;
    other.h2=inp1.paillierProofs.b2;
    privatePaillier=generateInitialKeys(2048);
    ui=genRandZ(con.q,con.prng,false);
    kgenS1 outp;
    outp.paillierProofs=privatePaillier.extractPacket();
    Element y=con.group.GetCurve().ScalarMultiply(con.base,ui);
    std::pair<hashCommitment,hashDecommitment> coms=getCommitment<SHA3_256>(y,256);
    outp.com=coms.first;
    decom=coms.second;
    #ifdef DPRINT
    cout <<"Server com: " <<coms.first.toJson().dump()<<endl;
    cout <<"Server decom: " <<coms.second.toJson().dump()<<endl;
    #endif
    chaincode=rotateEntropy(chaincode,chlimit,outp.com.digest,outp.paillierProofs.nonSquare.rseed,outp.paillierProofs.primePower.rseed);
    SV_PROTO_YIELD_OUTPUT(outp)
    SV_PROTO_EXECUTE(FeldmanVssServer,share,ui,con.q);
    xi=share.x;
    SV_PROTO_LOAD_MESSAGE(kgenS2,inp2);
    if(!verifyHashCommitment<SHA3_256>(theirs,inp2.decom))
    {
        SV_EOUT("Incorrect public key decom from client")
    }
    if(!(inp2.decom.y==share.v0_oth))
    {
        SV_EOUT("Shared client public key does not match commitment")
    }
     if(!verifySchnorr(share.X_oth,inp2.xProof,con))    
     {
          SV_EOUT("Server could not prove knowledge of xi") 
     }
    chaincode=rotateEntropy(chaincode,chlimit,inp2.xProof.s);
    kgenS2Server out3;
    out3.xProof=proveSchnorr(xi, con);
    out3.decom=decom;
    //Integer combo=secbitlenCombine(xi,chaincode); server should just send priv key
    out3.encBackup=backupPubKey.ApplyFunction(xi);
    Element pshr=_globalContext.group.GetCurve().ScalarMultiply(_globalContext.base,xi);
    out3.backupProof=createRsaEllipticProof(backupPubKey,pshr,xi);
    chaincode=rotateEntropy(chaincode,chlimit,out3.xProof.rseed,out3.xProof.s);
    SV_PROTO_OUTPUT(out3)
    SV_PROTO_END
}


json SignServer::process_state(json& _input)
{
 if(presData==nullptr)
    {
        SV_EOUT("PreSign data not set!");
    }
    if(presData->derPath!=derivePath)
    {
         SV_EOUT("PreSign data path invalid for this signature");
    }
   baseContext con; ///might want top make a parameter or global... 
   SV_PROTO_START;
   recoveryParam=presData->recoveryParam;
   SV_PROTO_LOAD_MESSAGE(signS1,resp);
   s=(ModularMultiplication(msg,presData->ki,con.q)+ModularMultiplication(presData->r,presData->sigmai,con.q))%con.q;
   signS1 rt;
   rt.si=s;
   s+=resp.si;
   s=s%con.q;
   if(s>(con.q/2))
  {
      s=con.q-s;
      recoveryParam^=1;
  }
  Integer is=s.InverseMod(con.q);
  Integer u1=ModularMultiplication(msg,is,con.q);
  Integer u2=ModularMultiplication(presData->r,is,con.q);
  Element rq=con.group.GetCurve().Add(con.group.GetCurve().ScalarMultiply(con.base,u1),con.group.GetCurve().ScalarMultiply(presData->pubkey,u2));
  if(!(presData->r==(rq.x%con.q) ))
  {
      SV_EOUT("Resulting signature does not match");
  }
  
   SV_PROTO_OUTPUT(rt);
   SV_PROTO_END;
}




json FullSignServer::process_state(json& _input)
{
    SV_PROTO_START
    SV_PROTO_EXECUTE(PreSignServer,prevout,derivePath,keygenData);
    SV_PROTO_EXECUTE(SignServer,complout,derivePath,&prevout,msg);
    SV_PROTO_END
}