#pragma once
#include "headers.h"

struct paillierPubKey : public jsonInterface
{
	Integer n;
	Integer g;
    Integer nSq;
/*Since p q have the same length, we can use a simpler variant of the key
	where g = n + 1, lambda = (p - 1 )(q - 1), u = inverse of lambda mod n 	*/
	paillierPubKey():paillierPubKey(0){}
	paillierPubKey(Integer in)
	{
		n=in;
		g=in+Integer::One();
		nSq=n*n;
	}
	bool validate()
	{
		//not fully complete
		if(n==0) return false;
		if(n.BitCount()<minRSA) return false;
		if(n*n!=nSq) return false;
		if(g==0) return false;
		return true;
	}
	json toJson()
	{
		json ret;
		ITST(n)
		return ret;
	}
	void loadJson(json & js)
	{
		n=0;
		for(auto &kv :js.items())
		{
			CNV(n)
		}
		if(n!=0)
		{
			g=n+Integer::One();
		   nSq=n*n;
		}
	}
};

struct paillierSecKey :public jsonInterface
{
	Integer lambda;
	Integer u;
	Integer p;
	Integer q;
	json toJsonReduced()
	{
		json ret;
		ITST(p)
		ITST(q)
		return ret;	
	}
	void loadJsonReduced(json & js)
	{
	  for(auto &kv :js.items())
		{
			CNV(p)
			CNV(q)
		}	
		this->lambda = (p-1)*(q-1);//simpler formulation //LCM((p - 1) , (q - 1));
	    this->u = (this->lambda).InverseMod(p*q); 
	}
	json toJson()
	{
		json ret;
		ITST(lambda)
		ITST(u)
		ITST(p)
		ITST(q)
		return ret;
	}
	bool validate()
	{
		if(p==0) return false;
		if(q==0) return false;
		if((p-1)*(q-1)!=lambda) return false;
		return true;
	}
	void loadJson(json & js)
	{
	   for(auto &kv :js.items())
		{
			CNV(lambda)
			CNV(u)
			CNV(p)
			CNV(q)
		}
	}
};



struct paillierKeyPair :public jsonInterface
{
	paillierPubKey pk;
	paillierSecKey sk;

	json toJson()
	{
		json ret;
		ret["pk"]=pk.toJson();
		ret["sk"]=sk.toJson();
		return ret;
	}
	void loadJson(json & js)
	{
       for(auto &kv :js.items())
		{
			CNO(pk);
			CNO(sk);
		}
	}
	bool validate()
	{
		if(!pk.validate()) return false;
		if(!sk.validate()) return false;
		return true;
	}
	paillierKeyPair(Integer n, Integer p, Integer q);
	paillierKeyPair(paillierSecKey&& sk);
	paillierKeyPair(){};
	paillierKeyPair(Integer totient,Integer n);
};
struct FujisakiPrep
{
    Integer p;
    Integer subP;
    Integer g;
	FujisakiPrep()
	{
		p=0;subP=0;g=0;
	}
	json toJson()
	{
		json ret;
		ITST(p)
		ITST(subP)
		ITST(g)
		return ret;
	}
	void loadJson(json & js)
	{
       for(auto &kv :js.items())
		{
			CNV(p);
			CNV(subP);
			CNV(g);
		}
	}
};
struct nonSquareProof
{
	Integer y;
	Integer rseed;
	json toJson()
	{
		json ret;
		ITST(y);
		ITST(rseed);
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
        CNV(y);
		CNV(rseed);
		}
	}
};


struct sigmaLProof:jsonInterface
{
Integer rseed;
Element alpha;
Integer t;
Integer u;
json toJson()
	{
		json ret;
		ITST(t);
		ITST(u);
		ITST(rseed);
		ITSE(alpha);
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
        CNV(t);
		CNV(u);
		CNE(alpha);
		CNV(rseed);
		}
	}
	bool validate()
	{
		if(alpha.identity) return false;
		if(rseed==0) return false;
		return true;
	}
};



struct SRLProof:jsonInterface
{
Integer rseed;
Element alpha;
Element beta;
Integer t;
Integer u;
json toJson()
	{
		json ret;
		ITST(t);
		ITST(u);
		ITST(rseed);
		ITSE(alpha);
		ITSE(beta);
		return ret;
	}
	void loadJson(json& js)
	{

		for(auto &kv :js.items())
		{
        CNV(t);
		CNV(u);
		CNE(alpha);
		CNE(beta);
		CNV(rseed);
		}
	}
	bool validate()
	{
		if(alpha.identity) return false;
		if(beta.identity) return false;
		if(rseed==0) return false;
		return true;
	}
};

struct primePowerProof
{
  Integer rseed;
  unsigned int bitlen;
  vector<Integer> congrs;
  json toJson()
  {
	  json ret;
	  ITST(rseed);
      ret["bitlen"]=bitlen;
	  ITSV(congrs);
	  return ret;
  }
  void loadJson(json& js)
  {
	  for(auto &kv :js.items())
	  {
		  CNV(rseed);
		  if(kv.key()=="bitlen")  bitlen=kv.value();
	      CNA(congrs);
	  }
  }
};

struct fujiCore :jsonInterface
{
 paillierKeyPair E;
 Integer gsp,gsq;
 Integer b0;  //statistical zero knowledge protocols to prove modular... etc
 Integer subn;
 Integer alpha, beta;
 Integer b1,b2;
 struct fmPacket:jsonInterface
 { //includes fujisaki and paillier public parameters, Schnorr non-interactive proof, square proof
  Integer En;
  Integer b1,b2;

  // Schnorr proof
  Integer t;
  Integer s;

  nonSquareProof nonSquare;
  primePowerProof primePower;
   json toJson()
   {
	   json ret;
	   ITST(En);
	   ITST(b1);
	   ITST(b2);
	   ITST(En);
	   ITST(t);
	   ITST(s);
	   ret["nonSquare"]=nonSquare.toJson();
	   ret["primePower"]=primePower.toJson();
	   return ret;
   }
   fmPacket()
   {
   En=0;
   b1=0;
   b2=0;
   t=0;
   s=0;
   }
   void loadJson(json& js)
   {
	    for(auto &kv :js.items())
	   {
        CNV(En);
		CNV(b1);
		CNV(b2);
		CNV(t);
		CNV(s);
        CNO(nonSquare);
		CNO(primePower);
	   } 
   }
   fmPacket(json& js): fmPacket()
   {

	  loadJson(js);

   }
   bool validate()
   {
	   return En!=0;
   }
 };
 fujiCore (FujisakiPrep& pp, FujisakiPrep& qq,Integer n);
 fujiCore ()
 {

 }
 fmPacket extractPacket();
 json toJson()
 {
	 json ret;
	 ret["E"]=E.toJson();
	 ITST(gsp);
	 ITST(gsq);
	 ITST(b0);
	 ITST(subn);
	 ITST(alpha);ITST(beta);
	 ITST(b1);ITST(b2);
	 return ret;
 }
   void loadJson(json& js)
  {
	  for(auto &kv :js.items())
	  {
		  CNO(E);
	      CNV(gsp);
          CNV(gsq);
		  CNV(b0);
		  CNV(subn);
		  CNV(alpha);CNV(beta);
		  CNV(b1); CNV(b2);
	  }
  }
   bool validate()
  {
	  if(!E.validate()) return false;
	  if(b1==0) return false;
	  if(b2==0) return false;
	  //should not need for now, heavy 
      //Integer vb1=a_exp_b_mod_c(b0,alpha,E.pk.n);
      //Integer vb2=a_exp_b_mod_c(b0,beta,E.pk.n);
	  //if(vb1!=b1||vb2!=b2) return false;
	  return true;
  }
};



struct verifiedSideData :public jsonInterface
{
  paillierPubKey E;
  Integer h1,h2;
  bool loadFujiPacket(fujiCore::fmPacket& pack);
    json toJson()
   {
	   json ret;
	   ret["E"]=E.toJson();
	   ITST(h1);
	   ITST(h2);
	   return ret;
   }
    void loadJson(json& js)
  {
	  for(auto &kv :js.items())
	  {
		  CNO(E);
	      CNV(h1);
          CNV(h2);
	  }
  }
  bool validate()
  {
   if(!E.validate()) return false;
   if(h1==0||h2==0) return false;
   return true;
  }
  verifiedSideData():E(0)
  {

  }

};

//step 1 of Multiplicative to additive a*b --> x+y as per "One Round Threshold ECDSA with Identifiable Abort"
// https://eprint.iacr.org/2020/540.pdf
// allows removal of range proofs as per "Fast Multiparty Threshold ECDSA with Fast Trustless Setup" sec.6 (https://eprint.iacr.org/2019/114.pdf)
// useRangeProof: would calculate non-interactive range proofs
// isWC: checks the public value B is known by P2 (server) 

struct mtaRangeProof
{
	Integer z,u,w;
	Integer s,s1,s2;
	Integer rseed;
	json toJson()
	{
		json ret;
		ITST(z)
		ITST(u)
		ITST(w)
		ITST(s)
		ITST(s1)
		ITST(s2)
		ITST(rseed)
		return ret;
	}
	void loadJson(json& js)
	{
		for(auto &kv :js.items())
	  {
		  CNV(z)
		  CNV(u)
		  CNV(w)
		  CNV(s)
		  CNV(s1)
		  CNV(s2)
		  CNV(rseed) 
	  }
	}
};

struct mtaRespRangeProof :jsonInterface
{
	Integer u,z,zd,t,v,w;
	Integer s,s1,s2,t1,t2;
	Integer rseed;
	json toJson()
	{
		json ret;
		ITST(u)
		ITST(z)
		ITST(zd)
		ITST(t)
		ITST(v)
		ITST(w)
		ITST(s)
		ITST(s1)
		ITST(s2)
		ITST(t1)
		ITST(t2)
		ITST(rseed)
		return ret;
	}
	void loadJson(json& js)
	{
		for(auto &kv :js.items())
	  {
		CNV(u)
		CNV(z)
		CNV(zd)
		CNV(t)
		CNV(v)
		CNV(w)
		CNV(s)
		CNV(s1)
		CNV(s2)
		CNV(t1)
		CNV(t2)
		  CNV(rseed) 
	  }
	}
	bool validate()
	{
		if(rseed==0) return false;
        if(u==0) return false;
        if(z==0) return false;
        if(zd==0) return false;
        if(t==0) return false;
        if(v==0) return false;
        if(w==0) return false;

        if(s==0) return false;
        if(s1==0) return false;
        if(s2==0) return false;
        if(t1==0) return false;
  if(t2==0) return false;
return true;
	}
};


struct schnorrBaseProof:jsonInterface
{
 Element Qr;
 Integer rseed;
 Integer s;
 schnorrBaseProof()
 {
	 rseed=0;
	 s=0;
	 Qr.identity=true;
 }
 void loadJson(json&js)
 {
		for(auto &kv :js.items())
	  {
		  CNE(Qr)
		  CNV(rseed)
		  CNV(s)
	  } 
 }

 	json toJson()
	{
		json ret;
		ITSE(Qr)
		ITST(rseed)
		ITST(s)
		return ret;
	}
	bool validate()
	{
		//if(rseed==0) return false;
		if(s==0) return false;
		if(Qr.identity)
		 return false;
      return true;
	}
};

struct mtaSchnorrBaseProof:jsonInterface
{
 Element B;
 Element Bd;
 schnorrBaseProof Bproof;
 schnorrBaseProof Bdproof;
  void loadJson(json&js)
 {
		for(auto &kv :js.items())
	  {
		  CNE(B);
		  CNE(Bd);
         CNO(Bproof);
		 CNO(Bdproof);
	  }
 }
 	json toJson()
	{
		json ret;
		ITSE(B)
		ITSE(Bd)
		ret["Bproof"]=Bproof.toJson();
		ret["Bdproof"]=Bdproof.toJson();
		return ret;
	}
   bool validate()
	{
		if(B.identity) return false;
		if(Bd.identity) return false;
		if(!Bproof.validate()) return false;
		if(!Bdproof.validate()) return false;
		return true; 
	}
};


/*Proves that we know k1 such that encryption of it corresponds to logarithm D(c)=log_r(rk)*/
struct expPaillierProof: public jsonInterface
{
//	Integer alpha;
//	Integer beta;
//	Integer rho;
//	Integer gamma;
	Integer z,u2,u3;
	Element u1;
	Integer rseed;
	Integer s1,s2,s3;
  void loadJson(json&js)
 {
		for(auto &kv :js.items())
	  {
     CNV(z); CNV(u2); CNV(u3);
CNE(u1);
CNV(rseed);
CNV(s1)
CNV(s2)
CNV(s3)
	  }
 }
 	json toJson()
	{
		json ret;
	    ITSE(u1);
		ITST(z);
		ITST(u2);
		ITST(u3);
		ITST(rseed);
		ITST(s1);
		ITST(s2);
		ITST(s3);
		return ret;
	}
   bool validate()
	{
		if(rseed==0) return false;
		// ...
		return true; 
	}

};
struct fujisakiCore
{
	Integer N;
	Integer h1,h2;
};

struct hashCommitment:jsonInterface
{
 Integer digest;
 json toJson()
 {
	 json ret; ITST(digest); return ret;
 }
 void loadJson(json & js)
 {
	 auto jf=js.find("digest");
	 if(jf!=js.end())
	 {
		std::string vl=jf.value(); digest=decodeInteger(vl);
	 }
 }
 bool validate()
 {
	 return true;
 }
};


struct rsaEllipticProof:jsonInterface
{
 std::string binaryDigest;
 vector<Integer> c;
 vector<Integer> ry;
 json toJson()
 {
	 json ret; 
     ITSV(c);
     ITSV(ry); 
     ITSP(binaryDigest);
     return ret;
 }
 void loadJson(json & js)
 {
	for(auto &kv :js.items())
	  {
		  CNA(c);
		  CNA(ry);
         CNP(binaryDigest);
	  }
 }

 bool validate()
 {
     if(c.size()!=256) return false;
	 if(ry.size()!=256) return false;
     if(binaryDigest.length()!=256) return false;
     return true;
 }
};


struct hashDecommitment:jsonInterface
{
 Element y;
 Integer rseed;
  json toJson()
 {
	 json ret; ITSE(y); ITST(rseed); return ret;
 }
  void loadJson(json & js)
 {
	  for(auto &kv :js.items())
	  {
		  CNE(y);
		  CNV(rseed);
	  }
 }
 bool validate(){return true;}
};


template<typename hash>
std::pair<hashCommitment,hashDecommitment> getCommitment(Element y,int rbytes)
{
	AutoSeededRandomPool prng;
	Integer rand(prng,  rbytes);
	hash h;
	vector<byte> data=concatEncode(y.x,y.y);
    vector<byte> nonce=concatEncode(rand);
	vector<byte> out;
    h.Update((const byte*)data.data(), data.size());
    out.resize(h.DigestSize());
	h.Final((byte*)&out[0]);
	hashCommitment o1;
	hashDecommitment o2;
	o1.digest=Integer(out.data(),out.size());
	o2.rseed=rand;
	o2.y=y;
	return std::pair<hashCommitment,hashDecommitment>(o1,o2);
}

template<typename hash>
bool verifyHashCommitment(hashCommitment& hk, hashDecommitment& hd)
{
  hash h;
  vector<byte> nonce=concatEncode(hd.rseed);
  vector<byte> data=concatEncode(hd.y.x,hd.y.y);
  vector<byte> out;
  h.Update((const byte*)data.data(), data.size());
  out.resize(h.DigestSize());
  h.Final((byte*)&out[0]);
  Integer cmp=Integer(out.data(),out.size());
  return cmp==hk.digest;
}




/* Generates nonzero Zq up to limit, optionally checks for GCD*/
extern Integer genRandZ(const Integer & limit,RandomNumberGenerator & prng,bool ast);

template <typename... Ts>
Integer rotateEntropy(Integer prev, Integer q,Ts&... all)
{
	vector<byte> additives=concatEncode(all...);
	vector<byte> nonce=concatEncode(prev);
	while(additives.size()<64)additives.push_back(1);
	SeededRng512 rgen(additives.data(),additives.size(),nonce.data(),nonce.size());
   return genRandZ(q,rgen, false);
}


Integer largePrimeGen(int secBits,RandomNumberGenerator & prng);
FujisakiPrep fujiGen(int secBits,RandomNumberGenerator & prng,int discardResidue);
extern rsaEllipticProof createRsaEllipticProof(RSA::PublicKey& pk,Element& pubshare,Integer privshare);
extern bool  verifyRsaEllipticProof(rsaEllipticProof& proof, RSA::PublicKey& pk,Element& pubshare,Integer& backup);


extern nonSquareProof proveNonSquare(paillierKeyPair & key);
extern bool verifyNonSquare(paillierPubKey& key, nonSquareProof& bp);
extern bool fujiVerify(fujiCore::fmPacket & fuji);
extern primePowerProof provePrimePowerProduct(paillierKeyPair & key,unsigned int bitlen);
extern bool verifyPrimePowerProduct(paillierPubKey& key, primePowerProof& proof);
extern sigmaLProof createSigmalProof(Element& g, Element& h,Element& T,Integer& sgm, Integer & el);
extern bool verifySigmalProof(sigmaLProof& proof,Element& g, Element& h,Element& T);
extern SRLProof createSRLProof(Element& g, Element& h,Element& R,Element& S,Element& T,Integer& sgm, Integer & el);
extern bool verifySRLProof(SRLProof& proof,Element& g, Element& h,Element& R,Element& S,Element& T);
extern  bool verifyExpPaillierProof(expPaillierProof& proof,Element & g, Element& y,Integer & w,fujiCore& sec,verifiedSideData& othr);
extern expPaillierProof createExpPaillierProof(Integer& nu,Integer& r,Element & g, Element& y,Integer & w,fujiCore& fuji,verifiedSideData& othr);
extern Integer paillierEncWithRand(Integer& msg, paillierPubKey& pk, Integer& r);
extern Integer paillierEnc(Integer& msg, paillierPubKey& pk);
extern Integer paillierDec(Integer& c, paillierKeyPair key);
extern fujiCore generateInitialKeys(int secBits);

#ifdef USE_RANGE_PROOFS
extern mtaRangeProof rangeProofMtaNiProver(fujiCore& fuji,verifiedSideData& other,  Integer&c , Integer& m, Integer& r, Integer& q,baseContext& _con);
extern bool rangeProofMtaVerify(mtaRangeProof& proof,fujiCore& fuji,verifiedSideData& other,  Integer& c,Integer& q);
extern bool rangeRespProofMtaVerify(mtaRespRangeProof& proof,fujiCore& core,Integer& c1,Integer& c2,Integer& q,baseContext& _con);
extern mtaRespRangeProof rangeRespProofMtaNiProver(verifiedSideData& key,  Integer&c1 ,Integer&c2, Integer& m, Integer& y,Integer& r, Integer& q,baseContext& _con);
#endif

extern schnorrBaseProof proveSchnorr(Integer x, baseContext& _con);
extern bool verifySchnorr(Element& pk,schnorrBaseProof& proof,baseContext& _con);

