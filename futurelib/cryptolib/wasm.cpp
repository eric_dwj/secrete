#include <functional>
#include <string>
#include <unordered_map>
#include <stdexcept>
#include <cstdlib>

#include "wasm.h"

void freeStr(void* str) {
  free(str);
}

#define NEW_C_STR(str) (strdup((str).c_str()))

#define SV_UNPACK(partype) json store_json; \
  try \
  { \
   store_json=json::parse(store); \
  } \
  catch(...) \
  { \
   return NEW_C_STR(jsonError("Store corrupted!").dump()); \
  } \
  json parameters_json; \
   try \
  { \
   parameters_json=json::parse(parameters); \
  } \
  catch(...) \
  { \
   return NEW_C_STR(jsonError("Parameters corrupted!").dump()); \
  } \
  partype kparams; \
  try \
  { \
  kparams.loadJson(parameters_json); \
  } \
  catch(...) \
  { \
   return NEW_C_STR(jsonError("Parameters invalid!").dump()); \
  } \
  if(!kparams.validate()) \
  { \
  return NEW_C_STR(jsonError("Parameters not set correctly").dump()); \
  } 

#define SV_PROCESS(proto,...) proto __protocol=proto(__VA_ARGS__); \
                              json state=__protocol.process_state(store_json);

#define SV_PACK_START(outtype)   if(state["complete"]==true) \
   { \
     state["output"]=__protocol.final_output(); \
     outtype out; \
     out.loadJson(state["output"]); 


#define SV_PACK_END(...) state["output"]["export"]={__VA_ARGS__} ; \
                         state.erase("save"); \
                         }\
                         return NEW_C_STR(state.dump());


const char* generateKeyClient(const char* parameters,const char* store) 
{
  SV_UNPACK(keygenParams)
  SV_PROCESS(KeyGenClient,kparams.backupPublicKey)
 
  SV_PACK_START(KeyGenClient::output)
  bpub xp=bgetRootFromPublic(out.pubkey,out.chaincode);
  state["output"]["xpub"]=bpubEncode(xp);
  SV_PACK_END("encryptedOurBackup","encryptedTheirBackup","xpub")

}


const char* getDerivedPublic (const char* xpub,const char* path) 
{
  std::string s1=xpub;
  std::string s2=path;
return NEW_C_STR(getDerivedPublicInternal(s1,s2).dump());
}
const char* preSignClient(const char* parameters,const char* store) 
{
  SV_UNPACK(preSignParams)
  SV_PROCESS(PreSignClient,kparams.derivationPath,&kparams.key);
  SV_PACK_START(PreSignClient::output)
  SV_PACK_END("derPath","pubkey","recoveryParam","r")
}


const char* signClient(const char* parameters,const char* store) 
{
  SV_UNPACK(signParams)
  SV_PROCESS(SignClient,kparams.derivationPath,&kparams.preSignData,kparams.message);
  SV_PACK_START(SignClient::output)
  SV_PACK_END("recoveryParam","r","s")
}


const char* fullSignClient(const char* parameters,const char* store) 
{
  SV_UNPACK(fullSignParams)
  SV_PROCESS(FullSignClient,kparams.derivationPath,&kparams.key,kparams.message);
  SV_PACK_START(SignClient::output)
  SV_PACK_END("recoveryParam","r","s")
}



