#include <functional>
#include <string>
#include <unordered_map>
#include <stdexcept>

#include "main.h"

using namespace CryptoPP;

#define SV_UNPACK(partype) json store_json; \
  try \
  { \
   store_json=json::parse(store); \
  } \
  catch(...) \
  { \
   return jsonError("Store corrupted!").dump(); \
  } \
  json parameters_json; \
   try \
  { \
   parameters_json=json::parse(parameters); \
  } \
  catch(...) \
  { \
   return jsonError("Parameters corrupted!").dump(); \
  } \
  partype kparams; \
  try \
  { \
  kparams.loadJson(parameters_json); \
  } \
  catch(...) \
  { \
   return jsonError("Parameters invalid!").dump(); \
  } \
  if(!kparams.validate()) \
  { \
  return jsonError("Parameters not set correctly").dump(); \
  } 

#define SV_PROCESS(proto,...) proto __protocol=proto(__VA_ARGS__); \
                              json state=__protocol.process_state(store_json);

#define SV_PACK_START(outtype)   if(state["complete"]==true) \
   { \
     state["output"]=__protocol.final_output(); \
     outtype out; \
     out.loadJson(state["output"]); 


#define SV_PACK_END(...) state["output"]["export"]={__VA_ARGS__} ; \
                         state.erase("save"); \
                         }\
                         return state.dump();
   
std::string CryptoLib::generateKeyClient(std::string parameters,std::string store) 
{
  SV_UNPACK(keygenParams)
  SV_PROCESS(KeyGenClient,kparams.backupPublicKey)
 
  SV_PACK_START(KeyGenClient::output)
  bpub xp=bgetRootFromPublic(out.pubkey,out.chaincode);
  state["output"]["xpub"]=bpubEncode(xp);
  bpub tp=bgetRootFromPublic(out.pubkey,out.chaincode,true);
  state["output"]["tpub"]=bpubEncode(tp);
  SV_PACK_END("encryptedOurBackup","encryptedTheirBackup","xpub","tpub")

}

std::string CryptoLib::generateKeyServer(std::string parameters,std::string store) 
{
    SV_UNPACK(keygenParams)
  SV_PROCESS(KeyGenServer,kparams.backupPublicKey)
  SV_PACK_START(KeyGenClient::output)
  bpub xp=bgetRootFromPublic(out.pubkey,out.chaincode);
  state["output"]["xpub"]=bpubEncode(xp);
    bpub tp=bgetRootFromPublic(out.pubkey,out.chaincode,true);
  state["output"]["tpub"]=bpubEncode(tp);
  SV_PACK_END("encryptedOurBackup","encryptedTheirBackup","xpub","tpub")
}

std::string CryptoLib::getDerivedPublic(std::string xpub,std::string path) 
{
return ::getDerivedPublicInternal(xpub,path).dump();
}

std::string CryptoLib::preSignClient(std::string parameters,std::string store) 
{
  SV_UNPACK(preSignParams)
  SV_PROCESS(PreSignClient,kparams.derivationPath,&kparams.key);
  SV_PACK_START(PreSignClient::output)
  SV_PACK_END("derPath","pubkey","recoveryParam","r")
}
std::string CryptoLib::preSignServer(std::string parameters,std::string store) 
{
  SV_UNPACK(preSignParams)
  SV_PROCESS(PreSignServer,kparams.derivationPath,&kparams.key);
  SV_PACK_START(PreSignClient::output)
  SV_PACK_END("derPath","pubkey","recoveryParam","r")
}


std::string CryptoLib::signClient(std::string parameters,std::string store) 
{
  SV_UNPACK(signParams)
  SV_PROCESS(SignClient,kparams.derivationPath,&kparams.preSignData,kparams.message);
  SV_PACK_START(SignClient::output)
  SV_PACK_END("recoveryParam","r","s")
}
std::string CryptoLib::signServer(std::string parameters,std::string store) 
{
  SV_UNPACK(signParams)
  SV_PROCESS(SignServer,kparams.derivationPath,&kparams.preSignData,kparams.message);
  SV_PACK_START(SignClient::output)
  SV_PACK_END("recoveryParam","r","s")
}


std::string CryptoLib::fullSignClient(std::string parameters,std::string store) 
{
  SV_UNPACK(fullSignParams)
  SV_PROCESS(FullSignClient,kparams.derivationPath,&kparams.key,kparams.message);
  SV_PACK_START(SignClient::output)
  SV_PACK_END("recoveryParam","r","s")
}
std::string CryptoLib::fullSignServer(std::string parameters,std::string store) 
{
  SV_UNPACK(fullSignParams)
  SV_PROCESS(FullSignServer,kparams.derivationPath,&kparams.key,kparams.message);
  SV_PACK_START(SignClient::output)
  SV_PACK_END("recoveryParam","r","s")
}

//// JS Wrapper

#define RETURN(statement) { \
  try { \
    return statement; \
  } catch (const std::exception& e) { \
    Napi::TypeError::New(env, e.what()).ThrowAsJavaScriptException(); \
    return Napi::String::New(env, ""); \
  } catch (...) { \
    Napi::TypeError::New(env, "unknown error").ThrowAsJavaScriptException(); \
    return Napi::String::New(env, ""); \
  } \
}

Napi::String CryptoLib::generateKeyClientJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "generateKeyClient(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::generateKeyClient(info[0].ToString(),info[1].ToString())));
}

Napi::String CryptoLib::generateKeyServerJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "generateKeyServer(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::generateKeyServer(info[0].ToString(),info[1].ToString())));
}

Napi::String CryptoLib::preSignClientJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "preSignClientJS(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::preSignClient(info[0].ToString(),info[1].ToString())));
}

Napi::String CryptoLib::preSignServerJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "preSignServerJS(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::preSignServer(info[0].ToString(),info[1].ToString())));
}


Napi::String CryptoLib::signClientJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "signClientJS(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::signClient(info[0].ToString(),info[1].ToString())));
}

Napi::String CryptoLib::signServerJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "signServerJS(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::signServer(info[0].ToString(),info[1].ToString())));
}

Napi::String CryptoLib::fullSignClientJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "fullSignClientJS(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::fullSignClient(info[0].ToString(),info[1].ToString())));
}

Napi::String CryptoLib::fullSignServerJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString() )
  {
    Napi::TypeError::New(env, "fullSignServerJS(parameters,store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::fullSignServer(info[0].ToString(),info[1].ToString())));
}





Napi::String CryptoLib::getDerivedPublicJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 2 || !info[0].IsString() || !info[1].IsString())
  {
    Napi::TypeError::New(env, "getDerivedPublic(xpub, derivationPath) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::getDerivedPublic(info[0].ToString(), info[1].ToString())));
}


/*
Napi::String CryptoLib::signJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 3 || !info[0].IsString() || !info[1].IsNumber() || !info[2].IsNumber())
  {
    Napi::TypeError::New(env, "sign(input, party, step) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::sign(info[0].ToString(),
    info[1].As<Napi::Number>().Uint32Value(),
    info[2].As<Napi::Number>().Uint32Value())));
}



Napi::String CryptoLib::getRecoveryKeyJS(const Napi::CallbackInfo &info)
{
  Napi::Env env = info.Env();
  if (info.Length() < 1 || !info[0].IsString())
  {
    Napi::TypeError::New(env, "getRecoveryKey(store) expected").ThrowAsJavaScriptException();
    return Napi::String::New(env, "");
  }
  RETURN(Napi::String::New(env, CryptoLib::getRecoveryKey(info[0].ToString())));
}
*/
Napi::Object InitAll(Napi::Env env, Napi::Object exports)
{
  exports.Set("generateKeyServer", Napi::Function::New(env, CryptoLib::generateKeyServerJS));
  exports.Set("generateKeyClient", Napi::Function::New(env, CryptoLib::generateKeyClientJS));
  exports.Set("preSignClient", Napi::Function::New(env, CryptoLib::preSignClientJS));
  exports.Set("preSignServer", Napi::Function::New(env, CryptoLib::preSignServerJS));
  exports.Set("signClient", Napi::Function::New(env, CryptoLib::signClientJS));
  exports.Set("signServer", Napi::Function::New(env, CryptoLib::signServerJS));
  exports.Set("fullSignClient", Napi::Function::New(env, CryptoLib::fullSignClientJS));
  exports.Set("fullSignServer", Napi::Function::New(env, CryptoLib::fullSignServerJS));
  exports.Set("getDerivedPublic", Napi::Function::New(env, CryptoLib::getDerivedPublicJS));
 // exports.Set("getRecoveryKey", Napi::Function::New(env, CryptoLib::getRecoveryKeyJS));
  return exports;
}

NODE_API_MODULE(Cryptolib, InitAll)
