#pragma once
#include "cryptoPrimitive/headers.h"
#include "cryptoPrimitive/clientProtocols.h"
#include <emscripten/emscripten.h>

// internal
int getVal(char x);
string getAddrFromPk(Integer& pk);

extern "C" {
  EMSCRIPTEN_KEEPALIVE const char* generateKeyClient(const char* parameters,const char* store);
  EMSCRIPTEN_KEEPALIVE const char* preSignClient(const char* parameters,const char* store);
  EMSCRIPTEN_KEEPALIVE const char* signClient(const char* parameters,const char* store);
  EMSCRIPTEN_KEEPALIVE const char* fullSignClient(const char* parameters,const char* store);
  EMSCRIPTEN_KEEPALIVE const char* getDerivedPublic (const char* xpub,const char* path);
  //EMSCRIPTEN_KEEPALIVE const char* generateKeyServer(const char* parameters,const char* store);
  
 /* EMSCRIPTEN_KEEPALIVE const char* sign(const char* input, unsigned int party, unsigned int step);
  EMSCRIPTEN_KEEPALIVE const char* getDerivedPublic(const char* xpub, const char* derivationPath);*/
  EMSCRIPTEN_KEEPALIVE void freeStr(void* str);
}
