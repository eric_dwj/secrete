const path = require('path');
const webpackMerge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

const baseconf = {
  entry: './browser/browser.mjs',
  mode: 'development',
  target: 'web',
  output: {
    filename: 'cryptolib.bundle.js',
    path: path.resolve(__dirname, 'dist'),
    libraryExport: 'default',
    libraryTarget: 'umd',
    library: 'CryptolibClient',
  },
  plugins: [
    // new BundleAnalyzerPlugin()

  ]
};

const publicPath = process.env.PUBLIC_PATH || '/';
console.log(`wasm is downloaded at publicPath = ${publicPath}`);

const confs = {
  web: webpackMerge(baseconf, {
    target: 'web',
    mode: 'production',
    output: {
      path: path.resolve(__dirname, 'out/webpack'),
      filename: 'cryptolib.web.js',
      publicPath,
    },
    module: {
      rules: [
        {
          test: /cryptolib\.wasm$/,
          type: 'javascript/auto',
          loader: 'file-loader',
          options: {
            publicPath
          }
        }
      ]
    },
    resolve: {
      alias: {
        './config': path.resolve(__dirname, 'js/web/config.js'),
        './bindings': path.resolve(__dirname, 'cryptolib/CryptolibNative.js'),
        './keystore/KeyStore': path.resolve(__dirname, 'js/web/KeyStore.js'),
        '../utils/log4js': path.resolve(__dirname, 'js/web/log4js.js'),
      }
    },
    externals: ['fs'],
  }),
  node: webpackMerge(baseconf, {
    target: 'node',
    output: {
      filename: 'cryptolib.node.js'
    },
    externals: [nodeExternals()]
  })
};

module.exports = confs.web;
