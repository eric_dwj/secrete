import request from '@/utils/request';

export function profile() {
  return request().then(() => {
    return {
      username: 'Bill Gates',
      email: 'bill.gates@sbibits.com',
    };
  });
}
