import request from '@/utils/request';
import cookie from '@/utils/cookie';
import { TOKEN } from '@/constants/auth';

export function login(email, password) {
  return request(email, password).then(() => {
    cookie.set(TOKEN, 'token');
  });
}

export function logout() {
  cookie.remove(TOKEN);
}
