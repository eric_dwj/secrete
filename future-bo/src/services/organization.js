export function list(offset, limit, options) {  
  return Promise.resolve(offset, limit, options).then(() => {
    return {
      list: [
        { 
          organizationId: 1,
          name: 'Google',
          created: new Date(),
          owner: 3,
          member: 10,
          wallet: 4,
          BTC: '10.123',
          XRP: '3.123',
          ETH: '300.2123123',
        },
        {
          organizationId: 2,
          name: 'SBI VC',
          created: new Date(),
          owner: 3,
          member: 10,
          wallet: 4,
          BTC: '10.123',
          XRP: '3.123',
          ETH: '300.2123123',
        },
        {
          organizationId: 3,
          name: 'Airbnb',
          created: new Date(),
          owner: 3,
          member: 10,
          wallet: 4,
          BTC: '10.123',
          XRP: '3.123',
          ETH: '300.2123123',
        },
        {
          organizationId: 4,
          name: 'Tradingfront',
          created: new Date(),
          owner: 3,
          member: 10,
          wallet: 4,
          BTC: '10.123',
          XRP: '3.123',
          ETH: '300.2123123',
        },
      ],
      count: 4,
    };
  });
}
