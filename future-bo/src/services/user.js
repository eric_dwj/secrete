export function list(offset, limit, options) {
  return Promise.resolve(offset, limit, options).then(() => {
    return {
      list: [
        {
          userId: '1',
          email: 'x1@google.com',
          organization: {
            name: 'Google',
            organizationId: 1,
          },
          group: 'owner',
          wallet: 3,
          status: 'LOCKED',
          created: new Date(),
        },
        {
          userId: '2',
          email: 'y1@airbnb.com',
          organization: {
            name: 'Airbnb',
            organizationId: 1,
          },
          group: 'owner',
          wallet: 3,
          status: 'ENABLED',
          created: new Date(),
        },
        {
          userId: '3',
          email: 'x1@sbibits.com',
          organization: {
            name: 'SBI VC',
            organizationId: 1,
          },
          group: 'owner',
          wallet: 3,
          status: 'LOCKED',
          created: new Date(),
        },
        {
          userId: '4',
          email: 'x1@tradingfront.com',
          organization: {
            name: 'Tradingfront',
            organizationId: 1,
          },
          group: 'member',
          wallet: 3,
          status: 'PENDING',
          created: new Date(),
        },
      ],
      count: 30,
    };
  });
}
