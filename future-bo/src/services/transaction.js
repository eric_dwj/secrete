import request from '@/utils/request';

export function list(offset, limit, options) {
  return request(offset, limit, options).then(() => {
    return {
      list: (new Array(10)).fill({
        created: new Date(),
        coin: 'BTC',
        address: 'adfasdfhewkfhajkbvkjabvryabv',
        type: 1,
        status: 'CREATED',
        amount: '123123.12303',
        fee: '1.23',
        creater: {
          email: 'xxxx@sbibits.com',
          userId: 1,
        },
        approver: {
          email: 'yyyy@sbibits.com',
          userId: 2,
        },
        txid: 'ba257e35fe8c9675f0eec09d57445a38ee84a6e28a1029556a01b8dab44d65b0',
        transferId: '1',
      }),
      count: 40,
    };
  });
}
