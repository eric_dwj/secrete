import date from '@/utils/date';

export function coinCount() {
  return Promise.resolve().then(() => {
    return Array.from(new Array(60), (_, index) => index).map((item) => ({
      date: date('2020-01-01').add(item, 'd').format('YYYY-MM-DD'),
      BTC: Math.random() * 100,
      XRP: Math.random() * 180,
      ETH: Math.random() * 200,
    }));
  });
}

export function txCount() {
  return Promise.resolve().then(() => {
    return Array.from(new Array(60), (_, index) => index).map((item) => ({
      date: date('2020-01-01').add(item, 'd').format('YYYY-MM-DD'),
      BTC: Math.round(30 * item + 10),
      XRP: Math.round(35 * item + 10),
      ETH: Math.round(45 * item + 10),
    }));
  });
}
