export default function request() {
  return new Promise((resolve) => {
    setTimeout(resolve, 1200);
  });
}
