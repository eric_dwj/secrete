import Vue from 'vue';
import locale from 'element-ui/lib/locale';
import i18n from '@/plugins/i18n';
import { ZH_CN, EN_US } from '@/locales';

const instance = new Vue({
  i18n,
});

export function changeLocale(lang) {
  i18n.locale = lang;
  locale.use(
    ({
      [ZH_CN]: 'zh-CN',
      [EN_US]: 'en',
    })[lang],
  );
}

export default instance;
