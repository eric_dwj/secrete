import Vue from 'vue';
import VueI18n from 'vue-i18n';
import locale from 'element-ui/lib/locale';

import { 
  ZH_CN, 
  EN_US, 
  messages,
  numberFormats,
} from '@/locales';

Vue.use(VueI18n);
Vue.use((VueConstructor) => {
  VueConstructor.prototype.$n = function (value, format) {
    try {
      return numberFormats[format](value);
    } catch (err) {
      return value.toString();
    }
  };
});

const i18n = new VueI18n({
  locale: EN_US || ZH_CN,
  messages,
});
locale.i18n((key, value) => i18n.t(key, value));

export default i18n;
