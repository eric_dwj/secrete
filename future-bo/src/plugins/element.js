import Vue from 'vue';
import ElementUI from 'element-ui';
import ECharts from 'vue-echarts';
import Section from '@/components/Section.vue';
import RemoteSearch from '@/components/RemoteSearch.vue';
import Coin from '@/components/Coin.vue';
import Copy from '@/components/Copy.vue';
import 'element-ui/lib/theme-chalk/index.css';
import '@/theme/variables.scss';

Vue.use(ElementUI);
Vue.component('v-chart', ECharts);
Vue.component('el-section', Section);
Vue.component('remote-search', RemoteSearch);
Vue.component('coin', Coin);
Vue.component('copy', Copy);
