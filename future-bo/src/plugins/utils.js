import Vue from 'vue';
import nprogress from 'nprogress';

Vue.use((VueConstructor) => {
  VueConstructor.prototype.$progress = nprogress;
});
