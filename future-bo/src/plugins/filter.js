import Vue from 'vue';
import date from '@/utils/date';
import * as wallet from '@/constants/wallet';

Vue.filter('formatDate', (value, format = 'YYYY-MM-DD HH:mm') => {
  return date(value).format(format);
});

Vue.filter('txid', (value, coin) => {
  switch (coin) {
    case wallet.BTC: 
      return `https://www.blockchain.com/search?search=${value}`;

    case wallet.XRP: 
      return `https://test.bithomp.com/explorer/${value}`;

    case wallet.ETH: 
      return `https://kovan.etherscan.io/tx/${value}`;

    default:
      return value;
  }
});
