export const OWNER = 'owner';
export const MEMBER = 'member';
export const USER_STATUS_ENABLED = 'ENABLED';
export const USER_STATUS_PENDING = 'PENDING';
export const USER_STATUS_LOCKED = 'LOCKED';
