export const STATUS_ENABLED = 'ENABLED';
export const BTC = 'BTC';
export const ETH = 'ETH';
export const XRP = 'XRP';
export const COINS_SORT = [BTC, ETH, XRP];
export const STATUS_NORMAL = 'normal';
export const STATUS_LOCKED = 'locked';
