import Num from '@/utils/number';
import * as tx from '@/constants/transaction';
import * as wallet from '@/constants/wallet';
import * as zhCN from './locale.zh-CN';
import * as enUS from './locale.en-US';

export const ZH_CN = zhCN.locale;
export const EN_US = enUS.locale;
export const messages = {
  [zhCN.locale]: zhCN.messages,
  [enUS.locale]: enUS.messages,
};
export const numberFormats = {
  count: (value) => {
    return new Num(value).toFormat(0);
  },
  [`coin.long.${wallet.BTC}`]: (value) => {
    return new Num(value).toFormat(8);
  },
  [`coin.long.${wallet.ETH}`]: (value) => {
    return new Num(value).toFormat(8);
  },
  [`coin.long.${wallet.XRP}`]: (value) => {
    return new Num(value).toFormat(tx.PLACES_XRP);
  },
  [`coin.${wallet.BTC}`]: (value) => {
    return new Num(value).toFormat(tx.PLACES_BTC);
  },
  [`coin.${wallet.ETH}`]: (value) => {
    return new Num(value).toFormat(tx.PLACES_ETH);
  },
  [`coin.${wallet.XRP}`]: (value) => {
    return new Num(value).toFormat(tx.PLACES_XRP);
  },
  'coin.limit': (value) => {
    return new Num(value).toFormat(4);
  },
  [`coin.short.${wallet.BTC}`]: (value) => {
    return new Num(value).toFormat(4);
  },
  [`coin.short.${wallet.ETH}`]: (value) => {
    return new Num(value).toFormat(2);
  },
  [`coin.short.${wallet.XRP}`]: (value) => {
    return new Num(value).toFormat(2);
  },
};
