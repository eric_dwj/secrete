import Vue from 'vue';
import VueRouter from 'vue-router';
import Auth from '@/layouts/Auth.vue';
import Dash from '@/layouts/Dash.vue';
import Home from '@/views/Home.vue';
import Login from '@/views/auth/Login.vue';
import TxList from '@/views/transaction/List.vue';
import TxDetail from '@/views/transaction/Detail.vue';
import TxMerge from '@/views/transaction/Merge.vue';
import UserList from '@/views/user/List.vue';
import UserCreate from '@/views/user/Create.vue';
import UserDetail from '@/views/user/Detail.vue';
import WalletList from '@/views/wallet/List.vue';
import WalletDetail from '@/views/wallet/Detail.vue';
import OrganizationList from '@/views/organization/List.vue';
import OrganizationCreate from '@/views/organization/Create.vue';
import OrganizationDetail from '@/views/organization/Detail.vue';
import Log from '@/views/Log.vue';
import Config from '@/views/Config.vue';
import NotFound from '@/views/NotFound.vue';
import Error from '@/views/Error.vue';
import { TOKEN } from '@/constants/auth';
import cookie from '@/utils/cookie';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: { name: 'home' },
  },
  {
    path: '/',
    component: Auth,
    children: [
      {
        path: 'login',
        name: 'login',
        component: Login,
      },
    ],
  },
  {
    path: '/dash/',
    component: Dash,
    children: [
      {
        path: '',
        name: 'home',
        component: Home,
      },
      {
        path: 'tx',
        name: 'tx-list',
        component: TxList,
      },
      {
        path: 'tx/merge',
        name: 'tx-merge',
        component: TxMerge,
      },
      {
        path: 'tx/:transferId',
        name: 'tx-detail',
        component: TxDetail,
      },
      {
        path: 'user',
        name: 'user-list',
        component: UserList,
      },
      {
        path: 'user/create',
        name: 'user-create',
        component: UserCreate,
      },
      {
        path: 'user/:userId',
        name: 'user-detail',
        component: UserDetail,
      },
      {
        path: 'wallet',
        name: 'wallet-list',
        component: WalletList,
      },
      {
        path: 'wallet/:walletId',
        name: 'wallet-detail',
        component: WalletDetail,
      },
      {
        path: 'organization',
        name: 'organization-list',
        component: OrganizationList,
      },
      {
        path: 'organization/create',
        name: 'organization-create',
        component: OrganizationCreate,
      },
      {
        path: 'organization/:organizationId',
        name: 'organization-detail',
        component: OrganizationDetail,
      },
      {
        path: 'log',
        name: 'log',
        component: Log,
      },
      {
        path: 'config',
        name: 'config',
        component: Config,
      },
    ],
  },
  {
    path: '500',
    name: 'error',
    component: Error,
  },
  {
    path: '*',
    name: 'not-found',
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name !== 'login' && !cookie.get(TOKEN)) return next({ name: 'login' });
  return next();
});

export default router;
