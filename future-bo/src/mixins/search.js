import * as organizationServ from '@/services/organization';

export default {
  methods: {
    searchUser() {

    },
    searchWallet() {

    },
    searchOrganization(name) {
      return organizationServ.list(0, 10, { name }).then((res) => {
        const { list } = res;

        return list.map((item) => ({
          value: item.organizationId,
          label: item.name,
        }));
      });
    },
  },
};
