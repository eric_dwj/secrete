import * as tx from '@/constants/transaction';
import * as role from '@/constants/role';
import * as organization from '@/constants/organization';

export const txStatus = {
  methods: {
    tagType(status) {
      switch (status) {
        case tx.STATUS_CANCELLED:
        case tx.STATUS_REJECTED:
          return 'danger';

        case tx.STATUS_CONFIRMED:
        case tx.STATUS_APPROVED:
          return 'success';

        default: 
          return 'warning';
      }
    },
  },
};

export const userStatus = {
  methods: {
    tagType(status) {
      switch (status) {
        case organization.USER_STATUS_PENDING:
          return 'info';

        case organization.USER_STATUS_LOCKED:
          return 'danger';

        case organization.USER_STATUS_ENABLED:
          return 'success';

        default: 
          return 'warning';
      }
    },
  },
};

export const roleStatus = {
  methods: {
    tagType(status) {
      switch (status) {
        case role.STATUS_ENABLED:
          return 'success';

        case role.STATUS_PENDING:
          return 'info';

        case role.STATUS_DISABLED:
        case role.STATUS_REMOVING:
          return 'danger';
          
        default:
          return 'warning';
      }
    },
  },
};
