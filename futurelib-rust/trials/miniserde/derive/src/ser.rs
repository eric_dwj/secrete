use crate::{attr, bound};
use proc_macro2::{Span, TokenStream};
use quote::quote;
use syn::{
    parse_quote, Data, DataEnum, DataStruct, DeriveInput, Error, Fields, FieldsNamed, Ident, Result,
};

pub fn derive(input: DeriveInput) -> Result<TokenStream> {
    match &input.data {
        Data::Struct(DataStruct {
            fields: Fields::Named(fields),
            ..
        }) => derive_struct(&input, &fields),
        Data::Enum(enumeration) => derive_enum(&input, enumeration),
        _ => Err(Error::new(
            Span::call_site(),
            "currently only structs with named fields are supported",
        )),
    }
}


/*        macro_rules! def_or { 
            ($func:ident,$ret:expr, $pre:ident)=> { $pre=$func($ret); &$pre  };
            (,$ret:expr, $pre:ident) => { $ret } 
              
          } */


fn derive_struct(input: &DeriveInput, fields: &FieldsNamed) -> Result<TokenStream> {
    let ident = &input.ident;
    let (impl_generics, ty_generics, where_clause) = input.generics.split_for_impl();
    let dummy = Ident::new(
        &format!("_IMPL_MINISERIALIZE_FOR_{}", ident),
        Span::call_site(),
    );

    let fieldname = &fields.named.iter().map(|f| &f.ident).collect::<Vec<_>>();
    let fieldstr = fields
        .named
        .iter()
        .map(attr::name_of_field)
        .collect::<Result<Vec<_>>>()?;
    let fieldfunc = fields
        .named
        .iter()
        .map(attr::serfunc_of_field).map(|a| match a {
            Some(b) => Some(Ident::new(&b,Span::call_site())),
            None =>None
            }
        )
        .collect::<Vec<_>>();
    //let funcindicator:Vec<bool>=fieldfunc.iter().map(|a|a.is_some()).collect();
    //println!("{:?}",fieldfunc);
    let index = 0usize..;

    let wrapper_generics = bound::with_lifetime_bound(&input.generics, "'__a");
    let (wrapper_impl_generics, wrapper_ty_generics, _) = wrapper_generics.split_for_impl();
    let bound = parse_quote!(miniserde::Serialize);
    let bounded_where_clause = bound::where_clause_with_bound(&input.generics, bound);
   // let substream: TokenStream = "self.preser=".parse().unwrap();
    let funcstream:Vec<TokenStream>=fieldfunc.iter().zip(fieldname.iter()).map(| (a,b) | match a {Some(name) =>
        format!("{{ self.preser={}(&self.data.{});  &self.preser}} ",name,b.as_ref().unwrap().to_string()).parse().unwrap()
        , 
        None => format!("&self.data.{}",b.as_ref().unwrap().to_string()).parse().unwrap()
        }  ).collect();

    Ok(quote! {

        #[allow(non_upper_case_globals)]
        const #dummy: () = {
            impl #impl_generics miniserde::Serialize for #ident #ty_generics #bounded_where_clause {
                fn begin(&self) -> miniserde::ser::Fragment {
                    miniserde::ser::Fragment::Map(miniserde::export::Box::new(__Map {
                        data: self,
                        state: 0,
                        preser: Box::new("".to_string())
                    }))
                }
            }

            struct __Map #wrapper_impl_generics #where_clause {
                data: &'__a #ident #ty_generics,
                state: miniserde::export::usize,
                preser:  Box<dyn Serialize>
            }

            impl #wrapper_impl_generics miniserde::ser::Map for __Map #wrapper_ty_generics #bounded_where_clause {
                fn next(&mut self) -> miniserde::export::Option<(miniserde::export::Cow<miniserde::export::str>,  &dyn Serialize)> {
                    let __state = self.state;
                    self.state = __state + 1;
                    
                    match __state {
                        #(
                            #index => {  miniserde::export::Some((
                                miniserde::export::Cow::Borrowed(#fieldstr),
                               // if let Some(fname)= #fieldfunc ? { miniserde::export::Cow::Owned(#fieldfunc(&self.data.#fieldname)) } else  { miniserde::export::Cow::Borrowed(&self.data.#fieldname) },
                               #funcstream,
                     //   if let Some(fname)= #fieldfunc  {
                              
                            ))} ,
                        )*
                        _ => miniserde::export::None,
                    }
                }
            }
        };
    })
}

fn derive_enum(input: &DeriveInput, enumeration: &DataEnum) -> Result<TokenStream> {
    if input.generics.lt_token.is_some() || input.generics.where_clause.is_some() {
        return Err(Error::new(
            Span::call_site(),
            "Enums with generics are not supported",
        ));
    }

    let ident = &input.ident;
    let dummy = Ident::new(
        &format!("_IMPL_MINISERIALIZE_FOR_{}", ident),
        Span::call_site(),
    );

    let var_idents = enumeration
        .variants
        .iter()
        .map(|variant| match variant.fields {
            Fields::Unit => Ok(&variant.ident),
            _ => Err(Error::new_spanned(
                variant,
                "Invalid variant: only simple enum variants without fields are supported",
            )),
        })
        .collect::<Result<Vec<_>>>()?;
    let names = enumeration
        .variants
        .iter()
        .map(attr::name_of_variant)
        .collect::<Result<Vec<_>>>()?;

    Ok(quote! {
        #[allow(non_upper_case_globals)]
        const #dummy: () = {
            impl miniserde::Serialize for #ident {
                fn begin(&self) -> miniserde::ser::Fragment {
                    match self {
                        #(
                            #ident::#var_idents => {
                                miniserde::ser::Fragment::Str(miniserde::export::Cow::Borrowed(#names))
                            }
                        )*
                    }
                }
            }
        };
    })
}
