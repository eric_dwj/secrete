use syn::{Attribute, Error, Field, Lit, Meta, NestedMeta, Result, Variant};
use proc_macro2::{  Span,  TokenStream,Group, Delimiter, Ident};
use quote::{TokenStreamExt, ToTokens};

/// Find the value of a #[serde(rename = "...")] attribute.
fn attr_rename(attrs: &[Attribute]) -> Result<Option<String>> {
    let mut rename = None;

    for attr in attrs {
        if !attr.path.is_ident("serde") {
            continue;
        }

        let list = match attr.parse_meta()? {
            Meta::List(list) => list,
            other => return Err(Error::new_spanned(other, "unsupported attribute")),
        };

        for meta in &list.nested {
            if let NestedMeta::Meta(Meta::NameValue(value)) = meta {
                if value.path.is_ident("rename") {
                    if let Lit::Str(s) = &value.lit {
                        if rename.is_some() {
                            return Err(Error::new_spanned(meta, "duplicate rename attribute"));
                        }
                        rename = Some(s.value());
                        continue;
                    }
                }
                if value.path.is_ident("serialize_with") {continue}
                if value.path.is_ident("deserialize_with") {continue}
                if value.path.is_ident("default") {continue}
            }
            return Err(Error::new_spanned(meta, "unsupported attribute"));
        }
    }

    Ok(rename)
}
fn attr_ser_with(attrs: &[Attribute]) -> Result<Option<String>> {
    let mut fnname = None;

    for attr in attrs {
        if !attr.path.is_ident("serde") {
            continue;
        }

        let list = match attr.parse_meta()? {
            Meta::List(list) => list,
            other => return Err(Error::new_spanned(other, "unsupported attribute")),
        };

        for meta in &list.nested {
            if let NestedMeta::Meta(Meta::NameValue(value)) = meta {
                if value.path.is_ident("serialize_with") {
                    if let Lit::Str(s) = &value.lit {
                        if fnname.is_some() {
                            return Err(Error::new_spanned(meta, "duplicate serialize_with attribute"));
                        }
                        fnname = Some(s.value());
                        continue;
                    }
                }
                if value.path.is_ident("rename") {continue}
                if value.path.is_ident("deserialize_with") {continue}
                if value.path.is_ident("default") {continue}
            }
            return Err(Error::new_spanned(meta, "unsupported attribute"));
        }
    }

    Ok(fnname)
}

fn attr_default(attrs: &[Attribute]) -> Result<Option<String>> {
    let mut fnname = None;

    for attr in attrs {
        if !attr.path.is_ident("serde") {
            continue;
        }

        let list = match attr.parse_meta()? {
            Meta::List(list) => list,
            other => return Err(Error::new_spanned(other, "unsupported attribute")),
        };

        for meta in &list.nested {
            if let NestedMeta::Meta(Meta::NameValue(value)) = meta {
                if value.path.is_ident("default") {
                    if let Lit::Str(s) = &value.lit {
                        if fnname.is_some() {
                            return Err(Error::new_spanned(meta, "duplicate default attribute"));
                        }
                        fnname = Some(s.value());
                        continue;
                    }
                }
                if value.path.is_ident("rename") {continue}
                if value.path.is_ident("deserialize_with") {continue}
                if value.path.is_ident("serialize_with") {continue}
            }
            return Err(Error::new_spanned(meta, "unsupported attribute"));
        }
    }

    Ok(fnname)
}

fn attr_deser_with(attrs: &[Attribute]) -> Result<Option<String>> {
    let mut fnname = None;

    for attr in attrs {
        if !attr.path.is_ident("serde") {
            continue;
        }

        let list = match attr.parse_meta()? {
            Meta::List(list) => list,
            other => return Err(Error::new_spanned(other, "unsupported attribute")),
        };

        for meta in &list.nested {
            if let NestedMeta::Meta(Meta::NameValue(value)) = meta {
                if value.path.is_ident("deserialize_with") {
                    if let Lit::Str(s) = &value.lit {
                        if fnname.is_some() {
                            return Err(Error::new_spanned(meta, "duplicate deserialize_with attribute"));
                        }
                        fnname = Some(s.value());
                        continue;
                    }
                }
                if value.path.is_ident("rename") {continue}
                if value.path.is_ident("serialize_with") {continue}
                if value.path.is_ident("default") {continue}
            }
            return Err(Error::new_spanned(meta, "unsupported attribute"));
        }
    }

    Ok(fnname)
}



/// Determine the name of a field, respecting a rename attribute.
pub fn name_of_field(field: &Field) -> Result<String> {
    let rename = attr_rename(&field.attrs)?;
    Ok(rename.unwrap_or_else(|| field.ident.as_ref().unwrap().to_string()))
}

pub struct WrappedOption(pub Option<String>);

impl ToTokens for WrappedOption {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match &self.0
        {
            Some(e)=>{ 
                tokens.append(Ident::new("Some",Span::call_site()));
                let mut tt:TokenStream =TokenStream::new();
                e.to_tokens(& mut tt);
                tokens.append(Group::new(Delimiter::Parenthesis,tt)); },
            None => { tokens.append(Ident::new("None",Span::call_site()))}
        };
       
    }
}

/// Determine the function of a field, respecting a rename attribute.
pub fn serfunc_of_field(field: &Field) -> Option<String> {
    let rename = match attr_ser_with(&field.attrs)
{
    Ok(r) =>r,
    Err(_) =>return None
};
    match rename {
        Some(r) => Some(r),
        None => None
    }
}


pub fn deserfunc_of_field(field: &Field) -> Option<String> {
    let rename = match attr_deser_with(&field.attrs)
    {
        Ok(r) =>r,
        Err(_) =>return None
    };
        match rename {
            Some(r) => Some(r),
            None => None
        }
}


pub fn default_of_field(field: &Field) -> Option<String> {
    let rename = match attr_default(&field.attrs)
    {
        Ok(r) =>r,
        Err(_) =>return None
    };
        match rename {
            Some(r) => Some(r),
            None => None
        }
}


/// Determine the name of a variant, respecting a rename attribute.
pub fn name_of_variant(var: &Variant) -> Result<String> {
    let rename =attr_rename(&var.attrs)?;
    Ok(rename.unwrap_or_else(|| var.ident.to_string()))
}
