use miniserde::{json, Serialize, Deserialize,RawValue, Error};
use num_bigint_dig::BigUint;
use std::str::FromStr;

#[derive( Debug)]
struct Marker{}

#[derive(Serialize, Deserialize, Debug)]
struct Example {
    code: u32,
    #[serde(serialize_with = "path")]
    message: String,
    #[serde(serialize_with = "dpath", deserialize_with="deserpath")]
    nmessage: String,
    #[serde(serialize_with = "sermark", deserialize_with="desermark")]
    mrk:Marker,
    #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
    b:BigUint
}
fn sermark(st:&Marker)-> Box<String>
{
 Box::new("*".to_string())
}
fn desermark(st:RawValue)-> Result<Marker,miniserde::Error>
{
  let  _:String=json::from_str(st.get()).unwrap();

 Ok(Marker{})
}

fn int_as_base64(key: &BigUint) -> Box<String>
{
  Box::new(base64::encode(key.to_bytes_be()))
}

pub fn int_from_base64(st: RawValue) -> Result<BigUint, Error>
{
  let bytes=match base64::decode(&st.get_dequoted_string()?)
  {
    Ok(r) =>r,
    Err(e) => 
    {println!("{} {:?}",st.get(),e);
      return Err(Error{})}
  };
  Ok(BigUint::from_bytes_be(&bytes))
}


fn path(st:&str)-> Box<String>
{
 Box::new("::".to_string()+&st.to_string())
}
fn dpath(st:&str)-> Box<String>
{
  Box::new( "++".to_string()+&st.to_string())
}

fn crop_letters(s: &mut String, pos: usize) {
  match s.char_indices().nth(pos) {
      Some((pos, _)) => {
          s.drain(..pos);
      }
      None => {
          s.clear();
      }
  }
}

fn deserpath(st:RawValue)-> Result<String,miniserde::Error>
{
  let mut  ret:String=json::from_str(st.get()).unwrap();
  crop_letters(&mut ret,2);
 Ok(ret)
}


fn main() {
  
  let mut example = Example {
    code: 200,
    message: "reminiscent of Serde".to_owned(),
    nmessage: "nreminiscent of Serde".to_owned(),
    mrk:Marker{},
    b:BigUint::from_str("0").unwrap()
};
let j = json::to_string(&example);
    println!("{}", j);
  example.message="new message".to_owned();
  let j = json::to_string(&example);
  let s="juststring";
    println!("{}", j);
    println!("{}", json::to_string(s)); 
    let k:Example = json::from_str(&j).unwrap();
    println!("{}", k.message);
    println!("{}", k.nmessage);
}
