/* eslint-disable prefer-arrow-callback */
/* eslint-disable mocha/no-mocha-arrows */
/* eslint-disable mocha/no-setup-in-describe */
/* eslint-disable class-methods-use-this */
/* eslint-disable import/no-unresolved */
/* eslint-disable guard-for-in */
/* global CryptolibClient, request */

function formRpcParam (method, ...params) {
  return {
    jsonrpc: '2.0',
    id: 1,
    method,
    params
  };
}
const clientObj = new CryptolibClient();
const serverObj = {
  config: {
    server: window.location.origin
  },
  async post (cmd, ...params) {
    const { server } = this.config;
    return request.post(`${server}/`).send(formRpcParam(cmd, ...params));
  },
  async get (path = '/') {
    const { server } = this.config;
    return request.get(`${server}${path}`);
  }
};

const wallet_id = Math.floor(Math.random() * 1000000000);
let call_id = 0;

async function rpc_call (cmd, params) {
  const label = `rpc_call ${wallet_id} ${cmd} ${call_id}`;
  call_id += 1;
  console.time(label);
  const resp = await serverObj.post(cmd, params);
  console.timeEnd(label);
  const { result, error } = resp.body;
  if (error) throw Error(`cryptolib error: ${JSON.stringify(error)}`);
  return { result };
}

function generateKeyClient (tx, key_id, recovery_service_provider, passphrase) {
  it(`generateKeyClient(${key_id})`, async function () {
    this.timeout(100000);
    const label = `generate Key: ${key_id}`;
    console.time(label);
    const { result, error } = await clientObj.generateKeyClient(String(key_id), passphrase, recovery_service_provider, rpc_call);
    console.timeEnd(label);
    if (error) throw new Error(error);
    Object.assign(tx, result);
  });
}

function run () {
  describe('Test key generation', () => {
    const out = {};
    const w = { _id: wallet_id, recovery_service_provider: 'sbi_japannext', passphrase: '5df2222222' };
    for (let i = 0; i < 50; ++i) {
      generateKeyClient(out, w._id, w.recovery_service_provider, w.passphrase);
      w._id += 1;
    }
  });
}

export default class StoryTest {
  constructor (params) {
    Object.assign(this, params);
  }

  run () {
    run();
  }
}
