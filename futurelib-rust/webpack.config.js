const path = require("path");
const CopyPlugin = require("copy-webpack-plugin");
const WasmPackPlugin = require("@wasm-tool/wasm-pack-plugin");
const webpackMerge = require('webpack-merge');
const nodeExternals = require('webpack-node-externals');

const dist = path.resolve(__dirname, "dist");

const baseconf = {
  entry: './browser/browser.mjs',
  mode: 'production',
  target: 'web',
  experiments:{syncWebAssembly: true},
  devServer: {
    contentBase: dist,
  },
  output: {
    filename: 'cryptolib.bundle.js',
    path: path.resolve(__dirname, 'dist'),
    libraryExport: 'default',
    libraryTarget: 'umd',
    library: 'CryptolibClient',
  },
  plugins: [
    // new BundleAnalyzerPlugin()
    new CopyPlugin([
      path.resolve(__dirname, "static")
    ]),

    new WasmPackPlugin({
      crateDirectory: __dirname,
    }),
  ]
};

const publicPath = process.env.PUBLIC_PATH || '/';
console.log(`wasm is downloaded at publicPath = ${publicPath}`);

const confs = {
  web: webpackMerge(baseconf, {
    target: 'web',
    mode: 'production',
    output: {
      path: path.resolve(__dirname, 'dist/webpack'),
      filename: 'cryptolib.web.js',
      publicPath,
    },
    module: {
      rules: [
        {
          test: /rustcryptolib\.wasm$/,
          type: 'javascript/auto',
          loader: 'file-loader',
          options: {
            publicPath
          }
        }
      ]
    },
    resolve: {
      alias: {
        './config': path.resolve(__dirname, 'js/web/config.js'),
        './bindings': path.resolve(__dirname, 'CryptolibNative.js'),
        './keystore/KeyStore': path.resolve(__dirname, 'js/web/KeyStore.js'),
        '../utils/log4js': path.resolve(__dirname, 'js/web/log4js.js'),
      }
    },
    externals: ['fs'],
  }),
  node: webpackMerge(baseconf, {
    target: 'node',
    output: {
      filename: 'cryptolib.node.js'
    },
    externals: [nodeExternals()]
  })
};

module.exports = confs.web;

