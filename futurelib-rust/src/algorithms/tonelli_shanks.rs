use crate::common_defs::*;

fn jacobi(a_in: &BigUint, b_in: &BigUint) -> i64
{
  assert_eq!(b_in.is_odd(), true);
  let mut b = b_in.clone();
  let mut a = a_in % b_in;
  let mut tmp;
  let mut result = 1;
  //thi seems wasteful, but I don't know a good way
  //let eight=BigUint::from_u64(8).unwrap();
  //let three=BigUint::from_u64(3).unwrap();

  while !a.is_zero()
  {
    let mut i: u32 = 0;
    while !a.is_odd()
    {
      i += 1;
      a >>= 1;
    }

    if i % 2 == 1 && (&b % &*UINT_EIGHT == *UINT_THREE || &b % &*UINT_EIGHT == *UINT_FIVE)
    {
      result = -result;
    }

    if &a % &*UINT_FOUR == *UINT_THREE && &b % &*UINT_FOUR == *UINT_THREE
    {
      result = -result;
    }
    tmp = a;
    a = b;
    b = tmp;

    a %= &b;
  }

  if b == *UINT_ONE
  {
    result
  }
  else
  {
    0
  }
}

pub struct TonneliPrep
{
  //ModularArithmetic ma;
  p: BigUint,
  s: usize,
  q: BigUint,
  dq: BigUint,
  c0: BigUint,
  dcheck: BigUint,
  mcheck: BigUint,
  mdcheck: BigUint,
  prec: Vec<BigUint>,
  preb: Vec<BigUint>,
}

impl TonneliPrep
{
  pub fn new(p: &BigUint) -> TonneliPrep
  {
    let modulo = p.clone();
    //this->p=p;
    let mut s: usize = 0;
    let mut q = p - &BigUint::one();
    while !q.is_odd()
    {
      s += 1;
      q >>= 1;
    }
    let mut z = BigUint::from_u64(129).unwrap();

    while jacobi(&z, &modulo) != -1
    {
      z += &*UINT_ONE;
      z %= &modulo;
    }
    let c = z.modpow(&q, &modulo);
    let dq = (&q + &*UINT_ONE) >> 1;
    let mut preb = Vec::<BigUint>::new();
    let mut prec = Vec::<BigUint>::new();
    for i in 1..s
    {
      let offs: usize = (s - i - 1).try_into().unwrap();
      let smpl = BigUint::one() << offs;
      let b = c.modpow(&smpl, &modulo);
      prec.push(b.modpow(&*UINT_TWO, &modulo));
      preb.push(b);
    }
    let subp = p >> 1;
    let dcheck = UINT_TWO.modpow(&subp, &modulo);
    let mcheck = (p - &*UINT_ONE).modpow(&subp, &modulo);
    let mdcheck = (p - &*UINT_TWO).modpow(&subp, &modulo);
    TonneliPrep {
      p: modulo,
      c0: c,
      preb: preb,
      prec: prec,
      dcheck: dcheck,
      mcheck: mcheck,
      mdcheck: mdcheck,
      s: s,
      dq: dq,
      q: q,
    }
  }

  pub fn tonelli_shanks_check(&self, x: &BigUint) -> i64
  {
    let subp = (&self.p) >> 1;
    let check = x.modpow(&subp, &self.p);
    let mut ret: i64 = 0;
    if check == *UINT_ONE
    {
      ret |= 1;
    }

    if check == self.dcheck
    {
      ret |= 2;
    }
    if check == self.mcheck
    {
      ret |= 4;
    }
    if check == self.mdcheck
    {
      ret |= 8;
    }

    ret
  }

  pub fn tonelli_shanks_prepped(&self, x: &BigUint) -> Option<BigUint>
  {
    let mut mm = self.s;
    let mut c = &self.c0;
    let mut t = x.modpow(&self.q, &self.p);
    let mut rr = x.modpow(&self.dq, &self.p);
    let mut first = true;
    let mut ctmp: BigUint;
    loop
    {
      if t.is_zero()
      {
        return Some(BigUint::zero());
      }
      if t == *UINT_ONE
      {
        return Some(rr);
      }
      let mut mt = t.clone();
      let mut i: usize = 1;
      while i < mm
      {
        mt = mt.modpow(&*UINT_TWO, &self.p);
        if mt == *UINT_ONE
        {
          break;
        }
        i += 1;
      }
      if i == mm
      {
        return None;
      }
      if mt != *UINT_ONE
      {
        return None;
      }

      if first
      {
        c = &self.prec[i - 1];
        rr = (rr * &self.preb[i - 1]) % &self.p;
      }
      else
      {
        let offs: usize = (mm - i - 1).try_into().unwrap();
        let smpl = BigUint::one() << offs;
        let b = c.modpow(&smpl, &self.p);
        ctmp = (&b * &b) % &self.p;
        c = &ctmp;
        rr = (&rr * &b) % &self.p;
      }

      mm = i;
      t = (&t * c) % &self.p;
      first = false;
    }
  }
}

pub fn square_root_safe(x: &BigUint, n: &BigUint, p: &TonneliPrep, q: &TonneliPrep) -> Option<BigUint>
{
  let pcheck = p.tonelli_shanks_check(x);
  let qcheck = q.tonelli_shanks_check(x);
  let ccheck = pcheck & qcheck;
  if ccheck == 0
  {
    return None;
  }
  let mut tx = x;
  let xtmp;
  match ccheck.trailing_zeros()
  {
    0 =>
    {}
    1 =>
    {
      xtmp = (x << 1) % n;
      tx = &xtmp
    }
    2 =>
    {
      xtmp = n - (x % n);
      tx = &xtmp
    }
    3 =>
    {
      xtmp = n - (&(x << 1) % n);
      tx = &xtmp
    }
    _ => return None,
  };

  let srt_p = p.tonelli_shanks_prepped(tx);
  let srt_q = q.tonelli_shanks_prepped(tx);
  if srt_q.is_none() || srt_p.is_none()
  {
    return None;
  }
  let srt_pw = srt_p.unwrap();
  let srt_qw = srt_q.unwrap();

  let (mut i0, mut i1, mut i2) = (0, 1, 2);
  let mut g: [BigInt; 3] = [
    BigInt::from_biguint(Sign::Plus, p.p.clone()),
    BigInt::from_biguint(Sign::Plus, q.p.clone()),
    BigInt::zero(),
  ];
  let mut s: [BigInt; 3] = [BigInt::one(), BigInt::zero(), BigInt::zero()];
  let mut t: [BigInt; 3] = [BigInt::zero(), BigInt::one(), BigInt::zero()];
  while !g[i1].is_zero()
  {
    let qi = &g[i0] / &g[i1];
    let mut tmp;
    tmp = &g[i0] - &qi * &g[i1];
    g[i2] = tmp;
    tmp = &s[i0] - &qi * &s[i1];
    s[i2] = tmp;
    tmp = &t[i0] - &qi * &t[i1];
    t[i2] = tmp;
    let tm = i0;
    i0 = i1;
    i1 = i2;
    i2 = tm;
  }
  if g[i0] != *INT_ONE
  {
    return None;
  }
  let tm1 = BigInt::from_biguint(Sign::Plus, srt_qw * &p.p);
  let tm2 = BigInt::from_biguint(Sign::Plus, srt_pw * &q.p);

  let mut ores = tm1 * &s[i0] + tm2 * &t[i0];
  let intn = BigInt::from_biguint(Sign::Plus, n.clone());
  while ores < BigInt::zero()
  {
    ores %= &intn;
    ores += &intn;
  }
  return Some(ores.to_biguint().unwrap() % n);
}
