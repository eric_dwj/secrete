/* Mostly stolen from https://github.com/dalek-cryptography/ed25519-dalek . Rip and tear! */
use crate::common_defs::*;
use core::fmt;
use core::fmt::Debug;
use core::fmt::Display;
pub use curve25519_dalek::constants;
use curve25519_dalek::digest::generic_array::typenum::U64;
use curve25519_dalek::edwards::CompressedEdwardsY;
use curve25519_dalek::edwards::EdwardsPoint;
use curve25519_dalek::scalar::Scalar;
use rand::{CryptoRng, RngCore};
pub use sha2::Sha512;
use zeroize::Zeroize;

/// The length of a ed25519 `Signature`, in bytes.
pub const SIGNATURE_LENGTH: usize = 64;

/// The length of a ed25519 `SecretKey`, in bytes.
pub const SECRET_KEY_LENGTH: usize = 32;

/// The length of an ed25519 `PublicKey`, in bytes.
pub const PUBLIC_KEY_LENGTH: usize = 32;

#[derive(Zeroize, Clone)]
#[zeroize(drop)] // Overwrite secret key material with null bytes when it goes out of scope.
pub struct SecretKey(pub [u8; SECRET_KEY_LENGTH]);

#[derive(Zeroize)]
#[zeroize(drop)] // Overwrite secret key material with null bytes when it goes out of scope.
pub struct ExpandedSecretKey
{
  pub key: Scalar,
  pub nonce: [u8; 32],
}

#[allow(dead_code)]
pub fn divide_scalar_bytes_by_cofactor(scalar: &mut [u8; 32])
{
  let mut low = 0u8;
  for i in scalar.iter_mut().rev()
  {
    let r = *i & 0b00000111; // save remainder
    *i >>= 3; // divide by 8
    *i += low;
    low = r << 5;
  }
}

#[allow(dead_code)]
pub fn multiply_scalar_bytes_by_cofactor(scalar: &mut [u8; 32])
{
  let mut high = 0u8;
  for i in scalar.iter_mut()
  {
    let r = *i & 0b11100000; // carry bits
    *i <<= 3; // multiply by 8
    *i += high;
    high = r >> 5;
  }
}

impl<'a> From<&'a SecretKey> for ExpandedSecretKey
{
  fn from(secret_key: &'a SecretKey) -> ExpandedSecretKey
  {
    let mut h: Sha512 = Sha512::default();
    let mut hash: [u8; 64] = [0u8; 64];
    let mut lower: [u8; 32] = [0u8; 32];
    let mut upper: [u8; 32] = [0u8; 32];

    h.update(secret_key.as_bytes());
    hash.copy_from_slice(h.finalize().as_slice());

    lower.copy_from_slice(&hash[00..32]);
    upper.copy_from_slice(&hash[32..64]);

    lower[0] &= 248;
    lower[31] &= 63;
    lower[31] |= 64;

    ExpandedSecretKey {
      key: Scalar::from_bits(lower),
      nonce: upper,
    }
  }
}
impl ExpandedSecretKey
{
  #[allow(non_snake_case, dead_code)]
  pub fn sign_prehashed<'a, D>(
    &self, prehashed_message: D, public_key: &PublicKey, context: Option<&'a [u8]>,
  ) -> Result<Signature, SignatureError>
  where
    D: Digest<OutputSize = U64>,
  {
    let mut h: Sha512;
    let mut prehash: [u8; 64] = [0u8; 64];
    let R: CompressedEdwardsY;
    let r: Scalar;
    let s: Scalar;
    let k: Scalar;

    let ctx: &[u8] = context.unwrap_or(b""); // By default, the context is an empty string.

    if ctx.len() > 255
    {
      return Err(SignatureError(InternalError::VerifyError));
    }

    let ctx_len: u8 = ctx.len() as u8;

    // Get the result of the pre-hashed message.
    prehash.copy_from_slice(prehashed_message.finalize().as_slice());

    // This is the dumbest, ten-years-late, non-admission of fucking up the
    // domain separation I have ever seen.  Why am I still required to put
    // the upper half "prefix" of the hashed "secret key" in here?  Why
    // can't the user just supply their own nonce and decide for themselves
    // whether or not they want a deterministic signature scheme?  Why does
    // the message go into what's ostensibly the signature domain separation
    // hash?  Why wasn't there always a way to provide a context string?
    //
    // ...
    //
    // This is a really fucking stupid bandaid, and the damned scheme is
    // still bleeding from malleability, for fuck's sake.
    h = Sha512::new()
      .chain(b"SigEd25519 no Ed25519 collisions")
      .chain(&[1]) // Ed25519ph
      .chain(&[ctx_len])
      .chain(ctx)
      .chain(&self.nonce)
      .chain(&prehash[..]);

    r = Scalar::from_hash(h);
    R = (&r * &constants::ED25519_BASEPOINT_TABLE).compress();

    h = Sha512::new()
      .chain(b"SigEd25519 no Ed25519 collisions")
      .chain(&[1]) // Ed25519ph
      .chain(&[ctx_len])
      .chain(ctx)
      .chain(R.as_bytes())
      .chain(public_key.as_bytes())
      .chain(&prehash[..]);

    k = Scalar::from_hash(h);
    s = &(&k * &self.key) + &r;

    Ok(Signature { R, s }.into())
  }
}
impl AsRef<[u8]> for SecretKey
{
  fn as_ref(&self) -> &[u8]
  {
    &self.0
  }
}

impl TryFrom<Vec<u8>> for SecretKey
{
  type Error = String;

  fn try_from(value: Vec<u8>) -> Result<Self, Self::Error>
  {
    match &value[..].try_into()
    {
      Ok(val) => Ok(SecretKey(*val)),
      Err(e) => Err(e.to_string()),
    }
  }
}
impl SecretKey
{
  /// Convert this secret key to a byte array.
  #[inline]
  pub fn to_bytes(&self) -> [u8; SECRET_KEY_LENGTH]
  {
    self.0
  }

  /// View this secret key as a byte array.
  #[inline]
  pub fn as_bytes<'a>(&'a self) -> &'a [u8; SECRET_KEY_LENGTH]
  {
    &self.0
  }
  #[inline]
  pub fn from_bytes(bytes: &[u8]) -> Result<SecretKey, SignatureError>
  {
    if bytes.len() != SECRET_KEY_LENGTH
    {
      return Err(SignatureError(InternalError::BytesLengthError {
        name: "SecretKey",
        length: SECRET_KEY_LENGTH,
      }));
    }
    let mut bits: [u8; 32] = [0u8; 32];
    bits.copy_from_slice(&bytes[..32]);

    Ok(SecretKey(bits))
  }
  pub fn generate<T>(csprng: &mut T) -> SecretKey
  where
    T: CryptoRng + RngCore,
  {
    let mut sk: SecretKey = SecretKey([0u8; 32]);

    csprng.fill_bytes(&mut sk.0);

    sk
  }
}

#[allow(dead_code)]
#[derive(Clone, Copy, Debug, Eq, PartialEq, Hash)]
pub enum InternalError
{
  PointDecompressionError,
  ScalarFormatError,
  /// An error in the length of bytes handed to a constructor.
  ///
  /// To use this, pass a string specifying the `name` of the type which is
  /// returning the error, and the `length` in bytes which its constructor
  /// expects.
  BytesLengthError
  {
    name: &'static str,
    length: usize,
  },
  /// The verification equation wasn't satisfied
  VerifyError,
}

impl Display for InternalError
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
  {
    match *self
    {
      InternalError::PointDecompressionError => write!(f, "Cannot decompress Edwards point"),
      InternalError::ScalarFormatError => write!(f, "Cannot use scalar with high-bit set"),
      InternalError::BytesLengthError { name: n, length: l } => write!(f, "{} must be {} bytes in length", n, l),
      InternalError::VerifyError => write!(f, "Verification equation was not satisfied"),
    }
  }
}

#[derive(Clone, Copy, Eq, PartialEq, Hash, Debug)]
pub struct SignatureError(pub InternalError);
impl Display for SignatureError
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
  {
    match self.0
    {
      InternalError::PointDecompressionError => write!(f, "Cannot decompress Edwards point"),
      InternalError::ScalarFormatError => write!(f, "Cannot use scalar with high-bit set"),
      InternalError::BytesLengthError { name: n, length: l } => write!(f, "{} must be {} bytes in length", n, l),
      InternalError::VerifyError => write!(f, "Verification equation was not satisfied"),
    }
  }
}
/// An ed25519 public key.
#[derive(Copy, Clone, Default, Eq, PartialEq)]
pub struct PublicKey(pub CompressedEdwardsY, pub EdwardsPoint);

impl Debug for PublicKey
{
  fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> ::core::fmt::Result
  {
    write!(f, "PublicKey({:?}), {:?})", self.0, self.1)
  }
}

impl AsRef<[u8]> for PublicKey
{
  fn as_ref(&self) -> &[u8]
  {
    self.as_bytes()
  }
}

impl<'a> From<&'a SecretKey> for PublicKey
{
  /// Derive this public key from its corresponding `SecretKey`.
  fn from(secret_key: &SecretKey) -> PublicKey
  {
    let mut h: Sha512 = Sha512::new();
    let mut hash: [u8; 64] = [0u8; 64];
    let mut digest: [u8; 32] = [0u8; 32];

    h.update(secret_key.as_bytes());
    hash.copy_from_slice(h.finalize().as_slice());

    digest.copy_from_slice(&hash[..32]);

    PublicKey::mangle_scalar_bits_and_multiply_by_basepoint_to_produce_public_key(&mut digest)
  }
}

impl TryFrom<Vec<u8>> for PublicKey
{
  type Error = String;

  fn try_from(value: Vec<u8>) -> Result<Self, Self::Error>
  {
    match PublicKey::from_bytes(&value[..])
    {
      Ok(val) => Ok(val),
      Err(e) => Err(format!("{:?}", e)),
    }
  }
}

impl TryFrom<[u8; 32]> for PublicKey
{
  type Error = String;

  fn try_from(value: [u8; 32]) -> Result<Self, Self::Error>
  {
    match PublicKey::from_bytes(&value[..])
    {
      Ok(val) => Ok(val),
      Err(e) => Err(format!("{:?}", e)),
    }
  }
}

impl PublicKey
{
  /// Convert this public key to a byte array.
  #[inline]
  pub fn to_bytes(&self) -> [u8; PUBLIC_KEY_LENGTH]
  {
    self.0.to_bytes()
  }

  /// View this public key as a byte array.
  #[inline]
  pub fn as_bytes<'a>(&'a self) -> &'a [u8; PUBLIC_KEY_LENGTH]
  {
    &(self.0).0
  }

  /// Construct a `PublicKey` from a slice of bytes.
  ///
  /// # Warning
  ///
  /// The caller is responsible for ensuring that the bytes passed into this
  /// method actually represent a `curve25519_dalek::curve::CompressedEdwardsY`
  /// and that said compressed point is actually a point on the curve.
  ///
  ///
  /// # Returns
  ///
  /// A `Result` whose okay value is an EdDSA `PublicKey` or whose error value
  /// is an `SignatureError` describing the error that occurred.
  #[inline]
  pub fn from_bytes(bytes: &[u8]) -> Result<PublicKey, SignatureError>
  {
    if bytes.len() != PUBLIC_KEY_LENGTH
    {
      return Err(SignatureError(InternalError::BytesLengthError {
        name: "PublicKey",
        length: PUBLIC_KEY_LENGTH,
      }));
    }
    let mut bits: [u8; 32] = [0u8; 32];
    bits.copy_from_slice(&bytes[..32]);

    let compressed = CompressedEdwardsY(bits);
    let point = compressed
      .decompress()
      .ok_or(SignatureError(InternalError::PointDecompressionError))?;

    Ok(PublicKey(compressed, point))
  }

  /// Internal utility function for mangling the bits of a (formerly
  /// mathematically well-defined) "scalar" and multiplying it to produce a
  /// public key.
  fn mangle_scalar_bits_and_multiply_by_basepoint_to_produce_public_key(bits: &mut [u8; 32]) -> PublicKey
  {
    bits[0] &= 248;
    bits[31] &= 127;
    bits[31] |= 64;

    let point = &Scalar::from_bits(*bits) * &constants::ED25519_BASEPOINT_TABLE;
    let compressed = point.compress();

    PublicKey(compressed, point)
  }

  /// Verify a signature on a message with this keypair's public key.
  ///
  /// # Return
  ///
  /// Returns `Ok(())` if the signature is valid, and `Err` otherwise.
  #[allow(non_snake_case)]
  pub fn verify(&self, message: &[u8], signature: &Signature) -> Result<(), SignatureError>
  {
    let mut h: Sha512 = Sha512::new();
    let R: EdwardsPoint;
    let k: Scalar;
    let minus_A: EdwardsPoint = -self.1;

    h.update(signature.R.as_bytes());
    h.update(self.as_bytes());
    h.update(&message);

    k = Scalar::from_hash(h);
    R = EdwardsPoint::vartime_double_scalar_mul_basepoint(&k, &(minus_A), &signature.s);

    if R.compress() == signature.R
    {
      Ok(())
    }
    else
    {
      Err(SignatureError(InternalError::VerifyError))
    }
  }

  /// Verify a `signature` on a `prehashed_message` using the Ed25519ph algorithm.
  ///
  /// # Inputs
  ///
  /// * `prehashed_message` is an instantiated hash digest with 512-bits of
  ///   output which has had the message to be signed previously fed into its
  ///   state.
  /// * `context` is an optional context string, up to 255 bytes inclusive,
  ///   which may be used to provide additional domain separation.  If not
  ///   set, this will default to an empty string.
  /// * `signature` is a purported Ed25519ph [`Signature`] on the `prehashed_message`.
  ///
  /// # Returns
  ///
  /// Returns `true` if the `signature` was a valid signature created by this
  /// `Keypair` on the `prehashed_message`.
  ///
  /// [rfc8032]: https://tools.ietf.org/html/rfc8032#section-5.1
  #[allow(non_snake_case)]
  pub fn verify_prehashed<D>(
    &self, prehashed_message: D, context: Option<&[u8]>, signature: &Signature,
  ) -> Result<(), SignatureError>
  where
    D: Digest<OutputSize = U64>,
  {
    let mut h: Sha512 = Sha512::default();
    let R: EdwardsPoint;
    let k: Scalar;

    let ctx: &[u8] = context.unwrap_or(b"");
    debug_assert!(ctx.len() <= 255, "The context must not be longer than 255 octets.");

    let minus_A: EdwardsPoint = -self.1;

    h.update(b"SigEd25519 no Ed25519 collisions");
    h.update(&[1]); // Ed25519ph
    h.update(&[ctx.len() as u8]);
    h.update(ctx);
    h.update(signature.R.as_bytes());
    h.update(self.as_bytes());
    h.update(prehashed_message.finalize().as_slice());

    k = Scalar::from_hash(h);
    R = EdwardsPoint::vartime_double_scalar_mul_basepoint(&k, &(minus_A), &signature.s);

    if R.compress() == signature.R
    {
      Ok(())
    }
    else
    {
      Err(SignatureError(InternalError::VerifyError))
    }
  }
}

#[allow(non_snake_case)]
#[derive(Copy, Eq, PartialEq, Clone)]
pub struct Signature
{
  /// `R` is an `EdwardsPoint`, formed by using an hash function with
  /// 512-bits output to produce the digest of:
  ///
  /// - the nonce half of the `ExpandedSecretKey`, and
  /// - the message to be signed.
  ///
  /// This digest is then interpreted as a `Scalar` and reduced into an
  /// element in ℤ/lℤ.  The scalar is then multiplied by the distinguished
  /// basepoint to produce `R`, and `EdwardsPoint`.
  pub R: CompressedEdwardsY,

  /// `s` is a `Scalar`, formed by using an hash function with 512-bits output
  /// to produce the digest of:
  ///
  /// - the `r` portion of this `Signature`,
  /// - the `PublicKey` which should be used to verify this `Signature`, and
  /// - the message to be signed.
  ///
  /// This digest is then interpreted as a `Scalar` and reduced into an
  /// element in ℤ/lℤ.
  pub s: Scalar,
}

impl Debug for Signature
{
  fn fmt(&self, f: &mut ::core::fmt::Formatter<'_>) -> ::core::fmt::Result
  {
    write!(f, "Signature( R: {:?}, s: {:?} )", &self.R, &self.s)
  }
}

impl Signature
{
  /// Convert this `Signature` to a byte array.
  #[inline]
  pub fn to_bytes(&self) -> [u8; SIGNATURE_LENGTH]
  {
    let mut signature_bytes: [u8; SIGNATURE_LENGTH] = [0u8; SIGNATURE_LENGTH];

    signature_bytes[..32].copy_from_slice(&self.R.as_bytes()[..]);
    signature_bytes[32..].copy_from_slice(&self.s.as_bytes()[..]);
    signature_bytes
  }

  /// Construct a `Signature` from a slice of bytes.
  #[inline]
  pub fn from_bytes(bytes: &[u8]) -> Result<Signature, SignatureError>
  {
    if bytes.len() != SIGNATURE_LENGTH
    {
      return Err(SignatureError(InternalError::BytesLengthError {
        name: "Signature",
        length: SIGNATURE_LENGTH,
      }));
    }
    let mut lower: [u8; 32] = [0u8; 32];
    let mut upper: [u8; 32] = [0u8; 32];

    lower.copy_from_slice(&bytes[..32]);
    upper.copy_from_slice(&bytes[32..]);

    if upper[31] & 224 != 0
    {
      return Err(SignatureError(InternalError::ScalarFormatError));
    }

    Ok(Signature {
      R: CompressedEdwardsY(lower),
      s: Scalar::from_bits(upper),
    })
  }
}
#[allow(dead_code)]
pub fn finish_signing(sk: &SecretKey, r: &Scalar, pk: &PublicKey, sig_half: &Signature, message: &[u8]) -> Signature
{
  let mut h: Sha512 = Sha512::new();
  let s: Scalar;
  let k: Scalar;
  let expanded = ExpandedSecretKey::from(sk);
  h.update(&sig_half.R.0);
  h.update(pk.as_bytes());
  h.update(&message);

  k = Scalar::from_hash(h);
  s = &(&k * &expanded.key) + r;

  Signature { R: sig_half.R, s }
}

impl Serialize for Signature
{
  fn begin(&self) -> Fragment
  {
    Fragment::Str(Cow::Owned(base64::encode(&self.to_bytes()[..])))
  }
}
make_place!(Place);
use crate::chk;
impl Deserialize for Signature
{
  fn begin(out: &mut Option<Self>) -> &mut dyn de::Visitor
  {
    impl de::Visitor for Place<Signature>
    {
      fn string(&mut self, s: &str) -> Result<(), miniserde::Error>
      {
        let bytes = chk!(base64::decode(&s));
        self.out = Some(chk!(Signature::from_bytes(&bytes)));
        Ok(())
      }
    }
    Place::new(out)
  }
}

pub fn scalar_as_base64(key: &Scalar) -> Box<String>
{
  Box::new(base64::encode(key.to_bytes()))
}

pub fn scalar_from_base64(st: RawValue) -> Result<Scalar, miniserde::Error>
{
  let bytes = chk!(base64::decode(&st.get_dequoted_string()?));
  let bits: [u8; 32] = chk!(bytes[..].try_into());
  Ok(Scalar::from_bits(bits))
}

impl Default for Signature
{
  fn default() -> Signature
  {
    Signature::from_bytes(&[0; SIGNATURE_LENGTH]).unwrap()
  }
}

impl Default for SecretKey
{
  fn default() -> SecretKey
  {
    SecretKey::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap()
  }
}
