use crate::algorithms::tonelli_shanks;
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::NonSquareProof;
use crate::proofs::PrimePowerProof;
use crate::seed_random;

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct PaillierPubKey
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub n: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub g: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub n_sq: BigUint,
}
/*Since p q have the same length, we can use a simpler variant of the key
where g = n + 1, lambda = (p - 1 )(q - 1), u = inverse of lambda mod n 	*/
impl PaillierPubKey
{
  pub fn new(n: BigUint) -> PaillierPubKey
  {
    let gg = &n + BigUint::one();
    let sq = &n * &n;
    PaillierPubKey { n: n, g: gg, n_sq: sq }
  }
  /*Paillier encryption with chosen randomness*/
  #[allow(dead_code)]
  pub fn encrypt_with_rand(&self, msg: &BigUint, r: &BigUint) -> BigUint
  {
    let c1 = self.g.modpow(msg, &self.n_sq);
    let c2 = r.modpow(&self.n, &self.n_sq);
    (c1 * c2) % &self.n_sq
  }
  #[allow(dead_code)]
  pub fn encrypt(&self, msg: &BigUint) -> BigUint
  {
    let mut rng = OsRng;
    let r = gen_rand_z(&mut rng, &self.n, true).unwrap();
    self.encrypt_with_rand(msg, &r)
  }

  pub fn verify_non_square(&self, bp: &NonSquareProof) -> bool
  {
    let mut gen = seed_random!(bp.rseed, self.n);
    let x = gen_rand_z(&mut gen, &self.n, true).unwrap();
    let ver = bp.y.modpow(&self.n, &self.n);
    ver == x
  }

  pub fn verify_prime_power_product(&self, proof: &PrimePowerProof) -> bool
  {
    if proof.bitlen < 20
    //too few bits
    {
      return false;
    }

    if proof.congrs.len() != proof.bitlen
    {
      return false; //invalid proof
    }
    let mut gen = seed_random!(proof.rseed, self.n);
    for elem in &proof.congrs
    {
      let rndi = gen_rand_z(&mut gen, &self.n, false).unwrap();
      let p2 = &self.n - &rndi;
      let p3 = &(&rndi << 1) % &self.n;
      let p4 = &(&p2 << 1) % &self.n;
      let sqr = &(elem * elem) % &self.n;
      if sqr != rndi && sqr != p2 && sqr != p3 && sqr != p4
      {
        return false;
      }
    }

    true
  }
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct VerifiedSideData
{
  pub pk: PaillierPubKey,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub h1: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub h2: BigUint,
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct PaillierSecKey
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  lambda: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  u: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  p: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  q: BigUint,
}

impl PaillierSecKey
{
  pub fn new(p: BigUint, q: BigUint) -> PaillierSecKey
  {
    let lam = (&p - BigUint::one()) * (&q - BigUint::one());
    let u: BigUint = mod_inverse(Cow::Borrowed(&lam), Cow::Owned(&p * &q))
      .unwrap()
      .to_biguint()
      .unwrap();
    PaillierSecKey {
      p: p,
      q: q,
      lambda: lam,
      u: u,
    }
  }
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct PaillierKeyPair
{
  pub pk: PaillierPubKey,
  pub sk: PaillierSecKey,
  //paillierKeyPair(Integer n, Integer p, Integer q);
  //paillierKeyPair(paillierSecKey&& sk);
  //paillierKeyPair(Integer totient,Integer n);
}

impl PaillierKeyPair
{
  pub fn new(n: BigUint, p: BigUint, q: BigUint) -> PaillierKeyPair
  {
    let pk = PaillierPubKey::new(n);
    let sk = PaillierSecKey::new(p, q);
    PaillierKeyPair { pk: pk, sk: sk }
  }
  pub fn prove_non_square(&self) -> NonSquareProof
  {
    let mut prng = OsRng;
    let rand = prng.gen_biguint(256);
    let mut gen = seed_random!(rand, self.pk.n);
    let x = gen_rand_z(&mut gen, &self.pk.n, true).unwrap();
    let m = mod_inv(&self.pk.n, &self.sk.lambda);
    NonSquareProof {
      rseed: rand,
      y: x.modpow(&m, &self.pk.n),
    }
  }
  pub fn prove_prime_power_product(&self, bitlen: usize) -> PrimePowerProof
  {
    let mut prng = OsRng;
    let rand = prng.gen_biguint(256);
    let mut bytes: Vec<u8> = rand.to_bytes_be();
    bytes.append(&mut self.pk.n.to_bytes_be());
    while bytes.len() < 64
    {
      bytes.push(125);
    }
    let mut gen = seed_random!(rand, self.pk.n);

    //tonelliShanksPrepped
    let p_prep = tonelli_shanks::TonneliPrep::new(&self.sk.p);
    let q_prep = tonelli_shanks::TonneliPrep::new(&self.sk.q);
    let mut congrs: Vec<BigUint> = Vec::new();
    for _ in 0..bitlen
    {
      let rndi = gen_rand_z(&mut gen, &self.pk.n, false).unwrap();
      match tonelli_shanks::square_root_safe(&rndi, &self.pk.n, &p_prep, &q_prep)
      {
        Some(sqr) => congrs.push(sqr),
        None => panic!("Invalid prime setup, no square root"),
      }
    }
    PrimePowerProof {
      bitlen: bitlen,
      rseed: rand,
      congrs: congrs,
    }
  }
  /*Paillier Decryption*/
  pub fn decrypt(&self, code: &BigUint) -> BigUint
  {
    let m1 = code.modpow(&self.sk.lambda, &self.pk.n_sq);
    let m2 = (&m1 - &*UINT_ONE) / &self.pk.n;
    (m2 * &self.sk.u) % &self.pk.n
  }
}

#[cfg(test)]
mod tests
{
  use crate::algorithms::fujisaki::*;
  use crate::algorithms::paillier::*;

  #[test]
  fn test_paillier_encdec()
  {
    let fuji = FujiCore::generate_initial_keys(2048);
    let q = &*Q_MODULUS;
    let mut rng = rand::rngs::OsRng;
    for _ in 0..128
    {
      let r = gen_rand_z(&mut rng, q, false).unwrap();
      let r2 = gen_rand_z(&mut rng, q, false).unwrap();
      let enc = fuji.keys.pk.encrypt(&r);
      let encr = (&enc * &fuji.keys.pk.encrypt(&r2)) % &fuji.keys.pk.n_sq;
      let dec = fuji.keys.decrypt(&enc);
      let decr = fuji.keys.decrypt(&encr);
      assert_eq!(dec, r);
      assert_eq!(decr, &r + &r2);
    }
  }
}
