use crate::algorithms::paillier::*;
use crate::common::*;
use crate::common_defs::*;
use crate::seed_random;

use crate::algorithms::paillier::VerifiedSideData;
use crate::proofs::NonSquareProof;
use crate::proofs::PrimePowerProof;

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct FujiCore
{
  pub keys: PaillierKeyPair,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gsp: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gsq: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub b0: BigUint, //statistical zero knowledge protocols to prove modular... etc
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub subn: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub alpha: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub beta: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub b1: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub b2: BigUint,
}

#[derive(Serialize, Deserialize, Default)]
pub struct FmPacket
{
  //includes fujisaki and paillier public parameters, Schnorr non-interactive proof, square proof
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub en: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub b1: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub b2: BigUint,
  // Schnorr proof
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub t: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub s: BigUint,
  //other proofs
  pub non_square: NonSquareProof,
  pub prime_power: PrimePowerProof,
}

impl FmPacket
{
  pub fn verify(&self) -> bool
  {
    let key = PaillierPubKey::new(self.en.clone());
    let nsp = key.verify_non_square(&self.non_square);
    let ppp = key.verify_prime_power_product(&self.prime_power);
    let mut gen = seed_random!(self.t, self.en, self.b1, self.b2);
    let challenge = gen_rand_z(&mut gen, &self.en, false).unwrap();
    let v1 = self.b1.modpow(&self.s, &self.en);
    let v2 = (&self.t * self.b2.modpow(&challenge, &self.en)) % &self.en;
    (v1 == v2) && nsp && ppp
  }
  pub fn to_side_data(self) -> Option<VerifiedSideData>
  {
    match self.verify()
    {
      true => Some(VerifiedSideData {
        pk: PaillierPubKey::new(self.en),
        h1: self.b1,
        h2: self.b2,
      }),
      false => None,
    }
  }
}

impl FujiCore
{
  pub fn generate_initial_keys(sec_bits: usize) -> FujiCore
  {
    let mut rng = OsRng;
    let sbits = sec_bits / 2;
    let mut prime = rng.gen_prime(sbits);
    let eight = BigUint::from_u64(8).unwrap();
    while prime.mod_floor(&eight) == BigUint::one()
    {
      prime = next_prime(&prime);
      if prime.bits() > sbits
      {
        prime = rng.gen_prime(sbits);
      }
    }
    let subp = (&prime) >> 1;
    let mut h = rng.gen_biguint_range(&*UINT_TWO, &(&prime - &*UINT_ONE));
    let mut g = h.modpow(&*UINT_TWO, &prime);
    while g == BigUint::one()
    {
      h = rng.gen_biguint_range(&*UINT_TWO, &(&prime - &BigUint::one()));
      g = h.modpow(&*UINT_TWO, &prime);
    }
    let rc = prime.mod_floor(&eight);
    let mut secundus = rng.gen_prime(sbits);
    let mut remn = secundus.mod_floor(&eight);
    while remn == BigUint::one() || remn == rc
    {
      secundus = next_prime(&secundus);
      if secundus.bits() > sbits
      {
        secundus = rng.gen_prime(sbits);
      }
      remn = secundus.mod_floor(&eight);
    }
    h = rng.gen_biguint_range(&*UINT_TWO, &(&secundus - &BigUint::one()));
    let mut gs = h.modpow(&*UINT_TWO, &secundus);
    while gs == BigUint::one()
    {
      h = rng.gen_biguint_range(&*UINT_TWO, &(&secundus - &BigUint::one()));
      gs = h.modpow(&*UINT_TWO, &secundus);
    }
    let n = &prime * &secundus;
    let subn = &subp * ((&secundus) >> 1);

    let (gsp, gsq, b0, ee) = if prime > secundus
    {
      let b0 = chinese_remainder(&gs, &secundus, &g, &prime, &n);
      let ee = PaillierKeyPair::new(n.clone(), secundus.clone(), prime.clone());
      (gs, g, b0, ee)
    }
    else
    {
      let b0 = chinese_remainder(&g, &prime, &gs, &secundus, &n);
      let ee = PaillierKeyPair::new(n.clone(), prime.clone(), secundus.clone());
      (g, gs, b0, ee)
    };
    let alpha = gen_rand_z(&mut rng, &subn, true).unwrap();
    let beta = gen_rand_z(&mut rng, &subn, true).unwrap();
    let b1 = b0.modpow(&alpha, &n);
    let b2 = b0.modpow(&beta, &n);
    FujiCore {
      keys: ee,
      gsp: gsp,
      gsq: gsq,
      b0: b0,
      b1: b1,
      b2: b2,
      subn: subn,
      alpha: alpha,
      beta: beta,
    }
  }

  pub fn extract_packet(&self) -> FmPacket
  {
    let mut prng = OsRng;
    let rnd = gen_rand_z(&mut prng, &self.subn, false).unwrap();
    let t = self.b1.modpow(&rnd, &self.keys.pk.n);
    let mut gen = seed_random!(t, self.keys.pk.n, self.b1, self.b2);
    let challenge = gen_rand_z(&mut gen, &self.keys.pk.n, false).unwrap();
    let ab = (&self.beta * mod_inv(&self.alpha, &self.subn)) % &self.subn;
    let ss = &rnd + &challenge * &ab;
    FmPacket {
      en: self.keys.pk.n.clone(),
      b1: self.b1.clone(),
      b2: self.b2.clone(),
      t: t,
      s: ss,
      non_square: self.keys.prove_non_square(),
      prime_power: self.keys.prove_prime_power_product(64),
    }
  }
}

#[cfg(test)]
mod tests
{
  use crate::algorithms::fujisaki::*;
  //use crate::common_defs::*;
  //use crate::common::*;
  #[test]
  fn test_fuji_generation_extraction()
  {
    for _ in 0..64
    {
      let fuji = FujiCore::generate_initial_keys(1024);
      assert!(fuji.keys.pk.verify_non_square(&fuji.keys.prove_non_square()));
      assert!(fuji
        .keys
        .pk
        .verify_prime_power_product(&fuji.keys.prove_prime_power_product(128)));
      let pack = fuji.extract_packet();
      assert!(pack.verify());
    }
  }
}
