use crate::chk;
use crate::common_defs::*;
use num_traits::Num;
pub fn make_chacha<'a>(inp: &'a [u8]) -> ChaChaRng
{
  let mut hasher = Sha3_256::new();
  hasher.update(inp);
  let result = hasher.finalize();
  let mut seed: [u8; 32] = [0; 32];
  for x in 0..32
  {
    if x > result.len()
    {
      break;
    }
    seed[x] = result[x];
  }
  ChaChaRng::from_seed(seed)
}

/* For random seeding*/
pub trait ToByteVector
{
  fn to_vector(&self) -> Vec<u8>;
}

pub trait FromByteVector
where
  Self: core::marker::Sized,
{
  fn from_bytes(bytes: &[u8]) -> Option<Self>;
}

impl FromByteVector for BigUint
{
  fn from_bytes(bytes: &[u8]) -> Option<Self>
  {
    Some(BigUint::from_bytes_be(bytes))
  }
}

impl FromByteVector for AffinePoint
{
  fn from_bytes(bytes: &[u8]) -> Option<Self>
  {
    let ep = match EncodedPoint::from_bytes(bytes)
    {
      Ok(res) => res,
      Err(_) => return None,
    };
    let pnt = AffinePoint::from_encoded_point(&ep);
    if pnt.is_some().into()
    {
      Some(pnt.unwrap())
    }
    else
    {
      None
    }
  }
}

impl ToByteVector for BigUint
{
  fn to_vector(&self) -> Vec<u8>
  {
    self.to_bytes_be()
  }
}

impl ToByteVector for AffinePoint
{
  fn to_vector(&self) -> Vec<u8>
  {
    self.to_encoded_point(true).as_bytes().to_vec()
  }
}

impl ToByteVector for BigInt
{
  fn to_vector(&self) -> Vec<u8>
  {
    let (s, mut v) = self.to_bytes_be();
    match s
    {
      Sign::Plus => v.push(0),
      Sign::Minus => v.push(1),
      Sign::NoSign => v.push(2),
    }
    v
  }
}
impl ToByteVector for Scalar
{
  fn to_vector(&self) -> Vec<u8>
  {
    self.to_bytes().into_iter().collect()
  }
}
pub fn to_biguint_basic<R: ToByteVector>(data: &R) -> BigUint
{
  BigUint::from_bytes_be(&data.to_vector())
}

#[macro_export]
macro_rules! vectorize {
    ($e:expr) => {{
        {
            ($e).to_vector() // Force types to be vector

        }
    }};
    ( $e:expr, $( $es:expr),+) => {{
        let mut v1 = $crate::vectorize! { $e };
        $(
            v1.append(&mut $crate::vectorize! {  $es });
         )+
         v1
    }};
}
pub fn bi_combine(a: &BigUint, b: &BigUint) -> BigUint
{
  (a << 256) + b
}

#[macro_export]
macro_rules! seed_random {
    (  $($es:expr), +) => {{

       let mut vct= $crate::vectorize! { $($es),+ } ;
       while vct.len() < 64
       {
         vct.push(125);
       }
       $crate::common::make_chacha(&vct)
    }};
}

#[macro_export]
macro_rules! rotate_entropy {
  ( $prev:expr,$modulo:expr, $($es:expr), +) => {{

    let mut vct= $crate::vectorize! { $prev, $($es),+ } ;
    while vct.len() < 64
    {
      vct.push(125);
    }
   let mut gen=  $crate::common::make_chacha(&vct);
   gen_rand_z(&mut gen,$modulo, false).unwrap()
 }};
}

pub fn mod_inv(a: &BigUint, b: &BigUint) -> BigUint
{
  mod_inverse(Cow::Borrowed(a), Cow::Borrowed(b))
    .unwrap()
    .to_biguint()
    .unwrap()
}

pub fn int_as_base64(key: &BigUint) -> Box<String>
{
  Box::new(base64::encode(key.to_bytes_be()))
}

pub fn buffer_as_base64<Buf: AsRef<[u8]>>(key: &Buf) -> Box<String>
{
  Box::new(base64::encode(key.as_ref()))
}

pub fn int_as_base16_64l(key: &BigUint) -> Box<String>
{
  let st = format!("{:064x}", key);
  Box::new(st)
}

pub fn int_as_base16h(key: &BigUint) -> Box<String>
{
  let st = format!("{:x}h", key);
  Box::new(st)
}

//DEFAULT

pub fn int_as_base58(key: &BigUint) -> Box<String>
{
  Box::new(bs58::encode(key.to_bytes_be()).into_string())
}

pub fn public_key_as_pem(key: &rsa::RSAPublicKey) -> Box<String>
{
  Box::new(key.to_pem_pkcs8().unwrap())
}

//st:RawValue)-> Result<String,miniserde::Error>
pub fn public_key_from_pem(st: RawValue) -> Result<rsa::RSAPublicKey, miniserde::Error>
{
  let pk = chk!(rsa::pem::parse(&st.get_dequoted_string()?));
  Ok(chk!(rsa::RSAPublicKey::try_from(pk)))
}

#[derive(Serialize, Deserialize, Clone)]
pub struct WrappedPubkey
{
  #[serde(
    serialize_with = "public_key_as_pem",
    deserialize_with = "public_key_from_pem",
    default = "rsaempty"
  )]
  pub pem: rsa::RSAPublicKey,
}

#[allow(dead_code)]
fn rsaempty() -> rsa::RSAPublicKey
{
  rsa::RSAPublicKey::new(BigUint::zero(), BigUint::from_u64(3).unwrap()).unwrap()
}

impl core::default::Default for WrappedPubkey
{
  fn default() -> Self
  {
    WrappedPubkey {
      pem: rsa::RSAPublicKey::new(BigUint::zero(), BigUint::from_u64(3).unwrap()).unwrap(),
    }
  }
}
pub fn point_as_base64(key: &AffinePoint) -> Box<String>
{
  Box::new(base64::encode(key.to_vector()))
}

pub fn int_from_base64(st: RawValue) -> Result<BigUint, miniserde::Error>
{
  let bytes = chk!(base64::decode(st.get_dequoted_string()?));
  Ok(BigUint::from_bytes_be(&bytes))
}

pub fn buffer_from_base64<Buf: AsRef<[u8]> + TryFrom<Vec<u8>>>(st: RawValue) -> Result<Buf, miniserde::Error>
{
  let bytes = chk!(base64::decode(&st.get_dequoted_string()?));
  Ok(chk!(bytes.try_into()))
}

pub fn int_from_base16_64l(st: RawValue) -> Result<BigUint, miniserde::Error>
{
  Ok(chk!(BigUint::from_str_radix(&st.get_dequoted_string()?, 16)))
}

pub fn int_from_base16h(st: RawValue) -> Result<BigUint, miniserde::Error>
{
  let mut string = st.get_dequoted_string()?;
  if Some('0') == string.chars().nth(0) && Some('x') == string.chars().nth(1)
  {
    string = string.chars().skip(2).collect();
  }
  else
  {
    string.pop();
  }
  Ok(chk!(BigUint::from_str_radix(&string, 16)))
}
make_place!(Place);

pub fn int_from_base58(deserializer: RawValue) -> Result<BigUint, miniserde::Error>
{
  let bytes = chk!(bs58::decode(&deserializer.get_dequoted_string()?).into_vec());
  Ok(BigUint::from_bytes_be(&bytes))
}

pub fn point_from_base64(st: RawValue) -> Result<AffinePoint, miniserde::Error>
{
  let bytes = chk!(base64::decode(&st.get_dequoted_string()?));
  let off = EncodedPoint::from_bytes(bytes).unwrap();
  Ok(AffinePoint::from_encoded_point(&off).unwrap_or(Default::default()))
}

pub fn vint_as_base64(key: &[BigUint]) -> Box<dyn Serialize>
{
  struct Wrapper(BigUint);
  impl Serialize for Wrapper
  {
    fn begin(&self) -> Fragment
    {
      Fragment::Str(Cow::Owned(*int_as_base64(&self.0)))
    }
  }
  let seq: Vec<Wrapper> = key.iter().cloned().map(|element| Wrapper(element)).collect();
  Box::new(seq)
}

pub fn vint_from_base64(st: RawValue) -> Result<Vec<BigUint>, miniserde::Error>
{
  struct Wrapper(BigUint);

  impl de::Visitor for Place<Wrapper>
  {
    fn string(&mut self, b: &str) -> miniserde::Result<()>
    {
      let bytes = chk!(base64::decode(b));
      self.out = Some(Wrapper(BigUint::from_bytes_be(&bytes)));
      Ok(())
    }
  }

  impl Deserialize for Wrapper
  {
    fn begin(out: &mut Option<Self>) -> &mut dyn de::Visitor
    {
      Place::new(out)
    }
  }
  let v: Vec<Wrapper> = miniserde::json::from_str(st.get())?;
  Ok(v.into_iter().map(|Wrapper(a)| a).collect())
}

#[allow(dead_code)]
pub fn fixed_len_buffer(enc: &BigUint, len: usize) -> Vec<u8>
{
  if len == 0
  {
    return Vec::new();
  }
  let mut encd = enc.to_vector();
  if encd.len() == len
  {
    return encd;
  }
  encd.resize(len, 0);
  encd
}

pub fn fixed_len_buffer_pref(enc: &BigUint, len: usize) -> Vec<u8>
{
  if len == 0
  {
    return Vec::new();
  }
  let mut encd = enc.to_vector();
  if encd.len() == len
  {
    return encd;
  }
  if encd.len() > len
  {
    encd.resize(len, 0);
    return encd;
  }
  let mut en = vec![0; len - encd.len()];
  en.extend(&encd);
  en
}

pub fn chinese_remainder(a1: &BigUint, n1: &BigUint, a2: &BigUint, n2: &BigUint, prod: &BigUint) -> BigUint
{
  let mut sum = BigUint::zero();
  let p1 = prod / n1;
  sum += a1 * &p1 * &p1.mod_inverse(n1).unwrap().to_biguint().unwrap();
  let p2 = prod / n2;
  sum += a2 * &p2 * &p2.mod_inverse(n2).unwrap().to_biguint().unwrap();
  sum % prod
}

/* Generates nonzero Zq up to limit, optionally checks for GCD*/
pub fn gen_rand_z<R: Rng>(prng: &mut R, limit: &BigUint, ast: bool) -> Option<BigUint>
{
  let mut rand = prng.gen_biguint_range(&BigUint::one(), limit);
  let mut cnt = 0;
  while ast &&
    {
      let (a, _, _) = extended_gcd(Cow::Borrowed(&rand), Cow::Borrowed(&limit), false);
      a
    } != BigInt::one() &&
    cnt < 100
  {
    rand = prng.gen_biguint_range(&BigUint::one(), limit);
    cnt += 1;
  }

  if cnt == 100
  {
    return None;
  }
  Some(rand)
}

pub fn rsa_encrypt_bigint<R: PublicKey, Rn: Rng>(rng: &mut Rn, pk: &R, data: &BigUint) -> BigUint
{
  let padding = PaddingScheme::new_pkcs1v15_encrypt();
  let enc_data = pk
    .encrypt(rng, padding, &data.to_bytes_be())
    .expect("failed to encrypt bigint");
  BigUint::from_bytes_be(&enc_data)
}

#[allow(dead_code)]
pub fn rsa_decrypt_bigint(sk: &RSAPrivateKey, data: &BigUint) -> BigUint
{
  let padding = PaddingScheme::new_pkcs1v15_encrypt();
  let dec_data = sk.decrypt(padding, &data.to_bytes_be()).expect("failed to decrypt");
  BigUint::from_bytes_be(&dec_data)
}

pub fn rsa_encrypt_seeded<R: rsa::PublicKey>(pk: &R, data: &BigUint) -> BigUint
{
  let mut rng = seed_random!(pk.n(), pk.e());
  rsa_encrypt_bigint(&mut rng, pk, data)
}

#[inline]
pub fn to_scalar(int: &BigUint) -> Scalar
{
  let q = &*Q_MODULUS;
  let y = int % q;
  let mut bbuf = y.to_bytes_be();
  if bbuf.len() < 32
  {
    let mut zero_vec = vec![0; 32 - bbuf.len()];
    zero_vec.extend(bbuf);
    bbuf = zero_vec;
  }
  Scalar::from_bytes_reduced(k256::FieldBytes::from_slice(&bbuf))
}

#[allow(dead_code)]
pub fn rsa_encrypt_data<R: Rng, P: PublicKey>(rng: &mut R, message: &[u8], pk: P) -> Vec<u8>
{
  let padding = PaddingScheme::new_pkcs1v15_encrypt();
  let enc_data = pk
    .encrypt(rng, padding, &message[..])
    .expect("failed to encrypt binary blob");
  enc_data
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct HashCommitment
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub digest: BigUint,
}
#[derive(Serialize, Deserialize, Default, Clone)]
pub struct HashDecommitment
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub y: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub rseed: BigUint,
}

#[derive(Serialize, Deserialize)]
pub struct PointSerializer
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub d: AffinePoint,
}

pub fn get_hash_commitment<H: Digest>(y: &AffinePoint, rbits: usize) -> (HashCommitment, HashDecommitment)
{
  let mut prng = OsRng;
  let rand = prng.gen_biguint(rbits);
  let mut h: H = H::new();
  h.update(y.to_vector());
  h.update(rand.to_vector());
  let res = h.finalize();
  return (
    HashCommitment {
      digest: BigUint::from_bytes_be(&res),
    },
    HashDecommitment {
      y: y.clone(),
      rseed: rand,
    },
  );
}

pub fn verify_hash_commitment<H: Digest>(hk: &HashCommitment, hd: &HashDecommitment) -> bool
{
  let mut h: H = H::new();
  h.update(hd.y.to_vector());
  h.update(hd.rseed.to_vector());
  let res = h.finalize();
  let ver = BigUint::from_bytes_be(&res);
  ver == hk.digest
}

/*
extern rsaEd255Proof createRsaEdProof(RSA::PublicKey& pk,ed25519_pk pubshare,ed25519_sk privshare);
extern bool  verifyRsaEd255Proof(rsaEd255Proof& proof, RSA::PublicKey& pk,ed25519_pk pubshare,Integer& backup);

*/
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct StatusMessage
{
  pub status: String,
}

#[cfg(test)]
mod tests
{
  use num_bigint_dig::RandBigInt;
  //use num_bigint_dig::BigUint;
  //use crate::statics::*;
  //use k256::Scalar;
  use crate::common::*;
  use k256::ProjectivePoint; //AffinePoint,
  #[test]
  fn test_rsa_encdec()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = rsa::RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = rsa::RSAPublicKey::from(&private_key);
    for _ in 0..1024
    {
      let i = rng.gen_biguint(1024);
      let enc = crate::common::rsa_encrypt_seeded(&public_key, &i);
      let dec = crate::common::rsa_decrypt_bigint(&private_key, &enc);
      assert_eq!(dec, i);
    }
  }

  #[test]
  fn scalar_to_curve()
  {
    {
      let i = &*UINT_ONE;
      let scal = crate::common::to_scalar(i);
      let chk = crate::common::to_biguint_basic(&scal);
      assert_eq!(chk, *i);
    }
    let base = (ProjectivePoint::generator() * crate::common::to_scalar(&*UINT_ONE)).to_affine();
    assert_eq!(base, ProjectivePoint::generator().to_affine());
    let mut rng = rand::rngs::OsRng;
    let q = &*Q_MODULUS;
    for _ in 0..1024
    {
      let i = &rng.gen_biguint(1024) % q;
      let scal = crate::common::to_scalar(&i);
      let chk = crate::common::to_biguint_basic(&scal);
      assert_eq!(chk, i);
    }
  }
}

pub struct HexSlice<'a>(&'a [u8]);

impl<'a> HexSlice<'a>
{
  pub fn new<T>(data: &'a T) -> HexSlice<'a>
  where
    T: ?Sized + AsRef<[u8]> + 'a,
  {
    HexSlice(data.as_ref())
  }
}

// You can choose to implement multiple traits, like Lower and UpperHex
impl core::fmt::Display for HexSlice<'_>
{
  fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result
  {
    for byte in self.0
    {
      // Decide if you want to pad the value or have spaces inbetween, etc.
      write!(f, "{:02x}", byte)?;
    }
    Ok(())
  }
}
