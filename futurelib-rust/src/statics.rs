use k256::Scalar;
use num_bigint_dig::{BigInt, BigUint};
use num_traits::{identities::One, identities::Zero, FromPrimitive};

lazy_static! {
  pub static ref UINT_ZERO: BigUint = BigUint::zero();
  pub static ref INT_ZERO: BigInt = BigInt::zero();
  pub static ref UINT_ONE: BigUint = BigUint::one();
  pub static ref INT_ONE: BigInt = BigInt::one();
  pub static ref UINT_TWO: BigUint = BigUint::from_u64(2).unwrap();
  pub static ref INT_TWO: BigInt = BigInt::from_u64(2).unwrap();
  pub static ref UINT_THREE: BigUint = BigUint::from_u64(3).unwrap();
  pub static ref INT_THREE: BigInt = BigInt::from_u64(3).unwrap();
  pub static ref UINT_FOUR: BigUint = BigUint::from_u64(4).unwrap();
  pub static ref INT_FOUR: BigInt = BigInt::from_u64(4).unwrap();
  pub static ref UINT_FIVE: BigUint = BigUint::from_u64(5).unwrap();
  pub static ref INT_FIVE: BigInt = BigInt::from_u64(5).unwrap();
  pub static ref UINT_SIX: BigUint = BigUint::from_u64(6).unwrap();
  pub static ref INT_SIX: BigInt = BigInt::from_u64(6).unwrap();
  pub static ref UINT_SEVEN: BigUint = BigUint::from_u64(7).unwrap();
  pub static ref INT_SEVEN: BigInt = BigInt::from_u64(7).unwrap();
  pub static ref UINT_EIGHT: BigUint = BigUint::from_u64(8).unwrap();
  pub static ref INT_EIGHT: BigInt = BigInt::from_u64(8).unwrap();
  pub static ref Q_MODULUS: BigUint =
    BigUint::from_bytes_be(Scalar::one().negate().to_bytes().as_ref()) + BigUint::from_slice(&[1]);
  pub static ref FRONTEND_PARTY: BigUint = BigUint::one();
  pub static ref BACKEND_PARTY: BigUint = BigUint::from_u64(2).unwrap();
}
