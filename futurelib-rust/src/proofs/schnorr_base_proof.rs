use crate::common::*;
use crate::common_defs::*;
use crate::seed_random;

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct SchnorrBaseProof
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub qr: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub rseed: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub s: BigUint,
}

impl SchnorrBaseProof
{
  pub fn new(x: &BigUint) -> SchnorrBaseProof
  {
    let mut rng = OsRng;
    let q = &*Q_MODULUS;
    let r = gen_rand_z(&mut rng, q, false).unwrap();
    let rseed = rng.gen_biguint(256);
    let qr = (ProjectivePoint::generator() * to_scalar(&r)).to_affine();
    let mut gen = seed_random!(qr, rseed);
    let challenge = gen_rand_z(&mut gen, q, false).unwrap();
    SchnorrBaseProof {
      rseed: rseed,
      qr: qr,
      s: r + challenge * x,
    }
  }
  pub fn verify(&self, pk: &AffinePoint) -> bool
  {
    let q = &*Q_MODULUS;
    let mut gen = seed_random!(self.qr, self.rseed);
    let challenge = gen_rand_z(&mut gen, q, false).unwrap();
    let qs = (ProjectivePoint::generator() * to_scalar(&self.s)).to_affine();
    let qy = (ProjectivePoint::from(*pk) * to_scalar(&challenge) + self.qr).to_affine();
    qs == qy
  }
}
#[cfg(test)]
mod tests
{
  use crate::common::*;
  use crate::common_defs::*;
  use crate::proofs::SchnorrBaseProof;
  #[test]
  fn check_schnorr_proof()
  {
    let q = &*Q_MODULUS;
    let mut prng = rand::rngs::OsRng;
    for _ in 0..64
    {
      let el = gen_rand_z(&mut prng, q, false).unwrap();
      let pk = (ProjectivePoint::generator() * crate::common::to_scalar(&el)).to_affine();
      let proof = SchnorrBaseProof::new(&el);
      let pstr = miniserde::json::to_string(&proof);
      let reproof: SchnorrBaseProof = miniserde::json::from_str(&pstr).unwrap();
      assert!(reproof.verify(&pk));
    }
  }
}
