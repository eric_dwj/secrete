use crate::common::*;
use crate::common_defs::*;

#[derive(Serialize, Deserialize, Default)]
pub struct RsaEllipticProof
{
  binary_digest: String,
  #[serde(serialize_with = "vint_as_base64", deserialize_with = "vint_from_base64")]
  c: Vec<BigUint>,
  #[serde(serialize_with = "vint_as_base64", deserialize_with = "vint_from_base64")]
  ry: Vec<BigUint>,
}

impl RsaEllipticProof
{
  pub fn new<R: rsa::PublicKey>(pk: &R, pubshare: &AffinePoint, privshare: &Scalar) -> RsaEllipticProof
  {
    let mut prng = OsRng;
    let mut ret = RsaEllipticProof {
      binary_digest: "".to_string(),
      c: Vec::new(),
      ry: Vec::new(),
    };
    let q = &*Q_MODULUS;
    let privshare = to_biguint_basic(privshare);
    let edat = rsa_encrypt_seeded(pk, &privshare);
    let mut hasher = Sha3_256::new();
    hasher.update(pubshare.to_vector());
    hasher.update(edat.to_vector());
    let mut rr = Vec::<BigUint>::new();
    let mut yy = Vec::<BigUint>::new();
    let mut cc0 = Vec::<BigUint>::new();
    let mut cc1 = Vec::<BigUint>::new();

    //let mut rng=seed_random!(pubshare);
    for _ in 0..256
    {
      let r = prng.gen_biguint_range(&*UINT_ONE, &q);
      let y = &(&privshare + &r) % q;
      let c0 = rsa_encrypt_seeded(pk, &r);
      let c1 = rsa_encrypt_seeded(pk, &y);
      let q_tag = (ProjectivePoint::generator() * to_scalar(&r)).to_affine();
      // tempStr=concatEncode(c0,c1,Q_tag.x);
      hasher.update(c0.to_vector());
      hasher.update(c1.to_vector());
      hasher.update(q_tag.to_vector());
      rr.push(r);
      yy.push(y);
      cc0.push(c0);
      cc1.push(c1);
    }
    let result = hasher.finalize();
    for k in result
    {
      ret.binary_digest += &alloc::format!("{:08b}", k);
    }
    let mut j = 0;
    for ch in ret.binary_digest.chars()
    {
      match ch
      {
        '0' =>
        {
          ret.c.push(cc1[j].clone());
          ret.ry.push(rr[j].clone());
        }
        '1' =>
        {
          ret.c.push(cc0[j].clone());
          ret.ry.push(yy[j].clone());
        }
        _ =>
        {
          panic!("Invalid string format");
        }
      }
      j += 1;
    }
    ret
  }
  #[allow(dead_code)]
  pub fn verify<R: rsa::PublicKey>(&self, pk: &R, pubshare: &AffinePoint, backup: &BigUint) -> bool
  {
    let mut hasher = Sha3_256::new();
    hasher.update(pubshare.to_vector());
    hasher.update(backup.to_vector());
    let q_share = pubshare;
    let mut j = 0;
    for ch in self.binary_digest.chars()
    {
      let ry = &self.ry[j];
      let c = &self.c[j];
      let c0: Cow<BigUint>;
      let c1: Cow<BigUint>;
      let q_tag: AffinePoint;

      match ch
      {
        '0' =>
        {
          c0 = Cow::Owned(rsa_encrypt_seeded(pk, ry));
          q_tag = (ProjectivePoint::generator() * to_scalar(ry)).to_affine();
          c1 = Cow::Borrowed(c);
        }
        '1' =>
        {
          c1 = Cow::Owned(rsa_encrypt_seeded(pk, ry));
          q_tag = (ProjectivePoint::generator() * to_scalar(ry) - q_share).to_affine();
          // Q_tag = _globalContext.group.GetCurve().Subtract(tempQ, Q_share);
          c0 = Cow::Borrowed(c);
        }
        _ => return false,
      }
      hasher.update(c0.to_vector());
      hasher.update(c1.to_vector());
      hasher.update(q_tag.to_vector());
      j += 1;
    }
    let result = hasher.finalize();
    let mut st: String = "".to_string();
    for k in result
    {
      st += &alloc::format!("{:08b}", k);
    }
    st == self.binary_digest
  }
}
#[cfg(test)]
mod tests
{
  use crate::common::*;
  use crate::common_defs::*;
  use crate::proofs::rsa_elliptic_proof::*;
  #[test]
  fn rsa_elliptic_proof()
  {
    let mut rng = rand::rngs::OsRng;
    let q = &*Q_MODULUS;
    for _ in 0..64
    {
      let bits = 2048;
      let private_key = rsa::RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
      let public_key = rsa::RSAPublicKey::from(&private_key);
      let i = &rng.gen_biguint(1024) % q;
      let scal = to_scalar(&i);
      let pp = (ProjectivePoint::generator() * scal).to_affine();
      let proof = RsaEllipticProof::new(&public_key, &pp, &scal);
      let backup = rsa_encrypt_seeded(&public_key, &i);
      let pstr = miniserde::json::to_string(&proof);
      let reproof: RsaEllipticProof = miniserde::json::from_str(&pstr).unwrap();
      assert!(reproof.verify(&public_key, &pp, &backup));
    }
  }
}
