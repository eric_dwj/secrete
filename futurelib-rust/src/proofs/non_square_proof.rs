use crate::common::*;
use crate::common_defs::*;

#[derive(Serialize, Deserialize, Default)]
pub struct NonSquareProof
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub y: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub rseed: BigUint,
}
