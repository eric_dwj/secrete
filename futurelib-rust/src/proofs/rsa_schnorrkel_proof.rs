use crate::common::*;
use crate::common_defs::*;
use curve25519_dalek::constants::*;
use schnorrkel::{PublicKey as ShPk, SecretKey as ShSk, PUBLIC_KEY_LENGTH, SECRET_KEY_LENGTH};
#[derive(Serialize, Deserialize, Default)]
pub struct RsaSchnorrkelProof
{
  binary_digest: String,
  #[serde(serialize_with = "vint_as_base64", deserialize_with = "vint_from_base64")]
  c: Vec<BigUint>,
  #[serde(serialize_with = "vint_as_base64", deserialize_with = "vint_from_base64")]
  ry: Vec<BigUint>,
}
const PROOF_ROUNDS: usize = 64;
impl RsaSchnorrkelProof
{
  pub fn new<R: rsa::PublicKey>(pk: &R, pubshare: &ShPk, privshare: &ShSk) -> RsaSchnorrkelProof
  {
    let mut ret = RsaSchnorrkelProof {
      binary_digest: "".to_string(),
      c: Vec::new(),
      ry: Vec::new(),
    };
    let prebug = BigUint::from_bytes_be(&privshare.to_bytes());
    //let exp_priv = ExpandedSecretKey::from(privshare);
    let edat = rsa_encrypt_seeded(pk, &prebug);
    let mut hasher = Sha3_256::new();
    hasher.update(pubshare.to_bytes());
    hasher.update(edat.to_vector());

    let mut rr = Vec::<BigUint>::new();
    let mut yy = Vec::<BigUint>::new();
    let mut cc0 = Vec::<BigUint>::new();
    let mut cc1 = Vec::<BigUint>::new();
    // assert_eq!(&exp_priv.key*&constants::ED25519_BASEPOINT_TABLE,pubshare.1);
    //let mut rng=seed_random!(pubshare);
    for _ in 0..PROOF_ROUNDS
    {
      let rnd = ShSk::generate();
      //let rnd_ex = ExpandedSecretKey::from(&rnd);
      let r = BigUint::from_bytes_be(&rnd.to_bytes());
      let rndpk = &privshare.key + &rnd.key;
      let y = BigUint::from_bytes_be(rndpk.as_bytes());
      let c0 = rsa_encrypt_seeded(pk, &r);
      let c1 = rsa_encrypt_seeded(pk, &y);
      let epk = &rnd.key * &RISTRETTO_BASEPOINT_TABLE;
      hasher.update(&c0.to_vector());
      hasher.update(&c1.to_vector());
      hasher.update(&epk.compress().0);

      //#[cfg(test)]
      // println!("qtag: {} {:?}", rr.len(), &epk.compress().0);

      rr.push(r);
      yy.push(y);
      cc0.push(c0);
      cc1.push(c1);
    }
    let result = hasher.finalize();
    for k in result
    {
      ret.binary_digest += &alloc::format!("{:08b}", k);
    }
    let mut j = 0;
    for ch in ret.binary_digest.chars()
    {
      if j >= PROOF_ROUNDS
      {
        break;
      }
      match ch
      {
        '0' =>
        {
          ret.c.push(cc1[j].clone());
          ret.ry.push(rr[j].clone());
        }
        '1' =>
        {
          ret.c.push(cc0[j].clone());
          ret.ry.push(yy[j].clone());
        }
        _ =>
        {
          panic!("Invalid string format");
        }
      }
      j += 1;
    }
    ret
  }
  pub fn verify<R: rsa::PublicKey>(&self, pk: &R, pubshare: &ShPk, backup: &BigUint) -> bool
  {
    let mut hasher = Sha3_256::new();
    hasher.update(pubshare.to_bytes());
    hasher.update(backup.to_vector());
    // /let mut q_share=pubshare.clone();
    if self.c.len() != PROOF_ROUNDS
    {
      return false;
    };
    if self.ry.len() != PROOF_ROUNDS
    {
      return false;
    };
    let mut j = 0;
    for ch in self.binary_digest.chars()
    {
      if j >= PROOF_ROUNDS
      {
        break;
      };
      let ry = &self.ry[j];
      let c = &self.c[j];
      let c0: Cow<BigUint>;
      let c1: Cow<BigUint>;
      let q_tag: curve25519_dalek::ristretto::RistrettoPoint;
      match ch
      {
        '0' =>
        {
          c0 = Cow::Owned(rsa_encrypt_seeded(pk, ry));
          let rnd = match ShSk::from_bytes(&fixed_len_buffer_pref(&ry, SECRET_KEY_LENGTH))
          {
            Ok(res) => res,
            Err(_) => return false,
          };

          q_tag = &rnd.key * &RISTRETTO_BASEPOINT_TABLE;
          c1 = Cow::Borrowed(c);
        }
        '1' =>
        {
          c1 = Cow::Owned(rsa_encrypt_seeded(pk, ry));
          let buf: [u8; PUBLIC_KEY_LENGTH] = match fixed_len_buffer_pref(&ry, PUBLIC_KEY_LENGTH)[..].try_into()
          {
            Ok(res) => res,
            Err(_) => return false,
          };
          let rnd = curve25519_dalek::scalar::Scalar::from_bits(buf);
          let qq = &rnd * &RISTRETTO_BASEPOINT_TABLE;
          q_tag = qq - pubshare.0.as_point();
          c0 = Cow::Borrowed(c);
        }
        _ => return false,
      }
      // #[cfg(test)]
      // println!("qtag: {} {:?}", j, &q_tag.compress().0);
      hasher.update(c0.to_vector());
      hasher.update(c1.to_vector());
      hasher.update(&q_tag.compress().0);
      j += 1;
    }
    let result = hasher.finalize();
    let mut st: String = "".to_string();
    for k in result
    {
      st += &alloc::format!("{:08b}", k);
    }
    // #[cfg(test)]
    //  println!("Inp:  {:?}", self.binary_digest);
    //  #[cfg(test)]
    //  println!("Ours: {:?}", st);
    st == self.binary_digest
  }
}

#[cfg(test)]
mod tests
{
  use crate::common::*;
  use crate::common_defs::*;
  use crate::proofs::rsa_schnorrkel_proof::*;
  #[test]
  fn rsa_schnorrkel_proof()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = rsa::RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = rsa::RSAPublicKey::from(&private_key);
    for _ in 0..128
    {
      let rnd = ShSk::generate();
      let psh = rnd.to_public();
      let prebug = BigUint::from_bytes_be(&rnd.to_bytes());
      let backup = rsa_encrypt_seeded(&public_key, &prebug);
      let proof = RsaSchnorrkelProof::new(&public_key, &psh, &rnd);
      let pstr = miniserde::json::to_string(&proof);
      let reproof: RsaSchnorrkelProof = miniserde::json::from_str(&pstr).unwrap();
      assert!(reproof.verify(&public_key, &psh, &backup));
    }
  }
}
