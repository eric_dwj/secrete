use crate::algorithms::paillier::*;
use crate::common::*;
use crate::common_defs::*;
use crate::seed_random;

/*Proves that we know k1 such that encryption of it corresponds to logarithm D(c)=log_r(rk)*/
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct ExpPaillierProof
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  z: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  u2: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  u3: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  u1: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  rseed: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  s1: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  s2: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  s3: BigUint,
}

impl ExpPaillierProof
{
  pub fn new(
    nu: &BigUint, r: &BigUint, g: &AffinePoint, y: &AffinePoint, w: &BigUint, fkeys: &PaillierKeyPair,
    othr: &VerifiedSideData,
  ) -> ExpPaillierProof
  {
    let q = &*Q_MODULUS;
    let mut prng = OsRng;
    let qcube = q * q * q;
    let rand = prng.gen_biguint(256);
    let alpha = gen_rand_z(&mut prng, &(&qcube - q * q), false).unwrap();
    let beta = gen_rand_z(&mut prng, &fkeys.pk.n, true).unwrap();
    let rho = gen_rand_z(&mut prng, &othr.pk.n, false).unwrap();
    let gamma = gen_rand_z(&mut prng, &(&othr.pk.n * &qcube), false).unwrap();
    let z = (othr.h1.modpow(nu, &othr.pk.n) * othr.h2.modpow(&rho, &othr.pk.n)) % &othr.pk.n;
    let u1 = (ProjectivePoint::from(*g) * to_scalar(&alpha)).to_affine();
    let u2 = fkeys.pk.encrypt_with_rand(&alpha, &beta);
    let u3 = (othr.h1.modpow(&alpha, &othr.pk.n) * othr.h2.modpow(&gamma, &othr.pk.n)) % &othr.pk.n;
    let mut gen = seed_random!(rand, g, y, w, z, u1, u2, u3);
    let challenge = gen_rand_z(&mut gen, q, false).unwrap();

    ExpPaillierProof {
      rseed: rand,
      z: z,
      u1: u1,
      u2: u2,
      u3: u3,
      s1: &challenge * nu + &alpha,
      s2: (r.modpow(&challenge, &fkeys.pk.n) * beta) % &fkeys.pk.n,
      s3: challenge * rho + gamma,
    }
  }
  pub fn verify(
    &self, g: &AffinePoint, y: &AffinePoint, w: &BigUint, sec: &PaillierKeyPair, h1: &BigUint, h2: &BigUint,
    othr: &VerifiedSideData,
  ) -> bool
  {
    let q = &*Q_MODULUS;
    let mut gen = seed_random!(self.rseed, g, y, w, self.z, self.u1, self.u2, self.u3);
    let challenge = gen_rand_z(&mut gen, q, false).unwrap();
    if self.z >= sec.pk.n
    {
      return false;
    }
    if self.u2 >= othr.pk.n_sq
    {
      return false;
    }
    if self.u3 >= sec.pk.n
    {
      return false;
    }

    let v1 = (ProjectivePoint::from(*g) * to_scalar(&self.s1) +
      ProjectivePoint::from(*y) * to_scalar(&(q - &challenge)))
    .to_affine();
    if v1 != self.u1
    {
      return false;
    }
    let iw = mod_inv(&w, &othr.pk.n_sq);
    let tt1 = (othr.pk.g.modpow(&self.s1, &othr.pk.n_sq) * self.s2.modpow(&othr.pk.n, &othr.pk.n_sq)) % &othr.pk.n_sq;
    let v2 = (iw.modpow(&challenge, &othr.pk.n_sq) * tt1) % &othr.pk.n_sq;
    if v2 != self.u2
    {
      return false;
    }
    let iz = mod_inv(&self.z, &sec.pk.n);
    let tt2 = (h1.modpow(&self.s1, &sec.pk.n) * h2.modpow(&self.s3, &sec.pk.n)) % &sec.pk.n;
    let v3 = (iz.modpow(&challenge, &sec.pk.n) * tt2) % &sec.pk.n;
    if v3 != self.u3
    {
      return false;
    }
    true
  }
}

#[cfg(test)]
mod tests
{
  use crate::algorithms::fujisaki::*;
  use crate::common::*;
  use crate::common_defs::*;
  use crate::proofs::exp_paillier_proof::*;
  //use crate::proofs::VerifiedSideData;
  #[test]
  fn check_expaillier_proofs()
  {
    let fuji1 = FujiCore::generate_initial_keys(2048);
    let fuji2 = FujiCore::generate_initial_keys(2048);
    let other1 = fuji1.extract_packet().to_side_data().unwrap();
    let other2 = fuji2.extract_packet().to_side_data().unwrap();
    let mut prng = rand::rngs::OsRng;
    let q = &*Q_MODULUS;
    for _ in 0..64
    {
      let el = gen_rand_z(&mut prng, q, false).unwrap();
      let nu = gen_rand_z(&mut prng, q, false).unwrap();
      let rr1 = gen_rand_z(&mut prng, &fuji1.keys.pk.n, true).unwrap();
      let ww1 = fuji1.keys.pk.encrypt_with_rand(&nu, &rr1);
      let r_p = ProjectivePoint::generator() * to_scalar(&el);
      let yy9 = (r_p * to_scalar(&nu)).to_affine();
      let epr = ExpPaillierProof::new(&nu, &rr1, &r_p.to_affine(), &yy9, &ww1, &fuji1.keys, &other2);
      let pstr = miniserde::json::to_string(&epr);
      let reproof: ExpPaillierProof = miniserde::json::from_str(&pstr).unwrap();
      assert!(reproof.verify(&r_p.to_affine(), &yy9, &ww1, &fuji2.keys, &fuji2.b1, &fuji2.b2, &other1));
    }
  }
}
