use crate::common::*;
use crate::common_defs::*;

#[derive(Serialize, Deserialize, Default)]
pub struct PrimePowerProof
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub rseed: BigUint,
  pub bitlen: usize,
  #[serde(serialize_with = "vint_as_base64", deserialize_with = "vint_from_base64")]
  pub congrs: Vec<BigUint>,
}
