use crate::common::*;
use crate::common_defs::*;
use crate::proofs::SchnorrBaseProof;

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct MtaSchnorrBaseProof
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub b: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub bd: AffinePoint,
  pub bproof: SchnorrBaseProof,
  pub bdproof: SchnorrBaseProof,
}
