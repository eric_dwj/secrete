use crate::common::fixed_len_buffer_pref;
use crate::common::to_scalar;
use crate::common::FromByteVector;
use crate::common::HexSlice;
use crate::common::ToByteVector;
use crate::statics::*;
use core::convert::TryInto;
use hmac::{Hmac, Mac, NewMac};
use k256::{AffinePoint, ProjectivePoint};
use num_bigint_dig::BigUint;
use ripemd160::Ripemd160;
use sha2::Digest;
use sha2::Sha256;
const XPUB_VERSION: u32 = 0x0488B21E;
const XPRV_VERSION: u32 = 0x0488ADE4;
const TPUB_VERSION: u32 = 0x043587CF;
const TPRV_VERSION: u32 = 0x04358394;
use crate::alloc::string::ToString;
use alloc::format;
use alloc::string::String;
use alloc::vec;
use alloc::vec::Vec;

pub fn bitcoin_checksum(buf: &[u8], len: i32) -> u32
{
  let used_len = if len <= 0 || (len as usize) > buf.len()
  {
    buf.len()
  }
  else
  {
    len as usize
  };
  let mut hasher = Sha256::new();
  hasher.update(&buf[0..used_len]);
  let res1 = hasher.finalize();
  let mut hasher = Sha256::new();
  hasher.update(res1);
  let output = hasher.finalize();
  u32::from_be_bytes(output[0..4].try_into().expect("slice with incorrect length"))
}
pub struct ExtendedKey<Key: ToByteVector + FromByteVector>
{
  pub version: u32,
  pub depth: u8,
  pub fingerprint: [u8; 4],
  pub child_number: u32,
  pub chaincode: BigUint,
  pub key: Key,
  pub is_testnet: bool,
}

impl<Key: ToByteVector + FromByteVector> ToByteVector for ExtendedKey<Key>
{
  fn to_vector(&self) -> Vec<u8>
  {
    let mut ret: Vec<u8> = Vec::new();
    ret.extend(&self.version.to_be_bytes());
    ret.extend(&self.depth.to_be_bytes());
    ret.extend(&self.fingerprint);
    ret.extend(&self.child_number.to_be_bytes());
    ret.extend(fixed_len_buffer_pref(&self.chaincode, 32));
    let key_enc = self.key.to_vector();
    if key_enc.len() < 33
    {
      //privmark
      let br: Vec<u8> = vec![0; 33 - key_enc.len()];
      ret.extend(&br);
    }
    ret.extend(&self.key.to_vector());
    let checksum = bitcoin_checksum(&ret, -1);
    ret.extend(&checksum.to_be_bytes());
    ret
  }
}
impl<Key: ToByteVector + FromByteVector + Clone> ExtendedKey<Key>
{
  pub fn decode(inp: &str) -> Result<ExtendedKey<Key>, String>
  {
    let mut dec = match bs58::decode(inp).with_alphabet(bs58::alphabet::BITCOIN).into_vec()
    {
      Ok(res) => res,
      Err(e) => return Err(format!("{}", e)),
    };
    if dec.len() < 82
    {
      let mut br: Vec<u8> = vec![0; 82 - dec.len()];
      br.extend(&dec);
      dec = br;
    }
    let chk = u32::from_be_bytes(dec[78..82].try_into().expect("decoded slice with incorrect length"));
    let actual_sum = bitcoin_checksum(&dec, 78);
    if chk != actual_sum
    {
      return Err(format!(
        "Provided checksum: {}, Calculated checksum: {}",
        chk, actual_sum
      ));
    }
    let key: Key;
    if dec[45] == 0
    {
      key = match Key::from_bytes(&dec[46..78])
      {
        Some(key) => key,
        None => return Err("Could not decode private key".to_string()),
      };
    }
    else
    {
      key = match Key::from_bytes(&dec[45..78])
      {
        Some(key) => key,
        None => return Err("Could not decode public key".to_string()),
      };
    }
    let version = u32::from_be_bytes(dec[0..4].try_into().expect("slice with incorrect length"));
    Ok(ExtendedKey {
      version: version,
      depth: u8::from_be_bytes(dec[4..5].try_into().expect("slice with incorrect length")),
      fingerprint: dec[5..9].try_into().expect("slice with incorrect length"),
      child_number: u32::from_be_bytes(dec[9..13].try_into().expect("slice with incorrect length")),
      chaincode: BigUint::from_bytes_be(&dec[13..45]),
      key: key,
      is_testnet: version == TPUB_VERSION || version == TPRV_VERSION,
    })
  }
  pub fn encode(&self) -> String
  {
    bs58::encode(self.to_vector())
      .with_alphabet(bs58::alphabet::BITCOIN)
      .into_string()
  }
  pub fn new(chaincode: &BigUint, key: &Key, test: bool) -> Self
  {
    //seems wasteful, but do I care?
    let version = if key.to_vector().len() > 32
    {
      if test
      {
        TPUB_VERSION
      }
      else
      {
        XPUB_VERSION
      }
    }
    else
    {
      if test
      {
        TPRV_VERSION
      }
      else
      {
        XPRV_VERSION
      }
    };
    ExtendedKey {
      version: version,
      depth: 0,
      fingerprint: [0; 4],
      child_number: 0,
      chaincode: chaincode.clone(),
      key: key.clone(),
      is_testnet: test,
    }
  }
}

pub type Xpub = ExtendedKey<AffinePoint>;

#[allow(dead_code)]
pub type Xprv = ExtendedKey<BigUint>;

pub fn private_to_public(key: &Xprv) -> Xpub
{
  Xpub {
    version: if key.version == XPRV_VERSION
    {
      XPUB_VERSION
    }
    else
    {
      TPUB_VERSION
    },
    depth: key.depth,
    fingerprint: key.fingerprint.clone(),
    child_number: key.child_number,
    chaincode: key.chaincode.clone(),
    key: (ProjectivePoint::generator() * to_scalar(&key.key)).to_affine(),
    is_testnet: key.is_testnet,
  }
}

pub type DerivePath = Vec<(bool, u32)>;

pub fn str_to_derive(st: &str) -> Option<DerivePath>
{
  match st
    .split("/")
    .enumerate()
    .map(|(num, st)| {
      if num == 0 && st == "m"
      {
        return Ok((false, 0));
      };

      if st.len() == 0
      {
        return match st.parse::<u32>()
        {
          Err(e) => Err(e),
          Ok(hmm) => Ok((false, hmm)),
        };
      }
      let vect: Vec<_> = st.chars().collect();
      if vect[vect.len() - 1] == '\''
      {
        let sub: String = vect[..vect.len() - 1].into_iter().collect::<String>();
        return match sub.parse::<u32>()
        {
          Ok(res) => Ok((true, res)),
          Err(e) => Err(e),
        };
      }
      return match st.parse::<u32>()
      {
        Ok(res) => Ok((false, res)),
        Err(e) => Err(e),
      };
    })
    .skip(1)
    .collect()
  {
    Ok(res) => Some(res),
    Err(_) => None,
  }
}

pub struct DeriveAccumulator
{
  pub pubkey: AffinePoint,
  pub chaincode: BigUint,
  pub accumulated: BigUint,
}

pub fn derive_accumulate(acc: &mut DeriveAccumulator, index: u32) -> bool
{
  let q = &*Q_MODULUS;

  if index >= (1 << 31)
  {
    return false;
  }
  let keybt: Vec<u8> = fixed_len_buffer_pref(&acc.chaincode, 32);

  let mut concat = acc.pubkey.to_vector();
  let mut hmac = Hmac::<sha2::Sha512>::new_varkey(&keybt).expect("Should work, hmac error?");
  let sr = index.to_be_bytes();
  concat.extend(&sr);
  hmac.update(&concat);
  let outbuf = hmac.finalize().into_bytes();
  let newchain = BigUint::from_bytes_be(&outbuf[32..]);
  let il = BigUint::from_bytes_be(&outbuf[..32]);
  if il >= *q
  {
    //println!("Rare overflow");
    return derive_accumulate(acc, index + 1);
  }
  let epar = ProjectivePoint::generator() * to_scalar(&il);
  if epar == ProjectivePoint::identity()
  {
    //println!("Rare public overflow");
    return derive_accumulate(acc, index + 1);
  };

  acc.accumulated = (&acc.accumulated + &il) % q;
  acc.chaincode = newchain;
  acc.pubkey = (ProjectivePoint::from(acc.pubkey) + epar).to_affine();
  true
}
pub fn bip_fingerprint(e: &AffinePoint) -> [u8; 4]
{
  let encoding = e.to_vector();
  let mut prefinger = sha2::Sha256::new();
  prefinger.update(&encoding);
  let outpre = prefinger.finalize();

  let mut finger = Ripemd160::new();
  finger.update(&outpre);
  let out = finger.finalize();
  return out[..4].try_into().unwrap();
}

pub fn get_derived_public_internal(xpub: &str, path: &str) -> String
{
  let mut xp = match Xpub::decode(xpub)
  {
    Ok(res) => res,
    Err(_) => return "{\"state\":\"failure\",\"error\":\"invalid xpub string\"}".to_string(),
  };
  let dpath = match str_to_derive(path)
  {
    Some(res) => res,
    None => return "{\"state\":\"failure\",\"error\":\"invalid derivation path\"}".to_string(),
  };
  for (chk, ind) in dpath
  {
    if chk
    {
      return "{\"state\":\"failure\",\"error\":\"hardened paths not supported\"}".to_string();
    }
    xp = derive_index_public(&xp, ind);
  }
  alloc::format!("{{\"result\":\"0x{}\"}}", HexSlice::new(&xp.key.to_vector()))
}
pub fn derive_index(parent: &Xprv, index: u32) -> Xprv
{
  let q = &*Q_MODULUS;

  if index >= (1 << 31)
  {
    return Xprv::new(&*UINT_ZERO, &*UINT_ZERO, true);
  } //silent fail...
  let keybt: Vec<u8> = fixed_len_buffer_pref(&parent.chaincode, 32);
  let pubkey = (ProjectivePoint::generator() * to_scalar(&parent.key)).to_affine();
  let mut concat = pubkey.to_vector();
  let mut hmac = Hmac::<sha2::Sha512>::new_varkey(&keybt).expect("HMAC init failed");
  let sr = index.to_be_bytes();
  concat.extend(&sr);
  hmac.update(&concat);
  let outbuf = hmac.finalize().into_bytes();
  let newchain = BigUint::from_bytes_be(&outbuf[32..]);
  let il = BigUint::from_bytes_be(&outbuf[..32]);
  if il >= *q
  {
    //println!("Rare overflow");
    return derive_index(parent, index + 1);
  }
  let epar = ProjectivePoint::generator() * to_scalar(&il);
  if epar == ProjectivePoint::identity()
  {
    //println!("Rare public overflow");
    return derive_index(parent, index + 1);
  };
  let newkey = (&parent.key + &il) % q;
  if newkey == *UINT_ZERO
  {
    return derive_index(parent, index + 1);
  }
  Xprv {
    chaincode: newchain,
    child_number: index,
    key: newkey,
    depth: parent.depth + 1,
    is_testnet: parent.is_testnet,
    version: parent.version,
    fingerprint: bip_fingerprint(&pubkey),
  }
}
pub fn derive_index_public(parent: &Xpub, index: u32) -> Xpub
{
  let q = &*Q_MODULUS;

  if index >= (1 << 31)
  {
    return Xpub::new(&*UINT_ZERO, &ProjectivePoint::generator().to_affine(), true);
  } //silent fail...
  let keybt: Vec<u8> = fixed_len_buffer_pref(&parent.chaincode, 32);
  let mut concat = parent.key.to_vector();
  let mut hmac = Hmac::<sha2::Sha512>::new_varkey(&keybt).expect("HMAC init failed");
  let sr = index.to_be_bytes();
  concat.extend(&sr);
  hmac.update(&concat);
  let outbuf = hmac.finalize().into_bytes();
  let newchain = BigUint::from_bytes_be(&outbuf[32..]);
  let il = BigUint::from_bytes_be(&outbuf[..32]);
  if il >= *q
  {
    //println!("Rare overflow");
    return derive_index_public(parent, index + 1);
  }
  let epar = ProjectivePoint::generator() * to_scalar(&il);
  if epar == ProjectivePoint::identity()
  {
    //println!("Rare public overflow");
    return derive_index_public(parent, index + 1);
  };
  let newkey = (ProjectivePoint::from(parent.key) + ProjectivePoint::generator() * to_scalar(&il)).to_affine();
  if ProjectivePoint::from(newkey) == ProjectivePoint::identity()
  {
    return derive_index_public(parent, index + 1);
  }
  Xpub {
    chaincode: newchain,
    child_number: index,
    key: newkey,
    depth: parent.depth + 1,
    is_testnet: parent.is_testnet,
    version: parent.version,
    fingerprint: bip_fingerprint(&parent.key),
  }
}

#[cfg(test)]
mod tests
{
  use crate::xpub::*;
  #[test]
  fn parse_xpub()
  {
    //let xprv="xprvA1J6yTC3VdytjiqFXX9sUkt1A8uMit4c9dYno97G6thtQMm6yiZxRcU5CRMziRTNawmrTzD69MppNyWd6eBtnnqQi2qCMq72ECDwLhfEjkJ";
    let xpub =
     // "xpub6EHTNxiwL1YBxCuidYgsqtpjiAjr8LnTWrUPbXWsfEEsHA6FXFtCyQnZ3iTXHGanr98DiajTscMGVUKEUkJJdW7WUZGzaGmYBdkkmp9Fgzu";
     "xpub661MyMwAqRbcF4wnYibVEPdKncgUWRRX6d1TTFYokgJewLdRZ3NWyL8uuyEkPv8vj7KsXcKmhRcG4fuSj3WEuAsD2xqRyZiB3wf3aPZ4oox";
    let key: ExtendedKey<AffinePoint> = ExtendedKey::decode(xpub).unwrap();
    assert_eq!(key.version, XPUB_VERSION);
    let enc_key = key.encode();
    assert_eq!(enc_key, xpub);
  }
  #[test]
  fn parse_xprv()
  {
    let xprv =
      "xprvA1J6yTC3VdytjiqFXX9sUkt1A8uMit4c9dYno97G6thtQMm6yiZxRcU5CRMziRTNawmrTzD69MppNyWd6eBtnnqQi2qCMq72ECDwLhfEjkJ";
    //let xpub="xpub6EHTNxiwL1YBxCuidYgsqtpjiAjr8LnTWrUPbXWsfEEsHA6FXFtCyQnZ3iTXHGanr98DiajTscMGVUKEUkJJdW7WUZGzaGmYBdkkmp9Fgzu";
    let key: ExtendedKey<BigUint> = ExtendedKey::decode(xprv).unwrap();
    assert_eq!(key.version, XPRV_VERSION);
    let enc_key = key.encode();
    assert_eq!(enc_key, xprv);
  }
  #[test]
  fn check_private_derive()
  {
    let xprv =
      "xprv9s21ZrQH143K2v3wwWQS1mHnfYVHdirC8kqFGo87Y4YDxCZNvDb43pTRQKGjNogtanSjNuKuysBb3B1uutc47LgFY58SL2Rwm994y4RacDy";
    let xpub =
      "xpub661MyMwAqRbcFQ8R3XwSNuEXDaKn3Ba3Vykr5BXj6Q5CpztXTkuJbcmuFbrbs8xnGjMecMEYdUUTD48TYjffcQhkyyCzxQAd6znmkd5qLDc";
    let xp = Xprv::decode(xprv).unwrap();
    assert_eq!(private_to_public(&xp).encode(), xpub);
    let der = derive_index(&xp, 1);
    let pder = derive_index_public(&private_to_public(&xp), 1);
    let xprv1 =
      "xprv9uJaMsKEiTbLxXKQY2YM33o8Go3C5zZnKmj342TwFrkcLnpCExtG9SkRN7P3ueYTyQpXUPLKzya8BbqqemFsrTb5kDkMmJ6TUzXWXeAzG1v";
    let xpub1 =
      "xpub68HvmNr8Yq9eB1Pse45MQBjrppsgVTHdgzedrQsYpCHbDb9LnWCWhF4uDPoaSMvH6ToLD6mqH6Wc9S8DqJEDJcMbrAKibjjEAKRSeuuNf9m";
    assert_eq!(der.encode(), xprv1);
    assert_eq!(pder.encode(), xpub1);
    assert_eq!(private_to_public(&der).encode(), xpub1);
    let der = derive_index(&xp, 2);
    let pder = derive_index_public(&private_to_public(&xp), 2);
    let xprv2 =
      "xprv9uJaMsKEiTbM1AWkvppyF2yrVQgjVuta4KQEEp3h9EhBmx6wugLm9ieAxdowNkhbpkyw9TNVzpnv9z5B4XrT7rcqquWvNbAhJCoeojCZbQ6";
    let xpub2 =
      "xpub68HvmNr8Yq9eDebE2rMycAvb3SXDuNcRRYKq3CTJhaEAekS6TDf1hWxeovoR516ke56PG226hkgeQVMTCCMcYAyyrojhAa7ADPGCcgPjDQa";
    assert_eq!(der.encode(), xprv2);
    assert_eq!(pder.encode(), xpub2);
    assert_eq!(private_to_public(&der).encode(), xpub2);
    let der = derive_index(&derive_index(&xp, 1), 2);
    let pder = derive_index_public(&derive_index_public(&private_to_public(&xp), 1), 2);
    let xprv1_2 =
      "xprv9vx7oiULoGF3AGmkYPqh9s2SkYkKhi5fPpTUMJ1iW5pJJ7gUCTzJQbNADQsKf4xhGCnnZCz2n1vqVq1t9tPBco66QCv6Hno6uj7AtxQNFTH";
    let xpub1_2 =
      "xpub69wUDE1EddoLNkrDeRNhWzyBJaap7AoWm3P59gRL4RMHAv1ck1JYxPge4iC71UXQUysX9DHwbLWGxeuCPYRFAmWa69b8wux6CJCEo7Rjr8y";
    assert_eq!(der.encode(), xprv1_2);
    assert_eq!(pder.encode(), xpub1_2);
    assert_eq!(private_to_public(&der).encode(), xpub1_2);
  }
}
