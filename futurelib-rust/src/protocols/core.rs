/// Protocol core module, defines macros used in all protocols
/// Each protocol must have core/naming struct (empty), parameters struct  - (supplied from outside through json), 
/// state struct - used to maintain state between JS invocations, saved as json,
/// output struct - struct that gets emitted after protocol ends. 
/// Due to macro hygiene, each step of the protocols havew to be a closure with fixed parameters 
/// Parameters are: (parameterStruct, mutable stateStruct, inputMessage, mutable outputMessage)
/// helper macros to convert messages to format proper for current step are geet! and yeet!
/// Each closure must return ExecutionControl enum. Most of the time, it is ExecutionControl::None
/// In case of error, it is ExecutionControl::Error(errormessage: String)
/// ExecutionControl::ChildStart is used if the next step invokes await_child!
/// ChildWait is used inside await_child! macro 
/// 
/// The main macros are:
/// declare_protocol_flow! -> actually defines protocol as series of steps. Has preamble, steps and output generation aspects. See the code for examples.
/// geet!(in_msg,MsgType) -> returns input message  parsed as MsgType (or errors out if parsing does not work)
/// yeet!(out_msg,message) -> sets message as output of current step. Should be used once per step (and omitted on the last client step)
/// await_child!(state,in_msg,out_msg,ChildType,child_parameters) -> returns Output of the child protocol, assuming it ran correctly.
/// 
/// The traits for protocol data are:
/// ParValidation -> parameter validation, in case it is needed or possible, have to be implemented for parameter struct
/// ProtocolSavedState -> for saved state, which has to have access to ProtocolState and child state
/// OutputWithExports -> implemented for output struct to get access to exports field. Exports are used in JS library to separate fields that are not encrypted.

use crate::common_defs::*;

//parameters: protocol global state (external)
//savedsate: current proto step
#[derive(Serialize, Deserialize, Clone)]
pub struct ProtocolState
{
  pub _terminated: bool,
  pub _error: bool,
  pub _step_offset: i64,
}
impl ProtocolState
{
  pub fn new() -> ProtocolState
  {
    ProtocolState {
      _terminated: false,
      _error: false,
      _step_offset: -1,
    }
  }
}
impl core::default::Default for ProtocolState
{
  fn default() -> Self
  {
    ProtocolState::new()
  }
}
pub trait ProtocolSavedState: Serialize + Deserialize + Default
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState;
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>;
  fn new() -> Self;
}

pub trait OutputWithExports: Serialize + Deserialize
{
  fn get_exports(&self) -> Vec<String>;
  fn set_exports(&mut self, exports: &[&str]);
  fn finalize(&mut self);
}
pub trait ParValidation: Serialize + Deserialize
{
  fn validate(&self) -> bool;
}

pub fn message_to_raw<M: Serialize>(m: &M) -> Box<RawValue>
{
  Box::new(RawValue::create(m))
}


// error, result=>packet, save, output -> final
// save and output have fixed types, but result does not, since packet return varies depending on stage
// save must contain ProtocolState in some form
#[derive(Serialize, Deserialize)]
pub struct ProtoWrapper<Save, Out>
{
  pub complete: Option<bool>,
  pub save: Option<Save>,
  pub state: String,
  pub result: Box<RawValue>,
  pub output: Option<Out>,
  pub error: Option<String>,
}

impl<Save, Out> ProtoWrapper<Save, Out>
{
  pub fn is_result_empty(&self) -> bool
  {
    let sub = self.result.get();
    sub == "" || sub == "{}" || sub == "null" || sub == "{ }"
  }
  pub fn new_ok<Res: Serialize>(
    sv: Save, res: Res, state: &str, complete: bool, output: Option<Out>,
  ) -> ProtoWrapper<Save, Out>
  {
    //let res_str = miniserde::json::to_string(&res);
    ProtoWrapper {
      complete: {
        if complete
        {
          Some(complete)
        }
        else
        {
          None
        }
      },
      save: Some(sv),
      state: state.to_string(),
      result: Box::new(RawValue::create(&res)),
      error: None,
      output: output,
    }
  }
  pub fn new_err(message: &str, complete: bool) -> ProtoWrapper<Save, Out>
  {
    ProtoWrapper {
      complete: {
        if complete
        {
          Some(complete)
        }
        else
        {
          None
        }
      },
      save: None,
      state: "error".to_string(),
      result: Default::default(),
      error: Some(message.to_string()),
      output: None,
    }
  }
  pub fn new_empty() -> ProtoWrapper<Save, Out>
  {
    ProtoWrapper {
      complete: None,
      save: None,
      state: "empty".to_string(),
      result: Default::default(),
      error: None,
      output: None,
    }
  }
}

pub trait AbstractHierarchyProtocolSide
{
  type Output: OutputWithExports;
  type Parameters: ParValidation;
  type Saved: ProtocolSavedState;

  fn is_terminated(sv: &mut Self::Saved) -> bool
  {
    sv.get_internal_state()._terminated
  }
  fn is_error(sv: &mut Self::Saved) -> bool
  {
    sv.get_internal_state()._error
  }
  fn process_state(
    pars: &Self::Parameters, s: ProtoWrapper<Self::Saved, Self::Output>,
  ) -> ProtoWrapper<Self::Saved, Self::Output>;

  //<Pars:ParValidation+DeserializeOwned,Save:ProtocolSavedState,Out:OutputWithExports,Proto:AbstractHierarchyProtocolSide<Saved=Save,Output=Out,Parameters=Pars>>
  fn process_packet(parameters: &str, sstore: &str, exports: &[&str]) -> String
  {
    let store: ProtoWrapper<Self::Saved, Self::Output>;
    if sstore == "{}" || sstore == "{ }"
    {
      store = ProtoWrapper::<Self::Saved, Self::Output>::new_empty()
    }
    else
    {
      store = match miniserde::json::from_str(sstore)
      {
        Ok(r) => r,
        Err(e) =>
        {
          return miniserde::json::to_string(&ProtoWrapper::<Self::Saved, Self::Output>::new_err(
            &format!("Store corrupted: {:?} {}", e, sstore),
            false,
          ))
        }
      };
    }
    let kparams: Self::Parameters = match miniserde::json::from_str(parameters)
    {
      Ok(r) => r,
      Err(e) =>
      {
        return miniserde::json::to_string(&ProtoWrapper::<Self::Saved, Self::Output>::new_err(
          &format!("Parameters corrupted: {:?}", e),
          false,
        ))
      }
    };
    if !kparams.validate()
    {
      return miniserde::json::to_string(&ProtoWrapper::<Self::Saved, Self::Output>::new_err(
        "Parameters not set correctly!",
        false,
      ));
    }
    if store.complete == Some(true)
    {
      return miniserde::json::to_string(&store);
    }
    let mut state = Self::process_state(&kparams, store);
    if Some(true) == state.complete
    {
      if let Some(mut output) = state.output
      {
        output.finalize();
        if exports.len() > 0
        {
          output.set_exports(exports);
        }
        state.output = Some(output);
        state.save = None;
      }
    }
    miniserde::json::to_string(&state)
  }
}

#[derive(Serialize, Deserialize)]
pub struct DummyOutput {}

#[macro_export]
macro_rules! yeet {
  ($refname:ident,$e:expr) => {{
    *$refname = message_to_raw(&$e);
  }};
}

pub enum ExecutionControl
{
  None,
  Error(String),
  ChildStart,
  ChildWait,
}

#[macro_export]
macro_rules! geet {
  ($refname:ident,$mtype:ty) => {{
    let inp2: $mtype = match miniserde::json::from_str($refname.get())
    {
      Ok(res) => res,
      Err(e) => return ExecutionControl::Error(format!("Invalid message type : {:?}, aborting", &e)),
    };
    inp2
  }};
}

#[macro_export]
macro_rules! declare_protocol_flow
{

    ($name:ident,$pars:ty,$state:ty,$outp:ty, $(preamble {$($pre:stmt)+});* ;$(step $step:expr);+ ;output $out:expr ) => {
        impl AbstractHierarchyProtocolSide for $name
        {
            type  Output=$outp;
            type Parameters=$pars;
            type Saved=$state;

        fn process_state(pars:&Self::Parameters,mut s:ProtoWrapper<Self::Saved,Self::Output>)->ProtoWrapper<Self::Saved,Self::Output>
        {
         //macro
         let state= match &mut s.save
         {
          Some(ref mut state) =>{ if state.get_internal_state()._terminated &&! state.get_internal_state()._error
                 {
                     return ProtoWrapper::new_ok(state.clone(), DummyOutput{}, "success",  true, None)
                 }
                 if state.get_internal_state()._terminated && state.get_internal_state()._error
                 {
                     return ProtoWrapper::new_err("Already terminated with error", false)
                 }
                 state
          }
          None =>
          {
          s.save=Some(Self::Saved::new());
          match &mut s.save
          {
             Some(ref mut state) =>state,
             None =>     return ProtoWrapper::new_err("Error creating state", false)
          }
          }
         };
        let message=&mut s.result;
        let mut _out_message=message_to_raw(&DummyOutput{});
        let mut hval=state.get_internal_state()._step_offset;
        if hval==-1
        {
            hval=0;
        }
        let mut _step_counter=0;
        $( $($pre)+ )*
        let mut steps:Vec<Box<dyn FnMut(&Self::Parameters,&mut Self::Saved,&mut Box<RawValue>, &mut Box<RawValue>)->ExecutionControl>>= <[_]>::into_vec(Box::new([$(Box::new($step)),+]));
        let mut output_fn:Box<dyn FnMut(&Self::Parameters,&mut Self::Saved,&mut Box<RawValue>, &mut Box<RawValue>)->Self::Output>=Box::new($out);
        loop {
        if hval< (steps.len() as i64) && hval>=0
        {
            let ind=hval as usize;
          match steps[ind](pars,state,message,&mut _out_message)
          {
            ExecutionControl::Error(error) =>{ return ProtoWrapper::new_err(&error, false) },
            ExecutionControl::None =>{hval+=1; break; },
            ExecutionControl::ChildStart =>{hval+=1; },
            ExecutionControl::ChildWait=>{break; }
          }
        }
        }
        if hval==(steps.len() as i64)
      {
          state.get_internal_state()._terminated=true;
          //also produce output...
          let output:Self::Output=output_fn(pars,state,message, &mut _out_message);
          s.output=Some(output) ;
      }
         //end macro
         state.get_internal_state()._step_offset=hval;
         if  state.get_internal_state()._terminated
         {
             s.complete=Some(true);
         }
         else
         {
             s.complete=Some(false);
         }
         s.state="success".to_string();
         s.result=_out_message;
         s
        }
    }
    };

}

#[macro_export]
macro_rules! await_child
{
    ($state:ident,$in_msg:ident,$out_msg:ident,$proto:ident, $parameters:expr)=>{{


            let __data:<$proto as AbstractHierarchyProtocolSide>::Parameters=$parameters;//<$proto as AbstractHierarchyProtocolSide>::Parameters {$($id:$val),*};
            let mut _istate:ProtoWrapper<<$proto as AbstractHierarchyProtocolSide>::Saved,<$proto as AbstractHierarchyProtocolSide>::Output>= match $state.get_child_value()
            {
                Some(res) => match miniserde::json::from_str(res.get())
                {
                    Ok(pack)=>pack,
                    Err(_e) => return ExecutionControl::Error("Child state corrupted".to_string())
                },
                None => ProtoWrapper::<<$proto as AbstractHierarchyProtocolSide>::Saved,<$proto as AbstractHierarchyProtocolSide>::Output>::new_empty()
            };
            if _istate.complete==Some(true)
            {
               if _istate.output.is_none()
               {
                return ExecutionControl::Error("Child terminal output corrupted".to_string())
               }
            }
            else
            {
                _istate.result=$in_msg.clone();
                let mut _cres=$proto::process_state(&__data,_istate);
                if _cres.error.is_some()
                {
                    return ExecutionControl::Error(_cres.error.unwrap());
                }
                if _cres.complete!=Some(true) || !_cres.is_result_empty()
                {
                    *$out_msg=_cres.result;
                    _cres.result=Default::default();
                    *$state.get_child_value()=Some(Box::new(RawValue::create(&_cres)));
                    return ExecutionControl::ChildWait;
                }
                _istate=_cres;
                //yeet!(out_msg,outp);
            }
           let _out=match _istate.output
           {
               Some(out)=>out,
               None => return ExecutionControl::Error("Child protocol terminated without result".to_string())
           };
           *$state.get_child_value()=None;
           _out
    }}
}

#[cfg(test)]
pub mod tests
{
  use crate::protocols::core::*;
  //use rsa::*;
  use libc_print::*;
  pub fn run_protocol<Client: AbstractHierarchyProtocolSide, Server: AbstractHierarchyProtocolSide>(
    cp: &Client::Parameters, sp: &Server::Parameters,
  ) -> (Client::Output, Server::Output)
  {
    let client_pstr = miniserde::json::to_string(&cp);
    let server_pstr = miniserde::json::to_string(&sp);
    let mut cl_storage = ProtoWrapper::<Client::Saved, Client::Output>::new_empty();
    let mut sr_storage = ProtoWrapper::<Server::Saved, Server::Output>::new_empty();
    let mut cnt = 0;
    loop
    {
      let cl_store = miniserde::json::to_string(&cl_storage);
      libc_println!("Client inp: {}", cl_store);
      libc_println!("Client pars: {}", client_pstr);
      let pack_ret = Client::process_packet(&client_pstr, &cl_store, &vec!["export"]);
      libc_println!("Client return: {}", pack_ret);
      cl_storage = miniserde::json::from_str(&pack_ret).unwrap();
      assert!(cl_storage.state != "error");
      if sr_storage.complete != Some(true)
      {
        sr_storage.result = cl_storage.result;
        let sr_store = miniserde::json::to_string(&sr_storage);
        libc_println!("Server inp: {}", sr_store);
        let pack_ret = Server::process_packet(&server_pstr, &sr_store, &vec!["export"]);
        libc_println!("Server return: {}", pack_ret);
        sr_storage = miniserde::json::from_str(&pack_ret).unwrap();
        assert!(sr_storage.state != "error");
        cl_storage.result = sr_storage.result;
      }
      if cl_storage.complete == Some(true) && sr_storage.complete == Some(true)
      {
        break;
      }
      cnt += 1;
      assert!(cnt < 120);
    }
    (cl_storage.output.unwrap(), sr_storage.output.unwrap())
  }
}
