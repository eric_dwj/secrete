/// Schnorr key generation. Generates 2-2 key between client and server, following https://github.com/ZenGo-X/multi-party-schnorr/blob/master/papers/provably_secure_distributed_schnorr_signatures_and_a_threshold_scheme.pdf
/// Key is partially normalized, further normalization after derivation. FOr normalization details (sign flipping) see https://github.com/bitcoin/bips/blob/master/bip-0340.mediawiki
/// Parameters are backup_pub_key (RSA public key for backup) 
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::RsaEllipticProof;
use crate::protocols::core::*;
use crate::xpub::*;
use crate::{declare_protocol_flow, geet, rotate_entropy, yeet};

pub struct SchnorrKeyGenClient {}

//as per https://github.com/ZenGo-X/multi-party-schnorr/blob/master/papers/provably_secure_distributed_schnorr_signatures_and_a_threshold_scheme.pdf

#[derive(Serialize, Deserialize)]
pub struct SchnorrKeyGenParameters
{
  #[serde(rename = "backupPublicKey")]
  pub backup_pub_key: WrappedPubkey,
}
impl ParValidation for SchnorrKeyGenParameters
{
  fn validate(&self) -> bool
  {
    true
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SchnorrKeyGenState
{
  internal: ProtocolState,
  __cchild: Option<Box<RawValue>>,
  //  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  //  ui: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  ri: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  rid: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  si: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  f1: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  f1d: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gbase: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub hbase: BigUint,

  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  gg: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  hh: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  combined: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  combinedd: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  chaincode: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  encrypted_their_backup: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pubkey: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub_other: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  func_other: AffinePoint,
}

impl ProtocolSavedState for SchnorrKeyGenState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> SchnorrKeyGenState
  {
    Default::default()
  }
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct SchnorrKeyGenOutput
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub priv_key_share: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pub_mine: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pub_other: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub gg: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub hh: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub func_sum: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pubkey: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub chaincode: BigUint,
  #[serde(rename = "encryptedTheirBackup")]
  #[serde(serialize_with = "int_as_base58", deserialize_with = "int_from_base58")]
  pub encrypted_their_backup: BigUint,
  #[serde(rename = "encryptedOurBackup")]
  #[serde(serialize_with = "int_as_base58", deserialize_with = "int_from_base58")]
  pub encrypted_our_backup: BigUint,
  pub xpub: String,
  pub tpub: String,
  pub export: Vec<String>,
}

impl OutputWithExports for SchnorrKeyGenOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize)]
pub struct SchnorrKgenS1
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub gg_half: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub hh_half: AffinePoint,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrKgenS2
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub hh_half: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub gg_half: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub si: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sid: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit1: AffinePoint,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrKgenS3
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub si: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sid: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit1: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a1: AffinePoint,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrKgenS4
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a1: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub enc_backup: BigUint,
  pub backup_proof: RsaEllipticProof,
}

declare_protocol_flow! {
    SchnorrKeyGenClient, SchnorrKeyGenParameters,SchnorrKeyGenState,SchnorrKeyGenOutput,
    preamble {
        let chlimit=&*UINT_TWO<<255
        let mut rng=OsRng
        let q=&*Q_MODULUS
        let my_x=&*FRONTEND_PARTY
        let other_x=&*BACKEND_PARTY
    };
    step |_pars,state,_in_msg,out_msg|{ // create framework for Pedersen commits: Similar to Fujisaki.
        state.ri=gen_rand_z(&mut rng, q,false).unwrap();
        state.rid=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1d=gen_rand_z(&mut rng, q,false).unwrap();
        let alph=gen_rand_z(&mut rng, q,false).unwrap();
        state.gbase=alph;
        let bet=gen_rand_z(&mut rng, q,false).unwrap();
        state.hbase=bet;
        state.gg=(ProjectivePoint::generator() * to_scalar(&state.gbase)).to_affine();
        let outp=SchnorrKgenS1{gg_half:state.gg.clone(),hh_half:(ProjectivePoint::generator() * to_scalar(&state.hbase)).to_affine()};

        yeet!(out_msg,outp);
        ExecutionControl::None
    };
   step |_pars,state,in_msg,out_msg|{
    let inp2 = geet!(in_msg,SchnorrKgenS2);
    if ProjectivePoint::from(inp2.hh_half) == ProjectivePoint::identity() || ProjectivePoint::from(inp2.gg_half) == ProjectivePoint::identity()
    {
        return ExecutionControl::Error("Incorrect H or G from server".to_string());
    }
    // check pedersen...
    state.hh=(ProjectivePoint::from(inp2.hh_half)*to_scalar(&state.hbase)).to_affine();
    state.gg=(ProjectivePoint::from(inp2.gg_half)*to_scalar(&state.gbase)).to_affine();
    if ProjectivePoint::from(state.hh) == ProjectivePoint::identity() || ProjectivePoint::from(state.gg) == ProjectivePoint::identity() || state.hh==state.gg
    {
        return ExecutionControl::Error("Incorrect H or G generated from server".to_string());
    }
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,state.gg);
    let pn1=ProjectivePoint::from(state.gg)*to_scalar(&inp2.si)+ProjectivePoint::from(state.hh)*to_scalar(&inp2.sid);
    let pn2=ProjectivePoint::from(inp2.commit0)+ProjectivePoint::from(inp2.commit1)*to_scalar(my_x);
    if pn1!=pn2
    {
        // honestly not sure how well this works in 2 out of 2...
        return ExecutionControl::Error("Invalid pedersen commitment from server".to_string());
    }
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,state.hh,inp2.si,inp2.sid);
    state.si=inp2.si;
    state.combinedd=(&state.rid+&state.f1d*my_x+&inp2.sid)%q;
    let a00=(ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,a00);
    let out3= SchnorrKgenS3{
        si: (&state.ri+&state.f1*other_x)%q,
        sid: (&state.rid+&state.f1d*other_x)%q,
        commit0: (ProjectivePoint::from(state.gg)*to_scalar(&state.ri)+ProjectivePoint::from(state.hh)*to_scalar(&state.rid)).to_affine(),
        commit1: (ProjectivePoint::from(state.gg)*to_scalar(&state.f1)+ProjectivePoint::from(state.hh)*to_scalar(&state.f1d)).to_affine(),
        a0: a00,
        a1: (ProjectivePoint::generator() * to_scalar(&state.f1)).to_affine(),
    };

    yeet!(out_msg,out3);
    ExecutionControl::None
    };

 step |pars,state,in_msg,_out_msg|{
    let inp4 = geet!(in_msg,SchnorrKgenS4);
    let fcheck=ProjectivePoint::from(inp4.a0)+ProjectivePoint::from(inp4.a1)*to_scalar(my_x);
    let f2=ProjectivePoint::generator() * to_scalar(&state.si);
    if f2!=fcheck
    {
        return ExecutionControl::Error("Invalid feldman commitment from server".to_string());
    }
    state.pub_other=inp4.a0;
    state.func_other=inp4.a1;
    state.pubkey=(ProjectivePoint::from(state.pub_other)+ ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    //normalize??
    if state.pubkey.to_vector()[0]==3
    {
        state.pub_other=(-ProjectivePoint::from(state.pub_other)).to_affine();
      state.func_other=(-ProjectivePoint::from(state.func_other)).to_affine();
      state.ri=q-&state.ri;
      state.f1=q-&state.f1;
      state.si=q-&state.si;
        state.pubkey= (-ProjectivePoint::from(state.pubkey)).to_affine();
    }
    state.combined=(&state.ri+&state.f1*my_x+&state.si)%q;


    if ! inp4.backup_proof.verify(&pars.backup_pub_key.pem,&inp4.a0, &inp4.enc_backup)
    {
        return ExecutionControl::Error("Server could not prove backup encryption correctness".to_string());
    }

    state.encrypted_their_backup=inp4.enc_backup;
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,inp4.a0);
   ExecutionControl::None
   };
   output |pars,state,_in_msg,_out_msg|
   {
   let pub_mine= ProjectivePoint::generator() * to_scalar(&state.ri);
   SchnorrKeyGenOutput
    {
        priv_key_share:state.combined.clone(),
        pub_mine:pub_mine.to_affine(),
        pub_other:state.pub_other.clone(),
        func_sum: (ProjectivePoint::generator()*to_scalar(&state.f1)+ProjectivePoint::from(state.func_other)).to_affine(),
        xpub: Xpub::new(&state.chaincode,&state.pubkey,false).encode(),
        tpub: Xpub::new(&state.chaincode,&state.pubkey,true).encode(),
        pubkey:state.pubkey.clone(),
        gg:state.gg.clone(),
        hh:state.hh.clone(),
        chaincode:state.chaincode.clone(),
        encrypted_their_backup:state.encrypted_their_backup.clone(),
        encrypted_our_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem, &bi_combine(&state.ri,&state.chaincode)),
        export:vec!["xpub".to_string(),"tpub".to_string(),"encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string()]
     }
   }
}

#[cfg(feature = "server")]
pub struct SchnorrKeyGenServer {}

#[cfg(feature = "server")]
declare_protocol_flow! {
    SchnorrKeyGenServer, SchnorrKeyGenParameters,SchnorrKeyGenState,SchnorrKeyGenOutput,
    preamble {
        let chlimit=&*UINT_TWO<<255
        let mut rng=OsRng
        let q=&*Q_MODULUS
        let my_x=&*BACKEND_PARTY
        let other_x=&*FRONTEND_PARTY
    };
    step |_pars,state,in_msg,out_msg|{ // create framework for Pedersen commits: Similar to Fujisaki.
        let inp1 = geet!(in_msg,SchnorrKgenS1);
        if ProjectivePoint::from(inp1.gg_half) == ProjectivePoint::identity() ||ProjectivePoint::from(inp1.hh_half) == ProjectivePoint::identity()
        {
            return ExecutionControl::Error("Incorrect G from client".to_string());
        }
        let alph=gen_rand_z(&mut rng, q,false).unwrap();
        state.gbase=alph;
        let bet=gen_rand_z(&mut rng, q,false).unwrap();
        state.hbase=bet;

        state.hh=(ProjectivePoint::from(inp1.hh_half)*to_scalar(&state.hbase)).to_affine();
        state.gg=(ProjectivePoint::from(inp1.gg_half)*to_scalar(&state.gbase)).to_affine();
        if ProjectivePoint::from(state.hh) == ProjectivePoint::identity() || ProjectivePoint::from(state.gg) == ProjectivePoint::identity() || state.hh==state.gg
        {
            return ExecutionControl::Error("Incorrect H or G generated from client".to_string());
        }
        state.ri=gen_rand_z(&mut rng, q,false).unwrap();
        state.rid=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1d=gen_rand_z(&mut rng, q,false).unwrap();
        if state.hh==state.gg
        {
            return ExecutionControl::Error("Incorrect H generated".to_string());
        }
        let outp=SchnorrKgenS2{
            hh_half:(ProjectivePoint::generator() * to_scalar(&state.hbase)).to_affine(),
            gg_half:(ProjectivePoint::generator() * to_scalar(&state.gbase)).to_affine(),
            si: (&state.ri+&state.f1*other_x)%q,
            sid: (&state.rid+&state.f1d*other_x)%q,
            commit0: (ProjectivePoint::from(state.gg)*to_scalar(&state.ri)+ProjectivePoint::from(state.hh)*to_scalar(&state.rid)).to_affine(),
            commit1: (ProjectivePoint::from(state.gg)*to_scalar(&state.f1)+ProjectivePoint::from(state.hh)*to_scalar(&state.f1d)).to_affine(),
        };
        state.chaincode=rotate_entropy!(state.chaincode,&chlimit,state.gg);
        state.chaincode=rotate_entropy!(state.chaincode,&chlimit,state.hh,outp.si,outp.sid);
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
   step |pars,state,in_msg,out_msg|{
    let inp2 = geet!(in_msg,SchnorrKgenS3);
    // check pedersen...
    let pn1=ProjectivePoint::from(state.gg)*to_scalar(&inp2.si)+ProjectivePoint::from(state.hh)*to_scalar(&inp2.sid);
    let pn2=ProjectivePoint::from(inp2.commit0)+ProjectivePoint::from(inp2.commit1)*to_scalar(my_x);
    if pn1!=pn2
    {
        // honestly not sure how well this works in 2 out of 2...
        return ExecutionControl::Error("Invalid pedersen commitment from client".to_string());
    }

    state.si=inp2.si;
    //state.sid=inp2.sid;

    state.combinedd=&state.rid+&state.f1d*my_x+&inp2.sid;
    let a00=(ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,inp2.a0);
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,a00);
    let out3= SchnorrKgenS4{
        a0: a00,
        a1: (ProjectivePoint::generator() * to_scalar(&state.f1)).to_affine(),
        enc_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem, &state.ri),
        backup_proof:RsaEllipticProof::new(&pars.backup_pub_key.pem,&a00,&to_scalar(&state.ri))
    };
    state.pub_other=inp2.a0;
    state.func_other=inp2.a1;
    state.pubkey=(ProjectivePoint::from(state.pub_other)+ ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    if state.pubkey.to_vector()[0]==3
    {
        state.pub_other=(-ProjectivePoint::from(state.pub_other)).to_affine();
        state.func_other=(-ProjectivePoint::from(state.func_other)).to_affine();
        state.ri=q-&state.ri;
        state.f1=q-&state.f1;
        state.si=q-&state.si;
        state.pubkey= (-ProjectivePoint::from(state.pubkey)).to_affine();
    }
    state.combined=(&state.ri+&state.f1*my_x+&state.si)%q;
    yeet!(out_msg,out3);
    ExecutionControl::None
    };

   output |pars,state,_in_msg,_out_msg|
   {
   let pub_mine= ProjectivePoint::generator() * to_scalar(&state.ri);
  SchnorrKeyGenOutput
    {
        priv_key_share:state.combined.clone(),
        pub_mine:pub_mine.to_affine(),
        pub_other:state.pub_other.clone(),
        func_sum: (ProjectivePoint::generator()*to_scalar(&state.f1)+ProjectivePoint::from(state.func_other)).to_affine(),
        xpub: Xpub::new(&state.chaincode,&state.pubkey,false).encode(),
        tpub: Xpub::new(&state.chaincode,&state.pubkey,true).encode(),
        pubkey:state.pubkey.clone(),
        gg:state.gg.clone(),
        hh:state.hh.clone(),
        chaincode:state.chaincode.clone(),
        encrypted_their_backup:state.encrypted_their_backup.clone(),
        encrypted_our_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem, &bi_combine(&state.ri,&state.chaincode)),
        export:vec!["xpub".to_string(),"tpub".to_string(),"encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string()]
     }
   }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::schnorr_keygen::*;
  use rsa::*;
  #[test]
  fn schnorr_keygen_check()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = SchnorrKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = SchnorrKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) =
      run_protocol::<SchnorrKeyGenClient, SchnorrKeyGenServer>(&client_params, &server_params);
    assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pubkey, out_server.pubkey);
  }
}
