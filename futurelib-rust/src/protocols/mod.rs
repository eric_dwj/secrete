/// Protocols module definition. Expose new protocols here after adding .rs files into the folder
pub mod core;
pub mod eddsa25519_keygen;
pub mod eddsa25519_sign;
pub mod feldman_vss;
pub mod full_sign;
pub mod keygen;
pub mod mta;
pub mod presign;
pub mod schnorr_keygen;
pub mod schnorr_sign;
pub mod schnorrkel_keygen;
pub mod schnorrkel_sign;
pub mod sign;
