/// Eddsa signing process, adopted from https://link.springer.com/chapter/10.1007/3-540-47719-5_33 (https://github.com/ZenGo-X/multi-party-schnorr/tree/master/papers) 
/// Adapted for client-server architecture, so no logner very useful for more than two parties
use crate::algorithms::eddsa25519::{
  scalar_as_base64, scalar_from_base64, ExpandedSecretKey, PublicKey as EdPk, SecretKey as EdSk, Signature as EdSig,
  SECRET_KEY_LENGTH,
};
use crate::common::*;
use crate::common_defs::*;
use crate::protocols::core::*;
use crate::protocols::eddsa25519_keygen::*;
use crate::{declare_protocol_flow, geet, yeet};
use curve25519_dalek::scalar::Scalar as Ednum;
pub use sha2::Sha512;

#[derive(Serialize, Deserialize, Clone)]
pub struct Ed255SignParameters
{
  pub keygen: Ed255KeygenOutput,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub message: Vec<u8>,
}

impl ParValidation for Ed255SignParameters
{
  fn validate(&self) -> bool
  {
    true
  }
}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Ed255SignState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,
  pub signature: EdSig,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  ri: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  rid: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  si: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  f1: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  f1d: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  combined: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  combinedd: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub vv: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub_other: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  func_other: EdPk,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub gamma: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub sigma: Ednum,
}

impl ProtocolSavedState for Ed255SignState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> Ed255SignState
  {
    Default::default()
  }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Ed255SignOutput
{
  pub signature: EdSig,
  pub export: Vec<String>,
}

impl OutputWithExports for Ed255SignOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize)]
pub struct Ed255SignS1
{
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub si: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub sid: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit1: EdPk,
}

#[derive(Serialize, Deserialize)]
pub struct Ed255SignS2
{
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub si: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub sid: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit1: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a1: EdPk,
}
#[derive(Serialize, Deserialize)]
pub struct Ed255SignS3
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a1: EdPk,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub gamma: Ednum,
}

#[derive(Serialize, Deserialize)]
pub struct Ed255SignS4
{
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub gamma: Ednum,
}

pub struct Ed255SignClient {}
declare_protocol_flow! {
    Ed255SignClient, Ed255SignParameters,Ed255SignState,Ed255SignOutput,
    preamble {
      let mut rng=OsRng
      let my_x=&Ednum::from_bits([1; SECRET_KEY_LENGTH])
      let other_x=Ednum::from_bits([2; SECRET_KEY_LENGTH])
     };
    step |pars,state,_in_msg,out_msg|{
        let extsk=ExpandedSecretKey::from(&pars.keygen.secret);
        let mut h: Sha512 = Sha512::default();
        h.update(&extsk.nonce);
        h.update(&pars.message);
       // let sign=extsk.sign_prehashed(h,&(&pars.keygen.secret).into(),None).unwrap();
        let mt:[u8;32]=h.finalize()[..32].try_into().unwrap();
        state.ri=Ednum::from_bits(mt);
        state.rid=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
        state.f1=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
        state.f1d=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;


        let out= Ed255SignS1{
          si: &state.ri+&state.f1*other_x,
          sid: &state.rid+&state.f1d*other_x,
          commit0: to_edcom(pars.keygen.gg.1*&state.ri+pars.keygen.hh.1*&state.rid),
          commit1: to_edcom(pars.keygen.gg.1*&state.f1+pars.keygen.hh.1*&state.f1d),
      };
      yeet!(out_msg,out);
      ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
      let resp=geet!(in_msg,Ed255SignS2);
      let pn1=pars.keygen.gg.1*&resp.si+pars.keygen.hh.1*&resp.sid;
      let pn2=resp.commit0.1+resp.commit1.1*my_x;
      if pn1!=pn2
      {
          // honestly not sure how well this works in 2 out of 2...
          return ExecutionControl::Error("Invalid pedersen commitment from server".to_string());
      }
      let fcheck=resp.a0.1+resp.a1.1*my_x;
      state.si=resp.si;
      let f2=to_edpk(&state.si);
      if f2.1!=fcheck
      {
          return ExecutionControl::Error("Invalid feldman commitment from server".to_string());
      }


      state.combinedd=&state.rid+&state.f1d*my_x+&resp.sid;
      state.pub_other=resp.a0;
      state.func_other=resp.a1;
      state.vv=to_edcom(state.pub_other.1 + to_edpk(&state.ri).1);
      let a00=to_edpk(&state.ri);
      let a01=to_edpk(&state.f1);
      state.combined=&state.ri+&state.f1*my_x+&state.si;
      let prv=&pars.keygen.priv_key_share;
      let mut h: Sha512 = Sha512::new();
      let k: Ednum;
      h.update(&state.vv.0.0);
      h.update(pars.keygen.pubkey.as_bytes());
      h.update(&pars.message);
      k = Ednum::from_hash(h);
      //s = &(&k * &expanded.key) + r;
      state.gamma=prv*k+&state.combined;
      let out3= Ed255SignS3{
          a0: a00,
          a1: a01,
          gamma:state.gamma.clone()
      };
      yeet!(out_msg,out3);
      ExecutionControl::None
    };
    step |pars,state,in_msg,_out_msg|{

      let inp4 = geet!(in_msg,Ed255SignS4);
      let chk1=to_edpk(&inp4.gamma);
      let fns=&pars.keygen.func_sum;
      let mut h: Sha512 = Sha512::new();
      h.update(&state.vv.0.0);
      h.update(pars.keygen.pubkey.as_bytes());
      h.update(&pars.message);
      let e = Ednum::from_hash(h);
      let chk2=state.vv.1+state.func_other.1*(&other_x)+to_edpk(&state.f1).1*&other_x+(pars.keygen.pubkey.1+fns.1*(&other_x))*(&e);
      if chk2!=chk1.1
      {
          return ExecutionControl::Error("Invalid gamma from server".to_string());
      }
      let my_sig=my_x.clone();
      let other_sig= other_x.clone();
      let my_gamma=state.gamma.clone();
      let other_gamma= inp4.gamma.clone();

      let l0=&(&other_sig*(&other_sig-&my_sig).invert());
      let l1=&(&my_sig*(&my_sig-&other_sig).invert());
      let sgm=l0*&my_gamma+&other_gamma*l1;

      state.sigma=sgm;
      let chk1=to_edpk(&state.sigma).1-pars.keygen.pubkey.1*&e;
      if chk1!=state.vv.1
      {
       return ExecutionControl::Error("Invalid sign...".to_string())
      }
      let sig=EdSig{R: state.vv.0.clone(), s:state.sigma.clone()};
      if !pars.keygen.pubkey.verify(&pars.message, &sig).is_ok()
      {
        return ExecutionControl::Error("Invalid signature on client".to_string());
      }
      state.signature=sig;
       ExecutionControl::None
  };
    output |_pars,state,_in_msg,_out_msg|{
        Ed255SignOutput{
            signature:state.signature.clone(),
            export:vec!["signature".to_string()]
        }
    }
}
#[cfg(feature = "server")]
pub struct Ed255SignServer {}
#[cfg(feature = "server")]
declare_protocol_flow! {
  Ed255SignServer, Ed255SignParameters,Ed255SignState,Ed255SignOutput,
  preamble {
    let mut rng=OsRng
    let my_x=&Ednum::from_bits([2; SECRET_KEY_LENGTH])
    let other_x=Ednum::from_bits([1; SECRET_KEY_LENGTH])
   };
  step |pars,state,in_msg,out_msg|{
    let resp=geet!(in_msg,Ed255SignS1);
    let extsk=ExpandedSecretKey::from(&pars.keygen.secret);
    let mut h: Sha512 = Sha512::default();
    h.update(&extsk.nonce);
    h.update(&pars.message);
    let mt:[u8;32]=h.finalize()[..32].try_into().unwrap();
    state.ri=Ednum::from_bits(mt);
    state.rid=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.f1=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.f1d=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;

    let pn1=pars.keygen.gg.1*(&resp.si)+pars.keygen.hh.1*(&resp.sid);
    let pn2=resp.commit0.1+resp.commit1.1*my_x;
    if pn1!=pn2
    {
        return ExecutionControl::Error("Invalid pedersen commitment from client".to_string());
    }
    state.si=resp.si;
    state.combinedd=&state.rid+&state.f1d*my_x+&resp.sid;
    let a00=to_edpk(&state.ri);

    let out= Ed255SignS2{
        si: &state.ri+&state.f1*other_x,
        a1: to_edpk(&state.f1),
        a0: a00,
        sid: &state.rid+&state.f1d*other_x,
        commit0: to_edcom(pars.keygen.gg.1*&state.ri+pars.keygen.hh.1*&state.rid),
        commit1: to_edcom(pars.keygen.gg.1*&state.f1+pars.keygen.hh.1*&state.f1d),
    };
    yeet!(out_msg,out);
    ExecutionControl::None
  };
  step |pars,state,in_msg,out_msg|{
    let inp2 = geet!(in_msg,Ed255SignS3);
    let fcheck=inp2.a0.1+inp2.a1.1*(my_x);
    let f2=to_edpk(&state.si);
    if f2.1!=fcheck
    {
        return ExecutionControl::Error("Invalid feldman commitment from client".to_string());
    }
    state.pub_other=inp2.a0;
    state.func_other=inp2.a1;
    state.vv=to_edcom(state.pub_other.1 + to_edpk(&state.ri).1);
    state.combined=&state.ri+&state.f1*my_x+&state.si;
    let prv=&pars.keygen.priv_key_share;
    let fns=&pars.keygen.func_sum;
    let mut h: Sha512 = Sha512::new();
    h.update(&state.vv.0.0);
    h.update(pars.keygen.pubkey.as_bytes());
    h.update(&pars.message);
    let e = Ednum::from_hash(h);
    state.gamma=prv*&e+ &state.combined;
    let chk1=to_edpk(&inp2.gamma);
    let chk2=state.vv.1+state.func_other.1*(&other_x)+to_edpk(&state.f1).1*&other_x+(pars.keygen.pubkey.1+fns.1*(&other_x))*(&e);
    if chk2!=chk1.1
    {
        return ExecutionControl::Error("Invalid gamma from client".to_string());
    }
   let my_sig= my_x.clone();
   let other_sig= other_x.clone();
   let my_gamma= state.gamma.clone();
   let other_gamma= inp2.gamma.clone();

   let l0=&(&other_sig*(&other_sig-&my_sig).invert());
   let l1=&(&my_sig*(&my_sig-&other_sig).invert());
   let sgm=l0*&my_gamma+&other_gamma*l1;
   state.sigma=sgm;
   let chk1=to_edpk(&state.sigma).1-pars.keygen.pubkey.1*&e;
   if chk1!=state.vv.1
   {
    return ExecutionControl::Error("Invalid sign...".to_string())
   }
   let sig=EdSig{R: state.vv.0.clone(), s:state.sigma.clone()};
   if !pars.keygen.pubkey.verify(&pars.message, &sig).is_ok()
   {
     return ExecutionControl::Error("Invalid signature on client".to_string());
   }
   state.signature=sig;

    let out4= Ed255SignS4{
        gamma:state.gamma.clone()
    };

    yeet!(out_msg,out4);
    ExecutionControl::None
  };
  output |_pars,state,_in_msg,_out_msg|{
    Ed255SignOutput{
        signature:state.signature.clone(),
        export:vec![]
    }
}
}

#[cfg(test)]
pub mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::eddsa25519_keygen::*;
  use crate::protocols::eddsa25519_sign::*;
  //use crate::statics::*;
  use rsa::*;

  #[test]
  fn check_ed255sign_flow()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);
    for _ in 0..64
    {
      let client_params = Ed255KeygenParameters {
        backup_pub_key: WrappedPubkey {
          pem: public_key.clone(),
        },
      };
      let server_params = Ed255KeygenParameters {
        backup_pub_key: WrappedPubkey {
          pem: public_key.clone(),
        },
      };
      let (out_client, out_server) =
        run_protocol::<Ed255KeygenClient, Ed255KeygenServer>(&client_params, &server_params);
      assert_eq!(out_client.pubkey, out_server.pubkey);
      let message: Vec<u8> = b"EDDSA25519 proves knowledge of a secret number in the context of a single message"
        .into_iter()
        .cloned()
        .collect();
      let pkey = out_client.pubkey.clone();
      let sclient_params = Ed255SignParameters {
        keygen: out_client,
        message: message.clone(),
      };
      let sserver_params = Ed255SignParameters {
        keygen: out_server,
        message: message.clone(),
      };
      let (sgn_client, sgn_server) = run_protocol::<Ed255SignClient, Ed255SignServer>(&sclient_params, &sserver_params);
      assert_eq!(sgn_client.signature, sgn_server.signature);
      assert!(pkey.verify(&message, &sgn_server.signature).is_ok());
    }
  }
}
