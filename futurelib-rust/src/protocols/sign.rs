use crate::common::*;
use crate::common_defs::*;
use crate::protocols::core::*;
use crate::protocols::presign::*;
use crate::{declare_protocol_flow, geet, yeet};

pub struct SignClient {}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SignParameters
{
  #[serde(rename = "preSignData")]
  pub presign: PresignOutput,
  #[serde(rename = "derivationPath")]
  pub path: String,
  #[serde(serialize_with = "int_as_base16h", deserialize_with = "int_from_base16h")]
  pub message: BigUint,
}

impl ParValidation for SignParameters
{
  fn validate(&self) -> bool
  {
    self.presign.derive_path == self.path
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SignState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub s: BigUint,
  pub recovery_param: i32,
}

impl ProtocolSavedState for SignState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> Self
  {
    Default::default()
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SignOutput
{
  #[serde(rename = "recoveryParam")]
  pub recovery_param: i32,
  #[serde(serialize_with = "int_as_base16_64l", deserialize_with = "int_from_base16_64l")]
  pub r: BigUint,
  #[serde(serialize_with = "int_as_base16_64l", deserialize_with = "int_from_base16_64l")]
  pub s: BigUint,
  // derive_path: String,
  pub export: Vec<String>,
}
impl OutputWithExports for SignOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SignS1
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub si: BigUint,
}
declare_protocol_flow! {
    SignClient, SignParameters,SignState,SignOutput,
    preamble {
        let q=&*Q_MODULUS
    };
    step |pars,state,_in_msg,out_msg|{
      state.s=(&((&pars.message*&pars.presign.ki)%q)+&((&pars.presign.r*&pars.presign.sigmai)%q))%q;
      state.recovery_param=pars.presign.recovery_param;
      let ret=SignS1{si:state.s.clone()};
      yeet!(out_msg,ret);
      ExecutionControl::None
    };
    step |pars,state,in_msg,_out_msg|{
           let resp=geet!(in_msg,SignS1);
           state.s+=&resp.si;
           state.s%=q;
           if state.s> (q>>1)
           {
               state.s=q-&state.s;
               state.recovery_param^=1;
           }
           let is=mod_inv(&state.s,q);
           let u1=(&pars.message*&is)%q;
           let u2=(&pars.presign.r*&is)%q;
           let rq=(ProjectivePoint::generator()*to_scalar(&u1)+ProjectivePoint::from(pars.presign.pubkey)*to_scalar(&u2)).to_affine();
           let r_check= BigUint::from_bytes_be(&rq.to_vector()[1..]) %q;
           if r_check!=pars.presign.r
           {
               return ExecutionControl::Error("Resulting signature does not match".to_string())
           }
           ExecutionControl::None
    };
    output |pars,state,_in_msg,_out_msg|
    {
      SignOutput{
        r:pars.presign.r.clone(),
        s:state.s.clone(),
        recovery_param:state.recovery_param,
        export:vec!["recovery_param".to_string(),"r".to_string(),"s".to_string()]
      }
    }
}

pub struct SignServer {}
declare_protocol_flow! {
    SignServer, SignParameters,SignState,SignOutput,
    preamble {
        let q=&*Q_MODULUS
    };
    step |pars,state,in_msg,out_msg|{
        let resp=geet!(in_msg,SignS1);
        state.s=(&((&pars.message*&pars.presign.ki)%q)+&((&pars.presign.r*&pars.presign.sigmai)%q))%q;
        state.recovery_param=pars.presign.recovery_param;
        let rt=SignS1{
            si:state.s.clone()
        };
        state.s+=&resp.si;
        state.s%=q;
        if state.s>(q>>1)
  {
      state.s=q-&state.s;
      state.recovery_param^=1;
  }
  let is=mod_inv(&state.s,q);
  let u1=(&pars.message*&is)%q;
  let u2=(&pars.presign.r*&is)%q;
  let rq=(ProjectivePoint::generator()*to_scalar(&u1)+ProjectivePoint::from(pars.presign.pubkey)*to_scalar(&u2)).to_affine();
  let r_check= BigUint::from_bytes_be(&rq.to_vector()[1..])%q;
  if r_check!=pars.presign.r
  {
      return ExecutionControl::Error("Resulting signature does not match".to_string())
  }
        yeet!(out_msg,rt);
        ExecutionControl::None
    };
    output |pars,state,_in_msg,_out_msg|
    {
      SignOutput{
        r:pars.presign.r.clone(),
        s:state.s.clone(),
        recovery_param:state.recovery_param,
        export:vec!["recoveryParam".to_string(),"r".to_string(),"s".to_string()]
      }
    }
}

#[cfg(test)]
pub mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::keygen::*;
  use crate::protocols::presign::*;
  use crate::protocols::sign::*;
  //use crate::statics::*;
  use rsa::*;
  pub fn verify_prehashed(slf: &AffinePoint, z: &Scalar, r: &Scalar, s: &Scalar) -> Result<(), String>
  {
    // Ensure signature is "low S" normalized ala BIP 0062
    if s.is_high().into()
    {
      return Err("error".to_string());
    }

    let s_inv = s.invert().unwrap();
    let u1 = z * &s_inv;
    let u2 = *r * s_inv;

    let x = ((ProjectivePoint::generator() * u1) + (ProjectivePoint::from(*slf) * u2))
      .to_affine()
      .to_vector();
    let x = to_scalar(&BigUint::from_bytes_be(&x[1..]));

    if Scalar::from_bytes_reduced(&x.to_bytes()).eq(&r)
    {
      Ok(())
    }
    else
    {
      Err("error".to_string())
    }
  }
  #[test]
  fn check_sign_flow()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let q = &*Q_MODULUS;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) = run_protocol::<KeyGenClient, KeyGenServer>(&client_params, &server_params);
    assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pubkey, out_server.pubkey);
    let pclient_params = PresignParameters {
      path: "m/2".to_string(),
      keygen: out_client.clone(),
    };
    let pserver_params = PresignParameters {
      path: "m/2".to_string(),
      keygen: out_server.clone(),
    };
    let (pout_client, pout_server) = run_protocol::<PresignClient, PresignServer>(&pclient_params, &pserver_params);
    assert_eq!(pout_client.pubkey, pout_server.pubkey);
    assert_eq!(pout_client.r, pout_server.r);
    let s1 = (&pout_client.r * &pout_client.sigmai + &pout_server.r * &pout_server.sigmai) % q;
    let inv = mod_inv(&s1, q);
    let s2 = (&pout_server.r * inv) % q;
    let u2 = (ProjectivePoint::from(pout_client.pubkey) * to_scalar(&s2)).to_affine();
    let r_check = BigUint::from_bytes_be(&u2.to_vector()[1..]) % q;
    assert_eq!(r_check, pout_server.r);

    //let message=gen_rand_z(&mut rng,q, false).unwrap();
    let message: Vec<u8> = b"ECDSA proves knowledge of a secret number in the context of a single message"
      .into_iter()
      .cloned()
      .collect();
    let mut hasher = sha2::Sha256::new();
    hasher.update(&message);
    let res = hasher.finalize();
    let pkey = pout_client.pubkey.clone();
    let sclient_params = SignParameters {
      presign: pout_client,
      path: "m/2".to_string(),
      message: BigUint::from_bytes_be(&res),
    };
    let sserver_params = SignParameters {
      presign: pout_server,
      path: "m/2".to_string(),
      message: BigUint::from_bytes_be(&res),
    };
    let (sgn_client, sgn_server) = run_protocol::<SignClient, SignServer>(&sclient_params, &sserver_params);
    assert_eq!(sgn_client.s, sgn_server.s);

    assert!(verify_prehashed(
      &pkey,
      &to_scalar(&BigUint::from_bytes_be(&res)),
      &to_scalar(&sgn_client.r),
      &to_scalar(&sgn_client.s)
    )
    .is_ok());
  }
}
