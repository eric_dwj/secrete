/// Signing algorith using Schnorrkel library (https://github.com/w3f/schnorrkel)
/// takes message (vector of bytes), context (also vector of bytes, can be used as HD wallet path. This value is "substrate" for Polkadot Substrate, IIRC),
/// and keygen output as parameters
use crate::common::*;
use crate::common_defs::*;
use crate::protocols::core::*;
use crate::protocols::schnorrkel_keygen::*;
//use crate::xpub::*;
use crate::chk;
use crate::seed_random;
use crate::{declare_protocol_flow, geet, yeet};
use schnorrkel::{musig::*, signing_context, Keypair, PublicKey as PK, Signature as SSig};
pub struct SchnorrkelSignClient {}

#[derive(Serialize, Deserialize)]
pub struct SchnorrkelSignParameters
{
  pub keygen: SchnorrkelKeyGenOutput,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub message: Vec<u8>,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub context: Vec<u8>,
}
impl ParValidation for SchnorrkelSignParameters
{
  fn validate(&self) -> bool
  {
    true
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SchnorrkelSignState
{
  internal: ProtocolState,
  __cchild: Option<Box<RawValue>>,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  their_commitment: Commitment,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  their_reveal: Reveal,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  our_reveal: Reveal,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  their_cosign: Cosignature,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  random_seed: BigUint,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub reveal: Reveal,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub their_public: PK,
  #[serde(serialize_with = "sig_as_base64", deserialize_with = "sig_from_base64")]
  pub sig: SSig,
}
type CommitKey<'a> = CommitStage<&'a Keypair>;
type RevealKey<'a> = RevealStage<&'a Keypair>;
impl SchnorrkelSignState
{
  pub fn reconstruct_musig_commit<'a>(
    &self, pars: &'a SchnorrkelSignParameters,
  ) -> Option<MuSig<schnorrkel::context::Transcript, CommitKey<'a>>>
  {
    let t = signing_context(&pars.context).bytes(&pars.message);
    let mut rng = seed_random!(self.random_seed);
    let commit = MuSig::new(&pars.keygen.keypair.0, t, &mut rng);
    Some(commit)
  }
  pub fn reconstruct_musig_reveal<'a>(
    &self, pars: &'a SchnorrkelSignParameters,
  ) -> Option<MuSig<schnorrkel::context::Transcript, RevealKey<'a>>>
  {
    //let t = signing_context(b"keygen").bytes(b"--keygen-test--");
    let commit = self.reconstruct_musig_commit(pars);
    if commit.is_none()
    {
      return None;
    }
    let mut commit = commit.unwrap();
    if commit
      .add_their_commitment(pars.keygen.keypair.0.public, commit.our_commitment().clone())
      .is_ok()
    {
      return None;
    }
    if !commit
      .add_their_commitment(pars.keygen.their_public, self.their_commitment)
      .is_ok()
    {
      return None;
    }
    let reveal = commit.reveal_stage();
    Some(reveal)
  }
  pub fn reconstruct_musig_cosign<'a>(
    &self, pars: &'a SchnorrkelSignParameters,
  ) -> Option<MuSig<schnorrkel::context::Transcript, CosignStage>>
  {
    let reveal = self.reconstruct_musig_reveal(pars);
    if reveal.is_none()
    {
      return None;
    }

    let mut reveal = reveal.unwrap();
    let _ = reveal
      .add_their_reveal(pars.keygen.keypair.0.public, reveal.our_reveal().clone())
      .is_ok();
    if !reveal
      .add_their_reveal(pars.keygen.their_public, self.their_reveal.clone())
      .is_ok()
    {
      return None;
    }
    Some(reveal.cosign_stage())
  }
}
//use libc_print::*;
impl ProtocolSavedState for SchnorrkelSignState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> SchnorrkelSignState
  {
    let mut ret: SchnorrkelSignState = Default::default();
    let mut rng = OsRng;
    let chlimit = &*UINT_TWO << 512;
    ret.random_seed = gen_rand_z(&mut rng, &chlimit, false).unwrap();
    ret
  }
}

pub fn sig_as_base64(key: &SSig) -> Box<String>
{
  Box::new(base64::encode(&key.to_bytes()[..]))
}

pub fn sig_from_base64(st: RawValue) -> Result<SSig, miniserde::Error>
{
  let bytes = chk!(base64::decode(st.get_dequoted_string()?));
  Ok(chk!(SSig::from_bytes(&bytes)))
}
#[derive(Serialize, Deserialize, Default, Clone)]
pub struct SchnorrkelSignOutput
{
  #[serde(serialize_with = "sig_as_base64", deserialize_with = "sig_from_base64")]
  pub sig: SSig,
  pub export: Vec<String>,
}

impl OutputWithExports for SchnorrkelSignOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrkelSignS1
{
  //#[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  //pub si: BigUint,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commitment: Commitment,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub pk: PK,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrkelSignS2
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub reveal: Reveal,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrkelSignS3
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub cosign: Cosignature,
}

declare_protocol_flow! {
    SchnorrkelSignClient, SchnorrkelSignParameters,SchnorrkelSignState,SchnorrkelSignOutput,
    preamble {
        let mut rng= OsRng
        let chlimit=&*UINT_TWO<<512
    };
    step |pars,state,_in_msg,out_msg|{
        state.random_seed=gen_rand_z( &mut rng,&chlimit, false).unwrap();
        let commit=match state.reconstruct_musig_commit(pars)
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig on client could not be created".to_string()),
        };
        let outp=SchnorrkelSignS1{commitment:commit.our_commitment().clone(),pk:pars.keygen.keypair.0.public.clone()};
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelSignS1);
        if inp.pk!=pars.keygen.their_public
        {
            return ExecutionControl::Error("Server public key does not match".to_string());
        }
        state.their_public=inp.pk;
        state.their_commitment=inp.commitment;

        let reveal=match state.reconstruct_musig_reveal(pars)
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig reveal on client could not be created".to_string()),
        };

        let our=reveal.our_reveal();
        let outp=SchnorrkelSignS2{reveal:our.clone()};
        state.our_reveal=our.clone();
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelSignS2);
        state.their_reveal=inp.reveal;
        let cosign=match state.reconstruct_musig_cosign(pars)
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign on client could not be created".to_string()),
        };
        if pars.keygen.pk != cosign.public_key()
        {
            return ExecutionControl::Error("Combined public key does not match".to_string());
        }
        let outp=SchnorrkelKeyGenS3{cosign:cosign.our_cosignature().clone()};
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,_out_msg|{
        let inp=geet!(in_msg,SchnorrkelSignS3);
        let mut cosign=match state.reconstruct_musig_cosign(pars)
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign on client could not be created".to_string()),
        };
        let our_cosign=cosign.our_cosignature();
        if !cosign.add_their_cosignature(pars.keygen.keypair.0.public.clone(),our_cosign.clone()).is_ok()
        {
            return ExecutionControl::Error("Could not add our_cosign".to_string());
        }
        if !cosign.add_their_cosignature(state.their_public,inp.cosign).is_ok()
        {
            return ExecutionControl::Error("Could not add their_cosign".to_string());
        }
        let signature = match cosign.sign()
        {
         Some(s) =>s,
         None=> return ExecutionControl::Error("Could not generate verification signature".to_string()),
        };
        let t = signing_context(&pars.context).bytes(&pars.message);
        if !pars.keygen.pk.verify(t,&signature).is_ok()
        {
            return ExecutionControl::Error("Could not verify signature".to_string())
        }
        state.sig=signature;
        ExecutionControl::None
    };
    output |_pars,state,_in_msg,_out_msg|
    {
        SchnorrkelSignOutput
     {
        sig:state.sig.clone(),
        export:vec!["sig".to_string(),]
      }
    }
}

#[cfg(feature = "server")]
pub struct SchnorrkelSignServer {}
#[cfg(feature = "server")]
declare_protocol_flow! {
    SchnorrkelSignServer, SchnorrkelSignParameters,SchnorrkelSignState,SchnorrkelSignOutput,
    preamble {
        let mut rng= OsRng
        let chlimit=&*UINT_TWO<<512
    };
    step |pars,state,in_msg,out_msg|{
        state.random_seed=gen_rand_z( &mut rng,&chlimit, false).unwrap();
        let commit=match state.reconstruct_musig_commit(pars)
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig on client could not be created".to_string()),
        };
        let outp=SchnorrkelSignS1{commitment:commit.our_commitment().clone(),pk:pars.keygen.keypair.0.public.clone()};
        let inp=geet!(in_msg,SchnorrkelKeyGenS1);
        state.their_public=inp.pk;
        state.their_commitment=inp.commitment;
        let _reveal=match state.reconstruct_musig_reveal(pars)
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig reveal on server could not be created".to_string()),
        };
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS2);
        state.their_reveal=inp.reveal;
        let reveal=match state.reconstruct_musig_reveal(pars)
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig 2nd reveal on server could not be created".to_string()),
        };
        let _cosign=match state.reconstruct_musig_cosign(pars)
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign on server could not be created".to_string()),
        };

        let our=reveal.our_reveal();
        let outp=SchnorrkelKeyGenS2{reveal:our.clone()};
        state.our_reveal=our.clone();
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS3);

        let mut cosign=match state.reconstruct_musig_cosign(pars)
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign2 on server could not be created".to_string()),
        };
        if pars.keygen.pk!=cosign.public_key()
        {
            return ExecutionControl::Error("Combined public key does not match server keygen".to_string());
        }
        let our_cosign=cosign.our_cosignature();
        if !cosign.add_their_cosignature(pars.keygen.keypair.0.public.clone(),our_cosign.clone()).is_ok()
        {
            return ExecutionControl::Error("Could not add our_cosign".to_string());
        }
        if !cosign.add_their_cosignature(state.their_public,inp.cosign).is_ok()
        {
            return ExecutionControl::Error("Could not add their_cosign".to_string());
        }
        let signature = match cosign.sign()
        {
         Some(s) =>s,
         None=> return ExecutionControl::Error("Could not generate verification signature".to_string()),
        };
        let t = signing_context(&pars.context).bytes(&pars.message);
        if !pars.keygen.pk.verify(t,&signature).is_ok()
        {
            return ExecutionControl::Error("Could not verify signature".to_string())
        }
        state.sig=signature;

        let outp=SchnorrkelSignS3{cosign:cosign.our_cosignature().clone(), };
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
      output |_pars,state,_in_msg,_out_msg|
    {
        SchnorrkelSignOutput
     {
        sig:state.sig.clone(),
        export:vec!["sig".to_string(),]
      }
    }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::schnorrkel_sign::*;
  use rsa::*;
  #[test]
  fn schnorrkel_sign_check()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = SchnorrkelKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = SchnorrkelKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) =
      run_protocol::<SchnorrkelKeyGenClient, SchnorrkelKeyGenServer>(&client_params, &server_params);
    let pk = out_client.pk.clone();
    assert_eq!(out_client.pk, out_server.pk);
    for i in 0..512
    {
      let k = format!("testMessage{}", i);
      let client_params = SchnorrkelSignParameters {
        context: b"substrate".to_vec(),
        message: k.as_bytes().to_vec(),
        keygen: out_client.clone(),
      };
      let server_params = SchnorrkelSignParameters {
        context: b"substrate".to_vec(),
        message: k.as_bytes().to_vec(),
        keygen: out_server.clone(),
      };
      let (out_sign_client, out_sign_server) =
        run_protocol::<SchnorrkelSignClient, SchnorrkelSignServer>(&client_params, &server_params);
      assert_eq!(out_sign_client.sig, out_sign_server.sig);
      let t = signing_context(b"substrate").bytes(&k.as_bytes());
      assert!(pk.verify(t, &out_sign_client.sig).is_ok())
    }
  }
}
