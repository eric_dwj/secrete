/// Presign protocol as per https://eprint.iacr.org/2020/540.pdf. Prepares data for signing, does not need message. Can be prepared in advance and then supplied message for Sign
/// Uses KeygenOutput and derivation path  (BIP32) as parameters
/// Does not support hardened path
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::ExpPaillierProof;
use crate::protocols::core::*;
use crate::protocols::keygen::KeyGenOutput;
use crate::protocols::mta::{MtaProtocolClient, MtaProtocolOutput, MtaProtocolParameters, MtaProtocolServer};
use crate::xpub::*;
use crate::{await_child, declare_protocol_flow, geet, yeet};
pub struct PresignClient {}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct PresignParameters
{
  #[serde(rename = "key")]
  pub keygen: KeyGenOutput,
  #[serde(rename = "derivationPath")]
  pub path: String,
}

impl ParValidation for PresignParameters
{
  fn validate(&self) -> bool
  {
    self.keygen.priv_key_share != *UINT_ZERO
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct PresignState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,

  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub offset: BigUint, //offset from original private key; calculated from chaincode;
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ki: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gammai: BigUint,
  pub gammacom: HashDecommitment,
  pub theirs: HashCommitment,
  pub kgm: MtaProtocolOutput,
  pub kw: MtaProtocolOutput,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub deltai: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sigmai: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub revdelta: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub r: BigUint,
  pub recovery_param: i32,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub gm: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub lambdai: AffinePoint,
  pub path: DerivePath,
}

impl ProtocolSavedState for PresignState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> Self
  {
    Default::default()
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct PresignOutput
{
  #[serde(rename = "derPath")]
  pub derive_path: String,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pubkey: AffinePoint, // after offset
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sigmai: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub r: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ki: BigUint,
  #[serde(rename = "recoveryParam")]
  pub recovery_param: i32,
  pub export: Vec<String>, //todo: add xpub tpub...
}
impl OutputWithExports for PresignOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct PresignS34
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub deltai: BigUint,
  pub di: HashDecommitment,
}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct PresignS5
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub lambda: AffinePoint,
  pub lproof: ExpPaillierProof,
}

declare_protocol_flow! {
    PresignClient, PresignParameters,PresignState,PresignOutput,
    preamble {
        let mut rng=OsRng
        let q=&*Q_MODULUS
    };
    step |pars,state,_in_msg,out_msg|{
        state.path=match str_to_derive(&pars.path)
        {
            Some(path)=>path,
            None => return ExecutionControl::Error("Invalid derivation path".to_string())
        };
        for pt in &state.path
        {
            if pt.0==true
            {
                return ExecutionControl::Error("Only non-hardened paths are supported".to_string())
            }
        }
        let mut acc=DeriveAccumulator{pubkey:pars.keygen.pubkey.clone(),chaincode:pars.keygen.chaincode.clone(),accumulated:BigUint::zero()};
        for pt in &state.path
        {
          if ! derive_accumulate(&mut acc, pt.1)
          {
            return ExecutionControl::Error("Incorrect derivation path indices".to_string())
          }
        }
        state.offset=acc.accumulated;
        state.ki=gen_rand_z(&mut rng,q,false).unwrap();
        state.gammai=gen_rand_z(&mut rng,q,false).unwrap();
        let y=  (ProjectivePoint::generator()*to_scalar(&state.gammai)).to_affine();
        let (com,decom)=get_hash_commitment::<sha3::Sha3_256>(&y, 256);
        //outp.com=coms.first;
        state.gammacom=decom;
        yeet!(out_msg,com);
        ExecutionControl::None
    };
    step |_pars,state,in_msg,_out_msg|{
        let comex=geet!(in_msg,HashCommitment);
        state.theirs=comex;
        ExecutionControl::ChildStart
    };
    step |pars,state,in_msg,out_msg|{
        state.kgm=await_child!(state,in_msg,out_msg,MtaProtocolClient,MtaProtocolParameters{a:state.ki.clone(),
            b:state.gammai.clone(),
        use_check:false,
    fuji:pars.keygen.paillier.clone(),
    counterparty:pars.keygen.other.clone()
     });
        ExecutionControl::ChildStart
     };
    step |pars,state,in_msg,out_msg|{
        state.kw=await_child!(state,in_msg,out_msg,MtaProtocolClient,MtaProtocolParameters{a:state.ki.clone(),
            b:pars.keygen.priv_key_share.clone(),
        use_check:false,
    fuji:pars.keygen.paillier.clone(),
    counterparty:pars.keygen.other.clone()
     });
     state.deltai=(((&state.ki*&state.gammai)%q)+&state.kgm.albr+&state.kgm.arbl)%q;
     state.sigmai=(&((&state.ki*&pars.keygen.priv_key_share)%q)+&state.kw.albr+&state.kw.arbl+&((&state.ki*&state.offset)%q))%q;
     let step34=PresignS34{
         di:state.gammacom.clone(),
         deltai:state.deltai.clone()
     };
     yeet!(out_msg,step34);
     ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let st34srv=geet!(in_msg,PresignS34);
        if !verify_hash_commitment::<sha3::Sha3_256>(&state.theirs,&st34srv.di)
        {
            return ExecutionControl::Error("Invalid gamma commitment".to_string())
        }
        state.deltai=(&state.deltai+&st34srv.deltai)%q;
        state.revdelta=mod_inv(&state.deltai,q);
        let pp=ProjectivePoint::from(st34srv.di.y)+ProjectivePoint::from(state.gammacom.y);
        state.gm=pp.to_affine();
        let rr=pp*to_scalar(&state.revdelta);
        let vct=rr.to_affine().to_vector();
        let odd=vct[0]==3;
        state.r=BigUint::from_bytes_be(&vct[1..]);
        let equal= state.r>*q;
        state.r%=q;
        state.recovery_param= if odd{1}else{0}+if equal {2} else {0};
         if state.r==BigUint::zero()
         {
           return ExecutionControl::Error("Poor RNG or implementation".to_string())
         }
         state.lambdai=(pp*to_scalar(&state.ki)).to_affine();
        let sgn5=PresignS5{
             lambda:state.lambdai.clone(),
             lproof:ExpPaillierProof::new(&state.ki,&state.kw.ra,&state.gm,&state.lambdai,&state.kw.ca,&pars.keygen.paillier.keys,&pars.keygen.other)
        };
        yeet!(out_msg,sgn5);
        ExecutionControl::None
    };
    step |pars,state,in_msg,_out_msg|{
      let slast=geet!(in_msg, PresignS5);
      if !slast.lproof.verify(&state.gm,&slast.lambda,&state.kw.c_other,&pars.keygen.paillier.keys,&pars.keygen.paillier.b1,&pars.keygen.paillier.b2,&pars.keygen.other)
      {
      return ExecutionControl::Error("Lambda proof verification failed".to_string());
      }
      let tlam=ProjectivePoint::from(state.lambdai)+ProjectivePoint::from(slast.lambda);
      let tst=ProjectivePoint::generator()* to_scalar(&state.deltai);
      if tst!=tlam
      {
        return ExecutionControl::Error("Lambda verification failed".to_string());
      }
      ExecutionControl::None
    };
    output |pars,state,_in_msg,_out_msg|
    {
      PresignOutput{
        derive_path:pars.path.clone(),
        ki:state.ki.clone(),
        pubkey: (ProjectivePoint::from(pars.keygen.pubkey)+ProjectivePoint::generator()*to_scalar(&state.offset)).to_affine(),
        r:state.r.clone(),
        sigmai:state.sigmai.clone(),
       recovery_param:state.recovery_param,
        export:vec!["recovery_param".to_string()]
      }
    }
}

pub struct PresignServer {}

declare_protocol_flow! {
  PresignServer, PresignParameters,PresignState,PresignOutput,
  preamble {
      let mut rng=OsRng
      let q=&*Q_MODULUS
  };
  step |pars,state,in_msg,out_msg|{
    state.theirs=geet!(in_msg,HashCommitment);
    state.path=match str_to_derive(&pars.path)
    {
        Some(path)=>path,
        None => return ExecutionControl::Error("Invalid derivation path".to_string())
    };
    for pt in &state.path
    {
        if pt.0==true
        {
            return ExecutionControl::Error("Only non-hardened paths are supported".to_string())
        }
    }
    let mut acc=DeriveAccumulator{pubkey:pars.keygen.pubkey.clone(),chaincode:pars.keygen.chaincode.clone(),accumulated:BigUint::zero()};
    for pt in &state.path
    {
      if ! derive_accumulate(&mut acc, pt.1)
      {
        return ExecutionControl::Error("Incorrect derivation path indices".to_string())
      }
    }
    state.offset=acc.accumulated;
    state.ki=gen_rand_z(&mut rng,q,false).unwrap();
    state.gammai=gen_rand_z(&mut rng,q,false).unwrap();
    let y=  (ProjectivePoint::generator()*to_scalar(&state.gammai)).to_affine();
    let (com,decom)=get_hash_commitment::<sha3::Sha3_256>(&y, 256);
    state.gammacom=decom;
    yeet!(out_msg,com);
    ExecutionControl::None
  };
  step |pars,state,in_msg,out_msg|{
    state.kgm=await_child!(state,in_msg,out_msg,MtaProtocolServer,MtaProtocolParameters{a:state.ki.clone(),
      b:state.gammai.clone(),
  use_check:false,
fuji:pars.keygen.paillier.clone(),
counterparty:pars.keygen.other.clone()
});
  ExecutionControl::ChildStart
  };
  step |pars,state,in_msg,out_msg|{
    state.kw=await_child!(state,in_msg,out_msg,MtaProtocolServer,MtaProtocolParameters{a:state.ki.clone(),
      b:pars.keygen.priv_key_share.clone(),
  use_check:false,
fuji:pars.keygen.paillier.clone(),
counterparty:pars.keygen.other.clone()
});
state.deltai=(((&state.ki*&state.gammai)%q)+&state.kgm.albr+&state.kgm.arbl)%q;
state.sigmai=(&((&state.ki*&pars.keygen.priv_key_share)%q)+&state.kw.albr+&state.kw.arbl+&((&state.ki*&state.offset)%q))%q;
let st34cln=geet!(in_msg,PresignS34);
if !verify_hash_commitment::<sha3::Sha3_256>(&state.theirs, &st34cln.di)
{
  return ExecutionControl::Error("Invalid gamma commitment".to_string())
}
let deltotal=(&state.deltai+&st34cln.deltai)%q;
state.revdelta=mod_inv(&deltotal,q);
let pp=ProjectivePoint::from(st34cln.di.y)+ProjectivePoint::from(state.gammacom.y);
state.gm=pp.to_affine();
let rr=pp*to_scalar(&state.revdelta);
let vct=rr.to_affine().to_vector();
let odd=vct[0]==2;
state.r=BigUint::from_bytes_be(&vct[1..]);
let equal= state.r>*q;
state.r%=q;
state.recovery_param= if odd{1}else{0}+if equal {2} else {0};
if state.r==BigUint::zero()
{
 return ExecutionControl::Error("Poor RNG or implementation".to_string())
}
state.lambdai=(pp*to_scalar(&state.ki)).to_affine();
let out34=PresignS34{
  di:state.gammacom.clone(),
deltai:state.deltai.clone()
};
state.deltai=deltotal;
yeet!(out_msg,out34);
ExecutionControl::None
  };
  step |pars,state,in_msg,out_msg|{
    let slast=geet!(in_msg,PresignS5);
    if !slast.lproof.verify(&state.gm,&slast.lambda,&state.kw.c_other,&pars.keygen.paillier.keys,&pars.keygen.paillier.b1,&pars.keygen.paillier.b2,&pars.keygen.other)
    {
    return ExecutionControl::Error("Lambda proof verification failed".to_string());
    }

    let out5=PresignS5{
      lambda:state.lambdai.clone(),
      lproof:ExpPaillierProof::new(&state.ki,&state.kw.ra,&state.gm,&state.lambdai,&state.kw.ca,&pars.keygen.paillier.keys,&pars.keygen.other)
    };
    let tlam=ProjectivePoint::from(state.lambdai)+ProjectivePoint::from(slast.lambda);
    let tst=ProjectivePoint::generator()* to_scalar(&state.deltai);
    if tst!=tlam
    {
      return ExecutionControl::Error("Lambda verification failed".to_string());
    }
    yeet!(out_msg,out5);
    ExecutionControl::None
  };
  output |pars,state,_in_msg,_out_msg|
  {
    PresignOutput{
      derive_path:pars.path.clone(),
      ki:state.ki.clone(),
      pubkey: (ProjectivePoint::from(pars.keygen.pubkey)+ProjectivePoint::generator()*to_scalar(&state.offset)).to_affine(),
      r:state.r.clone(),
      sigmai:state.sigmai.clone(),
     recovery_param:state.recovery_param,
      export:vec!["recovery_param".to_string()]
    }
  }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::keygen::*;
  use crate::protocols::presign::*;

  use rsa::*;
  #[test]
  fn check_presign()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);
    let q = &*Q_MODULUS;
    let client_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) = run_protocol::<KeyGenClient, KeyGenServer>(&client_params, &server_params);
    assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pubkey, out_server.pubkey);
    let loc_prv = Xprv::new(
      &out_client.chaincode,
      &(&out_client.priv_key_share + &out_server.priv_key_share),
      false,
    );
    let pclient_params = PresignParameters {
      path: "m".to_string(),
      keygen: out_client.clone(),
    };
    let pserver_params = PresignParameters {
      path: "m".to_string(),
      keygen: out_server.clone(),
    };
    let (pout_client, pout_server) = run_protocol::<PresignClient, PresignServer>(&pclient_params, &pserver_params);
    assert_eq!(pout_client.pubkey, pout_server.pubkey);
    let pclient_params = PresignParameters {
      path: "m/1".to_string(),
      keygen: out_client.clone(),
    };
    let pserver_params = PresignParameters {
      path: "m/1".to_string(),
      keygen: out_server.clone(),
    };
    let (pout_client, pout_server) = run_protocol::<PresignClient, PresignServer>(&pclient_params, &pserver_params);
    let drv = derive_index(&loc_prv, 1);
    assert_eq!(pout_client.pubkey, pout_server.pubkey);
    assert_eq!(pout_client.r, pout_server.r);
    assert_eq!(
      ProjectivePoint::from(pout_client.pubkey),
      ProjectivePoint::generator() * to_scalar(&drv.key)
    );
    let s1 = (&pout_client.r * &pout_client.sigmai + &pout_server.r * &pout_server.sigmai) % q;
    let s2 = (&pout_server.r * mod_inv(&s1, q)) % q;
    let u2 = (ProjectivePoint::from(pout_client.pubkey) * to_scalar(&s2)).to_affine();
    let r_check = BigUint::from_bytes_be(&u2.to_vector()[1..]) % q;
    assert_eq!(r_check, pout_server.r);
    let xpb = Xpub::new(&out_client.chaincode, &out_client.pubkey, false);
    let xpe = derive_index_public(&xpb, 1);

    assert_eq!(xpe.key, pout_client.pubkey);
  }
  /* #[test]
  fn failme()
  {

   let pp=ProjectivePoint::generator();
   let px=BigUint::from_bytes_be(&pp.to_affine().to_vector()[1..]);
   println!("{:?}",pp);
   println!("{:x?}",&px.to_vector());
   println!("{}",px);
   assert!(false);
  }*/
}
