/// Eddsa key generation, adopted from https://link.springer.com/chapter/10.1007/3-540-47719-5_33 (https://github.com/ZenGo-X/multi-party-schnorr/tree/master/papers) 
/// Adapted for client-server architecture, so no logner very useful for more than two parties
use crate::algorithms::eddsa25519::{
  constants, scalar_as_base64, scalar_from_base64, ExpandedSecretKey, PublicKey as EdPk, SecretKey as EdSk,
  SECRET_KEY_LENGTH,
};
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::RsaEd255Proof;
use crate::protocols::core::*;
use crate::{declare_protocol_flow, geet, yeet};
use curve25519_dalek::scalar::Scalar as Ednum;
pub use sha2::Sha512;

#[derive(Serialize, Deserialize, Clone)]
pub struct Ed255KeygenParameters
{
  #[serde(rename = "backupPublicKey")]
  pub backup_pub_key: WrappedPubkey,
}

impl ParValidation for Ed255KeygenParameters
{
  fn validate(&self) -> bool
  {
    true
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Ed255KeygenState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub secret: EdSk,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  ri: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  rid: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  si: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  f1: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  f1d: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  gbase: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  hbase: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  gg: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  hh: EdPk,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  combined: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  combinedd: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pubkey: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub_other: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  func_other: EdPk,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub encrypted_their_backup: BigUint,
}

impl ProtocolSavedState for Ed255KeygenState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> Ed255KeygenState
  {
    Ed255KeygenState {
      internal: ProtocolState::new(),
      __cchild: None,
      ri: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      rid: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      si: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      gg: (&EdSk::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap()).into(),
      hh: (&EdSk::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap()).into(),
      combined: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      combinedd: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      gbase: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      hbase: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      f1: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      f1d: Ednum::from_bits([0; SECRET_KEY_LENGTH]),
      func_other: (&EdSk::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap()).into(),
      pubkey: (&EdSk::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap()).into(),
      pub_other: (&EdSk::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap()).into(),
      secret: EdSk::from_bytes(&[0; SECRET_KEY_LENGTH]).unwrap(),
      encrypted_their_backup: Default::default(),
    }
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct Ed255KeygenOutput
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub secret: EdSk,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub priv_key_share: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub pub_mine: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub pub_other: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub gg: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub hh: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub func_sum: EdPk,
  #[serde(rename = "publicKey")]
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub pubkey: EdPk,
  #[serde(rename = "encryptedTheirBackup")]
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub encrypted_their_backup: BigUint,
  #[serde(rename = "encryptedOurBackup")]
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub encrypted_our_backup: BigUint,
  pub export: Vec<String>,
}

impl OutputWithExports for Ed255KeygenOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self)
  {
    // self.public_key = self.sum_pk.clone();
  }
}

#[derive(Serialize, Deserialize)]
pub struct Ed255KeygenSetup
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  gg_half: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  hh_half: EdPk,
}
#[derive(Serialize, Deserialize)]
pub struct Ed255KeygenS1
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub gg_half: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub hh_half: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub public_half: EdPk,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub backup: BigUint,
  pub proof: RsaEd255Proof,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub si: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub sid: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit1: EdPk,
}
#[derive(Serialize, Deserialize)]
pub struct Ed255KeygenS2
{
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub si: Ednum,
  #[serde(serialize_with = "scalar_as_base64", deserialize_with = "scalar_from_base64")]
  pub sid: Ednum,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commit1: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a1: EdPk,
}

#[derive(Serialize, Deserialize)]
pub struct Ed255KeygenS3
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a0: EdPk,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub a1: EdPk,
}

pub struct Ed255KeygenClient {}
pub fn to_edpk(scal: &Ednum) -> EdPk
{
  let pt = scal * &constants::ED25519_BASEPOINT_TABLE;
  EdPk(pt.compress(), pt)
}
pub fn to_edcom(pt: curve25519_dalek::edwards::EdwardsPoint) -> EdPk
{
  EdPk(pt.compress(), pt)
}
declare_protocol_flow! {
  Ed255KeygenClient, Ed255KeygenParameters,Ed255KeygenState,Ed255KeygenOutput,
  preamble {
      let mut rng=OsRng
      let my_x=&Ednum::from_bits([1; SECRET_KEY_LENGTH])
      let other_x=Ednum::from_bits([2; SECRET_KEY_LENGTH])
  };
  step |_pars,state,_in_msg,out_msg|{
    state.secret=EdSk::generate(&mut rng);

    state.ri=ExpandedSecretKey::from(&state.secret).key;
    state.rid=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.f1=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.f1d=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.hbase=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.gbase=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    //let our_pk=EdPk::from(&state.secret);
    //let backup=rsa_encrypt_seeded(&pars.backup_pub_key.pem,&BigUint::from_bytes_be(state.secret.as_bytes()));
    let packet=Ed255KeygenSetup
    {
     hh_half:to_edpk(&state.hbase),
     gg_half:to_edpk(&state.gbase),
    };
    yeet!(out_msg,packet);
    ExecutionControl::None
  };
    step |pars,state,in_msg,out_msg|{
    let setup=geet!(in_msg,Ed255KeygenS1);
    state.hh=to_edcom(setup.hh_half.1*&state.hbase);
    state.gg=to_edcom(setup.gg_half.1*&state.gbase);
    if state.hh==state.gg
    {
        return ExecutionControl::Error("Incorrect G&H generated".to_string());
    }
    if !setup.proof.verify(&pars.backup_pub_key.pem, &setup.public_half, &setup.backup)
    {
    return ExecutionControl::Error("Privkey Backup validation error".to_string());
    }
    state.encrypted_their_backup=setup.backup;
    let pn1=to_edcom(state.gg.1*&setup.si+state.hh.1*&setup.sid);
    let pn2=to_edcom(setup.commit0.1+setup.commit1.1*my_x);//ProjectivePoint::from(inp2.commit0)+ProjectivePoint::from(inp2.commit1)*to_scalar(my_x);
    if pn1!=pn2
    {
        // honestly not sure how well this works in 2 out of 2...
        return ExecutionControl::Error("Invalid pedersen commitment from server".to_string());
    }
    state.si=setup.si;
    //state.sid=inp2.sid;
    state.combinedd=&state.rid+&state.f1d*my_x+&setup.sid;
    let a00=to_edpk(&state.ri);
    let out3= Ed255KeygenS2{
        si: &state.ri+&state.f1*other_x,
        sid: &state.rid+&state.f1d*other_x,
        commit0: to_edcom(state.gg.1*&state.ri+state.hh.1*&state.rid),
        commit1: to_edcom(state.gg.1*&state.f1+state.hh.1*&state.f1d),
        a0: a00,
        a1: to_edpk(&state.f1),
    };
    yeet!(out_msg,out3);
    ExecutionControl::None
  };
  step |_pars,state,in_msg,_out_msg|{

    let inp4 = geet!(in_msg,Ed255KeygenS3);
    let fcheck=to_edcom(inp4.a0.1+inp4.a1.1*my_x);
    let f2=to_edpk(&state.si);
    if f2!=fcheck
    {
        return ExecutionControl::Error("Invalid feldman commitment from server".to_string());
    }
    state.pub_other=inp4.a0;
    state.func_other=inp4.a1;
    state.pubkey=to_edcom(state.pub_other.1+ to_edpk(&state.ri).1);
    state.combined=&state.ri+&state.f1*my_x+&state.si;
    ExecutionControl::None
  };
  output |pars,state,_,_|{
    let pub_mine=  to_edpk(&state.ri);

    Ed255KeygenOutput{
      secret:state.secret.clone(),
      priv_key_share:state.combined.clone(),
        pub_mine:pub_mine,
        pub_other:state.pub_other.clone(),
        func_sum: to_edcom(to_edpk(&state.f1).1+state.func_other.1),
        pubkey:state.pubkey.clone(),
        gg:state.gg.clone(),
        hh:state.hh.clone(),
      encrypted_their_backup:state.encrypted_their_backup.clone(),
      encrypted_our_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem,&BigUint::from_bytes_be(state.secret.as_bytes())),
      export:vec!["encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string(),"publicKey".to_string()]
    }
  }
}

pub struct Ed255KeygenServer {}
declare_protocol_flow! {
  Ed255KeygenServer, Ed255KeygenParameters,Ed255KeygenState,Ed255KeygenOutput,
  preamble {
      let mut rng=OsRng
      let my_x=&Ednum::from_bits([2; SECRET_KEY_LENGTH])
      let other_x=Ednum::from_bits([1; SECRET_KEY_LENGTH])
  };
  step |pars,state,in_msg,out_msg|{
    let setup=geet!(in_msg,Ed255KeygenSetup);

    state.secret=EdSk::generate(&mut rng);
    state.hbase=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.gbase=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;

    state.hh=to_edcom(setup.hh_half.1*&state.hbase);
    state.gg=to_edcom(setup.gg_half.1*&state.gbase);

    if  state.hh==state.gg
    {
        return ExecutionControl::Error("Incorrect H or G generated from client".to_string());
    }
    state.ri=ExpandedSecretKey::from(&state.secret).key;
    state.rid=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.f1=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    state.f1d=ExpandedSecretKey::from(&EdSk::generate(&mut rng)).key;
    let our_pk=EdPk::from(&state.secret);
    let backup=rsa_encrypt_seeded(&pars.backup_pub_key.pem,&BigUint::from_bytes_be(state.secret.as_bytes()));
  //  state.our_pk=EdPk::from(&state.secret);
    let outp=Ed255KeygenS1{
        hh_half: to_edpk(&state.hbase),
        gg_half: to_edpk(&state.gbase),
        si: &state.ri+&state.f1*other_x,
        sid: &state.rid+&state.f1d*other_x,
        commit0: to_edcom(state.gg.1*&state.ri+state.hh.1*&state.rid),
        commit1: to_edcom(state.gg.1*&state.f1+state.hh.1*&state.f1d),
        proof:RsaEd255Proof::new(&pars.backup_pub_key.pem,&our_pk,&state.secret),
        backup:backup,
        public_half:our_pk,
    };
    yeet!(out_msg,outp);
    ExecutionControl::None
  };
  step |_pars,state,in_msg,out_msg|{
    let inp2 = geet!(in_msg,Ed255KeygenS2);
    // check pedersen...
    let pn1=state.gg.1*&inp2.si+state.hh.1*&inp2.sid;
    let pn2=inp2.commit0.1+inp2.commit1.1*my_x;
    if pn1!=pn2
    {
        // honestly not sure how well this works in 2 out of 2...
        return ExecutionControl::Error("Invalid pedersen commitment from client".to_string());
    }
    state.si=inp2.si;
    //state.sid=inp2.sid;

    state.combinedd=&state.rid+&state.f1d*my_x+&inp2.sid;
    let a00=to_edpk(&state.ri);
    let out3= Ed255KeygenS3{
        a0: a00,
        a1: to_edpk(&state.f1),
    };
    state.pub_other=inp2.a0;
    state.func_other=inp2.a1;
    state.pubkey=to_edcom(state.pub_other.1+ to_edpk(&state.ri).1);
    state.combined=&state.ri+&state.f1*my_x+&state.si;
    yeet!(out_msg,out3);
    ExecutionControl::None
  };

  output |pars,state,_,_|{
    let pub_mine=  to_edpk(&state.ri);

    Ed255KeygenOutput{
      secret:state.secret.clone(),
      priv_key_share:state.combined.clone(),
        pub_mine:pub_mine,
        pub_other:state.pub_other.clone(),
        func_sum: to_edcom(to_edpk(&state.f1).1+state.func_other.1),
        pubkey:state.pubkey.clone(),
        gg:state.gg.clone(),
        hh:state.hh.clone(),
      encrypted_their_backup:state.encrypted_their_backup.clone(),
      encrypted_our_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem,&BigUint::from_bytes_be(state.secret.as_bytes())),
      export:vec!["encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string(),"publicKey".to_string()]
    }
  }
}
