/// Schnorrkel key generation - uses Schnorkel library directly, though the library was vendored in and modified for ease of access to some variables. 
/// It was necessary since the whole state has to be recoverable on each protocol step, and therefore had to be serialized  - that includes random number state for witness generation
/// There is no derivation used in Polkadot, AFAIk, so no derivation was implemented. Not sure how it would be done for context-based library anyway. Maybe context can be used as path
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::RsaSchnorrkelProof;
use crate::protocols::core::*;
use crate::chk;
use crate::seed_random;
use crate::{declare_protocol_flow, geet, yeet};
use schnorrkel::{musig::*, signing_context, Keypair, PublicKey as PK};
pub struct SchnorrkelKeyGenClient {}

#[derive(Serialize, Deserialize)]
pub struct SchnorrkelKeyGenParameters
{
  #[serde(rename = "backupPublicKey")]
  pub backup_pub_key: WrappedPubkey,
  // pub context_string:String,
}
impl ParValidation for SchnorrkelKeyGenParameters
{
  fn validate(&self) -> bool
  {
    true
  }
}
#[derive(Clone)]
pub struct KeypairWrap(pub Keypair);
pub fn keypair_as_base64(key: &KeypairWrap) -> Box<String>
{
  Box::new(base64::encode(&key.0.to_bytes()[..]))
}

pub fn keypair_from_base64(st: RawValue) -> Result<KeypairWrap, miniserde::Error>
{
  let bytes = chk!(base64::decode(st.get_dequoted_string()?));
  Ok(KeypairWrap(chk!(Keypair::from_bytes(&bytes))))
}

pub fn pkey_as_base64(key: &PK) -> Box<String>
{
  Box::new(base64::encode(&key.to_bytes()[..]))
}

pub fn pkey_from_base64(st: RawValue) -> Result<PK, miniserde::Error>
{
  let bytes = chk!(base64::decode(st.get_dequoted_string()?));
  Ok(chk!(PK::from_bytes(&bytes)))
}

impl Default for KeypairWrap
{
  fn default() -> KeypairWrap
  {
    KeypairWrap(Keypair::generate())
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SchnorrkelKeyGenState
{
  internal: ProtocolState,
  __cchild: Option<Box<RawValue>>,
  #[serde(serialize_with = "keypair_as_base64", deserialize_with = "keypair_from_base64")]
  keypair: KeypairWrap,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  their_commitment: Commitment,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  their_reveal: Reveal,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  our_reveal: Reveal,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  their_cosign: Cosignature,
  //#[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  //our_cosign:Cosignature,
  //#[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  //chaincode: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  random_seed: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  encrypted_their_backup: BigUint,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub pk: PK,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub reveal: Reveal,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub their_public: PK,
}
type CommitKey<'a> = CommitStage<&'a Keypair>;
type RevealKey<'a> = RevealStage<&'a Keypair>;
impl SchnorrkelKeyGenState
{
  pub fn reconstruct_musig_commit<'a>(&self) -> Option<MuSig<schnorrkel::context::Transcript, CommitKey>>
  {
    let t = signing_context(b"keygen").bytes(b"--keygen-test--");
    let mut rng = seed_random!(self.random_seed);
    let commit = MuSig::new(&self.keypair.0, t, &mut rng);
    Some(commit)
  }
  pub fn reconstruct_musig_reveal<'a>(&self) -> Option<MuSig<schnorrkel::context::Transcript, RevealKey>>
  {
    //let t = signing_context(b"keygen").bytes(b"--keygen-test--");
    let commit = self.reconstruct_musig_commit();
    if commit.is_none()
    {
      return None;
    }
    let mut commit = commit.unwrap();
    if commit
      .add_their_commitment(self.keypair.0.public, commit.our_commitment().clone())
      .is_ok()
    {
      return None;
    }
    if !commit
      .add_their_commitment(self.their_public, self.their_commitment)
      .is_ok()
    {
      return None;
    }
    let reveal = commit.reveal_stage();
    Some(reveal)
  }
  pub fn reconstruct_musig_cosign<'a>(&self) -> Option<MuSig<schnorrkel::context::Transcript, CosignStage>>
  {
    let reveal = self.reconstruct_musig_reveal();
    if reveal.is_none()
    {
      return None;
    }

    let mut reveal = reveal.unwrap();
    let _ = reveal
      .add_their_reveal(self.keypair.0.public, reveal.our_reveal().clone())
      .is_ok();
    if !reveal
      .add_their_reveal(self.their_public, self.their_reveal.clone())
      .is_ok()
    {
      return None;
    }
    Some(reveal.cosign_stage())
  }
}
//use libc_print::*;
impl ProtocolSavedState for SchnorrkelKeyGenState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> SchnorrkelKeyGenState
  {
    let mut ret: SchnorrkelKeyGenState = Default::default();
    let mut rng = OsRng;
    let chlimit = &*UINT_TWO << 512;
    ret.random_seed = gen_rand_z(&mut rng, &chlimit, false).unwrap();
    ret
  }
}
#[derive(Serialize, Deserialize, Default, Clone)]
pub struct SchnorrkelKeyGenOutput
{
  #[serde(serialize_with = "keypair_as_base64", deserialize_with = "keypair_from_base64")]
  pub keypair: KeypairWrap,
  #[serde(rename = "encryptedTheirBackup")]
  #[serde(serialize_with = "int_as_base58", deserialize_with = "int_from_base58")]
  pub encrypted_their_backup: BigUint,
  #[serde(rename = "encryptedOurBackup")]
  #[serde(serialize_with = "int_as_base58", deserialize_with = "int_from_base58")]
  pub encrypted_our_backup: BigUint,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub pk: PK,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub their_public: PK,
  pub export: Vec<String>,
}

impl OutputWithExports for SchnorrkelKeyGenOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrkelKeyGenS1
{
  //#[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  //pub si: BigUint,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub commitment: Commitment,
  #[serde(serialize_with = "pkey_as_base64", deserialize_with = "pkey_from_base64")]
  pub pk: PK,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrkelKeyGenS2
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub reveal: Reveal,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrkelKeyGenS3
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub cosign: Cosignature,
}

#[derive(Serialize, Deserialize)]
pub struct SchnorrkelKeyGenS4
{
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub cosign: Cosignature,
  pub backup_proof: RsaSchnorrkelProof,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub enc_backup: BigUint,
}

declare_protocol_flow! {
    SchnorrkelKeyGenClient, SchnorrkelKeyGenParameters,SchnorrkelKeyGenState,SchnorrkelKeyGenOutput,
    preamble {
        let mut rng= OsRng
        let chlimit=&*UINT_TWO<<512
    };
    step |_pars,state,_in_msg,out_msg|{
        state.random_seed=gen_rand_z( &mut rng,&chlimit, false).unwrap();
        state.keypair=KeypairWrap(Keypair::generate());
        let commit=match state.reconstruct_musig_commit()
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig on client could not be created".to_string()),
        };
        //state.keypair.0.public;
        let outp=SchnorrkelKeyGenS1{commitment:commit.our_commitment().clone(),pk:state.keypair.0.public.clone()};
        //state.chaincode=rotate_entropy!(state.chaincode,&chlimit,state.commitment); PublicKey
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |_pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS1);
        state.their_public=inp.pk;
        state.their_commitment=inp.commitment;

        let reveal=match state.reconstruct_musig_reveal()
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig reveal on client could not be created".to_string()),
        };

        let our=reveal.our_reveal();
        let outp=SchnorrkelKeyGenS2{reveal:our.clone()};
        state.our_reveal=our.clone();
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |_pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS2);
        state.their_reveal=inp.reveal;
        let cosign=match state.reconstruct_musig_cosign()
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign on client could not be created".to_string()),
        };
        state.pk=cosign.public_key();
        let outp=SchnorrkelKeyGenS3{cosign:cosign.our_cosignature().clone()};
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,_out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS4);
        if !inp.backup_proof.verify(&pars.backup_pub_key.pem, &state.their_public, &inp.enc_backup)
        {
        return ExecutionControl::Error("Privkey Backup validation error".to_string());
        }
        state.encrypted_their_backup=inp.enc_backup;
        let mut cosign=match state.reconstruct_musig_cosign()
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign on client could not be created".to_string()),
        };
        let our_cosign=cosign.our_cosignature();
        if !cosign.add_their_cosignature(state.keypair.0.public.clone(),our_cosign.clone()).is_ok()
        {
            return ExecutionControl::Error("Could not add our_cosign".to_string());
        }
        if !cosign.add_their_cosignature(state.their_public,inp.cosign).is_ok()
        {
            return ExecutionControl::Error("Could not add their_cosign".to_string());
        }
        let signature = match cosign.sign()
        {
         Some(s) =>s,
         None=> return ExecutionControl::Error("Could not generate verification signature".to_string()),
        };
        let t = signing_context(b"keygen").bytes(b"--keygen-test--");
        if !state.pk.verify(t,&signature).is_ok()
        {
            return ExecutionControl::Error("Could not verify signature".to_string())
        }
        ExecutionControl::None
    };
    output |pars,state,_in_msg,_out_msg|
    {
        SchnorrkelKeyGenOutput
     {
        keypair:state.keypair.clone(),
        encrypted_our_backup: rsa_encrypt_seeded(&pars.backup_pub_key.pem,&BigUint::from_bytes_be(&state.keypair.0.to_bytes())),
        encrypted_their_backup: state.encrypted_their_backup.clone(),
        pk:state.pk.clone(),
        their_public:state.their_public.clone(),
        export:vec!["pk".to_string(),"encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string()]
      }
    }
}

#[cfg(feature = "server")]
pub struct SchnorrkelKeyGenServer {}
#[cfg(feature = "server")]
declare_protocol_flow! {
    SchnorrkelKeyGenServer, SchnorrkelKeyGenParameters,SchnorrkelKeyGenState,SchnorrkelKeyGenOutput,
    preamble {
        let mut rng= OsRng
        let chlimit=&*UINT_TWO<<512
    };
    step |_pars,state,in_msg,out_msg|{
        state.random_seed=gen_rand_z( &mut rng,&chlimit, false).unwrap();
        state.keypair=KeypairWrap(Keypair::generate());
        let commit=match state.reconstruct_musig_commit()
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig on client could not be created".to_string()),
        };
        let outp=SchnorrkelKeyGenS1{commitment:commit.our_commitment().clone(),pk:state.keypair.0.public.clone()};
        let inp=geet!(in_msg,SchnorrkelKeyGenS1);
        state.their_public=inp.pk;
        state.their_commitment=inp.commitment;
        let _reveal=match state.reconstruct_musig_reveal()
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig reveal on server could not be created".to_string()),
        };
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |_pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS2);
        state.their_reveal=inp.reveal;
        let reveal=match state.reconstruct_musig_reveal()
        {
            Some(r) =>r,
            None => return ExecutionControl::Error("Musig 2nd reveal on server could not be created".to_string()),
        };
        let _cosign=match state.reconstruct_musig_cosign()
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign on server could not be created".to_string()),
        };

        let our=reveal.our_reveal();
        let outp=SchnorrkelKeyGenS2{reveal:our.clone()};
        state.our_reveal=our.clone();
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let inp=geet!(in_msg,SchnorrkelKeyGenS3);

        let mut cosign=match state.reconstruct_musig_cosign()
        {
         Some(a) =>a,
         None => return ExecutionControl::Error("Musig cosign2 on server could not be created".to_string()),
        };
        state.pk=cosign.public_key();
        let our_cosign=cosign.our_cosignature();
        if !cosign.add_their_cosignature(state.keypair.0.public.clone(),our_cosign.clone()).is_ok()
        {
            return ExecutionControl::Error("Could not add our_cosign".to_string());
        }
        if !cosign.add_their_cosignature(state.their_public,inp.cosign).is_ok()
        {
            return ExecutionControl::Error("Could not add their_cosign".to_string());
        }
        let signature = match cosign.sign()
        {
         Some(s) =>s,
         None=> return ExecutionControl::Error("Could not generate verification signature".to_string()),
        };
        let t = signing_context(b"keygen").bytes(b"--keygen-test--");
        if !state.pk.verify(t,&signature).is_ok()
        {
            return ExecutionControl::Error("Could not verify signature".to_string())
        }


        let outp=SchnorrkelKeyGenS4{cosign:cosign.our_cosignature().clone(),
            enc_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem,&BigUint::from_bytes_be(&state.keypair.0.secret.to_bytes())),
            backup_proof: RsaSchnorrkelProof::new(&pars.backup_pub_key.pem,&state.keypair.0.public,&state.keypair.0.secret),

        };
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
      output |_pars,state,_in_msg,_out_msg|
    {
        SchnorrkelKeyGenOutput
     {
        keypair:state.keypair.clone(),
        encrypted_our_backup: BigUint::zero(),
        encrypted_their_backup: BigUint::zero(),
        pk:state.pk.clone(),
        their_public:state.their_public.clone(),
        export:vec!["pk".to_string(),"encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string()]
      }
    }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::schnorrkel_keygen::*;
  use rsa::*;
  #[test]
  fn nasty()
  {
    let mut rng = OsRng;
    let chlimit = &*UINT_TWO << 512;
    let random_seed = gen_rand_z(&mut rng, &chlimit, false).unwrap();
    let mut rng = seed_random!(random_seed);
    let keypair = Keypair::generate();
    let t = signing_context(b"keygen").bytes(b"--keygen-test--");
    let commit1 = MuSig::new(&keypair, t, &mut rng);
    let cm1 = commit1.our_commitment();
    let mut rng = seed_random!(random_seed);
    let t1 = signing_context(b"keygen").bytes(b"--keygen-test--");
    let commit2 = MuSig::new(&keypair, t1, &mut rng);

    let cm2 = commit2.our_commitment();
    assert_eq!(cm1, cm2);
  }
  #[test]
  fn schnorrkel_keygen_check()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = SchnorrkelKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = SchnorrkelKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) =
      run_protocol::<SchnorrkelKeyGenClient, SchnorrkelKeyGenServer>(&client_params, &server_params);
    //   assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pk, out_server.pk);
  }
}
