/// EcDsa key generation following https://eprint.iacr.org/2020/540.pdf
/// Needs RSA public key as parameter for generating backup
/// allows use of backupRecovery to generate combined private key

use crate::algorithms::fujisaki::*;
use crate::algorithms::paillier::VerifiedSideData;
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::RsaEllipticProof;
use crate::proofs::SchnorrBaseProof;
use crate::protocols::core::*;
use crate::protocols::feldman_vss::*;
use crate::xpub::*;
use crate::{await_child, declare_protocol_flow, geet, rotate_entropy, yeet};

pub struct KeyGenClient {}

#[derive(Serialize, Deserialize)]
pub struct KeyGenParameters
{
  #[serde(rename = "backupPublicKey")]
  pub backup_pub_key: WrappedPubkey,
}
impl ParValidation for KeyGenParameters
{
  fn validate(&self) -> bool
  {
    true
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct KeyGenState
{
  internal: ProtocolState,
  __cchild: Option<Box<RawValue>>,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  ui: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  xi: BigUint, // priv key share
  decom: HashDecommitment,
  theirs: HashCommitment,
  share: FeldmanVssOutput,
  private_paillier: FujiCore,
  other: VerifiedSideData,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  chaincode: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  encrypted_their_backup: BigUint,
}
//use libc_print::*;

impl ProtocolSavedState for KeyGenState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> KeyGenState
  {
    KeyGenState {
      internal: ProtocolState::new(),
      __cchild: None,
      decom: Default::default(),
      ui: BigUint::zero(),
      xi: BigUint::zero(),
      theirs: Default::default(),
      share: Default::default(),
      private_paillier: Default::default(),
      other: Default::default(),
      chaincode: Default::default(),
      encrypted_their_backup: Default::default(),
    }
  }
}
#[derive(Serialize, Deserialize, Default, Clone)]
pub struct KeyGenOutput
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub priv_key_share: BigUint,
  pub paillier: FujiCore,
  pub other: VerifiedSideData,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pub_mine: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pub_other: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pubkey: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub chaincode: BigUint,
  #[serde(rename = "encryptedTheirBackup")]
  #[serde(serialize_with = "int_as_base58", deserialize_with = "int_from_base58")]
  pub encrypted_their_backup: BigUint,
  #[serde(rename = "encryptedOurBackup")]
  #[serde(serialize_with = "int_as_base58", deserialize_with = "int_from_base58")]
  pub encrypted_our_backup: BigUint,
  pub xpub: String,
  pub tpub: String,
  pub export: Vec<String>, //todo: add xpub tpub...
}

impl OutputWithExports for KeyGenOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize)]
pub struct KgenS1
{
  pub paillier_proofs: FmPacket,
  pub com: HashCommitment,
}
#[derive(Serialize, Deserialize)]
pub struct KgenS2
{
  xproof: SchnorrBaseProof,
  decom: HashDecommitment,
}
#[derive(Serialize, Deserialize)]
pub struct KgenS2Server
{
  pub xproof: SchnorrBaseProof,
  pub decom: HashDecommitment,
  pub backup_proof: RsaEllipticProof,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub enc_backup: BigUint,
}

declare_protocol_flow! {
    KeyGenClient, KeyGenParameters,KeyGenState,KeyGenOutput,
    preamble {
        let chlimit=&*UINT_TWO<<255
        let mut rng=OsRng
        let q=&*Q_MODULUS
    };
    step |_pars,state,_in_msg,out_msg|{
        state.private_paillier=FujiCore::generate_initial_keys(2048);
        state.ui=gen_rand_z(&mut rng,q,false).unwrap();
        let y=  (ProjectivePoint::generator() * to_scalar(&state.ui)).to_affine();
        let (com,decom) = get_hash_commitment::<Sha3_256>(&y,256);
        let outp=KgenS1{paillier_proofs:state.private_paillier.extract_packet(),com:com};
        state.decom=decom;
        state.chaincode=rotate_entropy!(state.chaincode,&chlimit,outp.com.digest,outp.paillier_proofs.en,outp.paillier_proofs.b1,outp.paillier_proofs.b2);
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
   step |_pars,state,in_msg,_out_msg|{
    let inp2 = geet!(in_msg,KgenS1);
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,inp2.com.digest,inp2.paillier_proofs.non_square.rseed,inp2.paillier_proofs.prime_power.rseed);
    state.other=match inp2.paillier_proofs.to_side_data()
    {
        Some(res)=>res,
        None =>return ExecutionControl::Error("Incorrect paillier key from server".to_string())
    };
    state.theirs=inp2.com;
    return ExecutionControl::ChildStart
    };

 step |_pars,state,in_msg,out_msg|{
     state.share=await_child!(state,in_msg,out_msg,FeldmanVssClient,FeldmanVssParameters{secret:state.ui.clone(),modulo:q.clone()});

    state.xi=state.share.x.clone();
    //println!("{}",serde_json::to_string(&DPint{d:(ProjectivePoint::generator() * crate::common::to_scalar(&state.xi)).to_affine()}).unwrap()) ;
    let  nxt=KgenS2{xproof:SchnorrBaseProof::new(&state.xi),decom:state.decom.clone() };
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,nxt.xproof.s);
    yeet!(out_msg,nxt);
   ExecutionControl::None
   };
   step |pars,state,in_msg,_out_msg| {
    let inp3 = geet!(in_msg,KgenS2Server);
    if !verify_hash_commitment::<Sha3_256>(&state.theirs,&inp3.decom)
    {
        return ExecutionControl::Error("Incorrect public key decommitment from server".to_string());
    }
    if inp3.decom.y!=state.share.v0_oth
    {
        return ExecutionControl::Error("Shared server public key does not match commitment".to_string());
    }
    let rot=(ProjectivePoint::from(state.share.v0_oth)*to_scalar(&state.ui)).to_affine();
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,rot);
    if ! inp3.xproof.verify(&state.share.x_oth)
    {
        return ExecutionControl::Error("Server could not prove knowledge of xi".to_string());
    }
    if ! inp3.backup_proof.verify(&pars.backup_pub_key.pem,&state.share.x_oth, &inp3.enc_backup)
    {
        return ExecutionControl::Error("Server could not prove backup encryption correctness".to_string());
    }
    state.encrypted_their_backup=inp3.enc_backup;
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,inp3.xproof.rseed,inp3.xproof.s);
    ExecutionControl::None
   };
   output |pars,state,_in_msg,_out_msg|
   {
    let pub_proj=ProjectivePoint::generator()*to_scalar(&state.xi);
    let pub_mine=pub_proj.to_affine();
    let pubkey=(pub_proj+ProjectivePoint::from(state.share.x_oth)).to_affine();
    KeyGenOutput
    {
        priv_key_share:state.xi.clone(),
        paillier:state.private_paillier.clone(),
        other:state.other.clone(),
        pub_mine:pub_mine,
        pub_other:state.share.x_oth.clone(),
        xpub: Xpub::new(&state.chaincode,&pubkey,false).encode(),
        tpub: Xpub::new(&state.chaincode,&pubkey,true).encode(),
        pubkey:pubkey,
        chaincode:state.chaincode.clone(),
        encrypted_their_backup:state.encrypted_their_backup.clone(),
        encrypted_our_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem, &bi_combine(&state.xi,&state.chaincode)),
        export:vec!["xpub".to_string(),"tpub".to_string(),"encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string()]
     }
   }
}
#[cfg(feature = "server")]
pub struct KeyGenServer {}
#[cfg(feature = "server")]
declare_protocol_flow! {
    KeyGenServer, KeyGenParameters,KeyGenState,KeyGenOutput,
    preamble {
        let chlimit=&*UINT_TWO<<255
        let mut rng=OsRng
        let q=&*Q_MODULUS
    };
    step |_pars,state,in_msg,out_msg|{
       let inp1=geet!(in_msg,KgenS1);
       state.chaincode=rotate_entropy!(state.chaincode,&chlimit,inp1.com.digest,inp1.paillier_proofs.en,inp1.paillier_proofs.b1,inp1.paillier_proofs.b2);
       state.other=match inp1.paillier_proofs.to_side_data()
       {
           Some(res)=>res,
           None =>return ExecutionControl::Error("Incorrect paillier key from client".to_string())
       };
       state.theirs=inp1.com;
       state.private_paillier=FujiCore::generate_initial_keys(2048);
       state.ui=gen_rand_z(&mut rng,q,false).unwrap();
       let y=  (ProjectivePoint::generator() * to_scalar(&state.ui)).to_affine();
       let (com,decom) = get_hash_commitment::<Sha3_256>(&y,256);
       let outp=KgenS1{paillier_proofs:state.private_paillier.extract_packet(),com:com};
       state.decom=decom;
       state.chaincode=rotate_entropy!(state.chaincode,&chlimit,outp.com.digest,outp.paillier_proofs.non_square.rseed,outp.paillier_proofs.prime_power.rseed);
       yeet!(out_msg,outp);
       ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        state.share=await_child!(state,in_msg,out_msg,FeldmanVssServer,FeldmanVssParameters{secret:state.ui.clone(),modulo:q.clone()});
        state.xi=state.share.x.clone();
        let inp2=geet!(in_msg,KgenS2);
        if !verify_hash_commitment::<Sha3_256>(&state.theirs,&inp2.decom)
    {
        return ExecutionControl::Error("Incorrect public key decommitment from server".to_string());
    }
    if inp2.decom.y!=state.share.v0_oth
    {
        return ExecutionControl::Error("Shared client public key does not match commitment".to_string());
    }


    if ! inp2.xproof.verify(&state.share.x_oth)
    {
        return ExecutionControl::Error("Client could not prove knowledge of xi".to_string());
    }
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,inp2.xproof.s);
    let rot=(ProjectivePoint::from(state.share.v0_oth)*to_scalar(&state.ui)).to_affine();
    state.chaincode=rotate_entropy!(state.chaincode,&chlimit,rot);
    //#[cfg(not(target_arch = "wasm32"))]
    //libc_println!("Server xi: {}", &state.xi);

    let pubshare=(ProjectivePoint::generator()*to_scalar(&state.xi)).to_affine();
      let out3=KgenS2Server{
          xproof:SchnorrBaseProof::new(&state.xi),
          decom:state.decom.clone(),
          enc_backup:rsa_encrypt_seeded(&pars.backup_pub_key.pem, &state.xi),
          backup_proof:RsaEllipticProof::new(&pars.backup_pub_key.pem,&pubshare,&to_scalar(&state.xi))
      };
      state.chaincode=rotate_entropy!(state.chaincode,&chlimit,out3.xproof.rseed,out3.xproof.s);
      //#[cfg(not(target_arch = "wasm32"))]
      //libc_println!("Server chain: {}", &state.chaincode);
      yeet!(out_msg,out3);
      ExecutionControl::None
    };
    output |_pars,state,_in_msg,_out_msg|
   {
    //secbitlenCombine(ret.privKeyShare,ret.chaincode)

    let pub_proj=ProjectivePoint::generator()*to_scalar(&state.xi);
    let pub_mine=pub_proj.to_affine();
    let pubkey=(pub_proj+ProjectivePoint::from(state.share.x_oth)).to_affine();
    KeyGenOutput
    {
        priv_key_share:state.xi.clone(),
        paillier:state.private_paillier.clone(),
        other:state.other.clone(),
        pub_mine:pub_mine,
        pub_other:state.share.x_oth.clone(),
        xpub: Xpub::new(&state.chaincode,&pubkey,false).encode(),
        tpub: Xpub::new(&state.chaincode,&pubkey,true).encode(),
        pubkey:pubkey,
        chaincode:state.chaincode.clone(),
        encrypted_their_backup:Default::default(),
        encrypted_our_backup:Default::default(),
        export:vec!["xpub".to_string(),"tpub".to_string(),"encryptedOurBackup".to_string(),"encryptedTheirBackup".to_string()]
     }
   }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::keygen::*;
  use rsa::*;
  #[test]
  fn keygen_check()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) = run_protocol::<KeyGenClient, KeyGenServer>(&client_params, &server_params);
    assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pubkey, out_server.pubkey);
  }
  #[derive(Serialize, Deserialize, Default)]
  struct T1
  {
    internal: ProtocolState,
    __cchild: Option<Box<RawValue>>,
    #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
    ui: BigUint,
    #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
    xi: BigUint, // priv key share
  }
  #[test]
  fn test_empty_to_default_deserialize()
  {
    let _l = r#"{"save":{}}"#;

    let _store2: T1 = miniserde::json::from_str("{}").unwrap();
    let _store2: KeyGenState = miniserde::json::from_str("{}").unwrap();
  }
}
