/// implementation of MTA (Multiple to additive share) algorithms, as per https://eprint.iacr.org/2020/540.pdf
/// Used as child in Presign protocol

use crate::algorithms::fujisaki::FujiCore;
use crate::algorithms::paillier::VerifiedSideData;
use crate::common::*;
use crate::common_defs::*;
use crate::proofs::MtaSchnorrBaseProof;
use crate::proofs::SchnorrBaseProof;
use crate::protocols::core::*;
use crate::{declare_protocol_flow, geet, yeet};

pub struct MtaProtocolClient {}
#[derive(Serialize, Deserialize, Clone)]
pub struct MtaProtocolParameters
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub a: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub b: BigUint,
  pub use_check: bool,
  pub fuji: FujiCore,
  pub counterparty: VerifiedSideData,
}

impl ParValidation for MtaProtocolParameters
{
  fn validate(&self) -> bool
  {
    !self.a.is_zero() && !self.b.is_zero()
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct MtaProtocolState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,

  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ra: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ca: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub cb: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub c_other: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sa: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub out_a_local_b_rem: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub out_a_rem_b_local: BigUint,
}

impl ProtocolSavedState for MtaProtocolState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> Self
  {
    Default::default()
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct MtaProtocolOutput
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub albr: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub arbl: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ra: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ca: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub c_other: BigUint,
  pub export: Vec<String>,
}

impl OutputWithExports for MtaProtocolOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct MtaS1
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub ca: BigUint,
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct MtaS2
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub cb: BigUint,
  pub use_check: bool,
  pub server_in: MtaS1,
  pub chk: MtaSchnorrBaseProof,
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct MtaS3
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  cb: BigUint,
  pub use_check: bool,
  pub chk: MtaSchnorrBaseProof,
}

declare_protocol_flow! {
    MtaProtocolClient, MtaProtocolParameters,MtaProtocolState,MtaProtocolOutput,
    preamble {
        let mut rng=OsRng
        let q=&*Q_MODULUS
    };
    step |pars,state,_in_msg,out_msg|{
        state.ra= gen_rand_z(&mut rng, &pars.fuji.keys.pk.n, true).unwrap();
        state.ca= pars.fuji.keys.pk.encrypt_with_rand(&pars.a,&state.ra);
        let outp=MtaS1{
          ca:state.ca.clone()
        };
     yeet!(out_msg,outp);
     ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let mut rng=OsRng;
        let inp2=geet!(in_msg,MtaS2);
        if pars.use_check!=inp2.use_check
        {
            return ExecutionControl::Error("Protocol type mismatch in MTA".to_string())
        }
        if pars.use_check
        {
            if !inp2.chk.bproof.verify(&inp2.chk.b)
            {
                return ExecutionControl::Error("Schnorr proof for B incorrect".to_string())
            }
            if !inp2.chk.bdproof.verify(&inp2.chk.bd)
            {
                return ExecutionControl::Error("Schnorr proof for Bd incorrect".to_string())
            }
        }
        let alphad=pars.fuji.keys.decrypt(&inp2.cb);
        state.out_a_local_b_rem=&alphad%q;
        if pars.use_check
        {

        let vc= ProjectivePoint::generator()*to_scalar(&state.out_a_local_b_rem);
        let v2= ProjectivePoint::from(inp2.chk.bd)+(ProjectivePoint::from(inp2.chk.b)*to_scalar(&pars.a));
        if vc!=v2
         {
            return ExecutionControl::Error("result does not match public value".to_string())
         }
        }
        let qc=q*q*q; //K
        let beta=gen_rand_z(&mut rng, &(&pars.counterparty.pk.n-&qc),false).unwrap();
        state.out_a_rem_b_local =(q-&beta%q)%q;
        let  r1=gen_rand_z(&mut rng,&pars.counterparty.pk.n,true).unwrap();
        state.cb=  (inp2.server_in.ca.modpow(&pars.b,&pars.counterparty.pk.n_sq)*pars.counterparty.pk.encrypt_with_rand(&beta,&r1))%&pars.counterparty.pk.n_sq;
        state.c_other= inp2.server_in.ca;
        let outp=MtaS3{cb:state.cb.clone(),
            use_check:pars.use_check,
           chk: if pars.use_check {
            MtaSchnorrBaseProof{
            b: (ProjectivePoint::generator()*to_scalar(&pars.b)).to_affine(),
            bd: (ProjectivePoint::generator()*to_scalar(&beta)).to_affine(),
            bproof:SchnorrBaseProof::new(&pars.b),
            bdproof:SchnorrBaseProof::new(&beta),
            }
           }
           else
           {
               Default::default()
           }
        };
        yeet!(out_msg,outp);
        ExecutionControl::None
    };
    step |_pars,_state,in_msg,_out_msg|{
        let status=geet!(in_msg,StatusMessage);
        if status.status!="OK"
        {
            return ExecutionControl::Error("Confirmation invalid".to_string())
        }
        ExecutionControl::None
    };
    output |_pars,state,_in_msg,_out_msg|
    {
        MtaProtocolOutput
        {
          albr:state.out_a_local_b_rem.clone(),
          arbl:state.out_a_rem_b_local.clone(),
          ra:state.ra.clone(),
          ca:state.ca.clone(),
          c_other:state.c_other.clone(),
          export:vec![]
        }
    }
}

pub struct MtaProtocolServer {}

declare_protocol_flow! {
    MtaProtocolServer, MtaProtocolParameters,MtaProtocolState,MtaProtocolOutput,
    preamble {
        let mut rng=OsRng
        let q=&*Q_MODULUS
    };
    step |pars,state,in_msg,out_msg|{
        let inp1=geet!(in_msg,MtaS1);
       state.c_other=inp1.ca;
       let qc=q*q*q; //K
       let beta=gen_rand_z(&mut rng, &(&pars.counterparty.pk.n-&qc),false).unwrap();
       state.out_a_rem_b_local =(q-&beta%q)%q;
       let  r1=gen_rand_z(&mut rng,&pars.counterparty.pk.n,true).unwrap();
       state.cb=  (state.c_other.modpow(&pars.b,&pars.counterparty.pk.n_sq)*pars.counterparty.pk.encrypt_with_rand(&beta,&r1))%&pars.counterparty.pk.n_sq;
       state.ra= gen_rand_z(&mut rng, &pars.fuji.keys.pk.n, true).unwrap();
       state.ca= pars.fuji.keys.pk.encrypt_with_rand(&pars.a,&state.ra);
       let srv=MtaS1
       {
           ca:state.ca.clone()
       };
       let outp=MtaS2{
          cb:state.cb.clone(),
          use_check:pars.use_check,
          server_in:srv,
          chk: if pars.use_check
          {
            MtaSchnorrBaseProof
            {
                b: (ProjectivePoint::generator()*to_scalar(&pars.b)).to_affine(),
                bd: (ProjectivePoint::generator()*to_scalar(&beta)).to_affine(),
                bproof:SchnorrBaseProof::new(&pars.b),
                bdproof:SchnorrBaseProof::new(&beta),
            }
          }
          else
          {
              Default::default()
          }
       };
       yeet!(out_msg,outp);
       ExecutionControl::None
    };
    step |pars,state,in_msg,out_msg|{
        let inp3=geet!(in_msg,MtaS3);
        if pars.use_check!=inp3.use_check
        {
           return ExecutionControl::Error("At stage 3, invalid version of server protocol".to_string());
        }
         if pars.use_check
         {
            if !inp3.chk.bproof.verify(&inp3.chk.b)
            {
                return ExecutionControl::Error("Schnorr proof for B incorrect on server".to_string())
            }
            if !inp3.chk.bdproof.verify(&inp3.chk.bd)
            {
                return ExecutionControl::Error("Schnorr proof for Bd incorrect on server".to_string())
            }
         }
         let alphad=pars.fuji.keys.decrypt(&inp3.cb);
         state.out_a_local_b_rem=&alphad%q;
         if pars.use_check
         {

         let vc= ProjectivePoint::generator()*to_scalar(&state.out_a_local_b_rem);
         let v2= ProjectivePoint::from(inp3.chk.bd)+(ProjectivePoint::from(inp3.chk.b)*to_scalar(&pars.a));
         if vc!=v2
          {
             return ExecutionControl::Error("result does not match public value on server".to_string())
          }
         }
         let sm=StatusMessage{status:"OK".to_string()};
         yeet!(out_msg,sm);
        ExecutionControl::None
    };
    output |_pars,state,_in_msg,_out_msg|
    {
        MtaProtocolOutput
        {
          albr:state.out_a_local_b_rem.clone(),
          arbl:state.out_a_rem_b_local.clone(),
          ra:state.ra.clone(),
          ca:state.ca.clone(),
          c_other:state.c_other.clone(),
          export:vec![]
        }
    }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::mta::*;
  #[test]
  fn mta_check()
  {
    let mut rng = rand::rngs::OsRng;
    let q = &*Q_MODULUS;
    let fuji_client = FujiCore::generate_initial_keys(2048);
    let fuji_server = FujiCore::generate_initial_keys(2048);

    let client_params = MtaProtocolParameters {
      a: gen_rand_z(&mut rng, q, true).unwrap(),
      b: gen_rand_z(&mut rng, q, true).unwrap(),
      use_check: true,
      counterparty: fuji_server.extract_packet().to_side_data().unwrap(),
      fuji: fuji_client,
    };
    let server_params = MtaProtocolParameters {
      a: gen_rand_z(&mut rng, q, true).unwrap(),
      b: gen_rand_z(&mut rng, q, true).unwrap(),
      use_check: true,
      counterparty: client_params.fuji.extract_packet().to_side_data().unwrap(),
      fuji: fuji_server,
    };

    let (out_client, out_server) = run_protocol::<MtaProtocolClient, MtaProtocolServer>(&client_params, &server_params);

    assert_eq!(
      (&out_client.albr + &out_server.arbl) % q,
      (&client_params.a * &server_params.b) % q
    );
    assert_eq!(
      (&out_client.arbl + &out_server.albr) % q,
      (&client_params.b * &server_params.a) % q
    );
  }
}
