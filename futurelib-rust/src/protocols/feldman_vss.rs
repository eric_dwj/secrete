/// Feldman Verifiable Secret Share as described in https://eprint.iacr.org/2020/540.pdf (One Round Threshold ECDSA with Identifiable Abort)

use crate::common::*;
use crate::common_defs::*;
use crate::protocols::core::*;
use crate::{declare_protocol_flow, geet, yeet};

pub struct FeldmanVssClient {}

#[derive(Serialize, Deserialize)]
pub struct FeldmanVssParameters
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub secret: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub modulo: BigUint,
}
impl ParValidation for FeldmanVssParameters
{
  fn validate(&self) -> bool
  {
    !self.modulo.is_zero() && !self.secret.is_zero()
  }
}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct FeldmanVssState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub my_a: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub my_sigma: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub vi_oth: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub v0_oth: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub share: BigUint,
}

impl ProtocolSavedState for FeldmanVssState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> FeldmanVssState
  {
    FeldmanVssState {
      internal: ProtocolState::new(),
      __cchild: None,
      my_a: BigUint::zero(),
      my_sigma: BigUint::zero(),
      vi_oth: ProjectivePoint::generator().to_affine(),
      v0_oth: ProjectivePoint::generator().to_affine(),
      share: BigUint::zero(),
    }
  }
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct FeldmanVssOutput
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub x_oth: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub v0_oth: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub x: BigUint,
  pub export: Vec<String>,
}

impl OutputWithExports for FeldmanVssOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize, Clone, Default)]
struct FVssS1
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  sigma: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  vi: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  v0: AffinePoint,
}

declare_protocol_flow! {
    FeldmanVssClient, FeldmanVssParameters,FeldmanVssState,FeldmanVssOutput,
    preamble {
        let my_x=&*FRONTEND_PARTY
        let other_x=&*BACKEND_PARTY
        let mut rng=rand::rngs::OsRng
    };
    step |pars,state,_in_msg,out_msg|{
        state.my_a=gen_rand_z( &mut rng,&pars.modulo,false).unwrap();
        let mut  outp:FVssS1=Default::default();
        state.my_sigma=(  (&state.my_a*my_x)%&pars.modulo+&pars.secret)%&pars.modulo;
        outp.sigma=((&state.my_a*other_x)%&pars.modulo+&pars.secret)%&pars.modulo;
        state.vi_oth=  (ProjectivePoint::generator() * to_scalar(&outp.sigma)).to_affine();
        outp.vi= (ProjectivePoint::generator() * to_scalar(&state.my_a)).to_affine();
        outp.v0=(ProjectivePoint::generator() * to_scalar(&pars.secret)).to_affine();
        yeet!(out_msg,outp);
        ExecutionControl::None
        };
    step |_pars,state,in_msg,_out_msg|{
        let inp2 = geet!(in_msg,FVssS1);
        //
        let v1=(ProjectivePoint::generator() * to_scalar(&inp2.sigma)).to_affine();
        let v2= (ProjectivePoint::from(inp2.v0)+(ProjectivePoint::from(inp2.vi) * to_scalar(my_x))) .to_affine();
        if v1!=v2
        {
            return ExecutionControl::Error("Feldman verification failed for client".to_string());
        }
        let v3= ProjectivePoint::from(inp2.v0)+ProjectivePoint::from(inp2.vi) * to_scalar(other_x);
        state.vi_oth=(ProjectivePoint::from(state.vi_oth)+v3).to_affine();
        state.v0_oth=inp2.v0;
        state.share=inp2.sigma;
        ExecutionControl::None
    };
    output |pars,state,_in_msg,_out_msg|
    {
        let output:FeldmanVssOutput=FeldmanVssOutput{x_oth:state.vi_oth,v0_oth:state.v0_oth,x:(&state.share+&state.my_sigma)%&pars.modulo,export:vec![]};
        output
    }

}

#[cfg(feature = "server")]
pub struct FeldmanVssServer {}

#[cfg(feature = "server")]
declare_protocol_flow! {
    FeldmanVssServer, FeldmanVssParameters,FeldmanVssState,FeldmanVssOutput,
    preamble { let my_x=&*BACKEND_PARTY
        let other_x=&*FRONTEND_PARTY
        let mut rng=rand::rngs::OsRng
    };
    step |pars,state,in_msg,out_msg|{
        let inp2 = geet!(in_msg,FVssS1);
        let v1=(ProjectivePoint::generator() * to_scalar(&inp2.sigma)).to_affine();
        let v2= (ProjectivePoint::from(inp2.v0)+(ProjectivePoint::from(inp2.vi) * to_scalar(my_x))) .to_affine();
        if v1!=v2
        {
            return ExecutionControl::Error("Feldman verification failed for server".to_string());
        }
        let v3= ProjectivePoint::from(inp2.v0)+ProjectivePoint::from(inp2.vi) * to_scalar(other_x);

        state.v0_oth=inp2.v0;
        state.share=inp2.sigma;
        state.my_a=gen_rand_z( &mut rng,&pars.modulo,false).unwrap();
        let mut  outp:FVssS1=Default::default();
        state.my_sigma=(  (&state.my_a*my_x)%&pars.modulo+&pars.secret)%&pars.modulo;
        outp.sigma=((&state.my_a*other_x)%&pars.modulo+&pars.secret)%&pars.modulo;
        state.vi_oth=(v3+ProjectivePoint::generator()*to_scalar(&outp.sigma)).to_affine();

        outp.vi= (ProjectivePoint::generator() * to_scalar(&state.my_a)).to_affine();
        outp.v0=(ProjectivePoint::generator() * to_scalar(&pars.secret)).to_affine();
        yeet!(out_msg,outp);
        ExecutionControl::None
     };
     output |pars,state,_in_msg,_out_msg|
     {
         let output:FeldmanVssOutput=FeldmanVssOutput{x_oth:state.vi_oth,v0_oth:state.v0_oth,x:(&state.share+&state.my_sigma)%&pars.modulo,export:vec![]};
         output
     }

}

#[cfg(test)]
mod tests
{
  use crate::common::*;
  use crate::common_defs::*;
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::feldman_vss::*;
  #[test]
  fn feldman_check()
  {
    // let client=FeldmanVssClient{};
    let q = &*Q_MODULUS;
    let mut rng = rand::rngs::OsRng;
    let client_params = FeldmanVssParameters {
      secret: gen_rand_z(&mut rng, q, false).unwrap(),
      modulo: q.clone(),
    };
    let server_params = FeldmanVssParameters {
      secret: gen_rand_z(&mut rng, q, false).unwrap(),
      modulo: q.clone(),
    };
    let (ou1, ou2) = run_protocol::<FeldmanVssClient, FeldmanVssServer>(&client_params, &server_params);
    let comp1 = (ProjectivePoint::generator() * to_scalar(&ou1.x)).to_affine();
    let comp2 = (ProjectivePoint::generator() * to_scalar(&ou2.x)).to_affine();
    assert_eq!(comp1, ou2.x_oth);
    assert_eq!(comp2, ou1.x_oth);
  }
}
