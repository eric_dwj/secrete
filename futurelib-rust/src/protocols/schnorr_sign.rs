/// Schnorr signature following https://github.com/ZenGo-X/multi-party-schnorr/blob/master/papers/provably_secure_distributed_schnorr_signatures_and_a_threshold_scheme.pdf
/// Uses secp256k1 curve (Bitcoin). Compatible with BIP340 test vectors (https://github.com/bitcoin/bips/blob/master/bip-0340.mediawiki)
/// Uses tagged hashing from BIP340. Has upport for 2 hashes, but only one is used for BIP
/// Parameters are hasher(sha2_256 for BIP), schnorr keygen output and message as a byte vector (not necessarily prehashed) and derivation path (not hardened) 
use crate::common::*;
use crate::common_defs::*;
use crate::protocols::core::*;
use crate::protocols::schnorr_keygen::*;
use crate::xpub::*;
use crate::{declare_protocol_flow, geet, yeet};
use sha2::digest::DynDigest;

pub trait WrappedHasher256
{
  fn name(&self) -> &'static str;
  fn new(&self) -> Box<dyn DynDigest>;
}
pub struct WrappedSha2_256 {}
impl WrappedHasher256 for WrappedSha2_256
{
  fn name(&self) -> &'static str
  {
    "sha2_256"
  }
  fn new(&self) -> Box<dyn DynDigest>
  {
    Box::new(sha2::Sha256::new())
  }
}

pub struct WrappedSha3_256 {}
impl WrappedHasher256 for WrappedSha3_256
{
  fn name(&self) -> &'static str
  {
    "sha3_256"
  }
  fn new(&self) -> Box<dyn DynDigest>
  {
    Box::new(sha3::Sha3_256::new())
  }
}

pub fn hasher_to_str(key: &Box<dyn WrappedHasher256>) -> Box<String>
{
  Box::new(key.name().to_string())
}

pub fn hasher_from_str(st: RawValue) -> Result<Box<dyn WrappedHasher256>, miniserde::Error>
{
  let name = st.get_dequoted_string()?;
  match name.as_str()
  {
    "sha2_256" => Ok(Box::new(WrappedSha2_256 {})),
    "sha3_256" => Ok(Box::new(WrappedSha3_256 {})),
    _ => Err(miniserde::Error {}),
  }
}
impl Default for Box<dyn WrappedHasher256>
{
  fn default() -> Self
  {
    Box::new(WrappedSha2_256 {})
  }
}
macro_rules! tagged_hash {
  ($hasher:expr,$tag:expr, $($v:expr),+) => {{
      let mut hasher=$hasher.new();
      hasher.update($tag.as_bytes());
      let tag=hasher.finalize();
      let mut hasher=$hasher.new();
      hasher.update(&tag);
      hasher.update(&tag);
      $(
          hasher.update($v);
      )+
      hasher.finalize()
  }};
}

pub fn verify_bip340(msg: &[u8], pk: &AffinePoint, sig: &[u8], hasher: &Box<dyn WrappedHasher256>) -> bool
{
  if pk.to_vector()[0] == 3
  {
    return false;
  }
  if sig.len() != 64
  {
    return false;
  }
  /*let rr:Vec<u8>=vec![3];
  rr.append(& mut sig[0..32].iter().cloned().collect());
  let off = match EncodedPoint::from_bytes(&rr)
  {
    Ok(res) =>res,
    Err(_) => return false
  };
   let tmp= AffinePoint::from_encoded_point(&off);
   if tmp.is_none().into() {return false}
   let rrp=tmp.unwrap();*/
  let s = BigUint::from_bytes_be(&sig[32..64]);
  if s >= *Q_MODULUS
  {
    return false;
  }
  let e = BigUint::from_bytes_be(&tagged_hash!(
    hasher,
    "BIP0340/challenge",
    &sig[0..32],
    &pk.to_vector()[1..],
    &msg[..]
  )) % &*Q_MODULUS;
  let rr = ProjectivePoint::generator() * to_scalar(&s) - ProjectivePoint::from(*pk) * to_scalar(&e);
  if rr == ProjectivePoint::identity()
  {
    return false;
  }
  let k = rr.to_affine().to_vector();
  if k[0] == 3
  {
    return false;
  }
  for (ai, bi) in sig[0..32].iter().zip(k[1..].iter())
  {
    if ai != bi
    {
      return false;
    }
  }
  return true;
}

pub struct SchnorrSignClient {}

#[derive(Serialize, Deserialize)]
pub struct SchnorrSignParameters
{
  #[serde(serialize_with = "hasher_to_str", deserialize_with = "hasher_from_str")]
  pub hasher: Box<dyn WrappedHasher256>,
  #[serde(rename = "key")]
  pub keygen: SchnorrKeyGenOutput,
  #[serde(rename = "derivationPath")]
  pub path: String,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub message: Vec<u8>,
}

impl ParValidation for SchnorrSignParameters
{
  fn validate(&self) -> bool
  {
    self.message.len() > 0 && self.path.len() > 0
  }
}
#[derive(Serialize, Deserialize, Clone, Default)]
pub struct SchnorrSignState
{
  internal: ProtocolState,
  __cchild: Option<Box<RawValue>>,
  //  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  //  ui: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  ri: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  rid: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  si: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  f1: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  f1d: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  combined: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  combinedd: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub vv: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub offset_pubkey: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub_other: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  func_other: AffinePoint,
  pub path: DerivePath,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub offset: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gamma: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sigma: BigUint,
  pub flip_pubkey: bool,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub cout: Vec<u8>, //64 bytes
}

impl ProtocolSavedState for SchnorrSignState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> SchnorrSignState
  {
    Default::default()
  }
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct SchnorrSignOutput
{
  #[serde(serialize_with = "int_as_base16h", deserialize_with = "int_from_base16h")]
  pub sigma: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub v: AffinePoint,
  #[serde(serialize_with = "buffer_as_base64", deserialize_with = "buffer_from_base64")]
  pub sig: Vec<u8>, //64 bytes
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub pubkey: AffinePoint,
  pub export: Vec<String>,
}
impl OutputWithExports for SchnorrSignOutput
{
  fn get_exports(&self) -> Vec<String>
  {
    self.export.clone()
  }
  fn set_exports(&mut self, exports: &[&str])
  {
    self.export = exports.into_iter().map(|x| x.to_string()).collect();
  }
  fn finalize(&mut self) {}
}

#[derive(Serialize, Deserialize)]
pub struct SchnorrSignS1
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub si: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sid: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit1: AffinePoint,
}

#[derive(Serialize, Deserialize)]
pub struct SchnorrSignS2
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub si: BigUint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub sid: BigUint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub commit1: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a1: AffinePoint,
}
#[derive(Serialize, Deserialize)]
pub struct SchnorrSignS3
{
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a0: AffinePoint,
  #[serde(serialize_with = "point_as_base64", deserialize_with = "point_from_base64")]
  pub a1: AffinePoint,
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gamma: BigUint,
}

#[derive(Serialize, Deserialize)]
pub struct SchnorrSignS4
{
  #[serde(serialize_with = "int_as_base64", deserialize_with = "int_from_base64")]
  pub gamma: BigUint,
}

declare_protocol_flow! {
    SchnorrSignClient, SchnorrSignParameters,SchnorrSignState,SchnorrSignOutput,
    preamble {
        let mut rng=OsRng
        let q=&*Q_MODULUS
        let my_x=&*FRONTEND_PARTY
        let other_x=&*BACKEND_PARTY
    };
    step |pars,state,_in_msg,out_msg|{
        state.path=match str_to_derive(&pars.path)
        {
            Some(path)=>path,
            None => return ExecutionControl::Error("Invalid derivation path".to_string())
        };
        for pt in &state.path
        {
            if pt.0==true
            {
                return ExecutionControl::Error("Only non-hardened paths are supported".to_string())
            }
        }
        let mut acc=DeriveAccumulator{pubkey:pars.keygen.pubkey.clone(),chaincode:pars.keygen.chaincode.clone(),accumulated:BigUint::zero()};
        for pt in &state.path
        {
          if ! derive_accumulate(&mut acc, pt.1)
          {
            return ExecutionControl::Error("Incorrect derivation path indices".to_string())
          }
        }
        state.offset=acc.accumulated;
        let mut offset_key=(ProjectivePoint::from(pars.keygen.pubkey)+ProjectivePoint::generator()*to_scalar(&state.offset)).to_affine();
        if offset_key.to_vector()[0]==3 //we'll need to flip...
        {
          offset_key=-offset_key;
          state.flip_pubkey=true;
          state.offset=q-&state.offset;
        }
        else
        {
          state.flip_pubkey=false;
        }
        state.offset_pubkey=offset_key;
        let   aux=tagged_hash!(pars.hasher,"BIP0340/aux",&fixed_len_buffer_pref(&pars.keygen.chaincode,32));
        let aux:Vec<u8>=aux.iter().zip(fixed_len_buffer_pref(&pars.keygen.priv_key_share,32).iter()).map(|(x,y)| x|y).collect();
        // approximate generation of nonce part in https://github.com/bitcoin/bips/blob/master/bip-0340.mediawiki
        state.ri=BigUint::from_bytes_be(&tagged_hash!(pars.hasher,"BIP0340/nonce",&aux,&state.offset_pubkey.to_vector()[1..],&pars.message))%q;//gen_rand_z(&mut rng, q,false).unwrap();
        if state.ri==*UINT_ZERO
        {
            return ExecutionControl::Error("Corrupted nonce, should be negligible probability".to_string())
        }
        state.rid=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1d=gen_rand_z(&mut rng, q,false).unwrap();
        let out= SchnorrSignS1{
            si: (&state.ri+&state.f1*other_x)%q,
            sid: (&state.rid+&state.f1d*other_x)%q,
            commit0: (ProjectivePoint::from(pars.keygen.gg)*to_scalar(&state.ri)+ProjectivePoint::from(pars.keygen.hh)*to_scalar(&state.rid)).to_affine(),
            commit1: (ProjectivePoint::from(pars.keygen.gg)*to_scalar(&state.f1)+ProjectivePoint::from(pars.keygen.hh)*to_scalar(&state.f1d)).to_affine(),
        };
        yeet!(out_msg,out);
        ExecutionControl::None
    };
   step |pars,state,in_msg,out_msg|{
    let inp2 = geet!(in_msg,SchnorrSignS2);

    // check pedersen...
    let pn1=ProjectivePoint::from(pars.keygen.gg)*to_scalar(&inp2.si)+ProjectivePoint::from(pars.keygen.hh)*to_scalar(&inp2.sid);
    let pn2=ProjectivePoint::from(inp2.commit0)+ProjectivePoint::from(inp2.commit1)*to_scalar(my_x);
    if pn1!=pn2
    {
        // honestly not sure how well this works in 2 out of 2...
        return ExecutionControl::Error("Invalid pedersen commitment from server".to_string());
    }
    let fcheck=ProjectivePoint::from(inp2.a0)+ProjectivePoint::from(inp2.a1)*to_scalar(my_x);
    state.si=inp2.si;
    let f2=ProjectivePoint::generator() * to_scalar(&state.si);
    if f2!=fcheck
    {
        return ExecutionControl::Error("Invalid feldman commitment from server".to_string());
    }


    state.combinedd=(&state.rid+&state.f1d*my_x+&inp2.sid)%q;
    state.pub_other=inp2.a0;
    state.func_other=inp2.a1;
    state.vv=(ProjectivePoint::from(state.pub_other)+ ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    let a00=(ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    let a01=(ProjectivePoint::generator() * to_scalar(&state.f1)).to_affine();
    if state.vv.to_vector()[0]==3 //flip after preparing for send
    {
      state.vv=(-ProjectivePoint::from(state.vv)).to_affine();
      state.pub_other=(-ProjectivePoint::from(state.pub_other)).to_affine();
      state.func_other=(-ProjectivePoint::from(state.func_other)).to_affine();
      state.ri=q-&state.ri;
      state.f1=q-&state.f1;
      state.si=q-&state.si;
    }
    state.cout=state.vv.to_vector()[1..].iter().cloned().collect();
    if state.cout.len()!=32
    {
      return ExecutionControl::Error("Invalid R calculated".to_string());
    }
    state.combined=(&state.ri+&state.f1*my_x+&state.si)%q;
    let privflip=q-&pars.keygen.priv_key_share;
    let prv=if state.flip_pubkey {&privflip} else {&pars.keygen.priv_key_share};
    let e=BigUint::from_bytes_be(&tagged_hash!(pars.hasher,"BIP0340/challenge",&state.vv.to_vector()[1..],&state.offset_pubkey.to_vector()[1..],&pars.message))%q;
    state.gamma=( &(prv+&state.offset)*e+&state.combined)%q;
    let out3= SchnorrSignS3{
        a0: a00,
        a1: a01,
        gamma:state.gamma.clone()
    };
    yeet!(out_msg,out3);
    ExecutionControl::None
    };

 step |pars,state,in_msg,_out_msg|{
    let inp4 = geet!(in_msg,SchnorrSignS4);
    let chk1=ProjectivePoint::generator()*to_scalar(&inp4.gamma);
    let fnflip=-pars.keygen.func_sum;
    let fns=if state.flip_pubkey {fnflip} else {pars.keygen.func_sum.clone()};

    let e=BigUint::from_bytes_be(&tagged_hash!(pars.hasher,"BIP0340/challenge",&state.vv.to_vector()[1..],&state.offset_pubkey.to_vector()[1..],&pars.message))%q;
    let chk2=ProjectivePoint::from(state.vv)+ProjectivePoint::from(state.func_other)*to_scalar(&other_x)+ProjectivePoint::generator()*&to_scalar(&state.f1)*to_scalar(&other_x)
             +(ProjectivePoint::from(state.offset_pubkey)+ProjectivePoint::from(fns)*to_scalar(&other_x))*to_scalar(&e);
    if chk2!=chk1
    {
        return ExecutionControl::Error("Invalid gamma from server".to_string());
    }
    let my_sig=BigInt::from_biguint(Sign::Plus, my_x.clone());
    let other_sig=BigInt::from_biguint(Sign::Plus, other_x.clone());
    let my_gamma=BigInt::from_biguint(Sign::Plus, state.gamma.clone());
    let other_gamma=BigInt::from_biguint(Sign::Plus, inp4.gamma.clone());

    let l0=&(&other_sig/(&other_sig-&my_sig));
    let l1=&(&my_sig/(&my_sig-&other_sig));
    let sgm=l0*&my_gamma+&other_gamma*l1;

    state.sigma=match sgm.to_biguint()
    {
        Some(res) =>res,
        None =>  match (BigInt::from_biguint(Sign::Plus,q.clone())+sgm).to_biguint() {
          Some(res) =>res,
          None => return ExecutionControl::Error("Invalid sigma result".to_string())
        }

        // return ExecutionControl::Error("Invalid sigma result".to_string())
    }%q;

    let chk1=ProjectivePoint::generator()*to_scalar(&state.sigma)-ProjectivePoint::from(state.offset_pubkey)*to_scalar(&e);
    if chk1!=ProjectivePoint::from(state.vv)
    {
     return ExecutionControl::Error("Invalid sign...".to_string())
    }

    state.cout.append(&mut fixed_len_buffer_pref(&state.sigma, 32));

    if !verify_bip340(&pars.message,&state.offset_pubkey,&state.cout,&pars.hasher)
    {
      return ExecutionControl::Error("Invalid signature on client".to_string());
    }

   ExecutionControl::None
   };
   output |_pars,state,_in_msg,_out_msg|
   {
   //let pub_mine= ProjectivePoint::generator() * to_scalar(&state.ri);
   // #[cfg(not(target_arch = "wasm32"))]
  //  libc_println!("Client xi: {}", &state.xi);
   // #[cfg(not(target_arch = "wasm32"))]
  //  libc_println!("Client chain: {}", &state.chaincode);
  SchnorrSignOutput
    {
       sigma:state.sigma.clone(),
       v:state.vv.clone(),
       sig:state.cout.clone(),
       pubkey:state.offset_pubkey.clone(),
       export:vec!["sigma".to_string(),"v".to_string(),"sig".to_string(),"pubkey".to_string()]
     }
   }
}

#[cfg(feature = "server")]
pub struct SchnorrSignServer {}

#[cfg(feature = "server")]
declare_protocol_flow! {
    SchnorrSignServer, SchnorrSignParameters,SchnorrSignState,SchnorrSignOutput,
    preamble {
        let mut rng=OsRng
        let q=&*Q_MODULUS
        let my_x=&*BACKEND_PARTY
        let other_x=&*FRONTEND_PARTY
    };
    step |pars,state,in_msg,out_msg|{
        state.path=match str_to_derive(&pars.path)
        {
            Some(path)=>path,
            None => return ExecutionControl::Error("Invalid derivation path".to_string())
        };
        for pt in &state.path
        {
            if pt.0==true
            {
                return ExecutionControl::Error("Only non-hardened paths are supported".to_string())
            }
        }
        let mut acc=DeriveAccumulator{pubkey:pars.keygen.pubkey.clone(),chaincode:pars.keygen.chaincode.clone(),accumulated:BigUint::zero()};
        for pt in &state.path
        {
          if ! derive_accumulate(&mut acc, pt.1)
          {
            return ExecutionControl::Error("Incorrect derivation path indices".to_string())
          }
        }
        state.offset=acc.accumulated;
        let mut offset_key=(ProjectivePoint::from(pars.keygen.pubkey)+ProjectivePoint::generator()*to_scalar(&state.offset)).to_affine();
        if offset_key.to_vector()[0]==3 //we'll need to flip...
        {
          offset_key=-offset_key;
          state.flip_pubkey=true;
          state.offset=q-&state.offset;
        }
        else
        {
          state.flip_pubkey=false;
        }
        state.offset_pubkey=offset_key;
        let   aux=tagged_hash!(pars.hasher,"BIP0340/aux",&fixed_len_buffer_pref(&pars.keygen.chaincode,32));
        let aux:Vec<u8>=aux.iter().zip(fixed_len_buffer_pref(&pars.keygen.priv_key_share,32).iter()).map(|(x,y)| x|y).collect();
        // approximate generation of nonce part in https://github.com/bitcoin/bips/blob/master/bip-0340.mediawiki
        state.ri=BigUint::from_bytes_be(&tagged_hash!(pars.hasher,"BIP0340/nonce",&aux,&state.offset_pubkey.to_vector()[1..],&pars.message))%q;//gen_rand_z(&mut rng, q,false).unwrap();
        state.rid=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1=gen_rand_z(&mut rng, q,false).unwrap();
        state.f1d=gen_rand_z(&mut rng, q,false).unwrap();
        if state.ri==*UINT_ZERO
        {
            return ExecutionControl::Error("Corrupted nonce, should be negligible probability".to_string())
        }

        let inp=geet!(in_msg,SchnorrSignS1);
        let pn1=ProjectivePoint::from(pars.keygen.gg)*to_scalar(&inp.si)+ProjectivePoint::from(pars.keygen.hh)*to_scalar(&inp.sid);
        let pn2=ProjectivePoint::from(inp.commit0)+ProjectivePoint::from(inp.commit1)*to_scalar(my_x);
        if pn1!=pn2
        {
            return ExecutionControl::Error("Invalid pedersen commitment from client".to_string());
        }
        state.si=inp.si;

        state.combinedd=&state.rid+&state.f1d*my_x+&inp.sid;
        let a00=(ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();

        let out= SchnorrSignS2{
            si: (&state.ri+&state.f1*other_x)%q,
            a1: (ProjectivePoint::generator() * to_scalar(&state.f1)).to_affine(),
            a0: a00,
            sid: &state.rid+&state.f1d*other_x,
            commit0: (ProjectivePoint::from(pars.keygen.gg)*to_scalar(&state.ri)+ProjectivePoint::from(pars.keygen.hh)*to_scalar(&state.rid)).to_affine(),
            commit1: (ProjectivePoint::from(pars.keygen.gg)*to_scalar(&state.f1)+ProjectivePoint::from(pars.keygen.hh)*to_scalar(&state.f1d)).to_affine(),
        };
        yeet!(out_msg,out);
        ExecutionControl::None
    };
   step |pars,state,in_msg,out_msg|{
    let inp2 = geet!(in_msg,SchnorrSignS3);
    let fcheck=ProjectivePoint::from(inp2.a0)+ProjectivePoint::from(inp2.a1)*to_scalar(my_x);
    let f2=ProjectivePoint::generator() * to_scalar(&state.si);
    if f2!=fcheck
    {
        return ExecutionControl::Error("Invalid feldman commitment from client".to_string());
    }
    state.pub_other=inp2.a0;
    state.func_other=inp2.a1;
    state.vv=(ProjectivePoint::from(state.pub_other)+ ProjectivePoint::generator() * to_scalar(&state.ri)).to_affine();
    if state.vv.to_vector()[0]==3 //flip after preparing for send
    {
      state.vv=(-ProjectivePoint::from(state.vv)).to_affine();
      state.pub_other=(-ProjectivePoint::from(state.pub_other)).to_affine();
      state.func_other=(-ProjectivePoint::from(state.func_other)).to_affine();
      state.ri=q-&state.ri;
      state.f1=q-&state.f1;
      state.si=q-&state.si;
    }
    state.cout=state.vv.to_vector()[1..].iter().cloned().collect();
    if state.cout.len()!=32
    {
      return ExecutionControl::Error("Invalid R calculated".to_string());
    }
    state.combined=(&state.ri+&state.f1*my_x+&state.si)%q;
    let privflip=q-&pars.keygen.priv_key_share;
    let prv=if state.flip_pubkey {&privflip} else {&pars.keygen.priv_key_share};
    let fnflip=-pars.keygen.func_sum;
    let fns=if state.flip_pubkey {fnflip} else {pars.keygen.func_sum.clone()};

    let e=BigUint::from_bytes_be(&tagged_hash!(pars.hasher,"BIP0340/challenge",&state.vv.to_vector()[1..],&state.offset_pubkey.to_vector()[1..],&pars.message))%q;
    state.gamma=( &(prv+&state.offset)*&e+ &state.combined)%q;//&(&pars.keygen.priv_key_share+&state.offset)%q;
    let chk1=ProjectivePoint::generator()*to_scalar(&inp2.gamma);
    let chk2=ProjectivePoint::from(state.vv)+ProjectivePoint::from(state.func_other)*to_scalar(&other_x)+ProjectivePoint::generator()*&to_scalar(&state.f1)*to_scalar(&other_x)
    +(ProjectivePoint::from(state.offset_pubkey)+ProjectivePoint::from(fns)*to_scalar(&other_x))*to_scalar(&e);
if chk2!=chk1
    {
        return ExecutionControl::Error("Invalid gamma from client".to_string());
    }
   let my_sig=BigInt::from_biguint(Sign::Plus, my_x.clone());
   let other_sig=BigInt::from_biguint(Sign::Plus, other_x.clone());
   let my_gamma=BigInt::from_biguint(Sign::Plus, state.gamma.clone());
   let other_gamma=BigInt::from_biguint(Sign::Plus, inp2.gamma.clone());

   let l0=&(&other_sig/(&other_sig-&my_sig));
   let l1=&(&my_sig/(&my_sig-&other_sig));
   let sgm=l0*&my_gamma+&other_gamma*l1;
   state.sigma=match sgm.to_biguint()
   {
       Some(res) =>res,
       None =>  match (BigInt::from_biguint(Sign::Plus,q.clone())+sgm).to_biguint() {
        Some(res) =>res,
        None => return ExecutionControl::Error("Invalid sigma result".to_string())
      }
   } %q;
   let chk1=ProjectivePoint::generator()*to_scalar(&state.sigma)-ProjectivePoint::from(state.offset_pubkey)*to_scalar(&e);
   if chk1!=ProjectivePoint::from(state.vv)
   {
    return ExecutionControl::Error("Invalid sign...".to_string())
   }
   state.cout.append(&mut fixed_len_buffer_pref(&state.sigma, 32));
   if !verify_bip340(&pars.message,&state.offset_pubkey,&state.cout,&pars.hasher)
   {
     return ExecutionControl::Error("Invalid signature on server".to_string());
   }
    let out4= SchnorrSignS4{
        gamma:state.gamma.clone()
    };

    yeet!(out_msg,out4);
    ExecutionControl::None
    };

   output |_pars,state,_in_msg,_out_msg|
   {
  SchnorrSignOutput
    {
       sigma:state.sigma.clone(),
       v:state.vv.clone(),
       sig:state.cout.clone(),
       pubkey:state.offset_pubkey.clone(),
       export:vec!["sigma".to_string(),"v".to_string(),"sig".to_string(),"pubkey".to_string()]
     }
   }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::schnorr_sign::*;
  //use num_traits::*;
  // use crate::xpub::*;
  //use crate::statics::*;
  use core::num::ParseIntError;
  use rsa::*;

  fn check_schnorr_sign_flow()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = SchnorrKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = SchnorrKeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) =
      run_protocol::<SchnorrKeyGenClient, SchnorrKeyGenServer>(&client_params, &server_params);
    assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pubkey, out_server.pubkey);
    assert_eq!(out_client.gg, out_server.gg);
    assert_eq!(out_client.hh, out_server.hh);
    let message: Vec<u8> = b"ECDSA proves knowledge of a secret number in the context of a single message, Schnorr"
      .into_iter()
      .cloned()
      .collect();

    let pclient_params = SchnorrSignParameters {
      path: "m/2".to_string(),
      keygen: out_client.clone(),
      message: message.clone(),
      hasher: Box::new(WrappedSha2_256 {}),
    };
    let pserver_params = SchnorrSignParameters {
      path: "m/2".to_string(),
      keygen: out_server.clone(),
      message: message.clone(),
      hasher: Box::new(WrappedSha2_256 {}),
    };
    let (pout_client, pout_server) =
      run_protocol::<SchnorrSignClient, SchnorrSignServer>(&pclient_params, &pserver_params);

    assert_eq!(pout_client.sigma, pout_server.sigma);
    assert_eq!(pout_client.v, pout_server.v);
    /*let xpub = private_to_public(&derive_index(&loc_prv, 2));
    assert!(verify_prehashed(
      &xpub.key,
      &to_scalar(&BigUint::from_bytes_be(&res)),
      &to_scalar(&pout_client.r),
      &to_scalar(&pout_server.s)
    )
    .is_ok());*/
  }
  #[test]
  fn check_schnorr_sign_multi()
  {
    for _ in 0..64
    {
      check_schnorr_sign_flow();
    }
  }
  pub fn decode_hex(s: &str) -> Result<Vec<u8>, ParseIntError>
  {
    (0..s.len())
      .step_by(2)
      .map(|i| u8::from_str_radix(&s[i..i + 2], 16))
      .collect()
  }
  pub fn vec_to_positivepk(vc: &[u8]) -> Option<AffinePoint>
  {
    let mut rr: Vec<u8> = vec![2];
    rr.append(&mut vc.iter().cloned().collect());
    let off = match EncodedPoint::from_bytes(&rr)
    {
      Ok(res) => res,
      Err(_) => return None,
    };
    let tmp = AffinePoint::from_encoded_point(&off);
    if tmp.is_none().into()
    {
      return None;
    }
    let rrp = tmp.unwrap();
    return Some(rrp);
  }
  #[test]
  fn parse_schnorr_json()
  {
    let jsn = r#"{"message":"VGhpcyBkbyBiZSBhIHRlc3QgbWVzc2FnZSBvZiBhbnkgNWRmMjIyMjIyMl8xNjA0OTExMDYxODE0X3No","key":{"encrypt":"U2FsdGVkX18zkkkZZbvz+LOdjxh7t/TpLvitzeLiqvipdcOvWu3AOHOQ7tSRHrB9f8e1FhNzfsOO6WzjHZnHNM6u5wIRlSH6VYhFdvvMMPsb/VlpQ1PC89q9e6I0cw+vatVubYhtvjsM70uOE85cmMUpsLj5wLVEqJshbpJhcheovzCpvRo204v+ReTlq7a/E6q1k4tHnefENhYL8gWp7Ow9ixT9lC9mT98N3ItnjCi11fiW/tukdwtaOcG+r5MWbsuBLmwV8QA+SAHzHhx3xveEvkWlng/oUrqmmDychvEyqakMlRJKBGHW+ifYbjxCMhp9V/PxBam0Ckl/BSGMvNKAW24Css5hCSSdve9jOFumjxTXuVN/7DY7v/YcR5SPVMfnoPVqmtRUW58fyf7cI1kTQZ071p3WOpL0DSmDauAisAQwZT3MyVICJP8+V5cimMV2swGj3o5DTTd7sppWbQz+HmeZ2HbvsI3WkseSFUov4csqIjjO2Pmr0uYXMBnS+I9w+rVGgD5gsfNcAQ3u3vqC/qChtwt0FiWctRZKwK9zjAhsLJppWVKdgncjQj6XzdYKdEPUA6vJZ9zBePoqVWsqcoNFaou2iv2+g7eBt3OmG9TphwNLuS7UywCafUpYA/WrWJOD+1vzlQA/y35GXzOaq0vkthNnBHmunjfRdvIhFmnvzIUjWN77ne2gXzFJZg2V27keGu+6R+vJ5zFsnfZtXUeDlaBZxFNjtpOLb0c=","encryptedOurBackup":"8ZinAcUXjk82d9JyJEnqA7gbcTAXV8v4GteqznpRGwAk4WT1jYRN3edPJr5uQ4Wgba7opD6M1YTQsNd47sVYUd8B9KU2PrUL3Q9vkrv7T9DXe8gBb7nZMRj15vdi7gQuc5k6J1zdfgn8UfEHwShRp46oNo1t9owFNaUbGWZUggm9TKUxs8mutTcNLmy6y6npFsqRZgBCz8zGXqwquun6jQW2JRXDR4Ht7JXUGX7zfPVYh1cK2G1w56QyucAmvWj95B4v8wes34Yr9NJA21G2aGuD3JEzmMVxYo4nagaY9vDip3qkrUnHeRAtcM3jhs2jLWnT1thViTAgsH133gNQTy48jYoDU5","encryptedTheirBackup":"43iBirTbUmrzboLUcLbZQhfk3EWiC3xCsdFUtASn9zL7hRahFJydk57zT6Nsb3LiDf2WJC6t5VKNMwBrynfUsKMEi1PMLSaB9S2pPEiEiNn8VzZkR7FSeiN1WF1CF3oSDF6ru65e8zVp2ZzB86dfks3eSiA4jMccHdzhacE2F9bp6iR4fBAHrJzm7uWAhR49yqmnKCdXnDe5NmfkaXMbfWTpvmmgeo2ThjffYajCbu1p7NPJsrv2S4Nkz2EJJBi1xJcewXr7FBEGhNzvNnopVymBvBWGLvAWX55DtMqj3acXna2tY8dkVXCriFgrRbTGvLMwypa5CHWete9vWUm7ETyLrRANQg","priv_key_share":"Ii3tjYoG10RF8jVWzxWyr/3/6dOlBPSvEJTDTmPmiqE=","pub_mine":"A6zTDtqKTorHeTozPrxR9nQgbBOeLNnrzv/x5NGB4dlZ","pub_other":"A8mfIiyHFmLlrQ8F1OM+6iNbEjLK1BZ4yq4+LX5jAVmx","gg":"Al72NifhHrJfJh8oZzDE9SqMDzrk53GP+uCI+q9hdKKV","hh":"AxN9QLIPxC5lGiU6X2WXcWK41hK0Ig6iXbK01MiMiTED","func_sum":"AieHWQnrdCWktgmuuZA10bWwoIZWPLei+8RkDc8ipcXP","pubkey":"Aqc++FAwd8YDKMGAJANM6d1JOvIJprE4ZMP/Q0KgM71l","chaincode":"cYFIFV65tITH2LygM3rA32EGl9q9bBZ8re/2RTUCq1c=","export":["encryptedOurBackup","encryptedTheirBackup","xpub","tpub"]},"derivationPath":"m/2/40","hasher":"sha2_256"}"#;
    let kparams: SchnorrSignParameters = miniserde::json::from_str(jsn).unwrap();

    assert!(kparams.validate());
  }
  // from https://github.com/bitcoin/bips/blob/master/bip-0340/test-vectors.csv
  #[test]
  fn check_bip140_verification()
  {
    let pclient_params = SchnorrSignParameters {
      path: "m/2".to_string(),
      keygen: Default::default(),
      message: vec![],
      hasher: Box::new(WrappedSha2_256 {}),
    };
    {
      let pk =
        vec_to_positivepk(&decode_hex("F9308A019258C31049344F85F89D5229B531C845836F99B08601F113BCE036F9").unwrap())
          .unwrap();
      let msg = decode_hex("0000000000000000000000000000000000000000000000000000000000000000").unwrap();
      let sig=decode_hex("E907831F80848D1069A5371B402410364BDF1C5F8307B0084C55F1CE2DCA821525F66A4A85EA8B71E482A74F382D2CE5EBEEE8FDB2172F477DF4900D310536C0").unwrap();
      assert!(verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    {
      let pk =
        vec_to_positivepk(&decode_hex("DFF1D77F2A671C5F36183726DB2341BE58FEAE1DA2DECED843240F7B502BA659").unwrap())
          .unwrap();
      let msg = decode_hex("243F6A8885A308D313198A2E03707344A4093822299F31D0082EFA98EC4E6C89").unwrap();
      let sig=decode_hex("6896BD60EEAE296DB48A229FF71DFE071BDE413E6D43F917DC8DCF8C78DE33418906D11AC976ABCCB20B091292BFF4EA897EFCB639EA871CFA95F6DE339E4B0A").unwrap();
      assert!(verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    {
      let pk =
        vec_to_positivepk(&decode_hex("DD308AFEC5777E13121FA72B9CC1B7CC0139715309B086C960E18FD969774EB8").unwrap())
          .unwrap();
      let msg = decode_hex("7E2D58D8B3BCDF1ABADEC7829054F90DDA9805AAB56C77333024B9D0A508B75C").unwrap();
      let sig=decode_hex("5831AAEED7B44BB74E5EAB94BA9D4294C49BCF2A60728D8B4C200F50DD313C1BAB745879A5AD954A72C45A91C3A51D3C7ADEA98D82F8481E0E1E03674A6F3FB7").unwrap();
      assert!(verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    {
      let pk =
        vec_to_positivepk(&decode_hex("25D1DFF95105F5253C4022F628A996AD3A0D95FBF21D468A1B33F8C160D8F517").unwrap())
          .unwrap();
      let msg = decode_hex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF").unwrap();
      let sig=decode_hex("7EB0509757E246F19449885651611CB965ECC1A187DD51B64FDA1EDC9637D5EC97582B9CB13DB3933705B32BA982AF5AF25FD78881EBB32771FC5922EFC66EA3").unwrap();
      assert!(verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    {
      let pk =
        vec_to_positivepk(&decode_hex("D69C3509BB99E412E68B0FE8544E72837DFA30746D8BE2AA65975F29D22DC7B9").unwrap())
          .unwrap();
      let msg = decode_hex("4DF3C3F68FCC83B27E9D42C90431A72499F17875C81A599B566C9889B9696703").unwrap();
      let sig=decode_hex("00000000000000000000003B78CE563F89A0ED9414F5AA28AD0D96D6795F9C6376AFB1548AF603B3EB45C9F8207DEE1060CB71C04E80F593060B07D28308D7F4").unwrap();
      assert!(verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    {
      let pk =
        vec_to_positivepk(&decode_hex("EEFDEA4CDB677750A420FEE807EACF21EB9898AE79B9768766E4FAA04A2D4A34").unwrap());
      assert!(pk == None);
    }
    {
      let pk =
        vec_to_positivepk(&decode_hex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC30").unwrap());
      assert!(pk == None);
    }

    let pk =
      vec_to_positivepk(&decode_hex("DFF1D77F2A671C5F36183726DB2341BE58FEAE1DA2DECED843240F7B502BA659").unwrap())
        .unwrap();
    let msg = decode_hex("243F6A8885A308D313198A2E03707344A4093822299F31D0082EFA98EC4E6C89").unwrap();
    {
      let sig=decode_hex("FFF97BD5755EEEA420453A14355235D382F6472F8568A18B2F057A14602975563CC27944640AC607CD107AE10923D9EF7A73C643E166BE5EBEAFA34B1AC553E2").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    // #6
    {
      let sig=decode_hex("1FA62E331EDBC21C394792D2AB1100A7B432B013DF3F6FF4F99FCB33E0E1515F28890B3EDB6E7189B630448B515CE4F8622A954CFE545735AAEA5134FCCDB2BD").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    // #7
    {
      let sig=decode_hex("6CFF5C3BA86C69EA4B7376F31A9BCB4F74C1976089B2D9963DA2E5543E177769961764B3AA9B2FFCB6EF947B6887A226E8D7C93E00C5ED0C1834FF0D0C2E6DA6").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    // #8
    {
      let sig=decode_hex("1FA62E331EDBC21C394792D2AB1100A7B432B013DF3F6FF4F99FCB33E0E1515F28890B3EDB6E7189B630448B515CE4F8622A954CFE545735AAEA5134FCCDB2BD").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    // #9
    {
      let sig=decode_hex("0000000000000000000000000000000000000000000000000000000000000000123DDA8328AF9C23A94C1FEECFD123BA4FB73476F0D594DCB65C6425BD186051").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }

    // #10
    {
      let sig=decode_hex("00000000000000000000000000000000000000000000000000000000000000017615FBAF5AE28864013C099742DEADB4DBA87F11AC6754F93780D5A1837CF197").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    // #11
    {
      let sig=decode_hex("4A298DACAE57395A15D0795DDBFD1DCB564DA82B0F269BC70A74F8220429BA1D69E89B4C5564D00349106B8497785DD7D1D713A8AE82B32FA79D5F7FC407D39B").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }

    // #12
    {
      let sig=decode_hex("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFFFFFC2F69E89B4C5564D00349106B8497785DD7D1D713A8AE82B32FA79D5F7FC407D39B").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
    // #13
    {
      let sig=decode_hex("6CFF5C3BA86C69EA4B7376F31A9BCB4F74C1976089B2D9963DA2E5543E177769FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEBAAEDCE6AF48A03BBFD25E8CD0364141").unwrap();
      assert!(!verify_bip340(&msg, &pk, &sig, &pclient_params.hasher));
    }
  }
}
