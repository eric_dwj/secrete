/// Full sign protocol, combination of Presign and Sign for EcDSA
use crate::common::*;
use crate::common_defs::*;
use crate::protocols::core::*;
use crate::protocols::keygen::*;
use crate::protocols::presign::*;
use crate::protocols::sign::*;
use crate::{await_child, declare_protocol_flow};

pub struct FullSignClient {}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct FullSignParameters
{
  #[serde(rename = "key")]
  pub keygen: KeyGenOutput,
  #[serde(rename = "derivationPath")]
  pub path: String,
  #[serde(serialize_with = "int_as_base16h", deserialize_with = "int_from_base16h")]
  pub message: BigUint,
}
impl ParValidation for FullSignParameters
{
  fn validate(&self) -> bool
  {
    self.keygen.priv_key_share != *UINT_ZERO
  }
}

#[derive(Serialize, Deserialize, Clone, Default)]
pub struct FullSignState
{
  pub internal: ProtocolState,
  pub __cchild: Option<Box<RawValue>>,
  pub presign: PresignOutput,
  pub sign: SignOutput,
}

impl ProtocolSavedState for FullSignState
{
  fn get_internal_state<'a>(&'a mut self) -> &'a mut ProtocolState
  {
    &mut self.internal
  }
  fn get_child_value<'a>(&'a mut self) -> &'a mut Option<Box<RawValue>>
  {
    &mut self.__cchild
  }
  fn new() -> Self
  {
    Default::default()
  }
}

declare_protocol_flow! {
    FullSignClient, FullSignParameters,FullSignState,SignOutput,;
    step |pars,state,in_msg,out_msg|{
        state.presign=await_child!(state,in_msg,out_msg,PresignClient,PresignParameters{keygen:pars.keygen.clone(),path:pars.path.clone()});
        ExecutionControl::ChildStart
    };
    step |pars,state,in_msg,out_msg|{
        state.sign=await_child!(state,in_msg,out_msg,SignClient, SignParameters{message:pars.message.clone(),path:pars.path.clone(),presign:state.presign.clone()});
        ExecutionControl::None
    };
    output |_pars,state,_in_msg,_out_msg|
    {
     state.sign.clone()
    }
}
#[cfg(feature = "server")]
pub struct FullSignServer {}
#[cfg(feature = "server")]
declare_protocol_flow! {
    FullSignServer, FullSignParameters,FullSignState,SignOutput,;
    step |pars,state,in_msg,out_msg|{
        state.presign=await_child!(state,in_msg,out_msg,PresignServer,PresignParameters{keygen:pars.keygen.clone(),path:pars.path.clone()});
        ExecutionControl::ChildStart
    };
    step |pars,state,in_msg,out_msg|{
        state.sign=await_child!(state,in_msg,out_msg,SignServer, SignParameters{message:pars.message.clone(),path:pars.path.clone(),presign:state.presign.clone()});
        ExecutionControl::None
    };
    output |_pars,state,_in_msg,_out_msg|
    {
     state.sign.clone()
    }
}

#[cfg(test)]
mod tests
{
  use crate::protocols::core::tests::run_protocol;
  use crate::protocols::full_sign::*;
  use crate::protocols::sign::tests::verify_prehashed;
  use crate::xpub::*;
  //use crate::statics::*;
  use rsa::*;

  #[test]
  fn check_fullsign_flow()
  {
    let mut rng = rand::rngs::OsRng;
    let bits = 2048;
    let private_key = RSAPrivateKey::new(&mut rng, bits).expect("failed to generate a key");
    let public_key = RSAPublicKey::from(&private_key);

    let client_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let server_params = KeyGenParameters {
      backup_pub_key: WrappedPubkey {
        pem: public_key.clone(),
      },
    };
    let (out_client, out_server) = run_protocol::<KeyGenClient, KeyGenServer>(&client_params, &server_params);
    assert_eq!(out_client.chaincode, out_server.chaincode);
    assert_eq!(out_client.pubkey, out_server.pubkey);
    let loc_prv = Xprv::new(
      &out_client.chaincode,
      &(&out_client.priv_key_share + &out_server.priv_key_share),
      false,
    );
    let message: Vec<u8> = b"ECDSA proves knowledge of a secret number in the context of a single message"
      .into_iter()
      .cloned()
      .collect();
    let mut hasher = sha2::Sha256::new();
    hasher.update(&message);
    let res = hasher.finalize();

    let pclient_params = FullSignParameters {
      path: "m/2".to_string(),
      keygen: out_client.clone(),
      message: BigUint::from_bytes_be(&res),
    };
    let pserver_params = FullSignParameters {
      path: "m/2".to_string(),
      keygen: out_server.clone(),
      message: BigUint::from_bytes_be(&res),
    };
    let (pout_client, pout_server) = run_protocol::<FullSignClient, FullSignServer>(&pclient_params, &pserver_params);

    assert_eq!(pout_client.s, pout_server.s);
    assert_eq!(pout_client.r, pout_server.r);
    let xpub = private_to_public(&derive_index(&loc_prv, 2));
    assert!(verify_prehashed(
      &xpub.key,
      &to_scalar(&BigUint::from_bytes_be(&res)),
      &to_scalar(&pout_client.r),
      &to_scalar(&pout_server.s)
    )
    .is_ok());
  }
}
