#![no_std]
extern crate alloc;
use num_bigint_dig::{BigUint, ToBigUint};

#[macro_use]
extern crate lazy_static;
//use serde_json::Result;

mod algorithms;
mod common;
mod common_defs;
mod proofs;
pub mod protocols;
mod statics;
pub mod xpub;
use crate::protocols::core::AbstractHierarchyProtocolSide;
use alloc::string::String;
use alloc::vec;
#[cfg(not(target_arch = "wasm32"))]
use alloc::vec::Vec;

#[cfg(not(target_arch = "wasm32"))]
use cstr_core::CString;

pub fn bytes_to_biguint(bytes: &[u8; 32]) -> BigUint
{
  bytes
    .iter()
    .enumerate()
    .map(|(i, w)| w.to_biguint().unwrap() << ((31 - i) * 8))
    .sum()
}

#[cfg(target_arch = "wasm32")]
use wasm_bindgen::prelude::*;

#[cfg(not(target_arch = "wasm32"))]
macro_rules! get_napi_string {
  ($env:ident,$buf:expr) => {{
    let mut len = 0;
    napi_get_value_string_utf8($env, $buf, core::ptr::null_mut(), 0, &mut len);
    let size = len as usize;
    let mut ve: Vec<u8> = Vec::with_capacity(size + 1);
    let raw = ve.as_mut_ptr();
    core::mem::forget(ve);
    let mut cap = 0;
    let _s = napi_get_value_string_utf8($env, $buf, raw as *mut i8, size + 1, &mut cap);
    let s = String::from_raw_parts(raw, cap as usize, size);
    s
  }};
}
macro_rules! decl_napi_fn {
  ($name:ident,$class:path, $($e:expr),*) => {
    #[allow(non_snake_case)]
    #[cfg(not(target_arch = "wasm32"))]
    pub unsafe extern "C" fn $name(env: napi_env, info: napi_callback_info) -> napi_value
    {
      //parameters: &str,store: &str)->String
      let mut buffer: [napi_value; 2] = core::mem::MaybeUninit::zeroed().assume_init();
      let mut argc = 2 as usize;
      napi_get_cb_info(
        env,
        info,
        &mut argc,
        buffer.as_mut_ptr(),
        core::ptr::null_mut(),
        core::ptr::null_mut(),
      );
      let parameters = get_napi_string!(env, buffer[0]);
      let store = get_napi_string!(env, buffer[1]);
      use $class as base;
      let ret = base::process_packet(
        &parameters,
        &store,
        &vec![$($e),*],
      );
      let mut local: napi_value = core::mem::zeroed();
      let stlen = ret.len();
      let p = CString::new(ret).expect("CString::new    failed");
      napi_create_string_utf8(env, p.as_ptr(), stlen, &mut local);
      local
    }

  }
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn generateKeyClient(parameters: &str, store: &str) -> String
{
  crate::protocols::keygen::KeyGenClient::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "xpub", "tpub"],
  )
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn generateSchnorrKeyClient(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorr_keygen::SchnorrKeyGenClient::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "xpub", "tpub"],
  )
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn generateSchnorrkelKeyClient(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorrkel_keygen::SchnorrkelKeyGenClient::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "pk"],
  )
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn generateSchnorrkelKeyServer(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorrkel_keygen::SchnorrkelKeyGenServer::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "pk"],
  )
}

decl_napi_fn!(
  generateKeyClient,
  crate::protocols::keygen::KeyGenClient,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "xpub",
  "tpub"
);

decl_napi_fn!(
  generateSchnorrKeyClient,
  crate::protocols::schnorr_keygen::SchnorrKeyGenClient,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "xpub",
  "tpub"
);

decl_napi_fn!(
  generateSchnorrkelKeyClient,
  crate::protocols::schnorrkel_keygen::SchnorrkelKeyGenClient,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "pk"
);

#[cfg(feature = "server")]
decl_napi_fn!(
  generateSchnorrkelKeyServer,
  crate::protocols::schnorrkel_keygen::SchnorrkelKeyGenServer,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "pk"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn generateKeyServer(parameters: &str, store: &str) -> String
{
  crate::protocols::keygen::KeyGenServer::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "xpub", "tpub"],
  )
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn generateSchnorrKeyServer(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorr_keygen::SchnorrKeyGenServer::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "xpub", "tpub"],
  )
}

#[cfg(feature = "server")]
decl_napi_fn!(
  generateKeyServer,
  crate::protocols::keygen::KeyGenServer,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "xpub",
  "tpub"
);

#[cfg(feature = "server")]
decl_napi_fn!(
  generateSchnorrKeyServer,
  crate::protocols::schnorr_keygen::SchnorrKeyGenServer,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "xpub",
  "tpub"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn signSchnorrClient(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorr_sign::SchnorrSignClient::process_packet(
    parameters,
    store,
    &vec!["sigma", "v", "sig", "pubkey"],
  )
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn signSchnorrServer(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorr_sign::SchnorrSignServer::process_packet(
    parameters,
    store,
    &vec!["sigma", "v", "sig", "pubkey"],
  )
}
decl_napi_fn!(
  signSchnorrClient,
  crate::protocols::schnorr_sign::SchnorrSignClient,
  "sigma",
  "v",
  "sig",
  "pubkey"
);

#[cfg(feature = "server")]
decl_napi_fn!(
  signSchnorrServer,
  crate::protocols::schnorr_sign::SchnorrSignServer,
  "sigma",
  "v",
  "sig",
  "pubkey"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn preSignClient(parameters: &str, store: &str) -> String
{
  crate::protocols::presign::PresignClient::process_packet(
    parameters,
    store,
    &vec!["derPath", "pubkey", "recoveryParam", "r"],
  )
}
decl_napi_fn!(
  preSignClient,
  crate::protocols::presign::PresignClient,
  "derPath",
  "pubkey",
  "recoveryParam",
  "r"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn preSignServer(parameters: &str, store: &str) -> String
{
  crate::protocols::presign::PresignServer::process_packet(
    parameters,
    store,
    &vec!["derPath", "pubkey", "recoveryParam", "r"],
  )
}
#[cfg(feature = "server")]
decl_napi_fn!(
  preSignServer,
  crate::protocols::presign::PresignServer,
  "derPath",
  "pubkey",
  "recoveryParam",
  "r"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn signClient(parameters: &str, store: &str) -> String
{
  crate::protocols::sign::SignClient::process_packet(parameters, store, &vec!["recoveryParam", "r", "s"])
}

decl_napi_fn!(
  signClient,
  crate::protocols::sign::SignClient,
  "recoveryParam",
  "r",
  "s"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn signServer(parameters: &str, store: &str) -> String
{
  crate::protocols::sign::SignServer::process_packet(parameters, store, &vec!["recoveryParam", "r", "s"])
}

#[cfg(feature = "server")]
decl_napi_fn!(
  signServer,
  crate::protocols::sign::SignServer,
  "recoveryParam",
  "r",
  "s"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn signEd25519Client(parameters: &str, store: &str) -> String
{
  crate::protocols::eddsa25519_sign::Ed255SignClient::process_packet(parameters, store, &vec!["signature"])
}
decl_napi_fn!(
  signEd25519Client,
  crate::protocols::eddsa25519_sign::Ed255SignClient,
  "signature"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn signSchnorrkelClient(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorrkel_sign::SchnorrkelSignClient::process_packet(parameters, store, &vec!["sig"])
}
decl_napi_fn!(
  signSchnorrkelClient,
  crate::protocols::schnorrkel_sign::SchnorrkelSignClient,
  "sig"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn signSchnorrkelServer(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorrkel_sign::SchnorrkelSignServer::process_packet(parameters, store, &vec!["sig"])
}
#[cfg(feature = "server")]
decl_napi_fn!(
  signSchnorrkelServer,
  crate::protocols::schnorrkel_sign::SchnorrkelSignServer,
  "sig"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn signEd25519Server(parameters: &str, store: &str) -> String
{
  crate::protocols::eddsa25519_sign::Ed255SignServer::process_packet(parameters, store, &vec!["signature"])
}
#[cfg(feature = "server")]
decl_napi_fn!(
  signEd25519Server,
  crate::protocols::eddsa25519_sign::Ed255SignServer,
  "signature"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn generateEd25519KeyClient(parameters: &str, store: &str) -> String
{
  crate::protocols::eddsa25519_keygen::Ed255KeygenClient::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "publicKey"],
  )
}

decl_napi_fn!(
  generateEd25519KeyClient,
  crate::protocols::eddsa25519_keygen::Ed255KeygenClient,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "publicKey"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn generateEd25519KeyServer(parameters: &str, store: &str) -> String
{
  crate::protocols::eddsa25519_keygen::Ed255KeygenServer::process_packet(
    parameters,
    store,
    &vec!["encryptedOurBackup", "encryptedTheirBackup", "publicKey"],
  )
}

#[cfg(feature = "server")]
decl_napi_fn!(
  generateEd25519KeyServer,
  crate::protocols::eddsa25519_keygen::Ed255KeygenServer,
  "encryptedOurBackup",
  "encryptedTheirBackup",
  "publicKey"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn fullSignClient(parameters: &str, store: &str) -> String
{
  crate::protocols::full_sign::FullSignClient::process_packet(parameters, store, &vec!["recoveryParam", "r", "s"])
}
decl_napi_fn!(
  fullSignClient,
  crate::protocols::full_sign::FullSignClient,
  "recoveryParam",
  "r",
  "s"
);

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn fullSignServer(parameters: &str, store: &str) -> String
{
  crate::protocols::full_sign::FullSignServer::process_packet(parameters, store, &vec!["recoveryParam", "r", "s"])
}
#[cfg(feature = "server")]
decl_napi_fn!(
  fullSignServer,
  crate::protocols::full_sign::FullSignServer,
  "recoveryParam",
  "r",
  "s"
);

#[macro_export]
macro_rules! chk {
  ($e:expr) => {{
    match $e
    {
      Ok(r) => r,
      Err(_e) => return Err(miniserde::Error {}),
    }
  }};
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn getDerivedPublic(xpub: &str, path: &str) -> String
{
  crate::xpub::get_derived_public_internal(xpub, path)
}

#[allow(non_snake_case)]
#[cfg(not(target_arch = "wasm32"))]
pub unsafe extern "C" fn getDerivedPublic(env: napi_env, info: napi_callback_info) -> napi_value
{
  //parameters: &str,store: &str)->String
  let mut buffer: [napi_value; 2] = core::mem::MaybeUninit::zeroed().assume_init();
  let mut argc = 2 as usize;
  napi_get_cb_info(
    env,
    info,
    &mut argc,
    buffer.as_mut_ptr(),
    core::ptr::null_mut(),
    core::ptr::null_mut(),
  );
  let xpub = get_napi_string!(env, buffer[0]);
  let path = get_napi_string!(env, buffer[1]);
  let ret = crate::xpub::get_derived_public_internal(&xpub, &path);
  let mut local: napi_value = core::mem::zeroed();
  let stlen = ret.len();
  let p = CString::new(ret).expect("CString::new    failed");
  napi_create_string_utf8(env, p.as_ptr(), stlen, &mut local);
  local
}

#[cfg(not(target_arch = "wasm32"))]
use nodejs_sys::{
  napi_callback_info, napi_create_function, napi_create_string_utf8, napi_env, napi_get_cb_info,
  napi_get_value_string_utf8, napi_set_named_property, napi_value,
};

#[cfg(not(target_arch = "wasm32"))]
macro_rules! export_napi {
  ($name:ident,$env:ident,$exports:ident) => {
    // creating a C String
    let st: &str = stringify!($name);
    let ln = st.len();
    let p = CString::new(st).expect("CString::new failed");
    // creating a location where pointer to napi_value be written
    let mut local: napi_value = core::mem::zeroed();
    napi_create_function(
      $env,
      //
      // pointer to function name
      p.as_ptr(),
      // length of function name
      ln,
      // rust function
      Some($name),
      // context which can be accessed by the rust function
      core::ptr::null_mut(),
      // output napi_value
      &mut local,
    );
    // set function as property
    napi_set_named_property($env, $exports, p.as_ptr(), local);
  };
}

#[no_mangle]
#[cfg(not(target_arch = "wasm32"))]
pub unsafe extern "C" fn napi_register_module_v1(env: napi_env, exports: napi_value) -> nodejs_sys::napi_value
{
  export_napi!(generateKeyClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(generateKeyServer, env, exports);

  export_napi!(preSignClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(preSignServer, env, exports);

  export_napi!(signClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(signServer, env, exports);

  export_napi!(signEd25519Client, env, exports);
  #[cfg(feature = "server")]
  export_napi!(signEd25519Server, env, exports);

  export_napi!(fullSignClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(fullSignServer, env, exports);

  export_napi!(generateEd25519KeyClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(generateEd25519KeyServer, env, exports);
  export_napi!(getDerivedPublic, env, exports);

  export_napi!(generateSchnorrKeyClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(generateSchnorrKeyServer, env, exports);

  export_napi!(signSchnorrClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(signSchnorrServer, env, exports);

  export_napi!(generateSchnorrkelKeyClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(generateSchnorrkelKeyServer, env, exports);

  export_napi!(signSchnorrkelClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(signSchnorrkelServer, env, exports);

  // returning exports
  exports
}
