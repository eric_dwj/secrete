use std::env::{self};

macro_rules! get(($name:expr) => (env::var($name).unwrap()));

pub fn main()
{
  let target = get!("TARGET");
  if target.contains("windows")
  {
    println!("cargo:rustc-link-search={}", get!("CARGO_MANIFEST_DIR"));
    println!("cargo:rustc-cdylib-link-arg=/DELAYLOAD:node.exe");
    println!("cargo:rustc-cdylib-link-arg=Delayimp.lib");
    println!("cargo:rustc-cdylib-link-arg=node.lib");
  }
}
