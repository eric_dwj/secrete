+## Cryptolib rewritten in Rust

This library is implementation of [One Round Threshold ECDSA with Identifiable Abort](https://eprint.iacr.org/2020/540.pdf) for two parties, or as close as I could manage.

### Prerequisites

- installed node and working npm, at least 12+
- installed rust, about 1.40+
- to build NAPI, libclang and clang headers are necessary
- to build wasm-pack, C compiler and linker are necessary (GCC toolchain, usually)
- wasm-pack should be installed (cargo install wasm-pack)
- create Release folder, if it is missing
- Run "npm install" after all of the above


### Building

Due to firewall,  `npm config set strict-ssl false` , `npm config set unsafe-perm true` and similar ones for Rust may be necessary (like `$env:CARGO_HTTP_CHECK_REVOKE='false'` for PowerShell). 

If prerequisites are installed, `npm run build` should create a *pkg* folder with wasm and wasm js wrapper for futher use in SDK, as well as fill *public* folder with webpack output. 

`npm run build-native` should build Cryptolib.node and copy it into Release folder for further use.

`npm run build-native-windows`  and `npm run build-native-windows` may work (on windows), but prerequisites are trickier. *node.lib* is a must-have. 

### Checks

- `cargo test --release` for basic build checks. Without *release* will take a lot longer
- `npm run start-test-p2` will start local server, usually on port 8080
    - `npm run test` on the same machine to check if native library works
    - go to "http://ip_of_server:8080" in your browser to check WASM
    - go to "http://ip_of_server:8080/gen_key.html" in your browser to run several iterations of key generation for average duration estimate

### Branches

Choose one according to taste

- *old_master* branch for `std` build
    - Uses serde
    - does not require hacked-up libraries
    - WASM about 1.5Mb
    - not updated for Schnorr, etc
- *master* branch is (now) for mostly `no_std` build (`rand` still uses std)
    - uses hacked-up miniserde
    - several dependencies modified and locally cloned to avoid regex in code
    - is weird
    - WASM about 700kb


### Formatting

For those who care: please use `cargo +nightly fmt`. Stable rustfmt *does not* work, for unknown reasons.

### Expansion

To add new protocols to the library, follows the existing as examples to implement the alogorithm itself as a series of steps for each sides, 
taking care to ensure sides mesh. Then, expose the new files in Protocols module.

After that, declare the new protocols in lib.rs, for wasm side and native side. Wasm side requires a single definition per function, for example:

```

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn signSchnorrkelClient(parameters: &str, store: &str) -> String
{
  crate::protocols::schnorrkel_sign::SchnorrkelSignClient::process_packet(parameters, store, &vec!["sig"])
}

```

All protocols using declare_protocol_flow macro implement process_packet that take parameters (json), store (json) and vector of exports.

Native side requires function declaration using decl_napi_fn! and addition of the function to exports in napi_register_module_v1 using export_napi! macro, for example:

```
decl_napi_fn!(
  signSchnorrkelClient,
  crate::protocols::schnorrkel_sign::SchnorrkelSignClient,
  "sig"
);
```

A general flow for adding a new protocol is then as follows:

- Add a new protocol file, defining necessary structs (see protocols/core.rs) and flows for client and server sides
- Add new file to protocols/mod.rs and check if it compiles
- Expose new functions to javascript in lib.rs, taking care to match native and Wasm names and to add export_napi! entries, for example:
```
#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
pub fn signNewProtoClient(parameters: &str, store: &str) -> String
{
  crate::protocols::newproto_sign::NewProtoSignClient::process_packet(parameters, store, &vec!["sig"])
}

#[cfg(target_arch = "wasm32")]
#[allow(non_snake_case)]
#[wasm_bindgen]
#[cfg(feature = "server")]
pub fn signNewProtoServer(parameters: &str, store: &str) -> String
{
  crate::protocols::newproto_sign::NewProtoSignServer::process_packet(parameters, store, &vec!["sig"])
}

decl_napi_fn!(
  signNewProtoClient,
  crate::protocols::newproto_sign::NewProtoSignClient,
  "sig"
);

#[cfg(feature = "server")]
decl_napi_fn!(
  signNewProtoServer,
  crate::protocols::newproto_sign::NewProtoSignServer,
  "sig"
);

#[no_mangle]
#[cfg(not(target_arch = "wasm32"))]
pub unsafe extern "C" fn napi_register_module_v1(env: napi_env, exports: napi_value) -> nodejs_sys::napi_value
{
  /// other protocols here...
  /// ...
  /// ...


  export_napi!(signNewProtoClient, env, exports);
  #[cfg(feature = "server")]
  export_napi!(signNewProtoServer, env, exports);


  exports
}

```
- Add new functions to CryptolibNative.js (here and in SDK!): add the names in `const funcNames = [...`
- Add new functions to js/CryptolibClient.js, for example
```
 async signNewProtoClient (key_id, sid, passphrase,context, message,  callCounterparty) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'newProto'); // get key
    if (!key_json) {
      return { error: 'This key does not exist' };
    }
    CryptolibBase.decrypt(key_json, passphrase);
    const params = { message: Buffer.from(message).toString('base64'), keygen: key_json, context: Buffer.from(context).toString('base64') }; //replace with actual parameters
    return this._process(key_id, 'sign', sid, 'newProto_sign', passphrase,
      async (state) => CryptolibNative.signNewProtoClient(JSON.stringify(params), JSON.stringify(state)),
      async (msg) => callCounterparty('signNewProtoServer', { key_id, sid, context,message, _message: msg }));
  }
```
- Add new function to  js/CryptolibServer.js, for example:
```
    async signNewProtoServer ({ key_id, sid, _message,context, message, passphrase }) {
      const key_json = await this.keyStore.getKey(key_id, 'keygen', 'newProto'); // get key
      if (!key_json) {
        return { error: 'This key and sid do not exist' };
      }
      if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
      const params = {
        message: Buffer.from(message).toString('base64'), keygen: key_json,
        context: Buffer.from(context).toString('base64')
      }; // replace with actual params
      return this._process(key_id, 'sign', sid, 'newProto_sign', passphrase, CryptolibNative.signNewProtoServer, params, _message);
    }
```
- expose new call in backend API (yaml? Not sure) 
- copy modified files to SDK
- update SDK with new build
- Should work... 
