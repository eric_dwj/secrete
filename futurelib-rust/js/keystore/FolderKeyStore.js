const CryptoJS = require('crypto-js');
const merge = require('lodash/merge');
const path = require('path');
const fs = require('fs');

module.exports = class FolderKeyStore {
  constructor ({ folder_path = 'key_store', salt }) {
    this.store = {};
    this.salt = salt;
    this.store_folder = folder_path;
  }

  async load (id) {
    const p = path.join(this.store_folder, id);
    if (!fs.existsSync(p)) {
      return {};
    }
    const doc = await fs.promises.readFile(p);
    if (this.salt) return this.decrypt(doc);
    return JSON.parse(doc);
  }

  async save (id) {
    if (!this._made_dir) {
      await fs.promises.mkdir(this.store_folder, { recursive: true, mode: 0o700 });
      this._made_dir = true;
    }
    const p = path.join(this.store_folder, id);
    let doc = JSON.stringify(this.store[id]);
    if (this.salt) doc = this.encrypt(doc);
    return fs.promises.writeFile(p, doc, { encoding: 'utf-8' });
  }

  encrypt (data) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), this.salt).toString();
  }

  decrypt (data) {
    return JSON.parse(CryptoJS.AES.decrypt(data, this.salt).toString(CryptoJS.enc.Utf8));
  }

  async ensure (id, category, sid) {
    if (!(id in this.store)) this.store[id] = await this.load(id);
    if (!(category in this.store[id])) this.store[id][category] = {};
    if (!(sid in this.store[id][category])) this.store[id][category][sid] = {};
  }

  async getKey (id, category, sid) {
    await this.ensure(id, category, sid);

    return this.store[id][category][sid];
  }

  async mergeToKey (id, category, sid, value) {
    await this.ensure(id, category, sid);
    merge(this.store[id][category][sid], value);
    return this.save(id);
  }

  async assignToKey (id, category, sid, value) {
    await this.ensure(id, category, sid);
    Object.assign(this.store[id][category][sid], value);
    return this.save(id);
  }

  async setKeyValue (id, category, sid, value) {
    await this.ensure(id, category, sid);
    this.store[id][category][sid] = value;
    return this.save(id);
  }

  async deleteKey (id, category, sid) {
    if (category && sid) {
      if (!(id in this.store)) await this.load(id);
      delete this.store[id][category][sid];
      return this.save(id);
    }
    delete this.store[id];
    const p = path.join(this.store_folder, id);
    try {
      await fs.promises.unlink(p);
    } catch (e) {
      // ignore error
    }
    return true;
  }
};
