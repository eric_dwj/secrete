/* eslint-disable class-methods-use-this */
const _ = require('lodash');
const Redis = require('ioredis');
const CryptoJS = require('crypto-js');
// const log = require('../utils/log4js').getLogger('[redis]');

function key_name (key_id, category = '0', sid = '0') {
  return `cryptolib2_${key_id}_${category}_${sid}`;
}

module.exports = class RedisStore {
  constructor ({ redis_con, remove_interval = 86400, salt }) {
    this.salt = salt;
    this.redis = new Redis(redis_con);
    this.remove_interval = remove_interval; // sec
  }

  encrypt (data) {
    return CryptoJS.AES.encrypt(JSON.stringify(data), this.salt).toString();
  }

  decrypt (data) {
    return JSON.parse(CryptoJS.AES.decrypt(data, this.salt).toString(CryptoJS.enc.Utf8));
  }

  async getKey (key_id, category, sid) {
    const key = key_name(key_id, category, sid);
    const sdoc = await this.redis.get(key);
    if (sdoc) return this.decrypt(sdoc);
    return {};
  }

  async mergeToKey (key_id, category, sid, value) {
    const old = await this.getKey(key_id, category, sid);
    _.merge(old, value);
    return this.setKeyValue(key_id, category, sid, old);
  }

  async assignToKey (key_id, category, sid, value) {
    const old = await this.getKey(key_id, category, sid);
    Object.assign(old, value);
    return this.setKeyValue(key_id, category, sid, old);
  }

  async setKeyValue (key_id, category, sid, value) {
    const key = key_name(key_id, category, sid);
    return this.redis.set(key, this.encrypt(value), 'EX', this.remove_interval);
  }

  // eslint-disable-next-line no-unused-vars
  async deleteKey (key_id, category, sid) {
    // not supporting delete
    return false;
  }
};
