/* eslint-disable arrow-body-style */
const bodyParser = require('body-parser');
const express = require('express');
const helmet = require('helmet');
const cors = require('cors');
const path = require('path');
const compression = require('compression');
const config = require('./config');
const CryptoLib = require('./CryptolibServer');

const cryptolib_obj = new CryptoLib();

function compact (str) {
  if (str && str.length > 1000) {
    return `${str.substring(0, 950)}...${str.substring(str.length - 947)}`;
  }
  return str;
}

express.static.mime.types.wasm = 'application/wasm';

const app = express();
app.use(helmet());
if (config.corsOption) app.use(cors(config.corsOption));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false }));
app.use(compression());
app.use(express.static(path.join(__dirname, '..', 'public')));

// for auto test ping server
app.get('/', (req, res) => {
  res.status(200).end();
});

app.post('/', async (req, res, next) => {
  const { id, method, params } = req.body;
  // console.log(compact(`${JSON.stringify({ method, params })}`));
  console.log(compact(`${JSON.stringify({ method })}`));
  if (!(typeof cryptolib_obj[method] === 'function')) {
    console.error(`[${method}][400] method not found`);
    return res.status(400).json({ error: 'method not found' });
  }
  try {
    const ret = await cryptolib_obj[method](...params);
    console.log(ret);
    ret.id = id;
    ret.jsonrpc = '2.0';
    console.log(compact(`[${method}][200]`));
    // console.log(compact(`[${method}][200] ${JSON.stringify(ret)}`));
    return res.status(200).send(ret);
  } catch (error) {
    console.error(`[${method}][400]`, error);
    return res.status(400).json({
      jsonrpc: '2.0',
      id,
      error: error.message });
  }
});

app.listen(config.server.port, () => {
  console.log(`Cryptolib party ${config.party} listening on port ${config.server.port}`);
});

module.exports = app;
