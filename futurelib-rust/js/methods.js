const methods = [
  'p1KeyGenStep1',
  'p2KeyGenStep1',
  'p1KeyGenStep2',
  'p2KeyGenStep2',
  'p1KeyGenStep3',
  'p2KeyGenStep3',
  'p1KeyGenStep4',
  'p2KeyGenStep4',
  'p1KeyGenStep5',
  'p2KeyGenStep5',
  'p1KeyGenStep6',
  'p2KeyGenStep6',
  'p2KeyGenStep7',
  'p1SigStep1',
  'p1SigStep2',
  'p1SigStep3',
  'p2SigStep1',
  'p2SigStep2'
];

module.exports = methods;
