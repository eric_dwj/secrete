/* eslint-disable import/no-dynamic-require */
/* eslint-disable global-require */
const config = require('./config/default');

if (process.env.APP_CONFIG) {
  Object.assign(config, require(process.env.APP_CONFIG));
}

module.exports = config;
