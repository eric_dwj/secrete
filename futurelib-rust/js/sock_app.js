#!/usr/bin/env node

const http = require('http');
const SocketIo = require('socket.io');
const express = require('express');
const path = require('path');
const { version } = require('../package.json');
const log = require('./utils/log4js').getLogger('sock_app');
const sock_route = require('./sock/route');

const port = 3001;

const app = express();
app.use(express.static(path.join(__dirname, '..', 'public/sock')));

const server = http.createServer(app);
const io = new SocketIo(server);
sock_route.route(io);

const onError = (err) => {
  log.error(err);
  if (err.syscall !== 'listen') {
    process.exit(-1);
  }
  const bind = `Port ${port}`;
  switch (err.code) {
    case 'EACCES':
      log.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      log.error(`${bind} is already in use`);
      process.exit(1);
      break;
    default:
      process.exit(-1);
  }
};

server.listen(port, () => {
  log.info('Cryptolib', version, 'listening on port', server.address().port, 'with pid', process.pid);
});
server.on('error', onError);
