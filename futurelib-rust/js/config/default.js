/* eslint-disable max-len */

module.exports = {
  keyStore: {
    scheme: 'memory'
  },
  backupPublicKey: {
    sbi_japannext: {
      pem: '-----BEGIN PUBLIC KEY-----\nMIIBoDANBgkqhkiG9w0BAQEFAAOCAY0AMIIBiAKCAYEAnLMQhN60K+m++j6ZzQ8I\nnIHOXJ3aVkk3Es3rNzDRQSwHr1+Rp/4/hqvhu9NzzQm9xxUZ0Lr1Po2/q+nX7784\n+D2ca+Ob+DBRPQX1SjPi3usv4txyl5CPTI/Fv5cr1WeiTc2CgkdRLeOlW7r0xHiC\nrtvGzViTb32NAdVviBwHrTKLUC7mGtXNaNoZuxvHe+GaB8C7fiK9ZT/zlmV213FF\nYiYEcVgYFv5VdkCV7rHn1FuuskdM0mR1uo2HHhWC7SZ/czMA+unVrsLK3C2caJ0P\n0mZzxvNuu7E1woXsHVbX5O/u2f28wnlBKO4Hy9SOOT0BGX8Vp7EuxbKA64bqIGIJ\n6wyvHwbjm2kwpF3AcY/pVGQh5m69nnpoyx+nbDlcO878TE4Q0jYn9112JbjQ+8Gh\nxg20paiUqoX/OUG0eFrWf3xs9581AsbHDSISFd90qBKZU1AsOeaPZH2wb/LiZigE\nrrG+NHqIsgPOfPbgQf+i156sYotikRuh7N8JdcATinX5AgER\n-----END PUBLIC KEY-----'
      // pem: '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAwRckB6TaDZmYLmcToffH\n9ELmn3gnCL3X7NyWuOiQOoM/RcPgurLaaIn4MoXVtkXz086LWE8OaTRvtG2yDQMp\ncBM1xEngYuiOfuzhEoNqeLZMAOsmcltT4vN1GWeJdJ42z7aiJ7xcqXffYiJjnyH7\ngmqIxf1EXJHPpxBdOt27MBv0bvI7diInoeSQ4R3vz8v8hQsW0m3/JbIqiNujGfis\n8wQfosFFZ1V67wckJYLfNQu04MhCnvg/sYzv5WKDT9JrxAXc7R9fknla1+f5IwuR\ny0ySlF9X9N5YgFHY9FSl/WcmFJXiLQfu8HNZh98v2c0KGBRRfYfwJpBIMWdjfOZ8\nfQIDAMjD\n-----END PUBLIC KEY-----'
    },
    secom: {
      pem: '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt62KNCldVEgnxSen+xg8\nH6baBBnHloYwI3UcV/Z8PpTk0s6/ap7nY+aZ392Uw2s8Rrm4oBTKxgR0LeU1wp+t\nbvtfzmnwzP/9NR0VZcXE1qF3R8gvwT1g/YdkegmEG6vUUWid2UzojyTdxg3xXVfl\nATnEk+9KheQmhT6Uwnf0erFFCvTd0dgApjmLasQAJDFR+pGCFrnS6iQllq/rA8Dy\nYYxMfyR5pJlOj1ObYRYcNA7PA7aCotwUfFWLc0KQ+tLk2yMGlCpbAGdaq/27Rtrs\nf73NCpttm2Yai68MED58F3VvGyKJuj5V+t9O9dYwUvJtJaiAty/mbtoWNvvycQTU\nnQIDAQAB\n-----END PUBLIC KEY-----'
    },
    sbi_vc: {
      pem: '-----BEGIN PUBLIC KEY-----\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt62KNCldVEgnxSen+xg8\nH6baBBnHloYwI3UcV/Z8PpTk0s6/ap7nY+aZ392Uw2s8Rrm4oBTKxgR0LeU1wp+t\nbvtfzmnwzP/9NR0VZcXE1qF3R8gvwT1g/YdkegmEG6vUUWid2UzojyTdxg3xXVfl\nATnEk+9KheQmhT6Uwnf0erFFCvTd0dgApjmLasQAJDFR+pGCFrnS6iQllq/rA8Dy\nYYxMfyR5pJlOj1ObYRYcNA7PA7aCotwUfFWLc0KQ+tLk2yMGlCpbAGdaq/27Rtrs\nf73NCpttm2Yai68MED58F3VvGyKJuj5V+t9O9dYwUvJtJaiAty/mbtoWNvvycQTU\nnQIDAQAB\n-----END PUBLIC KEY-----'
    }
  }
};
