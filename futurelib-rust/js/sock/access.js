const CryptoJS = require('crypto-js');
const KeyStore = require('../keystore/KeyStore');
const config = require('../config');
const log = require('../utils/log4js').getLogger('access');

class AccessControl {
  constructor () {
    this.keyStore = KeyStore.create(config.accessKeyStore);
  }

  async setWalletSeed (wallet_id, seed) {
    return this.keyStore.setKeyValue(wallet_id, 'keygen', 'default', seed);
  }

  async setXpubs (wallet_id, xpubs) {
    return this.keyStore.assignToKey(wallet_id, 'keygen', 'xpubs', { xpubs });
  }

  async getXpubs (wallet_id) {
    const { xpubs } = await this.keyStore.getKey(wallet_id, 'keygen', 'xpubs');
    return xpubs;
  }

  async getWalletSeedEncrypted (wallet_id) {
    return this.keyStore.getKey(wallet_id, 'keygen', 'default');
  }

  async changeWalletPassword (wallet_id, password, newPassword) {
    if (!newPassword || newPassword.length === 0) throw Error('password cannot be empty');
    const old = await this.getWalletSeedEncrypted(wallet_id);
    let seed;
    try {
      seed = CryptoJS.AES.decrypt(old, password).toString(CryptoJS.enc.Utf8);
    } catch (err) {
      log.error('changeWalletPassword', err);
      throw Error('wallet passphrase incorrect');
    }
    const encrypt = CryptoJS.AES.encrypt(seed, newPassword).toString();
    return this.setWalletSeed(wallet_id, encrypt);
  }
}

module.exports = AccessControl;
