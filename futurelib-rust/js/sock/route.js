const CryptoLib = require('../CryptolibClient');
const log = require('../utils/log4js').getLogger('sock/route');
const AccessControl = require('./access');
const config = require('../config');

config.keyStore.scheme = 'memory';
const cryptolib_obj = new CryptoLib();
const access_obj = new AccessControl();

function compact (str) {
  if (str && str.length > 1000) {
    return `${str.substring(0, 950)}...${str.substring(str.length - 947)}`;
  }
  return str;
}

function formRpcParam (method, params) {
  return {
    jsonrpc: '2.0',
    id: Date.now(),
    method,
    params
  };
}

class SockClient {
  constructor (socket) {
    this.socket = socket;
  }

  rpc_call (id, cmd, params) {
    return new Promise((resolve, reject) => {
      try {
        this.socket.emit(id, formRpcParam(cmd, params), ({ result, error }) => {
          if (error) reject(Error(`cryptolib error: ${JSON.stringify(error)}`));
          else resolve({ result });
        });
      } catch (e) { reject(e); }
    });
  }

  async generateKeyClient (key_id, passphrase, recovery_service_provider) {
    await cryptolib_obj.generateKeyClient(key_id, passphrase, recovery_service_provider,
      async (cmd, params) => this.rpc_call(this.socket.id, cmd, params));
    const [{ result: seed }, { result: xpub }, { result: tpub }, { result: paperKey }] = await Promise.all([
      cryptolib_obj.getEncryptedSeed(key_id),
      cryptolib_obj.getXpub(key_id, false),
      cryptolib_obj.getXpub(key_id, true),
      cryptolib_obj.getRecoveryKey(key_id),
    ]);
    // log.debug({ xpub, tpub, paperKey });
    await Promise.all([
      access_obj.setWalletSeed(key_id, seed),
      access_obj.setXpubs(key_id, [xpub, tpub]),
      cryptolib_obj.deleteKey(key_id)
    ]);
    return { result: { paperKey } };
  }

  async fullSignClient (key_id, sid, passphrase, derivePath, message) {
    // log.debug('fullSignClient', key_id, sid, derivePath, message);
    const encrypt = await access_obj.getWalletSeedEncrypted(key_id);
    await cryptolib_obj.assignToKey(key_id, 'keygen', 'default', { encrypt });
    const ret = await cryptolib_obj.fullSignClient(key_id, sid, passphrase, derivePath, message,
      async (cmd, params) => this.rpc_call(this.socket.id, cmd, params));
    // log.debug(ret);
    return ret;
  }
}

async function getXpub (key_id, is_testnet = false) {
  const [xpub, tpub] = await access_obj.getXpubs(key_id);
  if (is_testnet) return { result: tpub };
  return { result: xpub };
}

async function changeWalletPassword (wallet_id, password, newPassword) {
  await access_obj.changeWalletPassword(wallet_id, password, newPassword);
  return { result: { status: 'success' } };
}

const static_method_list = {
  getDerivedPublic: cryptolib_obj.getDerivedPublic.bind(cryptolib_obj),
  deleteKey: cryptolib_obj.deleteKey.bind(cryptolib_obj),
  getXpub,
  changeWalletPassword,
};

function route (io) {
  io.on('connection', (socket) => {
    log.info('client connected: socket id', socket.id);
    let client = new SockClient(socket);

    socket.on('jsonrpc', async ({ id, method, params }, cb) => {
      // log.debug(compact(`${JSON.stringify({ id, method, params })}`));
      log.debug(compact(`${JSON.stringify({ id, method })}`));
      try {
        let ret;
        if (method in client) {
          ret = await client[method](...params);
        } else if (method in static_method_list) {
          ret = await static_method_list[method](...params);
        } else {
          throw new Error('method not found');
        }
        ret.id = id;
        ret.jsonrpc = '2.0';
        log.debug(compact(`[${method}][200]`));
        // log.debug(compact(`[${method}][200] ${JSON.stringify(ret)}`));
        return cb(ret);
      } catch (error) {
        log.error(`[${method}][400]`, error);
        return cb({
          jsonrpc: '2.0',
          id,
          error: error.message });
      }
    });
    socket.on('disconnect', () => {
      log.debug('client disconnected: socket id', socket.id);
      client = null;
    });
  });
}

module.exports = {
  route
};
