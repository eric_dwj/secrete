/* eslint-disable class-methods-use-this */
const cryptoJS = require('crypto-js');
const KeyStore = require('./keystore/KeyStore');
let CryptolibNative = require('./bindings');

if (CryptolibNative.default) CryptolibNative = CryptolibNative.default;

const config = require('./config');

class CryptolibBase {
  constructor () {
    this.keyStore = KeyStore.create(config.keyStore);
    this.volatileKeyStore = KeyStore.create(config.volatileKeyStore || { scheme: 'memory' });
    this._key_stores = {
      sign: this.volatileKeyStore,
      keygen: this.keyStore,
    };
  }

  _getKeyStore (category) {
    return this._key_stores[category];
  }

  async getSubcategory (id, category, sid, subcategory) {
    const key_json = await this._getKeyStore(category).getKey(id, category, sid);
    if (subcategory) {
      if (subcategory in key_json) return key_json[subcategory];
      return {};
    }
    return key_json;
  }

  async replaceSubcategory (id, category, sid, subcategory, value) {
    if (subcategory) {
      await this._getKeyStore(category).assignToKey(id, category, sid, { [subcategory]: value });
    } else {
      await this._getKeyStore(category).setKeyValue(id, category, sid, value);
    }
  }

  async getDerivedPublic (xpub, derivationPath) {
    const ret = await CryptolibNative.getDerivedPublic(xpub, derivationPath);
    const { error, result } = JSON.parse(ret);
    if (error) return { error };
    return { result };
  }

  async mergeToKey (key_id, category, sid, value) {
    await this._getKeyStore(category).mergeToKey(key_id, category, sid, value);
    return { result: 'ok' };
  }

  async assignToKey (key_id, category, sid, value) {
    await this._getKeyStore(category).assignToKey(key_id, category, sid, value);
    return { result: 'ok' };
  }

  async setKeyValue (key_id, category, sid, value) {
    await this._getKeyStore(category).setKeyValue(key_id, category, sid, value);
    return { result: 'ok' };
  }

  async deleteKey (key_id, category) {
    if (category) await this._getKeyStore(category).deleteKey(key_id, category);
    else {
      await Promise.all([this.keyStore.deleteKey(key_id), this.volatileKeyStore.deleteKey(key_id)]);
    }
    return { result: 'ok' };
  }

  async getEncryptedSeed (key_id) {
    const key_json = await this._getKeyStore('keygen').getKey(key_id, 'keygen', 'default');
    const result = typeof key_json.encrypt === 'string' ? key_json.encrypt : JSON.stringify(key_json.encrypt);
    return { result };
  }

  async getXpub (key_id, is_testnet = false) {
    const key_json = await this._getKeyStore('keygen').getKey(key_id, 'keygen', 'default');
    if (is_testnet) return { result: key_json.tpub };
    return { result: key_json.xpub };
  }

  async getRecoveryKey (key_id) {
    const key_json = await this._getKeyStore('keygen').getKey(key_id, 'keygen', 'default');
    const ret = [key_json.encryptedOurBackup, key_json.encryptedTheirBackup];
    if (ret[0] && ret[1]) return { result: ret };
    return { error: 'cannot find recovery keys' };
  }

  static decrypt (key_json, passphrase) {
    if (passphrase && key_json.encrypt) {
      const clear_text = cryptoJS.AES.decrypt(key_json.encrypt, passphrase).toString(cryptoJS.enc.Utf8);
      Object.assign(key_json, JSON.parse(clear_text));
    }
  }

  static encrypt (obj, passphrase) {
    return cryptoJS.AES.encrypt(JSON.stringify(obj), passphrase).toString();
  }
}

module.exports = CryptolibBase;
