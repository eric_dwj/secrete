const CryptolibBase = require('./CryptolibBase');
const CryptolibNative = require('./bindings');
const config = require('./config');

class CryptolibServer extends CryptolibBase {
  async _process (id, category, sid, subcategory, passphrase, callSelf, params, message) {
    const key_json = await this.getSubcategory(id, category, sid, subcategory);
    if (key_json._complete) {
      return { error: 'This ID already completed protocol, delete to reuse' };
    }
    if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
    // console.log("MESSAGE:", _message);
    const state = {
      result: message,
      save: key_json
    };
    const { error, result, complete, output, save } = JSON.parse(await callSelf(JSON.stringify(params), JSON.stringify(state)));
    if (error) return { error };
    let toSave = save;
    if (complete) {
      toSave = output;
      toSave._complete = true;
    }
    if (toSave) {
      if (passphrase) {
        const nsave = {};
        const exported = [...(toSave.export || []), '_complete'];
        const enc = {};
        const exp = {};
        Object.entries(toSave).filter(([k]) => exported.includes(k)).forEach(([key, value]) => {
          exp[key] = value;
        });
        Object.entries(toSave).filter(([k]) => !exported.includes(k)).forEach(([key, value]) => {
          enc[key] = value;
        });
        Object.assign(nsave, exp);
        if (Object.keys(enc).length > 0) nsave.encrypt = CryptolibBase.encrypt(enc, passphrase);
        toSave = nsave;
      }
      await this.replaceSubcategory(id, category, sid, subcategory, toSave);
    }
    return { result };
  }

  async generateKeyServer ({ key_id, _message, recovery_service_provider, passphrase }) {
    const backupPublicKey = config.backupPublicKey[recovery_service_provider];
    return this._process(key_id, 'keygen', 'default', null, passphrase, CryptolibNative.generateKeyServer, { backupPublicKey }, _message);
  }

  async generateEd25519KeyServer ({ key_id, _message, recovery_service_provider, passphrase }) {
    const backupPublicKey = config.backupPublicKey[recovery_service_provider];
    return this._process(key_id, 'keygen', 'ed25519', null, passphrase, CryptolibNative.generateEd25519KeyServer, { backupPublicKey }, _message);
  }

  async generateSchnorrKeyServer ({ key_id, _message, recovery_service_provider, passphrase }) {
    const backupPublicKey = config.backupPublicKey[recovery_service_provider];
    return this._process(key_id, 'keygen', 'schnorr', null, passphrase, CryptolibNative.generateSchnorrKeyServer, { backupPublicKey }, _message);
  }

  async generateSchnorrkelKeyServer ({ key_id, _message, recovery_service_provider, passphrase }) {
    const backupPublicKey = config.backupPublicKey[recovery_service_provider];
    return this._process(key_id, 'keygen', 'schnorrkel', null, passphrase, CryptolibNative.generateSchnorrkelKeyServer, { backupPublicKey }, _message);
  }

  async preSignServer ({ key_id, sid, _message, derivationPath, passphrase }) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'default'); // get key
    if (!key_json) {
      return { error: 'This key does not exist' };
    }
    if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
    const params = {
      derivationPath, key: key_json
    };
    return this._process(key_id, 'sign', sid, 'presign', passphrase, CryptolibNative.preSignServer, params, _message);
  }

  async signServer ({ key_id, sid, _message, derivationPath, message, passphrase }) {
    const key_json = await this.getSubcategory(key_id, 'sign', sid, 'presign'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
    const params = {
      derivationPath, message, preSignData: key_json
    };
    return this._process(key_id, 'sign', sid, 'sign', passphrase, CryptolibNative.signServer, params, _message);
  }

  // message must be Buffer
  async signEd25519Server ({ key_id, sid, _message, message, passphrase }) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'ed25519'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
    const params = {
      message: Buffer.from(message).toString('base64'), keygen: key_json
    };
    return this._process(key_id, 'sign', sid, 'ed25519_sign', passphrase, CryptolibNative.signEd25519Server, params, _message);
  }

    // message must be Buffer
    async signSchnorrServer ({ key_id, sid, _message,derivationPath, message, passphrase }) {
      const key_json = await this.keyStore.getKey(key_id, 'keygen', 'schnorr'); // get key
      if (!key_json) {
        return { error: 'This key and sid do not exist' };
      }
      if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
      const params = {
        message: Buffer.from(message).toString('base64'), key: key_json,
        derivationPath: derivationPath, hasher: "sha2_256"
      };
      return this._process(key_id, 'sign', sid, 'schnorr_sign', passphrase, CryptolibNative.signSchnorrServer, params, _message);
    }

    
    // message and context must be Buffer
    async signSchnorrkelServer ({ key_id, sid, _message,context, message, passphrase }) {
      const key_json = await this.keyStore.getKey(key_id, 'keygen', 'schnorrkel'); // get key
      if (!key_json) {
        return { error: 'This key and sid do not exist' };
      }
      if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
      const params = {
        message: Buffer.from(message).toString('base64'), keygen: key_json,
        context: Buffer.from(context).toString('base64')
      };
      return this._process(key_id, 'sign', sid, 'schnorrkel_sign', passphrase, CryptolibNative.signSchnorrkelServer, params, _message);
    }

  async fullSignServer ({ key_id, sid, _message, derivationPath, message, passphrase }) {
    const key_json = await this.keyStore.getKey(key_id, 'keygen', 'default'); // get key
    if (!key_json) {
      return { error: 'This key and sid do not exist' };
    }
    if (passphrase) CryptolibBase.decrypt(key_json, passphrase);
    const params = {
      derivationPath, message, key: key_json
    };
    return this._process(key_id, 'sign', sid, 'sign', passphrase, CryptolibNative.fullSignServer, params, _message);
  }
}

module.exports = CryptolibServer;
