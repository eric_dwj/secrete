/* eslint-disable no-bitwise */
const bip32 = require('bip32');
const bip66 = require('bip66');
const request = require('supertest');
const expect = require('expect.js');
const bitcoinjs = require('bitcoinjs-lib');
const child = require('child_process');

const CryptolibClient = require('../js/CryptolibClient.js');

const clientObj = new CryptolibClient();
const p2 = request(process.env.SERVER_URL || 'http://localhost:8080');
const ZERO = Buffer.alloc(1, 0);

function formRpcParam (method, ...params) {
  return {
    jsonrpc: '2.0',
    id: 1,
    method,
    params
  };
}

async function rpc_call (cmd, params) {
  const resp = await p2.post('/').send(formRpcParam(cmd, params));
  const { result, error } = resp.body;
  if (error) throw Error(`cryptolib error: ${JSON.stringify(error)}`);
  return { result };
}


function deleteKey (party, key_id, ...params) {
  if (party === 1) {
    it(`deleteKey(${key_id}, ${JSON.stringify(params)}) at party ${party}`, async () => clientObj.deleteKey(key_id, ...params));
  } else {
    it(`deleteKey(${key_id}, ${JSON.stringify(params)}) at party ${party}`, async function () {
      return p2.post('/')
        .send(formRpcParam('deleteKey', key_id, ...params))
        .expect(200);
    });
  }
}


function generateKeyClient (tx, key_id, recovery_service_provider, passphrase) {
  it(`generateKeyClient(${key_id})`, async function () {
    this.timeout(100000);
    const { result, error } = await clientObj.generateKeyClient(key_id, passphrase, recovery_service_provider, rpc_call);
    if (error) throw new Error(error);
    Object.assign(tx, result);
  });
}
function getRecoveryKey (key_id, ret) {
  it('getRecoveryKey', async function () {
    const { result, error } = await clientObj.getRecoveryKey(key_id);
    if (error) throw Error(error);
    // console.log(result);
    expect(result).to.be.an('array');
    expect(result[0]).to.be.a('string');
    expect(result[1]).to.be.a('string');
    ret.recovery = result;
  });
}
async function fullSignClient (key_id, sid, passphrase, derivePath, message) {
  const { result, error } = await clientObj.fullSignClient(key_id, sid, passphrase, derivePath, message, rpc_call);
  if (error) throw new Error(error);

  return result;
}

function recover_key (pem_file, rec_json) {
  const output = child.execFileSync('./backupRecovery', [pem_file, rec_json]).toString('utf8');
  console.log(output);
  if (output.includes('xprv')) {
    const strs = output.split(/\r?\n/);
    const ret = {};
    strs.forEach((line) => {
      if (line.includes('xprv')) ret.xprv = line;
      if (line.includes('xpub')) ret.xpub = line;
    });
    return ret;
  }
  console.log('Error occurred while running backupRecovery');
  return {};
}
function genRandomHexString (len) {
  const hex = '0123456789abcdef';
  let output = '';
  for (let i = 0; i < len; ++i) {
    output += hex.charAt(Math.floor(Math.random() * hex.length));
  }
  return output;
}
function genRandomPath () {
  const len = Math.floor(Math.random() * 6) + 1;
  const output = [];
  for (let i = 0; i < len; ++i) {
    const step = Math.floor(Math.random() * 45);
    output.push(step);
  }
  return output.join('/');
}

function getXpub (out, party, key_id, testnet = false) {
  if (party === 1) {
    it(`getXpub(${key_id}) at party ${party}`, function (done) {
      clientObj.getXpub(key_id, testnet)
        .then((ret) => { const { result, error } = ret; if (error) throw new Error(error); out.xpub = result; done(); });
    });
  } else {
    const p = p2;
    it(`getXpub(${key_id}) at party ${party}`, async function () {
      const { body: { result, error } } = await p.post('/')
        .send(formRpcParam('getXpub', key_id, testnet))
        .expect(200);
      if (error) throw new Error(error);
      // console.log(`getXpub result = ${result}`);
      expect(result).to.be.a('string');
      out.xpub = result;
    });
  }
}
function toDER (x) {
  let i = 0;
  while (x[i] === 0) ++i;
  if (i === x.length) return ZERO;
  const _x = x.slice(i);
  if (_x[0] & 0x80) return Buffer.concat([ZERO, _x], 1 + _x.length);
  return _x;
}
function encode (r, s) {
  const hashType = 1;
  const hashTypeBuffer = Buffer.allocUnsafe(1);
  hashTypeBuffer.writeUInt8(hashType, 0);
  const RBuffer = toDER(Buffer.from(r, 'hex'));
  const SBuffer = toDER(Buffer.from(s, 'hex'));
  return Buffer.concat([bip66.encode(RBuffer, SBuffer), hashTypeBuffer]);
}
describe('CryptoLib2 "Paths and recovery" Story Test Mk 1.5 ', function () {
  const wallet = { _id: 'mainTempWallet', recovery_service_provider: 'sbi_japannext', passphrase: 'P@ssw0rd' };
  const outerLoops = 600;
  for (let i = 0; i < outerLoops; i++) {
    describe(`Lesser Outer loop ${i + 1}/${outerLoops}`, function () {
      const xpub1 = {};
      describe('Test key generation', function () {
        const out = {};
        const tpub1 = {};
        const xpub2 = {};
        const tpub2 = {};
        const acc = {};
        deleteKey(1, wallet._id);
        deleteKey(2, wallet._id);
        generateKeyClient(out, wallet._id, wallet.recovery_service_provider, wallet.passphrase);
        getRecoveryKey(wallet._id, acc);
        getXpub(xpub1, 1, wallet._id);
        getXpub(tpub1, 1, wallet._id, true);
        getXpub(xpub2, 2, wallet._id);
        getXpub(tpub2, 2, wallet._id, true);
        it('compare recc of p1, p2', async function () {
          console.log( JSON.stringify(acc.recovery));
          const priv = recover_key('rsa-priv.cryptopp.pem', JSON.stringify(acc.recovery));
          expect(priv.xprv).to.be.a('string');
          expect(priv.xpub).to.be.a('string');
          const node = bip32.fromBase58(xpub1.xpub);
          console.log(priv.xprv);
          console.log(xpub1);
          console.log(xpub2);
          expect(priv.xpub).to.equal(node.neutered().toBase58());
          xpub1.xprv = priv.xprv;
        });
        it('compare xpub of p1, p2', async function () {
          wallet.xpub = xpub1.xpub;
          wallet.out = out;
          expect(xpub1.xpub).to.be.a('string');
          expect(xpub2.xpub).to.be.a('string');
          expect(xpub1.xpub).to.equal(xpub2.xpub);
        });
        it('compare tpub of p1, p2', async function () {
          wallet.tpub = tpub1.xpub;
          expect(tpub1.xpub).to.be.a('string');
          expect(tpub2.xpub).to.be.a('string');
          expect(tpub1.xpub).to.equal(tpub2.xpub);
        });
      });
    });
  }
  for (let i = 0; i < outerLoops; i++) {
    describe(`Outer loop ${i + 1}/${outerLoops}`, function () {
      const xpub1 = {};
      describe('Test key generation', function () {
        const out = {};
        const tpub1 = {};
        const xpub2 = {};
        const tpub2 = {};
        const acc = {};
        deleteKey(1, wallet._id);
        deleteKey(2, wallet._id);
        generateKeyClient(out, wallet._id, wallet.recovery_service_provider, wallet.passphrase);
        getRecoveryKey(wallet._id, acc);
        getXpub(xpub1, 1, wallet._id);
        getXpub(tpub1, 1, wallet._id, true);
        getXpub(xpub2, 2, wallet._id);
        getXpub(tpub2, 2, wallet._id, true);
        it('compare recc of p1, p2', async function () {
          const priv = recover_key('rsa-priv.cryptopp.pem', JSON.stringify(acc.recovery));
          expect(priv.xprv).to.be.a('string');
          expect(priv.xpub).to.be.a('string');
          const node = bip32.fromBase58(xpub1.xpub);
          expect(priv.xpub).to.equal(node.neutered().toBase58());
          xpub1.xprv = priv.xprv;
        });
        it('compare xpub of p1, p2', async function () {
          wallet.xpub = xpub1.xpub;
          wallet.out = out;
          expect(xpub1.xpub).to.be.a('string');
          expect(xpub2.xpub).to.be.a('string');
          expect(xpub1.xpub).to.equal(xpub2.xpub);
        });
        it('compare tpub of p1, p2', async function () {
          wallet.tpub = tpub1.xpub;
          expect(tpub1.xpub).to.be.a('string');
          expect(tpub2.xpub).to.be.a('string');
          expect(tpub1.xpub).to.equal(tpub2.xpub);
        });
        describe('Compare path derivations', function () {
          const prefix1 = 'm/44/0/0/0/';
          const prefix2 = 'm/44/1/0/0/';
          const prefixgen = 'm/';
          const iters = 120;
          for (let j = 0; j < iters; ++j) {
            it(`Check random path derivation, ${j + 1}/${iters}`, async function () {
              const pr1 = prefix1 + j;
              const pr2 = prefix2 + j;
              const rnd = prefixgen + genRandomPath();
              const der1 = (await clientObj.getDerivedPublic(xpub1.xpub, pr1)).result;
              const der2 = (await clientObj.getDerivedPublic(xpub1.xpub, pr2)).result;
              const derr = (await clientObj.getDerivedPublic(xpub1.xpub, rnd)).result;
              const node = bip32.fromBase58(xpub1.xpub);
              const child1 = `0x${node.derivePath(pr1).publicKey.toString('hex')}`;
              const child2 = `0x${node.derivePath(pr2).publicKey.toString('hex')}`;
              const childr = `0x${node.derivePath(rnd).publicKey.toString('hex')}`;
              expect(der1).to.equal(child1);
              expect(der2).to.equal(child2);
              expect(derr).to.equal(childr);
            });
          }
          const sig_iters = 120;
          for (let j = 0; j < sig_iters; ++j) {
            describe(`Check signature gen, ${j + 1}/${sig_iters}`, function () {
              it(`Verify signature, ${j + 1}/${sig_iters}`, async function () {
                this.timeout(100000);
                const message = `${genRandomHexString(64)}`;
                const hash = Buffer.from(message, 'hex');
                const sid1 = `${genRandomHexString(15)}h`;
                const sid2 = `${genRandomHexString(15)}h`;
                const pr1 = prefix1 + j;
                const pr2 = prefix2 + j;
                const sign1 = await fullSignClient(wallet._id, `${sid1}`, wallet.passphrase, `${pr1}`, `${message}h`);
                const sign2 = await fullSignClient(wallet._id, `${sid2}`, wallet.passphrase, `${pr2}`, `${message}h`);
                const node = bip32.fromBase58(xpub1.xpub);
                const child1 = node.derivePath(pr1);
                const child2 = node.derivePath(pr2);
                const s1 = bitcoinjs.script.signature.decode(encode(sign1.r, sign1.s)).signature;
                const s2 = bitcoinjs.script.signature.decode(encode(sign2.r, sign2.s)).signature;
                expect(child1.verify(hash, s1)).to.be(true);
                expect(child2.verify(hash, s2)).to.be(true);
                expect(child2.verify(hash, s1)).to.be(false);
                expect(child1.verify(hash, s2)).to.be(false);
              });
            });
          }
        });
      });
    });
  }
});
