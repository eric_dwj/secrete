// const expect = require('expect.js');
const request = require('supertest');
const app = require('../app');

describe('Test start', function () {
  before(function (done) {
    console.log('Test sart');
    done();
  });

  after(function (done) {
    console.log('Test finished');
    done();
  });

  let p1s1out;
  it('p1KeyGenStep1', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p1KeyGenStep1',
      params: { }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p1s1out = result;
        console.log('-- p1s1out --', p1s1out);
        done();
      });
  });

  let p2s1out;
  it('p2KeyGenStep1', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep1',
      params: { }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s1out = result;
        console.log('-- p2s1out --', p2s1out);
        done();
      });
  });

  let p1s2out;
  it('p1KeyGenStep2', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p1KeyGenStep2',
      params: {
        p2DlogProof: p2s1out.p2DlogProof,
        p1ZkRandomness: p1s1out.p1ZkRandomness,
        p1PkCommitRandomness: p1s1out.p1PkCommitRandomness,
        p1SecretShare: p1s1out.p1SecretShare
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p1s2out = result;
        console.log('-- p1s2out --', p1s2out);
        done();
      });
  });

  let p2s2out;
  it('p2KeyGenStep2', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep2',
      params: {
        p2SecretShare: p2s1out.p2SecretShare,
        p1PkCommit: p1s1out.p1PkCommit,
        p1ZkCommit: p1s1out.p1ZkCommit,
        p1PublicSharex: p1s1out.p1PublicSharex,
        p1PublicSharey: p1s1out.p1PublicSharey,
        p1DlogProof: p1s1out.p1DlogProof,
        p1PkCommitRandomness: p1s2out.p1PkCommitRandomness,
        p1ZkRandomness: p1s2out.p1ZkRandomness
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s2out = result;
        console.log('-- p2s2out --', p2s2out);
        done();
      });
  });

  let p1s3out;
  it('p1KeyGenStep3', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p1KeyGenStep3',
      params: {
        p1SecretShare: p1s1out.p1SecretShare
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p1s3out = result;
        console.log('-- p1s3out --', p1s3out);
        done();
      });
  });

  let p2s3out;
  it('p2KeyGenStep3', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep3',
      params: {
        paKey: {
          pk: p1s3out.paKey.pk
        },
        sessionid: p1s3out.sessionid,
        cKey: p1s3out.cKey,
        p1Vec: p1s3out.p1Vec
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s3out = result;
        console.log('-- p2s3out --', p2s3out);
        done();
      });
  });

  let p2s4out;
  it('p2KeyGenStep4', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep4',
      params: {
        paKey: {
          pk: p1s3out.paKey.pk
        },
        p1PublicSharex: p1s1out.p1PublicSharex,
        p1PublicSharey: p1s1out.p1PublicSharey,
        cKey: p1s3out.cKey
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s4out = result;
        console.log('-- p2s4out --', p2s4out);
        done();
      });
  });

  let p1s4out;
  it('p1KeyGenStep4', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p1KeyGenStep4',
      params: {
        paKey: p1s3out.paKey,
        pdl: p2s4out.pdl
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p1s4out = result;
        console.log('-- p2s4out --', p1s4out);
        done();
      });
  });

  let p2s5out;
  it('p2KeyGenStep5', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep5',
      params: {
        pdl: p2s4out.pdl
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s5out = result;
        console.log('-- p2s5out --', p2s5out);
        done();
      });
  });

  let p1s5out;
  it('p1KeyGenStep5', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p1KeyGenStep5',
      params: {
        pdl: {
          a: p2s5out.pdl.a,
          b: p2s5out.pdl.b,
          c2: p2s4out.pdl.c2,
          alpha: p1s4out.pdl.alpha,
          Qhatx: p1s4out.pdl.Qhatx,
          Qhaty: p1s4out.pdl.Qhaty
        },
        p1SecretShare: p1s1out.p1SecretShare
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p1s5out = result;
        console.log('-- p1s5out --', p1s5out);
        done();
      });
  });

  let p2s6out;
  it('p2KeyGenStep6', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep6',
      params: {
        pdl: {
          cHat: p1s4out.pdl.cHat,
          Qhatx: p1s4out.pdl.Qhatx,
          Qhaty: p1s4out.pdl.Qhaty,
          Qdashx: p2s4out.pdl.Qdashx,
          Qdashy: p2s4out.pdl.Qdashy
        },
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s6out = result;
        console.log('-- p2s6out --', p2s6out);
        done();
      });
  });

  let p1s6out;
  it('p1KeyGenStep6', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p1KeyGenStep6',
      params: {
        p1SecretShare: p1s1out.p1SecretShare,
        sessionid: p1s3out.sessionid,
        cKey: p1s3out.cKey,
        paKey: {
          r: p1s3out.paKey.r,
          pk: p1s3out.paKey.pk
        }
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p1s6out = result;
        console.log('-- p1s6out --', p1s6out);
        done();
      });
  });

  let p2s7out;
  it('p2KeyGenStep7', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'p2KeyGenStep7',
      params: {
        sessionid: p1s3out.sessionid,
        cKey: p1s3out.cKey,
        paKey: {
          pk: p1s3out.paKey.pk
        },
        range: {
          e: p1s6out.range.e,
          c1: p1s6out.range.c1,
          c2: p1s6out.range.c2,
          ei: p1s6out.range.ei,
          z: p1s6out.range.z
        }
      }
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        const { result } = JSON.parse(res.text);
        p2s7out = result;
        console.log('-- p2s7out --', p2s7out);
        done();
      });
  });
});
