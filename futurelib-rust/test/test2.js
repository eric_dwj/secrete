const request = require('supertest');
const app = require('../app');

describe('main.h test', function () {
  it('generateKeys()', function (done) {
    this.timeout(10000);
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'generateKeys',
      params: []
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        JSON.parse(res.text);
        done();
      });
  });

  it('sign()', function (done) {
    const data = {
      jsonrpc: '2.0',
      id: 1,
      method: 'sign',
      params: ['1', '1234h']
    };
    request(app).post('/')
      .send(data)
      .expect(200)
      .then((res) => {
        JSON.parse(res.text);
        done();
      });
  });
});
