/* eslint-disable func-names */
/* eslint-disable prefer-rest-params */
/* eslint-disable prefer-spread */
/* eslint-disable no-unused-vars */
/* eslint-disable no-bitwise */
// const Module = require('./cryptolib');

const cryptolib = import('./pkg/index.js');
// const cryptolibWasm = require('./cryptolib.wasm').default;


let Module;

async function loadModule () {
  if (!Module) {
    Module = await cryptolib;
  }
  return Module;
}


function getCFunc (ident) {
  return Module[`${ident}`]; // closure exported function
}



const funcNames = ['generateKeyClient', 'preSignClient', 'signClient', 'fullSignClient', 'getDerivedPublic', 'generateEd25519KeyClient', 
                   'signEd25519Client',  'generateSchnorrKeyClient', 'signSchnorrClient', 'generateSchnorrkelKeyClient', 'signSchnorrkelClient'];
const CryptolibNative = {};
const functionCache = {};

async function preload (fname) {
  if (functionCache[fname]) return functionCache[fname];
  await loadModule();
  functionCache[fname] = getCFunc(fname);
  return functionCache[fname];
}

funcNames.forEach((fn) => {
  CryptolibNative[fn] = async function () {
    const fobj = await preload(fn);
    return fobj(...arguments);
  };
});


export default CryptolibNative;
