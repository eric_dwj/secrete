
/* eslint-disable no-undef */
const assert = require('assert');
const {
    deriveAddressFromXprv,
    derivePrivateKeyFromXprv,
    recoverSingleAddress,
    getBalance,
    getFee,
    getAccountInfo,
    signTransaction,
    derivePublicKeyFromXprv,
    deriveAddressFromXpub,
    retreiveAddress
} = require('../coins/xrp');

// npm run test-single -- test/test_xrp_lib.js

const xprv = 'xprv9s21ZrQH143K3xqfnfHdg3qzMnEWpLpd6m7ZHtRzH7KNKKePBAJesN4s5NuhTa4eA9gUTZG8mqJQtfJTNnBDFZWhii8dBLeVfEVNJjy8e4p';
const xpub = 'xpub661MyMwAqRbcGSv8tgpe3Bniup51DoYUTz3A6GqbqSrMC7yXihcuRAPLvduRmMob6tDg61ph53xPZDJM4AWg6Zfbux4iQJGzpDvce1gNsat'


describe('npm run test-single -- test/test_xrp_lib.js', () => {
    it('test lib bip32 => private key', async () => {
        const privateKey =  derivePrivateKeyFromXprv(xprv,  0 );
        assert.equal(privateKey, '4d47b1ab11e449ea17e2bdf99793ddc1069ded0ab858ad54513a634753f49c2f');
    });


    it('test lib bip32 => public key', async () => {
        const publicKey =  derivePublicKeyFromXprv(xprv,  0 );
        assert.equal(publicKey, '03b0928493f86ed2902974c94127d850a6092bbc57da5263b9f4f8f4a01bfd11b1');
    });

    it('test deriveAddressFromXpub bip32 => address', async () => {
        assert.equal(deriveAddressFromXpub(xpub,0), 'rUKnqYTWeBNhRcaXKLUnJZbTykaVTe575o');
    });

    it('test deriveAddressFromXprv bip32 => address', async () => {
        assert.equal(deriveAddressFromXprv(xprv,0), 'rUKnqYTWeBNhRcaXKLUnJZbTykaVTe575o');
    });

    it('test retreiveAddress', async () => {
    const addresses = await retreiveAddress({ xpub, skip: 0, limit: 10 });
    console.log(addresses);
    assert.equal(Array.isArray(addresses), true);
    });

    it('test get balance', async () => {
        const balance =  await getBalance('rUKnqYTWeBNhRcaXKLUnJZbTykaVTe575o');
        assert.notEqual(balance, null);
    });

    it('test get fee', async () => {
        assert.notEqual(getFee(), null);
    });

    it('test getAccountInfo', async () => {
        const result = await getAccountInfo('rUKnqYTWeBNhRcaXKLUnJZbTykaVTe575o')
        assert.notEqual(result, null);
        assert.notEqual(result.ledgerIndex, null);
        assert.notEqual(result.sequence, null);
    });
    
    it('test signTransaction', async () => {
        const { txid, txBlob } = await signTransaction({ xprv, to: 'rH9dAHCiM7bpiR5mKEjiijwDo4rMyA4grS' });
        assert.notEqual(txid, null);
        assert.notEqual(txBlob, null);
    });

    it('test recover', async () => {
        // const txid = await recover({ xprv, to: 'rH9dAHCiM7bpiR5mKEjiijwDo4rMyA4grS' });
        // assert.notEqual(txid, null);
    });
    
    it('test recover single address', async () => {
        const txid = await recoverSingleAddress({ secreteKey: '4D47B1AB11E449EA17E2BDF99793DDC1069DED0AB858AD54513A634753F49C2F', to: 'rH9dAHCiM7bpiR5mKEjiijwDo4rMyA4grS' });
        assert.notEqual(txid, null);
    });
});
