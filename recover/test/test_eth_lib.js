
/* eslint-disable no-undef */
const assert = require('assert');
const bip32 = require('bip32');
const ethers = require('ethers');
const {
    derivePrivateKeyFromXprv,
    deriveAddressFromXprv,
    getBalance,
    getFeeRate,
    getXpubFromXprv,
    deriveAddressFromXpub,
    retreiveAddress,
    exeRecoverV2,
    getSequence,
    parseDerivationPath,
    recoverSingleAddress,
} = require('../coins/eth');
// npm run test-single -- test/test_eth_lib.js

const xprv = 'xprv9s21ZrQH143K3xqfnfHdg3qzMnEWpLpd6m7ZHtRzH7KNKKePBAJesN4s5NuhTa4eA9gUTZG8mqJQtfJTNnBDFZWhii8dBLeVfEVNJjy8e4p';
const xpub = 'xpub661MyMwAqRbcGSv8tgpe3Bniup51DoYUTz3A6GqbqSrMC7yXihcuRAPLvduRmMob6tDg61ph53xPZDJM4AWg6Zfbux4iQJGzpDvce1gNsat'
const node = bip32.fromBase58(xprv);
const ethBip32DerivationPath = 'm/44/60/0/0';
describe('npm run test-single -- test/test_eth_lib.js', () => {
  it('test deriveXpubFromXprv bip32 => Xpub', async () => {
    assert.equal(getXpubFromXprv({xprv}), xpub);
  });

  it('test deriveAddressFromXpub bip32 => address', async () => {
    assert.equal(deriveAddressFromXpub(xpub,0), '0xeCA38D06930d7c9756fC3eB957449e703D4f857B');
    assert.equal(deriveAddressFromXpub(xpub,3308), '0xFb485BBF258495BDc574246CCfeB3552C918e5D3');
  });

  it('test retreiveAddress', async () => {
    const addresses = await retreiveAddress({ xpub, skip: 0, limit: 2 });
    console.log(addresses);
    assert.equal(Array.isArray(addresses), true);
  });
  
  it('test lib bip32 => private key', async () => {
    const child = node.derivePath(`${ethBip32DerivationPath}/0`);
    assert.equal(child.publicKey.toString('hex'), '023fa4569bda38359db42fc0cc3fe7d509055fed7683dcc4166bbb95278507cc08');
    assert.equal(child.privateKey.toString('hex'), '67c3e8743f26e8ca0f3b013d49072080d66b7dbb328875bc95419789f571d08f');
    const { address } = ethers.utils.HDNode.fromExtendedKey(xprv).derivePath(parseDerivationPath(0));
    assert.equal(address, '0xeCA38D06930d7c9756fC3eB957449e703D4f857B');
  });

  it('test derivePrivateKeyFromXprv bip32 => private key', async () => {
    assert.equal(derivePrivateKeyFromXprv(xprv,0), '67c3e8743f26e8ca0f3b013d49072080d66b7dbb328875bc95419789f571d08f');
    assert.equal(derivePrivateKeyFromXprv(xprv,3308), '93f1f7222f983127a30cdb4ffb1ebb1e101e3647f5a69362696b512e83c93b35');
  });

  it('test deriveAddressFromXprv bip32 => address', async () => {
    assert.equal(deriveAddressFromXprv(xprv,0), '0xeCA38D06930d7c9756fC3eB957449e703D4f857B');
    assert.equal(deriveAddressFromXprv(xprv,3308), '0xFb485BBF258495BDc574246CCfeB3552C918e5D3');
    assert.equal(deriveAddressFromXprv(xprv,908), '0xb75Ad017b587167016AbB7b2EFba5f02Fa998584');
    assert.equal(deriveAddressFromXprv(xprv,1823), '0xe2CA375948Cc4639c467C8Af50B68E26A331A531');
    assert.equal(deriveAddressFromXprv(xprv,1709), '0x9D489EEd375C706A124DD15e5E28dEdEb6B3CcEB');
    assert.equal(deriveAddressFromXprv(xprv,494), '0xf31aA4FFb51da5fb1fda9604E8dd3b6302a9601C');
    // assert.equal(derivePrivateKeyFromXprv(xprv,0), '67c3e8743f26e8ca0f3b013d49072080d66b7dbb328875bc95419789f571d08f');
  });

  it('test get balance', async () => {
    const balance =  await getBalance('0x9e8F37678D9a54a745e085b83d704720F40Df4Ad');
    assert.notEqual(balance, null);
  });

  it('test get fee rate', async () => {
    const gasPrice = await getFeeRate();
    assert.notEqual(gasPrice, null);
  });

  it('test get sequence', async () => {
    const sequence = await getSequence('0x9e8F37678D9a54a745e085b83d704720F40Df4Ad');
    assert.notEqual(sequence, null);
  });
  
  it('test processRecoverV2', async () => {
    exeRecoverV2({ xprv, to: '0xeCA38D06930d7c9756fC3eB957449e703D4f857B'})
  });
  
  it('recover single address', async () =>{
    await recoverSingleAddress({ secreteKey: '67c3e8743f26e8ca0f3b013d49072080d66b7dbb328875bc95419789f571d08f', to: '0xeCA38D06930d7c9756fC3eB957449e703D4f857B' })
  })
});
