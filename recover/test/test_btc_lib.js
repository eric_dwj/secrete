
/* eslint-disable no-undef */
const assert = require('assert');
const bip32 = require('bip32');
const {
    getUnspent,
    derivePrivateKeyFromXprv,
    deriveAddressFromXprv,
    getBalance,
    getAddressInfoFromXprv,
    getUnspentsInfo,
    deriveAddressFromXpub,
    // exeRecoverV2,
    retreiveAddress,
    scanSingleAddress,
    // recoverSingleAddress,
} = require('../coins/btc');
// npm run test-single -- test/test_btc_lib.js

const xprv = 'xprv9s21ZrQH143K3xqfnfHdg3qzMnEWpLpd6m7ZHtRzH7KNKKePBAJesN4s5NuhTa4eA9gUTZG8mqJQtfJTNnBDFZWhii8dBLeVfEVNJjy8e4p';
const xpub = 'xpub661MyMwAqRbcGSv8tgpe3Bniup51DoYUTz3A6GqbqSrMC7yXihcuRAPLvduRmMob6tDg61ph53xPZDJM4AWg6Zfbux4iQJGzpDvce1gNsat'
const node = bip32.fromBase58(xprv);
const btcBip32DerivationPath = 'm/44/1/0/0';
describe('npm run test-single -- test/test_btc_lib.js', () => {

  it('test lib bip32 => private key', async () => {
    const child = node.derivePath(`${btcBip32DerivationPath}/0`);
    assert.equal(child.publicKey.toString('hex'), '020566741427fb576ffcaa687f1dbdbfe456375210f451634ad9b1d7b38703fae5');
    assert.equal(child.toWIF(), 'KxZj9W4GdC2BwaEsx86c7k2BVyGyuT8actP89hs5s31BuiJoXZEA');
  });

  it('test deriveAddressFromXpub bip32 => address', async () => {
    assert.equal(deriveAddressFromXpub(xpub,0), 'tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn');
    assert.equal(deriveAddressFromXpub(xpub,3308), 'tb1qcjdcez242z2285ktxw06wqmqsec04tdt3d0mv7');
  });

  it('test derivePrivateKeyFromXprv bip32 => private key', async () => {
    assert.equal(derivePrivateKeyFromXprv(xprv,0), 'KxZj9W4GdC2BwaEsx86c7k2BVyGyuT8actP89hs5s31BuiJoXZEA');
    assert.equal(derivePrivateKeyFromXprv(xprv,1), 'KwrnbukT3vcupnvBn4b4j1HWuPAkfUs1bfBinw4SBY3tPDH5d5SE');
    assert.equal(derivePrivateKeyFromXprv(xprv,39), 'L1HeZUMG9ZTwMm97Hq1mCu53dFgDa81Woth2NGBkKXDrZdMigicq');
    assert.equal(derivePrivateKeyFromXprv(xprv,439), 'L21YKKzqZ1ZxTjDvuftzaxmFvk7GYKQJyMNK5whQA4rPdX8ozbdV');
    assert.equal(derivePrivateKeyFromXprv(xprv,471), 'Kz6fnbrjP4wZueUNg8v21v1C2BGJHbSo8BPoYp7haAGZMjVrf9tm');
  });

  it('test deriveAddressFromXprv bip32 => address', async () => {
    assert.equal(deriveAddressFromXprv(xprv,0), 'tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn');
    assert.equal(deriveAddressFromXprv(xprv,2), 'tb1qfsu6w02we7q5yk7vrquc5e7fze63v0fdlnd2vr');
    assert.equal(deriveAddressFromXprv(xprv,3), 'tb1qcwydwnzwy2pmn98d0yafz86j73kjznkk3dc70t');
    assert.equal(deriveAddressFromXprv(xprv,3308), 'tb1qcjdcez242z2285ktxw06wqmqsec04tdt3d0mv7');
    assert.equal(deriveAddressFromXprv(xprv,908), 'tb1qe6md6w4hxf0yl956fnx9xzremhuzht3tavdzph');
    assert.equal(deriveAddressFromXprv(xprv,1823), 'tb1qk76k2xkr0devsgsvtazvp2l4dhdf6xs7025edv');
  });

  it('test retreiveAddress', async () => {
    const addresses = await retreiveAddress({ xpub, skip: 0, limit: 10 });
    assert.equal(Array.isArray(addresses), true);
  });

  it('test get balance', async () => {
    const balance =  await getBalance('tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn');
    assert.notEqual(balance, null);
  });

  it('test get unspent', async () => {
    const unspents = await getUnspent('tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn');
    assert.notEqual(unspents, null);
  });

  it('test get address Info', async function(){
    this.timeout(10000000);
    const addressinfo = await getAddressInfoFromXprv({ xprv, skip:0, limit: 30 });
    assert.equal(Array.isArray(addressinfo), true);
  });

  it('test get unspent Info', async function(){
    const addressInfo = { 
        address: 'tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn',
        secreteKey: 'KxZj9W4GdC2BwaEsx86c7k2BVyGyuT8actP89hs5s31BuiJoXZEA',
        balance: '0.0002',
        index: 0,
        isChangeAddress: false 
    }
    const addressInfoWithUnspents = await getUnspentsInfo(addressInfo);
    assert.equal(Array.isArray(addressInfoWithUnspents), true);
  });

  // it('test exeRecoverV2', async () => {
    // const { txids } = await exeRecoverV2({ xprv, to: 'tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn' });
    // console.log(txids);
    // assert.notEqual(txids, null);
  // });
  
  it('test scan signle address', async () => {
    const { balance } = await scanSingleAddress({ address: 'tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn' });
    console.log(balance);
    assert.notEqual(balance, null);
  });

  // it('test recover signle address', async () => {
  //   const { txid } = await recoverSingleAddress({ secreteKey: 'KxZj9W4GdC2BwaEsx86c7k2BVyGyuT8actP89hs5s31BuiJoXZEA', to: 'tb1qaxyjywh7x9lwdmh0s4zfzw6p2ldpsyretu7nwn' })
  //   console.log(txid);
  //   assert.notEqual(txid, null);
  // });
  

  
});
