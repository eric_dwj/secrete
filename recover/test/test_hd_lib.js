
/* eslint-disable no-undef */
const assert = require('assert');
const { readFile, readJsonFile } = require('../coins/common');
const {
    callRecovery,
    getXprvAndXpubFromKeyStone
} = require('../coins/hd');
const prvkey = '-----BEGIN PUBLIC KEY-----\nMIIBoDANBgkqhkiG9w0BAQEFAAOCAY0AMIIBiAKCAYEAnLMQhN60K+m++j6ZzQ8I\nnIHOXJ3aVkk3Es3rNzDRQSwHr1+Rp/4/hqvhu9NzzQm9xxUZ0Lr1Po2/q+nX7784\n+D2ca+Ob+DBRPQX1SjPi3usv4txyl5CPTI/Fv5cr1WeiTc2CgkdRLeOlW7r0xHiC\nrtvGzViTb32NAdVviBwHrTKLUC7mGtXNaNoZuxvHe+GaB8C7fiK9ZT/zlmV213FF\nYiYEcVgYFv5VdkCV7rHn1FuuskdM0mR1uo2HHhWC7SZ/czMA+unVrsLK3C2caJ0P\n0mZzxvNuu7E1woXsHVbX5O/u2f28wnlBKO4Hy9SOOT0BGX8Vp7EuxbKA64bqIGIJ\n6wyvHwbjm2kwpF3AcY/pVGQh5m69nnpoyx+nbDlcO878TE4Q0jYn9112JbjQ+8Gh\nxg20paiUqoX/OUG0eFrWf3xs9581AsbHDSISFd90qBKZU1AsOeaPZH2wb/LiZigE\nrrG+NHqIsgPOfPbgQf+i156sYotikRuh7N8JdcATinX5AgER\n-----END PUBLIC KEY-----'
describe('npm run test-single -- test/test_hd_lib.js', () => {

    it('test recovery  => xpub and xprv', async () => {
        const { xprv, xpub} = await callRecovery({ keyfileName: 'rsa-priv.cryptopp.pem',  keystoneName:'tst.json', path: 'test'} );
        assert.notEqual(xprv, null);
        assert.notEqual(xpub, null);
        console.log(prvkey);
    });

    it('test recovery keyfile, keystone  => xpub and xprv', async () => {
        const keystone = await readJsonFile(`${process.cwd()}/test/tst.json`);
        const keyfile = await readFile(`${process.cwd()}/test/rsa-priv.cryptopp.pem`);    
        const { xprv, xpub} = await getXprvAndXpubFromKeyStone({ keyfile: keyfile.toString(),  keystone, path: 'test'} );
        assert.notEqual(xprv, null);
        assert.notEqual(xpub, null);
    });
});