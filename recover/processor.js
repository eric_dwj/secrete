/* eslint-disable global-require */
const config = require('config');
const log = require('log4js').getLogger('[app]');
const processor = {};
config.get('recovery.processor').filter((conf) => conf).forEach((conf) => {
  log.info(`loading processor: ${JSON.stringify(conf)}` );
  const [key, value] = Object.entries(conf)[0];
  log.info('method for:', key, 'path', value);
  if (value) processor[key] = require(`./${value}`);
});

module.exports = {
  processor
};
