## Project setup

# step 1: download submodule
git submodule update --init

# step 2: build back end package
yarn build-back

# step 3: build front end package
yarn build-front

# step 4: start back end
yarn start

# step 5: start front end
yarn serve