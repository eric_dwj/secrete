const log = require('log4js').getLogger('[coin/btc]');
const Big = require('bignumber.js');
const { getXpubFromXprv, saveToJsonFile, readJsonFile, isFileExists, generateFileName } = require('./common');
const { sleep } = require('./util');
const { NodeBase } = require('./node/NodeBase');
const { socket } = require('./socket');
const CCQS = require('./node/CCQS');
const {
  sign,
  getUnspent,
  getBalance,
  isValidateAddress,
  deriveAddressFromXpub,
  derivePrivateKeyFromXprv,
  deriveAddressFromXprv,
  getSetPath,
  parseDerivationPath,
  getFee,
  getFeeRate,
  deriveAddressFromSecreteKey,
} = require('./btcUtil');

const coin = 'BTC';
const nodeService = new NodeBase(CCQS);

let totalBalance = new Big('0');
const processingStatus = { recovering: false, scanning: false };

const scan = async ({ xpub, skip = 0, limit }) =>{
  retreiveAddress({ xpub, skip, limit });
  return {};
};

const getUnspentsInfo = async (addressInfo)=>{
  const { address } = addressInfo;
  const unspents = await getUnspent(address);
  return unspents.map((unspentObj)=> ({ ...addressInfo, ...unspentObj }))
};


const getAddressInfoFromXpub = async ({ xpub, skip = 0, limit }) =>{
  const indexes = [];
  for (let index = skip; index < skip + limit; index++) {
    indexes.push(index);
  }
  
  const findBalance = async (index, isChangeAddress) => {
    const address = deriveAddressFromXpub(xpub, index, isChangeAddress)
    const balance = await getBalance(address);
    totalBalance = totalBalance.plus(balance);
    socket.emit('address', { coin, address, index, balance, isChangeAddress: `${isChangeAddress}`, total: totalBalance.toString()});
    return { address, balance, index, isChangeAddress, coin };
  };

  const tasks = indexes.map((index)=>findBalance(index, false))
  .concat(indexes.map((index)=>findBalance(index, true)));

  const addressArr = await Promise.all(tasks);
  return addressArr.filter(({balance})=> `${balance}` !== '0');
};

const getAddressInfoFromXprv = async ({ xprv, skip = 0, limit }) =>{
  const indexes = [];
  for (let index = skip; index < skip + limit; index++) {
    indexes.push(index);
  }
  
  const findBalance = async (index, isChangeAddress) => {
    const address = deriveAddressFromXprv(xprv, index, isChangeAddress);
    const secreteKey = derivePrivateKeyFromXprv(xprv, index, isChangeAddress); 
    const balance = await getBalance(address);
    totalBalance = totalBalance.plus(balance);
    socket.emit('address', { coin, address, index, balance, isChangeAddress: `${isChangeAddress}`, total: totalBalance.toString() });
    return { address, secreteKey, balance, index, isChangeAddress}
  };

  const tasks = indexes.map((index)=>findBalance(index, false))
  .concat(indexes.map((index)=>findBalance(index, true)));

  const addressArr = await Promise.all(tasks);
  return addressArr.filter(({balance})=> `${balance}` !== '0');
};


const stopScanning = async () => {
  processingStatus.scanning = false;
  return {};
}

const retreiveAddress = async ({ xpub, skip = 0, limit }) =>{
  processingStatus.scanning = true;
  totalBalance = new Big('0');
  const totalUnspents = [];
  for (let index = 0; index < limit; index ++) {
    if(processingStatus.scanning){
      const unspents = await getUnspentsForSignV2({ xpub, skip: skip + index, limit: 1 }) || [];
      totalUnspents.push(...unspents);
    }
    await sleep(100);
  }
  await saveToJsonFile(`./local/${coin}_${generateFileName(xpub)}`, totalUnspents);
  return totalUnspents;
};


const getUnspentsForSignV2 = async({ xpub, skip, limit }) => {
  const addressInfos  = await getAddressInfoFromXpub({ xpub, skip, limit });
  if (addressInfos.length === 0) return null;
  const rawUnspents = await Promise.all(addressInfos.map(getUnspentsInfo));
  if (rawUnspents.length === 0) return null;
  const unspents = rawUnspents.reduce((a,b)=> a.concat(b),[]);
  return unspents;
};

const signTransactionV2 = async ({ xprv, to }) =>{
  const xpub = getXpubFromXprv({ xprv });
   const fileExists = await isFileExists(`./local/${coin}_${generateFileName(xpub)}`);
   if(!fileExists) {
    return socket.emit('error', 'scan file is not existed, please scan first.');
   }
  const totalUnspentsWithSecreteKey = await readJsonFile(`./local/${coin}_${generateFileName(xpub)}`);
  if (totalUnspentsWithSecreteKey.length === 0) return null;
  const getSecretKey = (unspent) =>{
    const { index, isChangeAddress } = unspent;
    const secreteKey = derivePrivateKeyFromXprv(xprv, index, isChangeAddress );
    return { ...unspent, secreteKey };
   }
  return sign({ unspents: totalUnspentsWithSecreteKey.map(getSecretKey), to});
};

const exeRecoverV2 = async ({ xprv, to }) =>{
  const { txBlob, amount, fee } = await signTransactionV2({ xprv, to });
  if(!txBlob) return { txids: null };
  const { txid } = await nodeService.broadcast(coin, txBlob);
  socket.emit( 'transaction', {coin, txid, fee, amount });
  return { txids: [txid] };
};

const recoverV2= async ({ xprv, to }) => {
  exeRecoverV2({ xprv, to });
  return {};
};

const scanSingleAddress = async ({ address }) => {
  const balance = await getBalance(address);
  socket.emit('address', { coin, address, index: 0, balance, isChangeAddress: true, total: balance });
  return { balance };
};

const recoverSingleAddress = async ({ secreteKey, to }) => {
  const address = await deriveAddressFromSecreteKey({ secreteKey });
  const balance = await getBalance(address);
  const addressInfos = [{ address, balance, index:0, isChangeAddress: false, coin, secreteKey }];
  if (addressInfos.length === 0) return null;
  const rawUnspents = await Promise.all(addressInfos.map(getUnspentsInfo));
  if (rawUnspents.length === 0) return null;
  const unspents = rawUnspents.reduce((a,b)=> a.concat(b),[]);
  const { txBlob, amount, fee } = await sign({ unspents, to});
  if(!txBlob) return null;
  const { txid } = await nodeService.broadcast(coin, txBlob);
  if(!txid) return null;
  log.info(`[recoverSingleAddress]: ${txid}`)
  socket.emit( 'transaction', {coin, txid, fee, amount });
  return txid;
};

module.exports = {
  isValidateAddress,
  derivePrivateKeyFromXprv,
  deriveAddressFromXprv,
  deriveAddressFromXpub,
  getXpubFromXprv,
  parseDerivationPath,
  getUnspentsInfo,
  getFeeRate,
  getBalance,
  getUnspent,
  getFee,
  getAddressInfoFromXprv,
  exeRecoverV2,
  retreiveAddress,
  recoverV2,
  getSetPath,
  scan,
  stopScanning,
  scanSingleAddress,
  recoverSingleAddress,
  deriveAddressFromSecreteKey,
};
