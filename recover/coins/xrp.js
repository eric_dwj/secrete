const Big = require('bignumber.js');
const { RippleAPI } = require('ripple-lib');
const { NodeBase } = require('./node/NodeBase');
const { getXpubFromXprv, saveToJsonFile, generateFileName } = require('./common');
const CCQS = require('./node/CCQS');
const { minus } = require('./util');
const { socket } = require('./socket');
const {  
  sign,
  getBalance,
  derivePrivateKeyFromXprv,
  deriveAddressFromXprv,
  deriveAddressFromSecreteKey,
  derivePublibkeyFromSecreteKey,
  derivePublicKeyFromXprv,
  deriveAddressFromXpub,
  getSetPath,
  getFee,
  getAccountInfo,
} = require('./xrpUtil');

const coin = 'XRP';
const ripple = new RippleAPI();
const nodeService = new NodeBase(CCQS);
const processingStatus = { recovering: false, scanning: false };

const stopScanning = async () => {
  processingStatus.scanning = false;
  return {};
};

const retreiveAddress = async ({ xpub }) =>{
  processingStatus.scanning = true;
  const ret = [];
  if(processingStatus.scanning){
    const address = deriveAddressFromXpub(xpub,0);
    const balance = await getBalance(address);
    socket.emit('address', { coin, address, index: 0, balance, isChangeAddress: 'false', total: balance });
    ret.push({ coin, address, index: 0, balance, isChangeAddress: false });
  }
  await saveToJsonFile(`./local/${coin}_${generateFileName(xpub)}`, ret);
  return ret;
};

const scan = async ({ xpub, skip = 0, limit }) =>{
  retreiveAddress({ xpub, skip, limit });
  return {};
};

const signTransaction = async ({ xprv, to }) =>{
  const address = deriveAddressFromXprv(xprv,0);
  const privateKey = String(derivePrivateKeyFromXprv(xprv, 0)).toUpperCase();
  const publicKey = String(derivePublicKeyFromXprv(xprv, 0)).toUpperCase();
  const balance = await getBalance(address);
  const rawAmount = minus(balance, getFee());
  const amount = minus(rawAmount, 20);
  socket.emit('address', { coin, address, index: 0, balance, isChangeAddress: false });
  const { txid, txBlob } = await sign({ from: address, privateKey, publicKey, amount, to });
  return { txid, txBlob };
}

const isValidateAddress = (address) => {
  const [base, dt] = address.split('?dt=');
  if (Big(dt).lt(0) || !ripple.isValidAddress(base)) return false;
  return true;
};
const exeRecover = async ({ xprv, to }) => {
  const { txBlob } =await signTransaction({ xprv, to});
  const { txid } = await nodeService.broadcast(coin, txBlob);
  return { txids: [txid] };
};

const recoverV2 = async ({ xprv, to }) => {
  exeRecover({ xprv, to });
  return {};
};

const scanSingleAddress = async ({ address }) => {
  const balance = await getBalance(address);
  socket.emit('address', { coin, address, index: 0, balance, isChangeAddress: 'false', total: balance });
  return { balance };
};

const recoverSingleAddress = async ({ secreteKey, to }) => {
  const address = deriveAddressFromSecreteKey({ secreteKey });
  const publicKey = derivePublibkeyFromSecreteKey({ secreteKey });
  const balance = await getBalance(address);
  const rawAmount = minus(balance, getFee());
  const amount = minus(rawAmount, 20);
  socket.emit('address', { coin, address, index: 0, balance, isChangeAddress: false });
  const { txBlob } = await sign({ from: address, privateKey: secreteKey, publicKey, amount, to });
  const { txid } = await nodeService.broadcast(coin, txBlob);
  return txid;
};

module.exports = {
  isValidateAddress,
  deriveAddressFromXprv,
  derivePrivateKeyFromXprv,
  derivePublicKeyFromXprv,
  getBalance,
  scanSingleAddress,
  recoverSingleAddress,
  getFee,
  getAccountInfo,
  signTransaction,
  deriveAddressFromXpub,
  getXpubFromXprv,
  retreiveAddress,
  recoverV2,
  getSetPath,
  scan,
  stopScanning,
  deriveAddressFromSecreteKey
};
