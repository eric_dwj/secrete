const bitcoinjs = require('bitcoinjs-lib');
const log = require('log4js').getLogger('[coin/btc]');
const config = require('config');
const coinSelect = require('coinselect');
const bip32 = require('bip32');
const CCQS = require('./node/CCQS');
const { add, times, minus, div } = require('./util');
const { NodeBase } = require('./node/NodeBase');

const coin = 'BTC';
const testnet = config.get('recovery.btc.testnet');
const baseUnit = config.get('recovery.decimal.btc');
const nodeService = new NodeBase(CCQS);

let derivePath = testnet ? 'm/44/1/0/0': 'm/44/0/0/0';
let changePath =  testnet ? 'm/44/1/0/1': 'm/44/0/0/1';

const getSetPath = ({ inputPath, inputChangePath }) =>{
  if(inputPath)  derivePath = inputPath;
  if(inputChangePath) changePath = inputChangePath;
  return { derivePath, changePath};
};

const btcNetworks = () => {
  if (testnet) return bitcoinjs.networks.testnet;
  return bitcoinjs.networks.bitcoin;
};

const deriveAddressFromXprv = (xprv, index, isChangeAddress = false) => {
  const node = bip32.fromBase58(xprv);
  const child = node.derivePath(parseDerivationPath(index, isChangeAddress));
  const { address } = bitcoinjs.payments.p2wpkh({ pubkey: Buffer.from(child.publicKey.toString('hex'),'hex'), network:btcNetworks() });
  return address;
};

const parseDerivationPath = (derivePathIndex, isChangeAddress = false) => {
  if (isChangeAddress) return `${changePath}/${derivePathIndex}`;
  return `${derivePath}/${derivePathIndex}`;
};

const derivePrivateKeyFromXprv = (xprv, index, isChangeAddress = false) => {
  const node = bip32.fromBase58(xprv);
  const child = node.derivePath(parseDerivationPath(index, isChangeAddress));
  return child.toWIF();
}; 

const deriveAddressFromXpub = (xpub, index, isChangeAddress) => {
  const { publicKey: pubkey } = bip32.fromBase58(xpub).derivePath(parseDerivationPath(index, isChangeAddress));
  const { address } = bitcoinjs.payments.p2wpkh({ pubkey, network: btcNetworks() });
  return address;
}; 

const isValidateAddress = (address) => {
  try {
    bitcoinjs.address.toOutputScript(address, btcNetworks());
    return true;
  } catch (e) {
    return false;
  }
};

const getFeeRate = async () => {
  // const { fastestFee, halfHourFee } = await node.getFeeRate(coin);
  const { fastestFee } = await nodeService.getFeeRate(coin);
  return fastestFee
};

const getBalance = async (address) => {
  const { balance } = await nodeService.getBalance(coin, address);
  return balance;
};

const getUnspent = async (address) =>{
  const unspent =  await nodeService.getUnspentByAddress(coin, address);
  return unspent;
};


const sign = async({ unspents, to}) =>{
    log.debug('sign', unspents, 'to', to);  
    const psbt = new bitcoinjs.Psbt({ network: btcNetworks() });
    const addInputs = () => {
      let totalInputValue = '0';
      for (let i = 0; i < unspents.length; i++) {
        const { hash, vout, script, value } = unspents[i];
        psbt.addInput({ hash, index: vout, witnessUtxo: { script: Buffer.from(script,'hex'), value } });
        totalInputValue = add(totalInputValue, value);
      }
      return totalInputValue;
    }
  
    const addOutput = async (totalInputValue) => {
      const feeRate = await getFeeRate();
      const utxos = unspents.map(unspent=> ({ txid: unspent.hash, vout: unspent.vout, address: unspent.address, value: unspent.value }));
      const targets = [{ address: to, value: parseInt(totalInputValue, 10) }];
      const { fee: feeSatoshi } = coinSelect(utxos, targets, feeRate);
      psbt.addOutput({ address: to, value: parseInt(minus(totalInputValue, feeSatoshi), 10) });
      return div(feeSatoshi, baseUnit);
    }
  
    const sign = (unspents) => {
      for (let i = 0;  i < unspents.length; i++) {
        const { secreteKey } = unspents[i];
        psbt.signInput(i, bitcoinjs.ECPair.fromWIF(secreteKey));
        psbt.validateSignaturesOfInput(i);
      }
      psbt.finalizeAllInputs();
    }
    // 1 add inputs
    const totalInputValue = addInputs();
    // 2 add outputs
    const fee = await addOutput(totalInputValue);
    // 3 sign
    sign(unspents);
    // 4 get txBlob
    
    return { txBlob: psbt.extractTransaction().toHex(), amount: div(totalInputValue,baseUnit), fee  };
  };

  const deriveAddressFromSecreteKey = async ({ secreteKey }) =>{
    const { address } = bitcoinjs.payments.p2wpkh({ pubkey: bitcoinjs.ECPair.fromWIF(secreteKey).publicKey, network: btcNetworks() });
    return address;
};

  module.exports = {
      sign,
      getUnspent,
      getBalance,
      getFeeRate,
      isValidateAddress,
      deriveAddressFromXpub,
      derivePrivateKeyFromXprv,
      deriveAddressFromXprv,
      deriveAddressFromSecreteKey,
      getSetPath,
      btcNetworks,
      parseDerivationPath,
  }