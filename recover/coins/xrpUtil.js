const { RippleAPI } = require('ripple-lib');
const rippleKeypairs = require("ripple-keypairs")
const bip32 = require('bip32');
const { NodeBase } = require('./node/NodeBase');
const CCQS = require('./node/CCQS');
const { socket } = require('./socket');
const { getPublicKeyFromPrivateKey } = require('./util');

const coin = 'XRP';
const ripple = new RippleAPI();
const nodeService = new NodeBase(CCQS);
let derivePath = 'm/44/144/0/0';
let changePath =  'm/44/144/0/1';

const getSetPath = ({ inputPath, inputChangePath }) =>{
  if(inputPath)  derivePath = inputPath;
  if(inputChangePath) changePath = inputChangePath;
  return { derivePath, changePath};
};

const parseDerivationPath = (derivePathIndex) => `m/44/144/0/0/${derivePathIndex}`;

const deriveAddressFromXpub = (xpub, index) => {
  const pubkey = bip32.fromBase58(xpub).derivePath(parseDerivationPath(index)).publicKey.toString('hex');
  const address = ripple.deriveAddress(pubkey);
  return address;
}; 
const derivePublibkeyFromSecreteKey = ({ secreteKey }) =>{
    return String(getPublicKeyFromPrivateKey(secreteKey)).toUpperCase();
 }

const deriveAddressFromSecreteKey = ({ secreteKey }) =>{
   return rippleKeypairs.deriveAddress(String(getPublicKeyFromPrivateKey(secreteKey)).toUpperCase());
}

const derivePublicKeyFromXprv = (xprv, index) => {
  const pubkey = bip32.fromBase58(xprv).derivePath(parseDerivationPath(index)).publicKey.toString('hex');
  return pubkey;
};

const deriveAddressFromXprv = (xprv, index) => {
    
  const pubkey = bip32.fromBase58(xprv).derivePath(parseDerivationPath(index)).publicKey.toString('hex');
  const address = ripple.deriveAddress(pubkey);
  return address;
};

const derivePrivateKeyFromXprv = (xprv, index) => {
  const node = bip32.fromBase58(xprv);
  const child = node.derivePath(parseDerivationPath(index));
  return child.privateKey.toString('hex')
}; 

const getBalance = async (address) => {
  const { balance } = await nodeService.getBalance(coin, address);
  return balance;
};

const getFee = () => {
  return '0.000012';
};

const getAccountInfo = async (address) =>{
  const result  = await nodeService.getAccountInfo(coin, address);
  return result;
};

const doPrepare =async (params) =>{
  const { sender, amount, destination, sourcetag, destinationtag, fee, ledgerNumber } = params;
  const { sequence, ledgerIndex } = await getAccountInfo(sender);
  const address = sender;
  const payment = {
    source: {
      address,
      maxAmount: {
        value: amount,
        currency: 'XRP'
      },
      tag: sourcetag
    },
    destination: {
      address: destination,
      amount: {
        value: amount,
        currency: 'XRP'
      },
      tag: destinationtag
    }
  };
  const instructions = {
    fee,
    maxLedgerVersion: ledgerIndex + ledgerNumber,
    sequence
  };
  return ripple.preparePayment(address, payment, instructions);
}

const sign = async ({ from, privateKey, publicKey,  to, amount,  sourcetag = 0,  destinationtag = 0, fee = getFee(), ledgerNumber = 100  }) => {
//  console.log({ from, privateKey, publicKey,  to, amount,  sourcetag,  destinationtag, fee , ledgerNumber });
 const { txJSON } = await doPrepare({ sender: from, amount, destination: to, sourcetag, destinationtag, fee, ledgerNumber });
 const keypair = { privateKey, publicKey };
 const response = ripple.sign(txJSON, keypair);
 const txid = response.id;
 const txBlob = response.signedTransaction;
 socket.emit( 'transaction', {coin, txid, fee, amount, ts: new Date() });
 return { txid, txBlob };
};

module.exports = {
    sign,
    getBalance,
    parseDerivationPath,
    derivePrivateKeyFromXprv,
    deriveAddressFromXprv,
    derivePublicKeyFromXprv,
    deriveAddressFromXpub,
    getSetPath,
    getFee,
    getAccountInfo,
    deriveAddressFromSecreteKey,
    derivePublibkeyFromSecreteKey
}