const log = require('log4js').getLogger('[coins/node/CCQS]');
const {
    ccqsQuery,
    ccqsSend
} = require('./CCQSUtil');

const broadcast = (coin, txBlob) =>{
    log.info(`[broadcase] coin: ${coin} txBlob: ${txBlob}`);
    return ccqsSend(coin,'sendTransaction', { coin, txBlob });
};

const getBalance = (coin, address) =>{
    return ccqsQuery(coin,'getBalanceByAddress', { coin, address });
};

const getAccountInfo =async (coin, address) =>{
    const result = await ccqsQuery(coin,'getAccountInfoByAddress', { coin, address });
    const { Sequence: sequence, ledger_current_index: ledgerIndex } = result;
    return { sequence, ledgerIndex};
};
const getFeeRate = async (coin) =>{
    const { fastestFee, halfHourFee, hourFee, gasPrice } = await ccqsQuery(coin,'getCurrentTransactionFee', { coin });
    return { fastestFee, halfHourFee, hourFee, gasPrice } ;
};

const getUnspentByAddress = async (coin, address) =>{
    const  { unspent }  = await ccqsQuery(coin, 'getUnspentByAddress', { address });
    return unspent.map(({ mintTxid, mintIndex, address, script, value, mintHeight })=>(
        { hash: mintTxid, vout: mintIndex, address, script, value, height: mintHeight  }
    ));
};

const getNonceByAddress = (coin, address) => {
    return ccqsQuery(coin, 'getNonceByAddress', { address });
};

module.exports = {
    getBalance,
    getFeeRate,
    getNonceByAddress,
    getUnspentByAddress,
    getAccountInfo,
    broadcast,
};
  