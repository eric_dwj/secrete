const config = require('config');
const axios = require('axios');
const log = require('log4js').getLogger('[coins/node/CCQSUtil]');

const { ccqsUrl } = config.get('services');

const ccqsQuery = async (coin, method, params, throwError = true) => {
  const _params = { coin, ...params };
  if (params && params.address) ([_params.address] = params.address.split('?'));
  try {
    const data = { jsonrpc: '2.0', id: 1, method, params: _params };
    const { data:  { result, error } } = await axios.post(`${ccqsUrl}/api/query`, data);
    if (result) return result;
    if (throwError && error && error.message) throw Error(error.message);
    if (error) return error;
  } catch (err) {
    log.error(coin, method, err);
    if (throwError) throw err;
  }
  return null;
};

const ccqsSend = async (coin, method, params, throwError = true) => {
  const _params = { coin, ...params };
  try {
    const { data: { result, error } } = await axios.post(
      `${ccqsUrl}/api/send`,
      {
        jsonrpc: '2.0',
        id: 1,
        method,
        params: _params
      }
    );
    if (result) return result;
    if (throwError && error && error.message) throw Error(error.message);
    if (error) return error;
  } catch (err) {
    log.error(err);
    if (throwError) throw err;
  }
  return null;
};

module.exports = {
    ccqsQuery,
    ccqsSend
};
  