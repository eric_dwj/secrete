class NodeBase {
    constructor (adapter) {
        this.adapter = adapter;
    }

    broadcast(coin, txBlob){
        return this.adapter.broadcast(coin, txBlob);
    }

    getAccountInfo(coin, address){
        return this.adapter.getAccountInfo(coin, address);
    }

    getBalance(coin, address){
        return this.adapter.getBalance(coin, address);
    }

    getFeeRate(coin){
        return this.adapter.getFeeRate(coin);
    }

    async getUnspentByAddress(coin, address){
        return this.adapter.getUnspentByAddress(coin, address);
    }

    getNonceByAddress(coin,address){
        return this.adapter.getNonceByAddress(coin, address);
    }
}

module.exports = { NodeBase };