const config = require('config');
const log = require('log4js').getLogger('[coin/eth]');
const CCQS = require('./node/CCQS');
const { NodeBase } = require('./node/NodeBase');
const { getXpubFromXprv, saveToJsonFile, readJsonFile, isFileExists, generateFileName } = require('./common');
const { times, div, minus, gt, sleep, Big } = require('./util');
const { socket } = require('./socket');
const { 
  getSequence, 
  getBalance, 
  getFeeRate, 
  getSetPath, 
  deriveAddressFromXprv, 
  deriveAddressFromXpub, 
  derivePrivateKeyFromXprv,
  deriveAddressFromSecreteKey,
  isValidateAddress,
  parseDerivationPath,
  sign,
} = require('./ethUtil')
const coin = 'ETH';
const nodeService = new NodeBase(CCQS);
let totalBalance = new Big('0');
const processingStatus = { recovering: false, scanning: false };

const stopScanning = async () => {
  log.info('call stopScanning start');
  processingStatus.scanning = false;
  log.info(`call stopScanning end ${JSON.stringify(processingStatus, null, 2)}`);
  return {};
};

const recoverV2 = async ({ xprv, to }) => {
  exeRecoverV2({ xprv, to })
  return {};
};

const retreiveAddress = async ({ xpub, skip = 0, limit }) =>{
  processingStatus.scanning = true;
  totalBalance = new Big('0');
  const addresses = [];
  for (let index = skip; index < skip + limit; index++) {
    if(processingStatus.scanning){
      const address = deriveAddressFromXpub(xpub, index);
      const balance = await getBalance(address);
      if(gt(balance, 0)) addresses.push({ coin, address, index, balance, isChangeAddress: false  });
      totalBalance = totalBalance.plus(balance);
      socket.emit('address', { coin, address, index, balance, isChangeAddress: 'false', total: totalBalance.toString() });
      await sleep(50);
    }
  }
  await saveToJsonFile(`./local/${coin}_${generateFileName(xpub)}`, addresses);
  return addresses;
}

const scan = async ({ xpub, skip = 0, limit }) =>{
  retreiveAddress({ xpub, skip, limit });
  return {};
};

const exeRecoverV2 = async ({ xprv, to }) =>{
   const xpub = getXpubFromXprv({xprv});
   const fileExists = await isFileExists(`./local/${coin}_${generateFileName(xpub)}`);
   if(!fileExists) {
    return socket.emit('error', 'scan file is not existed, please scan first.');
   }

  const addresses = await readJsonFile(`./local/${coin}_${generateFileName(xpub)}`);
  for (const  { index } of addresses) {
    await processRecover({xprv, to, index});
    await sleep(50);
  }
}

const processRecover = async ({ xprv, to, index }) => {
  const gasprice = await getFeeRate();
  const address = deriveAddressFromXprv(xprv, index);
  const balance = await getBalance(address);
  if(gt(balance, 0)){
    const sequence = await getSequence(address);
    const secreteKey = derivePrivateKeyFromXprv(xprv, index);
    const fee = div(times(gasprice, 21000), config.get('recovery.decimal').eth_gui);
    const amount = minus(balance, fee);
    const txBlob = await sign(to, `0x${secreteKey}`, sequence, gasprice, amount);
    const { txid } = await nodeService.broadcast(coin, txBlob) || {};
    socket.emit( 'transaction', { coin, txid, fee, amount, ts: new Date() });
    return txid;
  }
  return null;
};


const scanSingleAddress = async ({ address }) => {
  const balance = await getBalance(address);
  socket.emit('address', { coin, address, index: 0, balance, isChangeAddress: 'false', total: balance });
  return { balance };
};

const recoverSingleAddress = async ({ secreteKey, to }) =>{
  const gasprice = await getFeeRate();
  const address = await deriveAddressFromSecreteKey({ secreteKey });
  const balance = await getBalance(address);
  if(gt(balance, 0)){
    const sequence = await getSequence(address);
    const fee = div(times(gasprice, 21000), config.get('recovery.decimal').eth_gui);
    const amount = minus(balance, fee);
    const txBlob = await sign(to, `0x${secreteKey}`, sequence, gasprice, amount);
    const { txid } = await nodeService.broadcast(coin, txBlob) || {};
    socket.emit( 'transaction', { coin, txid, fee, amount, ts: new Date() });
    return txid;
  }
  return null;
};

module.exports = {
  isValidateAddress,
  parseDerivationPath,
  derivePrivateKeyFromXprv,
  deriveAddressFromXpub,
  deriveAddressFromXprv,
  getXpubFromXprv,
  getBalance,
  getFeeRate,
  getSequence,
  exeRecoverV2,
  sign,
  retreiveAddress,
  recoverV2,
  getSetPath,
  scan,
  stopScanning,
  scanSingleAddress,
  recoverSingleAddress,
  deriveAddressFromSecreteKey
};
