const ethers = require('ethers');
const bip32 = require('bip32');
const CCQS = require('./node/CCQS');
const config = require('config');
const { NodeBase } = require('./node/NodeBase');
const { times,toHex } = require('./util');

const coin = 'ETH';
const nodeService = new NodeBase(CCQS);
let derivePath = 'm/44/60/0/0';
let changePath =  'm/44/60/0/1';

const parseDerivationPath = (derivePathIndex) => `${derivePath}/${derivePathIndex}`;

const getSetPath = ({ inputPath, inputChangePath }) =>{
    if(inputPath)  derivePath = inputPath;
    if(inputChangePath) changePath = inputChangePath;
    return { derivePath, changePath};
};

const deriveAddressFromSecreteKey = async ({ secreteKey }) =>{
    const wallet = new ethers.Wallet(String(secreteKey).startsWith('0x') ? secreteKey : `0x${secreteKey}`);
    return wallet.getAddress();
};

const deriveAddressFromXprv = (xprv, index) => {
    const { address } = ethers.utils.HDNode.fromExtendedKey(xprv).derivePath(parseDerivationPath(index));
    return address;
};
  
const deriveAddressFromXpub = (xpub, index) => {
    const { address } = ethers.utils.HDNode.fromExtendedKey(xpub).derivePath(parseDerivationPath(index));
    return address;
}; 

const derivePrivateKeyFromXprv = (xprv, index) => {
    const node = bip32.fromBase58(xprv);
    const child = node.derivePath(parseDerivationPath(index));
    return child.privateKey.toString('hex');
}; 

const isValidateAddress = (address) => {
    try {
        ethers.utils.getAddress(address);
    } catch (e) {
        return false;
    }
    return true;
};

const getBalance = async (address) => {
    const { balance } = await nodeService.getBalance(coin, address);
    return balance;
};

const getFeeRate = async () => {
    const { gasPrice } = await nodeService.getFeeRate(coin);
    return gasPrice;
};

const getSequence = async (address) =>{
    const { nonce } =  await nodeService.getNonceByAddress(coin, address);
    return nonce;
};

const sign = async (to, secreteKey, sequence, gasprice, size) => {
    const wallet = new ethers.Wallet(secreteKey);
    const rawTx = {
        nonce: sequence,
        gasLimit: toHex('21000'),
        to,
        gasPrice: toHex(times(gasprice, config.get('recovery.decimal').eth_gui)),
        value: toHex(times(size, config.get('recovery.decimal').eth))
    };
    const transactionData = await wallet.signTransaction(rawTx);
    return transactionData;
};

module.exports = {
    sign,
    parseDerivationPath,
    getSequence,
    getFeeRate,
    getBalance,
    getSetPath,
    isValidateAddress,
    deriveAddressFromXprv,
    deriveAddressFromXpub,
    derivePrivateKeyFromXprv,
    deriveAddressFromSecreteKey,
}