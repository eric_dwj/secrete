const cmd=require('node-cmd');
const { saveToFile, saveToJsonFile } = require('./common');
const callRecovery = ({ keyfileName, keystoneName, path = 'local' }) => new Promise((resolve, reject)=>{
    const cmdStr = `${process.cwd()}/backupRecovery/backupRecovery ${process.cwd()}/${path}/${keyfileName} ${process.cwd()}/${path}/${keystoneName}`;
    cmd.get(
        cmdStr,
        function(err, data){
           if(err) return reject(err);
           const tmpArr = data.split('\n');
           if(!tmpArr[1] || !tmpArr[2]) return reject(Error('recover failed'));
           return resolve({ xprv:tmpArr[1], xpub: tmpArr[2] });
        }
    );
}); 

const getXprvAndXpubFromKeyStone = async({ keyfile, keystone, path = 'local' }) =>{
  let parsedKeystone;
  if(typeof keystone == "object") parsedKeystone =  keystone;
  if(typeof keystone == "string" && (String(keystone).includes('[') || String(keystone).includes(']'))) parsedKeystone = JSON.parse(keystone.replace(/\n/g, '').trim());
  
  if(!parsedKeystone){
    const splitedKeystone = keystone.replace(/\n/g, '').trim().split(',');
    if(splitedKeystone.length === 2) parsedKeystone = splitedKeystone;
  }

  if(!parsedKeystone) throw Error('Format of input keystone is not correct');
  const setFile = async () => {
    await saveToFile(`${process.cwd()}/${path}/keyfile.pem`, keyfile);
    await saveToJsonFile(`${process.cwd()}/${path}/keystone.json`, parsedKeystone);
  }

  const deletFile = async () =>{
    await saveToFile(`${process.cwd()}/${path}/keyfile.pem`, '');
    await saveToJsonFile(`${process.cwd()}/${path}/keystone.json`, []);
  }

  await setFile();
  const { xprv, xpub } = await callRecovery({ keyfileName: 'keyfile.pem', keystoneName: 'keystone.json', path });
  await deletFile();
  return { xprv, xpub };

}
module.exports = {
    callRecovery,
    getXprvAndXpubFromKeyStone
}