const log = require('log4js').getLogger('[utils/socketio]');

class Socket {
  init (io) {
    log.info('[init] start');
    this.io = io;
    this.io.on('connection', (socket) => {
      log.debug('user connected');
      socket.on('disconnect', () => {
        log.debug('user disconnected');
      });
    });
  }

  emit (event, data) {
    log.info(`[emit] event: ${event} data:${JSON.stringify(data)}`);
    try {
        this.io.sockets.emit(event, data);
    } catch (error) {
        log.error(error.message);
    }
    
  }
}

const socket = new Socket();
module.exports = { socket };
