
const bip32 = require('bip32');
const jsonfile = require('jsonfile');
const fs = require('fs');
const fileExists = require('file-exists');
const md5 = require('md5');
const getXpubFromXprv = ({xprv}) => bip32.fromBase58(xprv).neutered().toBase58();
const saveToJsonFile = (filename, obj) => jsonfile.writeFile(filename, obj);
const saveToFile = (file, content) => fs.writeFileSync(file, content);
const readJsonFile = (filename) => jsonfile.readFileSync(filename);
const readFile = (filename) => fs.readFileSync(filename);
const isFileExists = (filename) => fileExists(filename);
const generateFileName = (xpub) => `${md5(xpub).substr(0,16)}.json`;
const deepClone = (data) => JSON.parse(JSON.stringify(data));
const deepLoopMock = (datas) => {
    for (const key in datas) {
      if (typeof datas[key] === 'object') {
        deepLoopMock(datas[key]);
      } else if (Array.isArray(datas[key])) {
        for (const obj of datas[key]) {
          // eslint-disable-next-line no-unused-vars
          deepLoopMock(obj);
        }
      } else {
        if (String(key).toUpperCase().includes('PASSWORD')) datas[key] = '*';
        if (String(key).toUpperCase().includes('SECRET')) datas[key] = '*';
        if (String(key).toUpperCase().includes('ENCRYPT')) datas[key] = '*';
        if (String(key).toUpperCase().includes('PUBLICKEY')) datas[key] = '*';
        if (String(key).toUpperCase().includes('TOKEN')) datas[key] = '*';
        if (String(key).toUpperCase().includes('PUB')) datas[key] = '*';
        if (String(key).toUpperCase().includes('PAPERKEYID')) datas[key] = '*';
        if (String(key).toUpperCase().includes('XPRV')) datas[key] = '*';
        if (String(key).toUpperCase().includes('KEYSTONE')) datas[key] = '*';
        if (String(key).toUpperCase().includes('KEYFILE')) datas[key] = '*';
      }
    }
    return datas;
  }; 
module.exports = { 
  getXpubFromXprv, 
  saveToJsonFile, 
  readJsonFile, 
  isFileExists, 
  generateFileName, 
  deepLoopMock, 
  deepClone, 
  saveToFile,
  readFile 
};
