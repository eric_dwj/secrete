const Big = require('bignumber.js');
const EC = require('elliptic').ec;

const ec = new EC('secp256k1');
Big.NE = -22;
const toHex = (num) => `0x${Big(num).toString(16)}`;
const times = (a, b) => Big(a).times(b).toString();
const add = (a, b) => Big(a).plus(b).toString();
const div = (a, b) => Big(a).div(b).toString();
const minus = (a, b) => Big(a).minus(b).toString();
const gt = (a,b) => Big(a).gt(b);
const sleep = (ms) => new Promise((resolve) => {
    setTimeout(resolve, ms);
});
  
const getPublicKeyFromPrivateKey = (privateKey) => {
   const key = ec.keyFromPrivate(privateKey, 'hex');
   return key.getPublic().encodeCompressed('hex');
};

module.exports = { toHex, times, div, minus, add, gt, sleep, Big, getPublicKeyFromPrivateKey };