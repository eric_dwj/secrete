import Vue from 'vue';
import axios from 'axios'
import VueAxios from 'vue-axios'
import ElementUI from 'element-ui';
import io from 'socket.io-client';
import VueSocketIOExt from 'vue-socket.io-extended';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';
import store from './store';

const socket = io('http://127.0.0.1:3456');
Vue.config.productionTip = false;
Vue.use(VueAxios, axios);
Vue.use(ElementUI);
Vue.use(VueSocketIOExt, socket);
Vue.mixin({
  data: function() {
    return {
      server: { host: 'http://127.0.0.1:3456' }
    }
  }
})

new Vue({
  render: h => h(App),
  store,
}).$mount('#app')
