import Vue from 'vue';
import Vuex from 'vuex';
import Big from 'bignumber.js';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    form: {
      coin: 'ETH',
      xprv:'',
      xpub: '',
      // xprv: 'xprv9s21ZrQH143K3xqfnfHdg3qzMnEWpLpd6m7ZHtRzH7KNKKePBAJesN4s5NuhTa4eA9gUTZG8mqJQtfJTNnBDFZWhii8dBLeVfEVNJjy8e4p',
      // xpub: 'xpub661MyMwAqRbcGSv8tgpe3Bniup51DoYUTz3A6GqbqSrMC7yXihcuRAPLvduRmMob6tDg61ph53xPZDJM4AWg6Zfbux4iQJGzpDvce1gNsat',
      derivePath: '',
      to: '',
      privatekey: '',
      address: '',
      // privatekey: '67c3e8743f26e8ca0f3b013d49072080d66b7dbb328875bc95419789f571d08f',
      // address: '0xeCA38D06930d7c9756fC3eB957449e703D4f857B',
      indexStart: 0,
      indexEnd: 10,
      processingStatus: { recovering: false, scanning: false }
    },
    status: {
      totalAddressCount: 0,
      totalTransactionCount: 0,
      currentTransactionCount: 0,
      totalValue: '0',
      currentValue: '0',
    },
    addressTable: [],
    transactionTable: [],
  },
  getters: {
    getStatus: state => {
      return state.status;
    },
    getAddressTable: state => {
      return state.addressTable;
    },
    getTransactionTable: state => {
      return state.transactionTable;
    },
  },
  mutations: {
    changeRecoveringStatus(state, status){
      state.form.processingStatus.recovering = status;
    },
    reset (state) {
      Object.assign(state.form, {
        xprv: '',
        xpub: '',
        to: '',
        derivePath: '',
        indexStart: 0,
        indexEnd: 1,
        privatekey:'',
        address: '',
        processingStatus: { recovering: false, scanning: false }
      });
      state.addressTable = [];
      state.transactionTable = [];
      Object.assign(state.status, {
        totalAddressCount: 0,
        totalTransactionCount: 0,
        currentTransactionCount: 0,
        totalValue: '0',
        currentValue: '0',
      });
    },
    clear (state) {
      state.addressTable = [];
      state.transactionTable = [];
      Object.assign(state.status, {
        totalAddressCount: 0,
        totalValue: '0',
        currentAddressCount: 0,
        currentValue: '0',
        processingStatus: { recovering: false, scanning: false }
      });
    },
    updateStatus (state, payload) {
      const { totalValue, currentValue } = payload;
      if (totalValue) state.status.totalValue = totalValue;
      if (currentValue) state.status.currentValue = currentValue;
    },
    pushAddress (state, data) {
      state.addressTable.push(data);
      state.status.totalAddressCount += 1;
      if (state.form.coin === 'ETH' && Big(data.balance).gt(0)) state.status.totalTransactionCount += 1;
    },
    pushTransaction (state, data) {
      state.transactionTable.push(data);
      state.status.currentTransactionCount += 1;
    }
  },
  actions: {
  },
  modules: {
  },
});
