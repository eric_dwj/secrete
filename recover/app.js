const express = require('express');
const bodyParser = require('body-parser');
const SocketIo = require('socket.io');
// const path = require('path');
const http = require('http');
const cors = require('cors');
const log = require('./log4js').getLogger('[app]');
const { processor } = require('./processor');
const { socket } = require('./coins/socket');
const { deepLoopMock, deepClone } = require('./coins/common');
const app = express();
const server = http.createServer(app);
const io = new SocketIo(server);
socket.init(io);
const port = 3456;
// app.use(cors({ origin: ['*'], credentials: true, optionsSuccessStatus: 200 }));
app.use(cors());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false }));
app.use(express.static('dist'))

server.listen(port, () => {
  log.info(`listening on port ${port}`);
});

// communication with backend server
app.post('/', async (req, res) => {
  const { method, params } = req.body;
  const { handler, data } = params;
  try {
    log.info(`method: ${method}, params:  ${JSON.stringify(deepLoopMock(deepClone(params)), null, 2)}`);
    const result = await processor[String(handler).toLowerCase()][method](data);
    if( result && Object.keys(result).length === 0) result.status = 'ok';
    res.json({ result });
  } catch (err) {
    res.json({ error: err.message });
  }
});

app.get("/", function (req, res) {
  res.render("index");
});
