const log4js = require('log4js');
const config = require('config');

log4js.configure(config.get('logger'));

module.exports = log4js;
