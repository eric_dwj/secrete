#!/usr/bin/env bash
if [ -z "$1" ] ;then
    echo "./system.sh status disk"
    echo "./system.sh clean disk"
    echo "./system.sh setup crypto"
    echo "./system.sh rm crypto"
    exit
fi

if [  "$1" = 'rm' ] && [  "$2" = 'crypto' ]; then
    ./docker/remove.sh rm crypto
    exit
fi

if [  "$1" = 'setup' ] && [  "$2" = 'crypto' ]; then
    if [[ "$(docker images -q cryptolib:latest 2> /dev/null)" == "" ]]; then
        ./docker/build.sh build cryptolib
    fi
    ./docker/start.sh start party1
    ./docker/start.sh start party2
    exit
fi

if [  "$1" = 'status' ] && [  "$2" = 'disk' ]; then
    du -hd2
    exit
fi

if [  "$1" = 'clean' ] && [  "$2" = 'disk' ]; then
    rm -rf /var/cache/
    apt-get autoclean
    apt-get clean
    rm -rf ./log/*
    exit
fi